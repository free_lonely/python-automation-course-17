# -*- coding: utf-8 -*-
# @Time : 2021/4/7 10:11
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework8.py
# @Project : basic_gramer
import random

# --------------------------------------------------第一张---------------
# 4. python2.x和python3.x默认的编码格式分别是什么？
# ASCII码 和 utf-8

# 5. 将hello_new_world 按—符号进行分割
# str1 = 'hello_new_world'
# print(str1.split('_'))
# 6. 将数字1以0001的格式输出
# i = 1
# print('%04d' % i)

# --------------------------------------第二张--------------------------
# 1、字典a={1:1},是否正确?
# 正确
# a = {1: 1}
# print(a)

# 2、请合并列表a=[1,2,3,4]和列表b=[5,6,7,8]
# a = [1, 2, 3, 4]
# b = [5, 6, 7, 8]
# c = a + b
# print(c)

# 3、列表a,请写出实现正序排列,倒序排列,逆序排列的内置方法。
# a = [1, 5, 3, 4, 2]
# 正序
# a.sort()
# 倒序
# a.reverse()
# 逆序
# a.sort(reverse=True)
# print(a)

# 4、字典d={“k”:1,“v:2},请写出 d.items结果。
# d = {'k': 1, 'v': 2}
# for i in d.items():
#     print(i)

# 5、复杂列表[{'k': 1, 'v': 2}, {'k': 12, 'v': 22}, {'k': 13, 'v': 32}],请用内置方法写出按k倒序排列的代码
# list1 = [{'k': 1, 'v': 2}, {'k': 12, 'v': 22}, {'k': 13, 'v': 32}]
# list1.sort(key=lambda x: x['k'], reverse=True)
# print(list1)

# 6、集合s=set(1,2,3,4),d=set(2,4,9,0,3),请用内置方法写出他们的并集,交集,对
# s = set([1, 2, 3, 4])
# d = set([2, 4, 9, 0, 3])
# # 交集
# set1 = s & d
# print(set1)
# # 并集
# set1 = s | d
# print(set1)
# # 差集
# set1 = s - d
# print(set1)

# 7、如何把列表a=[“a”,“b”]里的各项,转为字符串并用逗号,连接。
# a = ['a', 'b']
# str1 = ','.join(a)
# print(str1)

# 请随机输出大于0小于1的数;请随机输出一个100以内的整形。

# 随机输出大于0小于1的数
i = random.random()
# print(i)

# 随机输出一个100以内的整形
# j = random.randint(0, 100)
# print(j)

# 8.  长度未知的元组a=(1,3,4,5,6,…),请随机输出n项
# a = (1, 2, 3, 4, 5, 6)
# a = list(a)
# # print(a)
#
# for i in range(len(a)):
#     # print(i)
#     b = a[random.randint(0, len(a)-1)]
#     print(b)
#     a.remove(b)

# ----------------------------------第三张----------------------------------
# 1. 判断下列描述是否正确,如果有错误,请指出错误的地方并改正
# ①字典:具有键值映射关系,无序,字典的键不能重复并且可以是任意数据类型。
#  字典的键不能重复，只能是不可变数据类型
# ②元组:不能修改,无序,不能索引切片,当元组中只有一个元素时,需要在元素的后面加逗号。
# 元组有序，可以索引切片
# ③列表:元素可以重复,有序,不可以反向索引,元素可以是任意类型。
# 可以反向索引
# ④集合:元素不可以重复,可以索引,a={}声明了一个空集合。
# 无序，没有下标，不能索引。a = {} 是空字典 ，空集合是 a = set()

# 2.  一行代码输出1-100和
# print(sum(range(1, 101)))

# 3. 用random.random()方法输出[25,60)之间的浮点数
# a = random.random()
# if 0.25 <= a < 0.6:
#     print(a * 100)
# else:
#     print('值不在[25-60)之间，值为 %d' % a)

# --------------------------------第四张------------------------------------------
# 2.一个list对象a=[1,2,4,3,2.2.4],需要去掉里面的重复值
# a = [1, 2, 4, 3, 2, 2, 4]
# a_set = set(a)
# # print(a_set)
# a_list = list(a_set)
# print(a_list)

"""
3.有一个文件 test. Txt,里面有数据
id name click  date
1 test 100 2012-04-18
2 aaa  12  2012-04-19
3 bbb  333 2012-04-18
4 cc   211 2012-04-17
5 ddd  334 2012-04-16
一共有5行4列数据,最后一列为日期。请按日期从小到大对数据进行排序
假如数据以 key: value的形式存储格式为id1, name test, click100date:2012-04-18
现在有了一个月的数据,统计下平均一天内的某个name的cick值

# """
# file_name = 'test.txt'
# my_dict = {}
# with open(file_name, 'r') as f:
#     lines = f.readlines()
#     # print(lines)
#
#     for i in lines:
#         print(i)
#         line = i.split(' ')
#         # print(line)
#         # my_dict.setdefault("id", line[0])
#         # my_dict.setdefault("name", line[1])
#         # my_dict.setdefault("click", line[2])
#         # my_dict.setdefault("date", line[3])
# # print(my_dict)

if __name__ == '__main__':
    print()
