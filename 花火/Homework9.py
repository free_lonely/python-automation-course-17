# -*- coding: utf-8 -*-
# @Time : 2021/4/11 13:49
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework9.py
# @Project : basic_gramer


# 1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
# *args:不定长参数之元组
# **kwargs：不定长参数之字典
# 当函数中的参数有很多个且不确定是什么时，需要使用不定长参数

# 2、（先自己想，然后再实验，最后查百度）
def f(x, l=[]):
    for i in range(x):
        l.append(i * i)
    print(l)
'''
f(2)
x = 2,l = []
    range(2) -- 0,1
    l = [0,1]
    
f(3, [3,2,1])
x = 3 , l = [3,2,1]
    range(3) -- 0,1,2
    l = [3,2,1,0,1,4]

f(3)
x = 3,l = []
    range(3) -- 0,1,2
    l = [0,1,4]

一个函数参数的默认值，仅仅在该函数定义的时候，被赋值一次。以后调用时，如果传入了参数，则用传入的参数；如果未传入参数，则去默认值的地址查找值并使用。
    所以，最终f(3)结果为[0,1,0,1,4]
'''

#
f(2)
f(3, [3, 2, 1])
f(3)




#
# 3、如何在一个函数内部修改全局变量
# global a
# 4、用lambda函数实现两个数相乘
# my_func = lambda a, b: print(a * b)
# my_func(1, 3)
# 5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# print([i for i in range(len(a)) if i % 2 != 0])


if __name__ == '__main__':
    print()
