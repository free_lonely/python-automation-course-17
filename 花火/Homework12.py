# -*- coding: utf-8 -*-
# @Time : 2021/4/15 11:35
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework12.py
# @Project : basic_gramer
#
# 1、创建一个对象后默认调用(A)
# A、__init__    B、__str__    C、__add__    D、__and__

# 2、类是对象的__模板_____、对象是类的___实例____.

# 3、对象是由___变量名____、__类名()_____两部分构成.

# 4、创建学生类：
# 	类名：Student
# 	属性：name（姓名）、age（年龄）、sex（性别）
# 方法：
# 	def info(self) # 打印学生的姓名、年龄、性别
# 	def draw(self) #打印”XX会画画呢”
# 描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.
class Student(object):
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def info(self):
        print(self.name)
        print(self.age)
        print(self.sex)

    def __str__(self):
        return '姓名：%s\n年龄：%d\n性别：%s\n' % (self.name, self.age, self.sex)


    def draw(self):
        print("%s 会画画" % self.name)


stu1 = Student('huohua', 12, 'female')
# stu1.info()
print(stu1)
stu1.draw()
print('-' * 20)
stu2 = Student('huihui', 21, 'male')
# stu2.info()
print(stu2)
stu2.draw()


# 5、创建动物类：
# 	类名：animal
# 	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
# 方法：
# 	def info(self) # 打印姓名、年龄、毛颜色
# 	def run（self）#打印“XX会跑呢”
# 描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.
#
class Animal(object):
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    # def info(self):
    #     print(self.name)
    #     print(self.age)
    #     print(self.color)

    def __str__(self):
        return '姓名：%s\n年龄：%d\n颜色：%s\n' % (self.name, self.age, self.color)

    def run(self):
        print('%s 会跑' % self.name)


tiger = Animal('小虎虎', 2, 'yellow')
# tiger.info()
print(tiger)
tiger.run()
print('-'*20)

cat = Animal('小猫咪', 2, 'yellow')
# cat.info()
print(cat)
cat.run()
print('-'*20)

if __name__ == '__main__':
    print()
