# -*- coding: utf-8 -*-
# @Time : 2021/4/11 13:49
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework9.py
# @Project : basic_gramer

# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)


# print("---------------------------")
# print("      学生管理系统 V1.0")
# print(" 1:添加学生")
# print(" 2:删除学生")
# print(" 3:修改学生")
# print(" 4:查询学生")
# print(" 5:显示所有学生")
# print(" 6:保存数据")
# print(" 7:退出系统")
# print("---------------------------")

'''
添加学生信息
new_name = input("请输入姓名:")
new_tel = input("请输入手机号:")
new_qq = input("请输入QQ:")
添加学生可能出现：
此用户名已经被占用,请重新输入
定义一个字典，用来存储用户的学生信息(这是一个字典)
向字典中添加数据
向列表中添加这个字典
'''
student_list = []


# student_dict = {"new_name": "lilu", "new_tel": "123323332233", "new_qq": "1625344533"}
def add_new_info():
    new_name = input("请输入姓名：")
    new_tel = input("请输入手机号：")
    new_qq = input("请输入QQ号：")
    student_dict = {"new_name": new_name, "new_tel": new_tel, "new_qq": new_qq}
    print(student_dict)
    if len(student_list) == 0:
        student_list.append(student_dict)
        print(student_list)
    else:
        for i in student_list:
            for j in i:
                if j == 'new_name' and i[j] == new_name:
                    print("此用户名已经被占用,请重新输入")
                    return
                else:
                    student_list.append(student_dict)
                    print(student_list)
                    return


# for x in range(2):
#     add_new_info()

'''
删除学生信息
"请输入要删除的序号:"

你确定要删除么?yes or no
输入序号有误,请重新输入
'''


def del_info():
    index = int(input("请输入要删除的序号:"))
    if index >= 0 and index < len(student_list):
        flag = input('你确定要删除么?yes or no')
        if flag == 'yes':
            del student_list[index]
    else:
        print('输入序号有误,请重新输入')
    print(student_list)


# del_info()

'''
修改学生信息
"请输入要修改的序号:"
"你要修改的信息是:"
input("请输入新的姓名:")
input("请输入新的手机号:")
input("请输入新QQ:")
print("输入序号有误,请重新输入")
'''


# [{'new_name': '1', 'new_tel': '2', 'new_qq': '3'}, {'new_name': '7', 'new_tel': '8', 'new_qq': '9'}]
def modify_info():
    index = int(input("请输入要修改的序号:"))
    if 0 <= index < len(student_list):
        print('你要修改的信息是：')
        name = input("请输入新的姓名：")
        tel = input('请输入新的手机号:')
        qq = input('请输入新QQ:')
        for key in student_list[index].keys():
            if key == 'new_name':
                student_list[index][key] = name
            elif key == 'new_tel':
                student_list[index][key] = tel
            else:
                student_list[index][key] = qq
        print('修改后的值为%s：' % student_list)
    else:
        print("输入序号有误,请重新输入")


# modify_info()

'''
查询学生信息
"请输入要查询的学生姓名:"
查询到的信息如下:
"没有您要找的信息...."
'''


def serach_info():
    name = input("请输入要查询的学生姓名:")
    for i in student_list:
        for j in i.keys():
            if i[j] == name:
                print('查询到的信息如下:%s' % i)
                return
            else:
                print("没有您要找的信息....")
                return


# serach_info()


'''
遍历学生信息
序号\t姓名\t\t手机号\t\tQQ
'''


def print_all_info():
    name = ''
    tel = ''
    qq = ''
    print('遍历学生信息')
    for students in student_list:
        index = student_list.index(students)
        for key in students.keys():
            if key == 'new_name':
                name = students[key]
            elif key == 'new_tel':
                tel = students[key]
            else:
                qq = students[key]
        print('序号:%d 姓名：%s 手机号：%s QQ:%s ' % (index, name, tel, qq))


# print_all_info()

"""加载之前存储的数据到文件中"""


def save_data():
    file = open('test.txt', 'w')
    str1 = str(student_list)
    file.write(str1)
    file.close()


# save_data()

def load_data():
    file = open('test.txt')
    res = file.read()
    student_list = res
    print(student_list)
    file.close()


load_data()

import os
# 控制整个流程
def main():
    """用来控制整个流程"""
    # 加载数据（1次即可）
    load_data()

    while True:
        # 1. 打印功能
        # print_menu()

        # 2. 获取用户的选择
        num = input("请输入要进行的操作(数字):")

        # 3. 根据用户选择,做相应的事情
        if num == "1":
            # 添加学生
            add_new_info()
        elif num == "2":
            # 删除学生
            del_info()
        elif num == "3":
            # 修改学生
            modify_info()
        elif num == "4":
            # 查询学生
            serach_info()
        elif num == "5":
            # 遍历所有的信息
            print_all_info()
        elif num == "6":
            # 保存数据到文件中
            save_data()
        elif num == "7":
            # 退出系统
            exit_flag = input("亲,你确定要退出么?~~~~(>_<)~~~~(yes or no) ")
            if exit_flag == "yes":
                break
        else:
            print("输入有误,请重新输入......")

        input("\n\n\n按回车键继续....")
        os.system("clear")  # 调用Linux命令clear完成清屏


# 程序的开始
main()

if __name__ == '__main__':
    print()
