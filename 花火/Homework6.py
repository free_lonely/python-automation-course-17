# -*- coding: utf-8 -*-
# @Time : 2021/3/31 21:59
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework6.py
# @Project : basic_gramer

'''
一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
'''
import random

offices = [[], [], []]
teachers = [1, 2, 3, 4, 5, 6, 7, 8]


for teacher in teachers:
    index = random.randint(0, 2)
    offices[index].append(teacher)

print(offices)


if __name__ == '__main__':
    print()
