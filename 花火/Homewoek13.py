# -*- coding: utf-8 -*-
# @Time : 2021/4/17 13:25
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homewoek13.py
# @Project : basic_gramer

# 1、下列程序结果是___初始化、类A、销毁___.
# class A:
#
#     def __init__(self):
#         print("初始化")
#
#     def __del__(self):
#         print("销毁")
#
#     def __str__(self):
#         return "类A"
#
#
# a = A()
# print(a)

#

# 2. 下列程序执行结果__动物在叫、汪汪汪____.
# class Animal(object):
#
#     def shut(self):
#         print("动物在叫")
# class Dog(Animal):
#
#     def shut(self):
#         super().shut()
#         print("汪汪汪")
# dog = Dog()
# dog.shut()
#

# 3. 创建猫类：
# 	类名：Cat
# 	属性：无
# 描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
# class Cat(object):
#     def __str__(self):
#         return '喵？喵？喵？'


# cat = Cat()
# print(cat)

# 4.创建计算器类：
# 	类名：Calculator
# 	属性：number_1（数字一）、number_2（数字二）
# 方法：
# 	def __init__(self,number_1,number_2):
# # 类的初始化方法
# 	def add(self) # 返回数字一加数字二的值
# 	def sub(self) # 返回数字一减去数字二的值
# 	def div(self) # 返回数字一除以数字二的值
# 	def mul(self) # 返回数字一乘以数字二的值
# 描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.

class Calculator(object):
    def __init__(self, number_1, number_2):
        self.number_1 = number_1
        self.number_2 = number_2

    def add(self):
        return self.number_1 + self.number_2

    def sub(self):
        return self.number_1 - self.number_2

    def div(self):
        return self.number_1 / self.number_2

    def mul(self):
        return self.number_1 * self.number_2


# cal = Calculator(4, 8)
# print(cal.add())
# print(cal.sub())
# print(cal.div())
# print(cal.mul())


# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
# class Animal:
#
#     def shut(self):
# 		# 打印叫声
#         pass
#
#     def eat(self):
# 		# 打印爱吃的食物
#         pass

class Animal(object):
    def __init__(self, sound, food):
        self.sound = sound
        self.food = food

    def shut(self):
        print(self.sound)

    def eat(self):
        print(self.food)


class Cat(Animal):
    def shut(self):
        print(self.sound)

    def eat(self):
        print(self.food)


class Dog(Animal):
    def shut(self):
        print(self.sound)

    def eat(self):
        print(self.food)


# cat = Cat('喵喵喵', '小鱼干')
# cat.shut()
# cat.eat()
# print('-'*30)
# dog = Dog('汪汪汪', '骨头')
# dog.shut()
# dog.eat()
# print(Cat.__mro__)

# 1、面向对象三大特性是___封裝___、__继承____、__多态____.
# 2、单例模式创建保证实例只创建_1_____次.


if __name__ == '__main__':
    print()
