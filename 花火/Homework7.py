# -*- coding: utf-8 -*-
# @Time : 2021/4/1 16:36
# @Author : lucy
# @Email : lilu7@asiainfo.com
# @File : Homework7.py
# @Project : basic_gramer
import random

# 1. 用任意语言循环输出整数0-10
# while循环
# i = 0
# while i <= 10:
#     print(i)
#     i += 1
# for循环
# for i in range(0, 11):
#     print(i)

# 2. 用任意语言逆序输出字符串，比如[欢乐逛欢迎你]，输出[你迎欢逛乐欢]
# str1 = '欢乐逛欢迎你'
# print(str1[::-1])

# 3. str = 'a,hello' ,在字符串中查找hello
# str2 = 'a,hello'
# print(str2.find('hello'))
# print(str2.index('hello'))

# 4. str = 'a, b, c, d',用逗号分割字符串，并保存在列表
# str3 = 'a,b,c,d'
# list1 = str3.split(',')
# print(list1)

# 5. 将'笔试题123A'中的123 换为‘进行中’
# str4 = '笔试题123A'
# print(str4.replace('123', '进行中'))

# -------------------——————————————————————————————————————————————————————--------------------------
# -----------------------------------------

# 1.下列将字符串"100"转换为数字100的正确的是( A )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

# 2.下列程序执行结果是( A  )
# numbers = [1, 5, 3, 9, 7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1

# 3.如何在列表中添加一个元素
# list2 = [1, 2, 3, 4, 5]
# list1 = [3, 4, 7, 8]
# list2.append('a')
# list2.insert(1, 'b')
# list2.extend(list1)
# print(list2)

# 4.对于列表什么是越界
# 长度超过下标值

# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 可变数据类型：list、dict
# 不可变数据类型：str、tuple、int、float、bool

# 6.从键盘中输入5个学生的名字,存储到列表中,然后打印出每个学生名字中的第2个字母
# student_list = []
# for i in range(1, 6):
#     student = input("请输入第%d位学生的名字：\n" % i)
#     student_list.append(student)
# print(student_list)
# for student in student_list:
#     print(student[1])

# 7.随机生成五个数字保存在列表中,取反并输出至终端.(取反:求出相反数,随机数范围是0到100)

# number_list = []
# for i in range(5):
#     num = random.randint(0, 100)
#     number_list.append(-num)
# print(number_list)

# number_list = []
# for i in range(5):
#     num = random.randint(0, 100)
#     number_list.append(num)
# print(number_list)
#
# number_list1 = []
# a = 0
# for i in number_list:
#     if i == 0:
#         a = i
#         number_list1.append(a)
#     else:
#         a = -i
#         number_list1.append(a)
# print(number_list1)

# 8.将下列两个列表合并,将合并后的列表升序并输出
# list1 = [1, 3, 4, 5, 7]
# list2 = [0, 66, 8, 9]
# list1.extend(list2)
# list1.sort()
# print(list1)

# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号),这些信息来自键盘的输入,储存完输出至终端
# person_dict = {}
# for person in range(2):
#     name = input("name = ")
#     age = input("age = ")
#     s_no = input("s_no = ")
#     person_dict.setdefault("name", name)
#     person_dict.setdefault("age", age)
#     person_dict.setdefault("s_no", s_no)
# print(person_dict)


# 10.有下列字典dict1,查找值为“lebg”对应的key并输出到终端.(结果应该是输出school)
# dict1 = {"school": "lebo", "date": 2018, "address": "beijing"}
# for key, value in dict1.items():
#     if value == 'lebo':
#         print(key)

# 11使用切片翻转列表num,将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端

# nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# nums.reverse()
# print(nums)
#
# res = 0
# for num in nums:
#     if nums.index(num) % 2 == 0:
#         res += num
#         print(num)
#     else:
#         continue
# print(res)

# nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# nums_1 = nums[::-2]
# res = 0
# for i in nums_1:
#     res += i
# print(res)

if __name__ == '__main__':
    print()
