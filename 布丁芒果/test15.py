# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test15'
__author = 'admin'
__time = '2021/4/21 14:58'

# 导入模块练习
# from 布丁芒果.test13 import Calculator

from  decimal import  Decimal
from  decimal import  getcontext
# 怎么会打印所有方法呢，我只想调用Calculator类的add方法？
# Calculator(1, 5).add()


# mm=Dog()
# mm.eat()
# 1、单例模式练习
class test(object):
    _instance = None  # 类属性为None

    # 一个实例保证只创建一个对象
    # __new__必现要有一个参数cls,必须要有返回值
    def __new__(cls, age, **kwargs):
        # 调用类对象必现用类名或cls调用
        if not cls._instance:
            #可以直接用object的__new__实例来，可以retrue父类的————new出来的实例
            cls._instance = object.__new__(cls)
        #
        return cls._instance
        #return  object.__new__(cls)

    def __init__(self, age):
        self.age = age

    # def __str__(self):
    #     return self.age


tes = test('12')
tes1 = test('12')
print(tes)
print(tes1)


# 2、捕捉异常练习
# (1)自定义异常
class AgeErr(Exception):
    # 属性
    def __init__(self, newAge):
        self.age = newAge

    # 打印
    def __str__(self):
        return '你的年龄是%s，输入有误重新输入' % self.age


class Person(object):
    def __init__(self, newAge):
        self.age = newAge
        if 0 < self.age <= 100:
            self.age = newAge
            print(self.age)
        else:
            raise AgeErr(newAge);


Person(10)
# (2)捕捉异常
# try:
#     nuber
#     read=open('ni hao','r')
#     read.read()
#     read.close()
# #except (IOError,FileNotFoundError,NameError):
# except NameError as result:
#     print('产生错误 result%s' %result)
# else:
#     print('没有捕捉异常')

# （3）try finally

import time

# try:
#     f = open('test13.py', 'r', encoding='utf-8')
#     try:
#         content = f.readline()
#         print(content)
#         if content == 0:
#             print('没有内容')
#
#         time.sleep(2)
#         print(content)
#     except Exception:
#         print(1)
#     finally:
#         f.close()
#         print('关闭文件')
#
# except Exception as e:
#     print('没哟这个文件错误%s' % e)

# 5精度计算

number1=123.45
number2=12.5
getcontext().prec=5 #设置精度为5
result=Decimal(number1)+Decimal(number2)
print('ni hao')
print(result)


