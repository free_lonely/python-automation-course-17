# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test_aoto25'
__author = 'admin'
__time = '2021/5/17 10:24'
__product__ = 'PyCharm'


# noinspection PyUnresolvedReferences
import pytest

# noinspection PyUnresolvedReferences
from selenium import webdriver

# noinspection PyUnresolvedReferences
from  selenium.webdriver.common.by import By


import  os

class TestPytest(object):

    def test_01(self):
        print('你好')
        testOs = os.getcwd()+'\\UDcheck\\output'
        print(testOs)
        assert 1 == 1
        return


    def test_02(self):
        print('函数2')
        #assert 1==2


if __name__=='__main__':
    # pytest.main(['-s','test_aoto25.py']) #用于显示print函数输出
    #  pytest.main(['-v','test_aoto25.py']) #-v执行结果更加详细，
    pytest.main(['-svx','test_aoto25.py'])  #-x在第一个错误时或测试失败时立即输出
    #错误信息备注：E表示代码错误