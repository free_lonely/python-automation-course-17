# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'test13'
__author = 'admin'
__time = '2021/4/17 13:22'


# 1、下列程序结果是：初始化、类A______.
class A:

    def __init__(self):
        print("初始化")

    def __del__(self):
        print("销毁")

    def __str__(self):
        return "类A"


a = A()
print(a)


# 2. 下列程序执行结果__动物在叫、汪汪汪____.
class Animal(object):

    def shut(self):
        print("动物在叫")


class Dog(Animal):

    def shut(self):
        super().shut()  # 子类调用被重写的父类的同名方法
        print("汪汪汪")


dog = Dog()
dog.shut()


# 3. 创建猫类：
# 	类名：Cat
# 	属性：无
# 描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
# 答：错，会输出二进制地址

# 4.创建计算器类：
# 	类名：Calculator
# 	属性：number_1（数字一）、number_2（数字二）
# 方法：
# 	def __init__(self,number_1,number_2):
# # 类的初始化方法
# 	def add(self) # 返回数字一加数字二的值
# 	def sub(self) # 返回数字一减去数字二的值
# 	def div(self) # 返回数字一除以数字二的值
# 	def mul(self) # 返回数字一乘以数字二的值
# 描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.
__all__=["people" ,"sub"]
class Calculator(object):

    def __init__(self, number1, number2):
        self.number1 = number1
        self.number2 = number2

    def add(self):  # 返回数字一加数字二的值

        result = self.number1 + self.number2
        print('%.2f +%.2f = %.2f' % (self.number1, self.number2, result))

    def sub(self):  # 返回数字一加数字二的值

        result = self.number1 - self.number2
        print('%.2f - %.2f = %.2f' % (self.number1, self.number2, result))

    def div(self):  # 返回数字一除以数字二的值
        result = self.number1 / self.number2
        print('%.2f / %.2f = %.2f' % (self.number1, self.number2, result))

    def mul(self):  # 返回数字一乘以数字二的值
        result = self.number1 * self.number2
        print('%.2f * %.2f = %.2f' % (self.number1, self.number2, result))


clalculator = Calculator(12, 20)  # 类实例化对象
clalculator.add()  # 调用加的方法
clalculator.sub()  # 调用减的方法
clalculator.div()
clalculator.mul()

# 5.
# 创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法


class Animal:

    def shut(self):
        print('喔喔叫')
# 打印叫声
#     def __Money(self):
#         print('私用方法练习')
    #私有方法强制调用，私有方法可以类内部通过self访问，但是不能通过对象方法，所有重写一个方法，强制调用
#     def Mont(self):
#         self.__Money()
    #属性练习
    def __init__(self):
        self.color ='黄色'
        self.__hobby ='跑跑跑'

    def eat(self):
        # 打印爱吃的食物
        print('最喜欢吃大米饭')
        #修改属性
    def update_hobby(self,hobby):
        # self.__hobby='跳跳跳'
        self.__hobby=hobby
        #得到私有属性的值
    def get_hobby(self):
        return  self.__hobby

class Dog(Animal):
    #重写父类的方法
    def shut(self):
        print('小狗喜欢汪汪叫')

    #重写父类的方法
    def eat(self):
        print('喜欢吃大骨头')
        # #同时调用父类被重写的方法（练习）
        # super(Cat, self).eat()



class Cat(Animal):

    def shut(self):
        print('小猫喵喵喵')

    def eat(self):
        print('喜欢吃鱼')
#创建Cat类对象cat
cat =Cat()
cat.eat() #调用对象cat的eat方法
cat.shut()#调用对象cat的shut方法
#创建Dog类对象dog
dog = Dog()
dog.eat()
dog.shut()

dog.update_hobby('跳跳跑跑叫叫')
print(dog.get_hobby())#得到修改后属性的值
# dog.Mont()
# 1、面向对象三大特性是___封装___、_继承_____、__多态____
# 2、单例模式创建保证实例只创建___1___次.
#类方法练习

class People(object):

    __china='中国'
    contry='四川'
    @classmethod

    def zhongg(cls):
        print('你好我是类对象所拥有的方法，第一个参数必现为cls,能够通过实例对象和类对象方法')
       #普通方法
    def comme(self):
        print('普通方法')
     #   --修改私有属性
    @classmethod
    def update(cls,china):
        cls.__china=china
#打印私有属性
    @classmethod
    def get(cls):
        return cls.__china
    @staticmethod
    def self():
        return  People.contry

people = People()
people.zhongg()
people.comme()
People.zhongg()
print(People.self())

People.update('中国欢迎你')
print(People.get())
#print()