# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test04'
__author = 'admin'
__time = '2021/3/29 14:27'



 #i = 1
# while i <= 5:
#     print(i)
#     # 如果i=2 执行下break
#     if i == 2:
#         break
#         print("哈哈")
#     i += 1

# num = 1
# while num <= 5 :
#     num += 1
#     print(num)
#     if num == 2:
#         print('你好现在已停止 %d'% num)
#         break
#     num += 1
#     print('测试')
#
#     #行和列
#     #先打印5行
# 1、使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
#  * *
# *
# num = 1
# while num <=5 :
#     j = 1
#     while j <= num :
#         print('* ', end='')
#         j += 1
#     print()
#     num += 1

number1 = 1
while number1<=5 :
    nuber2 = 4
    while  number1<= nuber2:
        print('* ' , end= '')
        nuber2 -= 1
    print()
    number1 += 1
# 2、break和continue的区别：
 #break 是但循环语句条件成立，break为停止，跳出循环，不执行break后面的代码
 #continue 是当循环条件成立，continue为继续，结束本次循环，,不执行continue后面的代码
# nub1=1
# while nub1<=5 :
#     if(nub1 <= 2):
#      print('你好')
#      nub1 += 1
#      #continue  #循环语句条件成立，第一次执行你好，contiune
#      nub1 +=2

# 3、按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
# 1.	截取字符串的第一位到第三位的字符
# for str1 in range(1,4):
#     print(str1)
#     str2 = str1
# print(str2 ,end='')
#print(str1[0:3])

str1 = '1234567890'
str2 = str1[0:3]
print('1、第一位到第三位的字符：%s' % str2)

# 2.	截取字符串最后三位的字符
str3 = str1[7:10]
print('2、最后三位的字符是：%s' % str3)

# 3.	截取字符串的全部字符
str4 = str1[0:10]
print('3、全部字符的值是：%s' % str4)

# 4.	截取字符串的第七个字符到结尾
str5 = str1[6:10]
print('4、第七个字符到结尾是：%s' % str5)

# 5.	截取字符串的第一位到倒数第三位之间的字符
str6 = str1[0:7]
print('5、第一位到倒数第三位之间是：%s' % str6)

# 6.	截取字符串的第三个字符
str7 = str1[2:3]
print('6、第三个字符是：%s' % str7)

# 7.	截取字符串的倒数第一个字符
str7 = str1[-1]
print('7、倒数第一个字符：%s' % str7)

# 8.	截取与原字符串顺序相反的字符串
str8 = str1[::-1]
print('8、原字符串顺序相反的字符串：%s' % str8)

# 9.	截取字符串倒数第三位与倒数第一位之间的字符
str9 = str1[-3:10]
print('9、倒数第三位与倒数第一位之间：%s' % str9)

# 10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
str10 =str1[:: 2]#每隔一个字符串截取一次
print('10、第一位字符到最后一位字符之间的字符，每隔一个字符截取一次：%s' % str10)
# for str10 in range(str1):
#     print(str10)
#     if str10 %2 ==1:
#         print('te')

