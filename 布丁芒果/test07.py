1.下列将字符串"100"转换为数字100的正确的是( A )
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
2.下列程序执行结果是(A   )
numbers = [1，5，3，9，7]
numbers.sort(reverse=True)
print(numbers)
A、[9，7，5，3，1]
B、[1，3，5，7，9]
C、1，3，5，7，9
D、9，7，5，3，1

3.如何在列表中添加一个元素

答：a_list = [‘a’,’b’,’c’,’d’]
a_list.append(‘e’) 
Or a_list.insert(2,’e’)

4.对于列表什么是越界
5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型

答：字符串’’、元祖（）是不可变数据类型，列表[]、字典{}是可变数据类型

6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
答：

name = []
i = 1
while i < 6 :
 nameinput = input('请输入第 % d个学生姓名:' %i)
 name.append(nameinput)
 i += 1
#print(name)

#for循环遍历所有名字
for Nuame2 in name :
 print(Nuame2[1]) #根据下标取每个学生名字中的第二个字母

7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)

import random
Number_list = []
i = 1
while i <= 5:
    Number = random.randint(0, 100) #随机数范围在0到100
    Number_list.append(Number) #循环添加
    i += 1

Result = Number_list.reverse()
print(Number_list)

8.将下列两个列表合并，将合并后的列表升序并输出.

list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
list1.extend(list2) #list1和list2合并，extend是一个一个的添加
list1.sort() #升序排序
print(list1)#打印

9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.

答：dictMsg = {"Name":'',"Age":0,'}
Name1 = input('请输入姓名')
Age1 = int(input('请输入年龄'))
Xuehao1 = input('请输入学号')
index =0
#字典通过key来赋值，key是唯一的，字典是没有对应的key,会在字典后面增加key、value
dictMsg['Name']=Name1
dictMsg['Age']=Age1
dictMsg['xuehao']=Xuehao1
print(dictMsg)

10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}

#用遍历dict1的key、values值，用if条件判断，并打印
for i ,j in dict1.items() :  
 if j==’lebo’:
print(i)
# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
Result = num[:: -1] #切片反正
resultCount =0
#用enumerate函数 for循环条件遍历出下标和元素值
for index,value in enumerate(Result) :
if index % 2==0 : #得出下标为偶数的
resultCount += value #下标为偶数的元素相加
print (resultCount)

#总结：循环条件后注意加：

#如果是列表为偶数的值相加
for numList in num :
 	If numList  %2==0:
resultCount += value
Print(resultCount)


