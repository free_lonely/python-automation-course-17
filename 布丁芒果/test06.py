# --*--coding: utf-8 --*--
__project__ = 'MYZ第三次课作业.py'
__fileName = 'test06'
__author = 'admin'

# noinspection PyUnresolvedReferences
import  random

# 1、使用任意语言输出0到10

i = 0
for numb1 in range(11):
    print('数字 % d' % numb1)
    i += 1


# 2、用任意语言输出逆序的字符串，比如说‘欢迎你到成都’，输出‘都成到你迎欢’
str1 = ['欢迎你到成都123']
print(type(str1))
test = str1.reverse()
print(str1)

# 3、 str='a,hello' 在str中查找hello
str = 'a,hello'
print(str.find('hello'))
print(str.index('hello'))
print(str.rfind('hello'))

# 4、str = 'a,b,c,d'，用逗号分隔，保存到列表中

str = 'a,b,c,d'
res = str.split(',')  # 逗号分隔
print(type(res))
print(res)

# 5、将”笔试题里 123A“ 替换为 123

str = "笔试题里 123A测试中心123A"
print(str.replace('123A','123'))

# 练习增删查
# demo1:增加append()对象,增加所有到列表后面

# Str = ['list','Carry','a','b']
# resut = Str.append('ac')
# print(Str)
#
# A = ['XiaoLi','Carry']
# # 将A放在i 循环打印
# for i in A:
#     print(i)
# #输入姓名，并添加到A列表
# Add = input('你输入你的姓名')
# res = A.append(Add)
# for j in A :
#     print(j)

# demo2 :extend() 一个一个的添加
Str = ['list','Carry','a','b', 'a']
res = Str.extend('你好')
print(Str)

# demo3 insert ()指定加到某个索引下标位置
res = Str.insert(1 , '琉璃')
print(Str)

# demo4 remove 删除
res = Str.remove('琉璃')
print(Str)

# demo5 通过下标修改的哪个元素
Str[0] = 'Marry'
for A in Str:
    print('修改后Str的值：%s' % A)
# demo6 查询 in

# if 'Marry' in Str:
#     print('Marry在呢')
# else:
#     print('Marry不在')
#     print(Str)

# demo 7 if 条件 not in 列表 不在

# res=input('请输入要查找的名字')
# if res not in Str:
#     print('你查找的姓名%s不在字典中' % res)
# else:
#     print('你查找的姓名%s在字典中' % res)
#
# demo 8 index 通过元素查找下标位置
print(Str.index('好'))

# demo 9 count() 查找元素在列表的总数
print(Str.count('a'))
print(Str)

#删除 del pop remove
# demo 9 del 通过下标删除
del Str[1]
print('删除后Str 值： %s' % Str)
# demo 10 pop 从右边开始删除，删除最后一个元素
print(Str)
Str.pop()
print(Str)

# demo 11 remove 指定一个元素删除,从第一个开始删
Str.remove('a')
print(Str)

# demo 12 排序 升序 sort(),降序 sort(reverse=True) 逆序 Str.reverse()
res = Str.reverse()
print(Str)
nub1 = [1 , 2 , 334 , 4 , 1 ,45 ,34 , 7 ,8]
nub1.sort()  # 升序
print(nub1)
nub1 = [1 , 2 , 334 , 4 , 1 ,45 ,34 , 7 ,8]
nub1.sort(reverse=True)     # 降序
print(nub1)

# 1. 列表嵌套
# 类似while循环的嵌套，列表也是支持嵌套的
#
# 一个列表中的元素又是一个列表，那么这就是列表的嵌套,找到师范大学并打印
#
schoolNames = [['北京大学','清华大学'],
                    ['南开大学','天津大学','天津师范大学'],
                    ['山东大学','中国海洋大学']]
res =schoolNames[1][2]
print(res)
schoolNames = [['北京大学','清华大学',['牛津大学']],
                    ['南开大学','天津大学','天津师范大学'],
                    ['山东大学','中国海洋大学']]
# 总结，一个方括号一层
res=schoolNames[0][2][0]
print(res)

# 2. 应用
# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
 # 三个办公室
 # 8个老师，
 #随机分配工位,2组，三个老师一个办公室，1组1个老师
dep = ['A','B', 'C']
techerName = ['琉璃','莉莉', '溜溜' , '天天', '滴滴' ,'大大' ,'Marry','Carry']

#先随机打印老师名字
# i =0
# for name  in techerName:
#     print(name)
#     res= random.randint(0,2)
#     dep[res] = techerName.append(name)
# i=1
# for deps in dep:
#     print('办公是多少人%s,and$s' %(i,len(deps)))
#     i += 1
#     for name in techerName:
#         print("老师名字%s" %name, end="")



