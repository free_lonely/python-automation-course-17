# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'test08'
__author = 'admin'
__time = '2021/4/8 17:09'

# 09
# 1、python中python2.x\python.3x分别用什么编码格式
# python2.x 编码格式：ASCII
# python3.x 编码格式：UTF-8

# 2、将“hello_world_new” 将_进行分割

str1 = "hello_world_new"
resut = str1.split('_')
print(resut)

# 3、将数字1以“0001”的格式输出到屏幕（填充）

nubmber = 1
# Strtest = str(nubmber).rjust(12)
# print(Strtest)
test = str("{:0>4}".format(nubmber))
print(test)

#第二张题  1到7题
#1、字典a={1: 1}是否正确？
#答：正确
# 2、请合并列表 a = [1,2,3,4,5,6] 列表 b =[7,8,9】

a = [1, 2, 3, 4, 5, 6]
b = [7, 8, 9]
#方法1
resutList =  a + b
resutList.sort()
print(resutList)
#方法2
a.extend(b)
print(a)
#3、列表a ,请实现正序排列、倒序排列、逆序排列的方法

a = [1,2,4,6,73,37,9,8,7]

#正序排列
zhengxu = a.sort()
print(a)
# 倒序
a.sort(reverse=True)
print(a)
#逆序
a.reverse()
print(a)
#4、字典d={"K":1,"V":2},请写出d.items的结果

d={"K":1,"V":2}
# dict_items[('k',1),('v',2)],为列表里的多个元祖
for key,value in d.items():
    print(key,value)

#5复杂列表 listInfo=[{'k':1,'v':2},{'k':12,'v':22},{'k':13,'v':33}],请用内置方法写出k的倒序排列的代码

listInfo=[{'k':1,'v':2},{'k':12,'v':22},{'k':13,'v':33}]
list2 =[]
#遍历出列表
for listMsg in listInfo :
    #遍历出key\vuales值，items()为列表的元祖数组
    print(list(listMsg.items()))
    for key,value in listMsg.items():
        if key == 'k' :
            list2.append(str(value)) #增加到列表里，int值没有sort函数，所以转换为str
list2.sort(reverse=True) #列表里的倒序排序
print(list2)

#6、集合s=set([1,2,3,4]) b=set([2,4,9,0,3]),请用内置方法写出他们的交集、并集，并。。。

s=set([1,2,3,4])
b=set([2,4,9,0,3])

#并集
c =  s | b
# 交集
d =  s & b

print('交集%s ,并集：%s' %(d,c))


#7、如何将列表a=['a','b']转换为字符串，用，号连接
#未完成
strMs = ''
a=['a','b']
b = ','.join(a)
print(b)
# for strM in a:
#     strMs=strM
#     print(strMs)
#一个集合单个元素后要加,逗号
a = (2 ,)
print(type(a))

#第三张
# 1.判断下列描述是否正确，如果错误，请指出错误的地方修改
#
# （1）字典，具有键值映射关系，无序，字典的键不能重复且可以任意数据类型
# 	答：错误，字典的键只能是不可变数据类型，列如字符串、int、元祖
# （2）元组：不能修改，无序，不能索引切片，当元组中只有一个元素时，需要再元素的后面加逗号
# 元组是有序的
# 答：对
#
# （3）列表：元素可以重复，有序，不可以反向索引，元素可以是任意类型
# 列表可以反向索引
# 答：对
#
# （4）集合：元素不可以重复，可以索引，a={}声明了一个空集合
# 	答：错，声明空集合：set()，集合是唯一的无序的，不可以索引，一般用于元祖和列表去重
# 2、一行代码实现求1到100的和

nuber = 1
result=0
while nuber <= 100 :
		result += nuber
		nuber += 1
print(result)

result1=sum(range(0,101))
print(result1)

# 3、按照下面要求写出完整代码：
# 使用random.random方法实现随机输出范围在[25,60)中的浮点数
import random

#b=random.randint(25,60)
#b=random.random(25,60)
b= random.uniform(25,60)
print("浮点数%.2f" %b)

# 第四张：第2题
# 2、一个list对象，a = [1,2,4,3,2,2,4]需要去掉里面的重复值

a = [1,2,4,3,2,2,4]
b= set(a) #转换为集合
c = list(b) #集合转换为列表
print(c)