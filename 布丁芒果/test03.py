# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'test03'
__author = 'admin'
__time = '2021/4/17 15:46'
mport random

# --product = 'PyCharm'

# 1、	说出变量名可以由哪些字符组成

# 答：变量名可由字母、下划线、数字组成，数字不能为第一个命名（列如：2userName 是错误的）

# 2、写出变量命名时的规则

# 答：变量名规则：
#  （1）小驼峰 userName
# （2）大驼峰 UserName
# （3） _加字母  user_login

# 3、	写出什么是驼峰法
# 答：有大驼峰、小驼峰
# 4、编写程序，完成以下要求：
# •	提示用户进行输入数据
# •	获取用户的数据（需要获取2个）
# •	对获取的两个数字进行求和运行，并输出相应的结果

a = float(input("请输入第一个数字"))  # 提示用户输入数据，并赋值到 a 临时变量、字符串转换为int类型

b = float(input("请输入第二个数字")) # 提示用户输入数据，并赋值到 b 临时变量、字符串转换为int

print(type(b)) # 打印 变量b类型

result = a+b  #变量a与b相加

print('a+b结果：%.2f' % result) #浮点型，保留两位小数

# 5、编写程序，完成以下信息的显示:
# •	==================================
# •	=        欢迎进入到身份认证系统V1.0
# •	= 1. 登录
# •	= 2. 退出
# •	= 3. 认证
# •	= 4. 修改密码
# •	==================================
print("	==================================")
print("•	=        欢迎进入到身份认证系统V1.0")
print("•	= 1. 登录")
print("•	= 2. 退出")
print("•	= 3. 认证")
print("•	= 4. 修改密码")
print("•	==================================")

# 6、编写程序，通过input()获取一个人的信息，然后按照下面格式显示
# ==================================
# 姓名: xxxxx
# QQ:xxxxxxx
# 手机号:131xxxxxx
# 公司地址:北京市xxxx
# ==================================
userName =input("请输入姓名:")
userQq=input("请输入QQ:")
userTel=input("请输入手机号码:")
userAtr=input("请输入公司地址:")
print("# ==================================")
print("姓   名：%s" % userName)
print("Q    Q：%s" % userQq)
print("手 机 号：%s" % userTel)
print("公司地址：%s" % userAtr)
print("# ==================================")


# 7、使用if，编写程序，实现以下功能：
# •	从键盘获取用户名、密码
# •	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
loginName = 'admin'
loginPassword = '123456'
userName2 = str(input("请输入用户名："))
userPassword2 = input("请输入密码：")

cont = 3
if loginName == userName2 and loginPassword == userPassword2:
    print("欢迎进入xxx的世界")
else:
    print("你用户密码或者用户名错误")
#     cont+=1
#     if cont>=3 :
#         print("账户被锁定")
#
#
#     isContius =input("是否继续登录Y/n")
#     if isContius=="n" :
#         print("退出登录")
#
#
# 8、有能力的同学可以用循环的方式做出石头剪刀布的游戏并且输入中文


contChioce=['石头', '剪刀', '布'] #使用数组存入多个值

compter_c=random.choice(contChioce) # 电脑随机出拳
user_c=input("请出拳：（石头、剪刀、布）")
while user_c not  in contChioce :         #当你输入的拳头不包括石头、剪刀、布，提示重新输入
    print("请重新出拳")
    user_c=input()
    print("显示结果")
print("电脑出拳：%s" % compter_c)
print("你出拳：%s" % user_c)
#判断结果
if(user_c==compter_c):
    print("你们打成平局")
elif (user_c== "石头"  and  compter_c=='剪刀') or (user_c== "剪刀"  and  compter_c=='布') or (user_c== "布"  and  compter_c=='石头') :     #判断你输入赢的几种条件
    print("恭喜你，你赢了")
else:
    print("你输了")
#判断是否需要继续玩
is_contiue=input("是否继续游戏，YorN")
if(is_contiue=='N'):
    print("游戏结束")