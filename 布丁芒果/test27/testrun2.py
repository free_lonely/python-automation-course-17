# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'testrun2'
__author = 'admin'
__time = '2021/5/19 14:37'
__product__ = 'PyCharm'

import unittest

import time

# 导入HTMLTestRunner包

from HTMLTestRunner_cn import HTMLTestRunner

rundir = './test_case'


discover = unittest.defaultTestLoader.discover(rundir, 'test*.py')

if __name__ == '__main__':
    now = time.strftime('%Y-%m-%d %H-%M-%S')
    redir = './test_report'
    reportDir = redir + "/" + now + 'report.html'
    # 创建或写入文件
    with open(reportDir, 'wb')as f:
        # 实例化htmlTestRunner

        runer = HTMLTestRunner(stream=f, title='测试报告', description='selenium百度')
        runer.run(discover)
