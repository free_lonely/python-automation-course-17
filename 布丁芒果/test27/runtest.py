# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'runtest'
__author = 'admin'
__time = '2021/5/18 18:07'
__product__ = 'PyCharm'

import unittest
import time

from HTMLTestRunner_cn import HTMLTestRunner

rundir = './test_case'
driver = unittest.defaultTestLoader.discover(rundir, 'test*.py')
if __name__ == "__main__":
    # 编辑名字
    now = time.strftime('%Y-%m-%d %H_%M_%S')
    reportdir = './test_report'
    reporName = reportdir + '/' + now+'report.html'
# 实例化html
    with open(reporName,'wb') as f:
        #实例化HtMleTestRunner,加写入文件，标题，描述
        runner = HTMLTestRunner(stream=f,title='测试报告',description='selenium百度')
        runner.run(driver)
