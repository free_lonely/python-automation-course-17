# --*--coding: utf-8 --*--
__project__ = 'TestHomeWork'
__fileName = 'test_aoto27'
__author = 'admin'
__time = '2021/5/18 16:13'
__product__ = 'PyCharm'

from selenium import webdriver

import unittest

from time import sleep
import time

# 注意继承unttest.TestCase
class TestBaidu(unittest.TestCase):

    def setUp(self):
        # 实例化浏览器
        self.driver = webdriver.Chrome()
        # 隐士等待
        self.driver.implicitly_wait(10)
        # 打开浏览器
        self.driver.get('http://www.baidu.com')
        # 浏览器最大化
        self.driver.maximize_window()

    def test_logBaidu(self):
        driver = self.driver
        driver.find_element_by_id('kw').send_keys('Selenium')
        driver.find_element_by_id('su').click()
        sleep(5)
        # 得到标题
        title = driver.title
        print(title)
        #断言的使用用捕捉断言文件错误捕捉异常异常
        try:
            self.assertEqual(title, "Selenium_百度搜")
        except AssertionError as ae:
            print(ae)
            now=time.strftime('%Y-%m-%d %H_%M_%S')
            imgedir='../test_report'
            imagename=imgedir+'/'+now+'asserImage.png'
            print('imagename%s'%imagename)
            driver.get_screenshot_as_file(imagename)
            #抛出异常
            raise
        driver.find_element_by_partial_link_text('Selenium(WEB自动化工具) - 百度百科').click()
        urls=driver.get_screenshot_as_file('../test_report/image1.png')
        print(urls)

        sleep(5)

    def tearDown(self):
        sleep(10)
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()
