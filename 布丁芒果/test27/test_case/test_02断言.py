# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test_02断言'
__author = 'admin'
__time = '2021/5/19 10:49'
__product__ = 'PyCharm'

from nuber import Math
import unittest


class testOne(unittest.TestCase):
    def One(self):

        dicmsg={'VideoId': 'None', 'DownloadPass': False, 'FailReason': 'ExceptionMessage: 响应状态代码不指示成功: 429 (Too Many Requests)。'}
        for values in dicmsg.values():
            print(str(dicmsg["VideoId"]))
            #"DownloadPass": true, "FailReason": ""
            if dicmsg["DownloadPass"]!=True and dicmsg['FailReason']!="":
                print('DownloadPass:%s,异常原因：%s' % (dicmsg["DownloadPass"], dicmsg['FailReason']))
                break
            print('下载成功')

    def test_add(self):
        j=Math(6,2)
        self.assertEqual(j.add_number(),8)

    def test_assertTrue(self):
        j=Math(6,2)
        self.assertTrue(j.add_number()<10)

    def test_assertIn(self):
        textname='helloward'
        self.assertIn(textname,'helloward')
    def test_assertIs(self):
        textname = 'helloward'
        self.assertIs(textname,'hello')

    def test_assertNotTrue(self):
        j=Math(6,2)
        self.assertFalse(j.add_number()<1)


if __name__=='__main__':
    unittest.main()