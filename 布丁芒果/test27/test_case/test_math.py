# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test_math'
__author = 'admin'
__time = '2021/5/19 9:26'
__product__ = 'PyCharm'

import unittest
import os


class Math:

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def add_number(self):
        sum = self.a + self.b
        return sum

    def jian_number(self):
        jian = self.a - self.b
        return jian


class TestMath(unittest.TestCase):

    def test_add(self):
        j = Math(6, 10)
        if self.assertEqual(j.add_number(), 10):
            print('正确')
        else:
            print('错误')

    def test_sub(self):
        j = Math(6, 1)
        #断言的使用
        if self.assertEqual(j.jian_number(), 5):
            print('减法两个数相等')
        else:
            print('减法两个数不相等')



# # 实例化调用
# if __name__ == '__main__':
#     # 实例化测试集
#     unitt = unittest.TestSuite()
#     unitt.addTest(TestMath('test_sub'))
#     unitt.addTest(TestMath("test_add"))
#     # 运行
#     runer = unittest.TextTestRunner()
#     runer.run(unitt)

