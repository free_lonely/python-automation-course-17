# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'insertStuMsg'
__author = 'admin'
__time = '2021/5/8 15:53'
__product__ = 'PyCharm'

from flask import Flask, render_template, request
import pymysql

app = Flask(__name__)
@app.route('/')
def index():
    return render_template('main.html')


@app.route('/insertStu')
def insert():
    try:
        conn = pymysql.connect(host="49.233.39.160", user="user", passwd="leboAa!#$123", database="lebo16", port=3306,
                               charset='utf8')
        cur = conn.cursor()
        # 请求以name获取值
        stuName = request.args["loginname"]
        address = request.args["adress"]
        classNa = request.args["classname"]
        sql = "insert student (name,hometown,class) values(%s,%s,%s)"
        count = cur.execute(sql, [stuName, address, classNa])
        if count == 1:
            print('插入成功')
        else:
            print('插入失败')

    except Exception as error:
        print('捕捉到异常信息%s' % error)

    data = [stuName, address]
    cur.close()
    conn.close()

    return render_template('insertStu.html', data=data)


app.debug = True
app.run()
