# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test23Auto'
__author = 'admin'
__time = '2021/5/11 17:44'
__product__ = 'PyCharm'

from selenium.webdriver.chrome.service import Service
from  selenium.webdriver.common.by import By
from selenium import webdriver
import time

# from selenium import ChromeDriver
# 实例化对象
# 注意大小写
# ChromeDriverService service = new ChromeDriverService.Builder() .usingChromeDriverExecutable(new File("D:\python37\chromedriver.exe")).usingAnyFreePort().build()
diverServer = Service(r"D:\python37\chromedriver.exe")
diverServer.command_line_args()
diverServer.start()
#打开谷歌浏览器
dirver = webdriver.Chrome()
dirver.maximize_window()#浏览器为最大窗口
time.sleep(5)
# url="https://weibo.com/login.php"
url = r"E:\hf\TestHomeWork\python-automation-course-17\布丁芒果\test23\注册A.html"
dirver.get(url)

# 1通过id
username = dirver.find_element_by_id('userA').send_keys('布丁芒果')
# username.send_keys('布丁芒果')

# 2#通过name
pas = dirver.find_element_by_name('passwordA')
pas.send_keys('123456')

# 3通过标签,下标为0开始
tel = dirver.find_elements_by_tag_name('input')[2]
tel.send_keys('19980405523')

# 4通过层叠样式 css_selector,可通过id选择器、name选中器、类选择器class
but = dirver.find_element_by_css_selector('#userA')
but.send_keys('层叠样式')

# 5通过超链接,可写部分文本
# link=dirver.find_element_by_partial_link_text('访问 新浪 网站').click()

# 6通过xpath
emal = dirver.find_element_by_xpath('//*[@id="emailA"]').send_keys('96862524@qq.com')

# 8通过class类进行定位
tel2 = dirver.find_element_by_class_name('telA').send_keys('896363633')

# 使用find_element()元素定位,
dirver.find_element(By.CSS_SELECTOR, "#userA").send_keys("test")

time.sleep(5)

# 7通过文本连接，必现写完整的链接文本
link2 = dirver.find_element_by_link_text('AA 新浪 网站').click()
time.sleep(10)

# 关闭浏览器
dirver.quit()
# 关闭chromedriver.exe进程

diverServer.stop()
