# --*--coding: utf-8 --*--
__project__ = 'python-automation-course-17'
__fileName = 'test24'
__author = 'admin'
__time = '2021/5/13 13:51'
__product__ = 'PyCharm'

# 导入selenium包webdriver包
from selenium import webdriver
from selenium.webdriver.common.by import By

from time import sleep
# 导入显示等待元素
from selenium.webdriver.support.wait import WebDriverWait
# 导入条件包
from selenium.webdriver.support import expected_conditions as EC

# 实例化对象
driver = webdriver.Chrome(r'D:\python37\chromedriver.exe')
url = r"E:\hf\TestHomeWork\python-automation-course-17\布丁芒果\test24\注册实例.html"
# 打开url
driver.get(url)
# 浏览器最大化
driver.maximize_window()
# # 设置浏览器宽高
# driver.set_window_size(800,900)
# #指定浏览器显示位置
#
# driver.set_window_position(800,900)
# 定位元素赋值
# 需求1：
#     1. 通过脚本执行输入 用户名：admin；密码：123456；电话号码：18611111111；电子邮件：123@qq.com;
#     2. 间隔3秒后，修改电话号码为：18600000000
#     3. 间隔3秒，点击注册用户A
#     4. 间隔3秒，关闭浏览器
#     5. 元素定位方法不限
""""
userName=driver.find_element_by_id('user')
userPwd=driver.find_element_by_name('password')
userTel=driver.find_element_by_xpath('//*[@id="tel"]')
userEmall=driver.find_element_by_css_selector('#email')

userName.send_keys('admin')
userPwd.send_keys('123456')
userTel.send_keys('18611111111')
userEmall.send_keys('123@qq.com')
sleep(3)
#清空电话号码，修改电话号码
driver.find_element_by_xpath('//*[@id="tel"]').clear()
#定位电话号码赋值
driver.find_element_by_xpath('//*[@id="tel"]').send_keys('18600000000')
#休眠3秒
sleep(3)
#点击注册A按钮
driver.find_element_by_css_selector('#zc > fieldset > button').click()
#休眠3秒
sleep(3)
"""

# -------------------------------------------------------------------------
# 需求2：显示等待
""""

#定位用户名
userName = WebDriverWait(driver,30).until(EC.presence_of_element_located((By.ID,'user')))
userName.send_keys('布丁芒果')

#定位密码
userPwd= WebDriverWait(driver,20).until(EC.presence_of_element_located((By.NAME,'password1')))
userPwd.send_keys('123456789')
"""
# 需求3：隐式等待
driver.implicitly_wait(10)
sleep(4)

"""# 需求3：切换窗口 注册实例切换到注册A窗口
1. 获取注册实例.html当前窗口句柄
2. 点击注册实例.html页面中注册A页面
3. 获取所有窗口句柄
4. 遍历判断窗口句柄并切换到注册A页面
5. 操作注册A页面元素，注册信息
#获取当前窗口句柄
"""
"""
currentHandl = driver.current_window_handle
print("当前窗口句柄为：", currentHandl)
# 点击注册按钮元素定位
driver.find_element_by_xpath('//*[@id="ZCA"]').click()
# 获取所有窗口句柄
handles = driver.window_handles
# 遍历出所有窗口
for handle in handles:
    if handle != currentHandl:
        # 定位到切换窗口
        driver.switch_to.window(handle)
        print("切换窗口句柄为：%s" % handle)
        # 定位元素
        userName = driver.find_element_by_id('userA').send_keys('你好呀')
        userPwd = driver.find_element_by_id('passwordA').send_keys('123456789')
        sleep(6)

        # 页面后退#该练习没有起作用
        driver.back()
        sleep(6)
        # 页面前进
        driver.forward()
        sleep(6)
"""
# 需求4：多表单切换
userName=driver.find_element_by_id('user').send_keys('布丁')
sleep(5)
#定位切换表单
#注册A
from1=driver.find_element_by_css_selector('#idframe1')
#定位切换表单，frame支持ID，name属性
driver.switch_to.frame('myframe1')
#定位用户名、密码
userName=driver.find_element_by_id('userA').send_keys('布丁注册A')
userPwd=driver.find_element_by_name('passwordA').send_keys('123456')
sleep(10)
print('界面A')
#恢复到默认frame
driver.switch_to_default_content()
userName=driver.find_element_by_id('user').send_keys('布丁')
userPwd=driver.find_element_by_name('password').send_keys('123456')
userTel=driver.find_element_by_xpath('//*[@id="tel"]').send_keys('123456')
print('主界面')
sleep(10)
# 需求5,获取弹框信息此处为copy,后面练习
# 设置JS脚本
js1='window.scrollTo(0,10000)'
js2='window.scrollTo(0,0)'
sleep(2)
# 调用JS
driver.execute_script(js1)
sleep(2)
driver.execute_script(js2)
sleep(2)


# 点击按钮
driver.find_element_by_css_selector("#alerta").click()
# 获取对话框
alert=driver.switch_to.alert
# 调用方法-同意
#alert.accept()

# 调用方法-取消
#alert.dismiss()

#需求6：截屏录取方法
"""
userName=driver.find_element_by_id('user').send_keys('布丁')
userPwd=driver.find_element_by_name('password').send_keys('123456')
userTel=driver.find_element_by_xpath('//*[@id="tel"]').send_keys('123456')
userEmall=driver.find_element_by_css_selector('#email').send_keys('793906367@qq.com')
#截屏录取并存到文件夹中
scrrenulr=driver.get_screenshot_as_file('../test24/image.png')
print(scrrenulr)

sleep(5)
"""
# 关闭浏览器
driver.quit()
