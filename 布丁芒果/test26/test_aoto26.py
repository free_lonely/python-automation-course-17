# --*--coding: utf-8 --*--
__project__ = 'TestHomeWork'
__fileName = 'test_aoto26'
__author = 'admin'
__time = '2021/5/17 15:53'
__product__ = 'PyCharm'

import unittest

# noinspection PyUnresolvedReferences
from selenium import webdriver

# noinspection PyUnresolvedReferences
import time


# from calculator import Math

# 新建类继承unittest.TestCase
class TestAutoUnittest(unittest.TestCase):
   # 初始化方法，注意大小写
    def setUp(self):
        # 打开浏览器
        self.driver = webdriver.Chrome()
        url = 'https://mail.126.com/'
        self.driver.get(url)
        # 浏览器最大化
        self.driver.maximize_window()
        # 隐士等待
        self.driver.implicitly_wait(10)

    # 登录

    def test_log(self):
        self.driver.switch_to_frame('#login-form')
        self.driver.get_screenshot_as_file('..\\test26\\image.png')
        username = self.driver.find_element_by_name('email').send.keys('wang_test_yx@126.com')
        userpwd = self.driver.find_element_by_name('password').send.keys('wang_123456')
        self.driver.find_find_element_by_id('dologin').click()


    # 两个数等于
    def test_log2(self):
        try:
            j = 10
            self.assertEqual(j, 10)
            print('测试2')
        except Exception as e:
            print('捕捉到异常%s' % e)
            raise e

    # 两个数不等于
    def test_log3(self):
        try:
            j = 15
            # 两个数不等于
            self.assertNotEqual(j, 10)
            print('测试:3')
        except Exception as e:
            print('捕捉到异常3：%s' % e)
            raise e

    # 两个字符串比较是否包含
    def test_log4(self):
        try:
            j = "Hello"
            # 两个字符串比较是否包含
            if self.assertIn(j, 'helloWord'):
                print('测试:4')
            else:
                print('test')

        except Exception as e:
            print('捕捉到异常4：%s' % e)
        raise e


# 两个字符串比较不包含
def test_log5(self):
    try:
        j = "test"
        # 两个字符串比较是否不包含
        self.assertNotIn(j, 'helloWord')
        print('测试:5')
    except Exception as e:
        print('捕捉到异常5：%s' % e)
        raise e


# 结束unittest
def teardown(self):
    self.sleep(10)
    self.driver.quit()

# 实例化unittest.testcase


if __name__ == 'main':
    # 调用main方法执行unittest内所有以test开头的方法
    # unittest.main()
    # 构造测试集
    suite = unittest.TestSuite()
    # 添加用例和方法
    suite.addTest(TestAutoUnittest('test_log2'))
    # suite.addTest(TestAutoUnittest('test_log'))

    # 执行测试用例
    runer = unittest.TextTestRunner()
    runer.run(suite)
