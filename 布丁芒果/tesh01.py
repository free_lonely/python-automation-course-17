# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'tesh01'
__author = 'admin'
__time = '2021/4/17 15:42'
#命名规则
#小驼峰发
myName='Carry'
#大驼峰法
MyAge=28
#连接单词
is_sex='Woman'
myFactory='我喜欢跳舞，唱歌，喜欢看美好的食物'
#导入模块
import  keyword

#查看版本有哪些关键字
print(keyword.kwlist)
#输出
print('Hello word')
print('我的年龄18岁')
print(myName,is_sex,myFactory)

my_age1=23
#%d格式占位符 后面的%要int新传
print("我的年龄是%d岁" % my_age1)
my_age1 +=1
print("我的年龄是%d岁" % my_age1)
my_age1 +=2
print("我的年龄是%d岁" %my_age1)
#%s=str 等于string
print("我的名字叫%s" % myName,"我的爱好是%s" %myFactory)
#%d 等于 digit

print("我的年龄是%d岁,很年轻" %my_age1)
myHeight=160.2
#%f=f 等于float，python中，为保留两位小数点
print("我的身高是%f" % myHeight)
isWomn=True
print("是否是女人：%s" %isWomn)

isMan = False
# 格式: 是否是男人:True,用%s显示true Or false,如果显示1 or 0 使用%d
print("是否是男人:%s" % isMan)
print("是否是男人:%d" % isMan)
print("我的名字叫%s,我的身高是：%.2f,我的年龄是%d" %(myName,myHeight, my_age1))
print("你好吗，\n--------我已经换行-----------------------")
print("hello", end="\n")
print("ni你好吗,测试中心",end="\n")
print("ni hao ma")
scor=90
#单纯想要一个符合%，需写两个%
print("班上90分以上的同学占人数%d%%" %scor)

my_EnglishName=input("请输入名字")
print(type(my_EnglishName))
#在python3中 无论输入的是什么格式的数据 最终记录数据的变量的类型都是字符串类型
print("输出结果%s" %my_EnglishName)

