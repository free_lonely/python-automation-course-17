# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'test10'
__author = 'admin'
__time = '2021/4/12 14:02'

import os

# 4、用lambda函数实现两个数相乘
# 匿名函数
f = lambda number1, number2: number1 * number2
print(f(2, 5))
# 5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
res = [i for i in a if (i % 2 == 1)]
print(res)

# 打开文件
# 匿名函数排序练习
stus = [{"name": "zhangsan", "age": 18}, {"name": "lisi", "age": 19}, {"name": "wangwu", "age": 17}]
stus.sort(key=lambda dict: dict['age'], reverse=True)
print(stus)


# 综合应用:学生管理系统(文件版)
# # 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
# 
# 有如下7个方法
def meinfo():
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")


# 列表存储学生信息
studengtMsg = [{'stuName': 'carry', 'stuQq': '23', 'stuTel': '2323'}]


# studengtMsg=[]


# 添加学生信息
def add_stuMsg():
    global studengtMsg
    print("---------添加学生信息-----------")
    new_name = input("请输入姓名：")
    new_qq = input("请输入QQ：")
    new_tel = input("请输入手机号:")
    # 定义一个字典，用来存储用户的学生信息(这是一个字典)
    dictStudengInfo = {}
    # 遍历列表的个数（字典）
    for stu in studengtMsg:
        # 遍历出列表的字典，可以直接通过key得到值
        # 必现得到字典的key为stuname的值与新输入的名字比较
        # print(values)  # 遍历出列表内所有的字典
        # print(values['stuName'])  # 通过字典key值得到value值
        if (stu['stuName'] == new_name):
            print('此用户名已经被占用,请重新输入！')
            new_name = input("请输入姓名：")
            return
    # 向字典中添加数据
    dictStudengInfo = {"stuName": new_name, 'stuQq': new_qq, 'stuTel': new_tel}
    # 向列表中添加这个字典
    studengtMsg.append(dictStudengInfo)  # 列表添加字典（学生信息）
    return studengtMsg


# 删除学生信息
def delet_stuMsg():
    global studengtMsg
    print('------删除学生信息---------')
    xuehao = int(input("请输入要删除的序号:"))

    # 方法一
    # if (0 <= xuehao < len(studengtMsg)):
    #     result = input("你确定要删除么?yes or no?")
    #     if (result == 'yes'):
    #         del studengtMsg[xuehao]
    #         print('删除成功！')
    # else:
    #     print('输入序号有误, 请重新输入')

    # 方法二
    # for遍历了下标两遍，且每个下标都去比较,else 必现放在for循环遍历完成后，无结果再提示
    for index, msg in enumerate(studengtMsg):
        # 把字典转换列表类型，得到下标序号
        if (index == xuehao):
            result = input("你确定要删除么?yes or no")
            if (result == 'yes'):
                del studengtMsg[xuehao]
                break
    else:
        print('输入序号有误, 请重新输入')


# delet_stuMsg()

# 修改学生信息
def update_stuMsg():
    global studengtMsg
    print('----------修改学生信息-----------------------------')
    udateNuber = int(input('请输入要修改的序号：'))
    # 判断输入序号必现在列表的长度内，列表的下标是0开始的
    if (0 <= udateNuber < len(studengtMsg)):
        # updateMsg= input('请输入要修改的信息是:姓名 or 手机号 or QQ')
        print('修改前信息:\n姓名：%s  手机号：%2s QQ: %2s ' % (
            studengtMsg[udateNuber]['stuName'], studengtMsg[udateNuber]['stuTel'], studengtMsg[udateNuber]['stuQq']))
        newName = input('请输入新的姓名:')
        newTel = input("请输入新的手机号:")
        newQQ = input("请输入新QQ:")

        # 所有的字段都修改
        # 根据列表的下标，字典的key值，修改value值
        studengtMsg[udateNuber]['stuName'] = newName
        studengtMsg[udateNuber]['stuTel'] = newTel
        studengtMsg[udateNuber]['stuQq'] = newQQ

        # #一个一个的修改
        # if(updateMsg=='姓名'):
        #     newName = input('请输入新的姓名:')
        #     studengtMsg[udateNuber]['stuName'] = newName
        # elif(updateMsg=='手机号'):
        #     newTel = input("请输入新的手机号:")
        #     studengtMsg[udateNuber]['stuTel'] = newTel
        # elif(updateMsg=='QQ'):
        #     newQQ=input("请输入新QQ:")
        #     studengtMsg[udateNuber]['stuQq'] = newQQ
        # else:
        #     print('输入修改信息有误,请重新输入')
    else:
        print('输入序号有误, 请重新输入')

    print('修改后的信息：\n姓名：%s  手机号：%2s QQ: %2s ' % (
        studengtMsg[udateNuber]['stuName'], studengtMsg[udateNuber]['stuTel'], studengtMsg[udateNuber]['stuQq']))


# """查询学生信息"""
def select_stumsg():
    print('----------查询学生信息-----------------------------')
    global studengtMsg
    selectName = input('请输入要查询的学生姓名:')
    for s in studengtMsg:
        if (selectName == s['stuName']):
            print('查询到的信息如下:\n姓名：%s  手机号：%2s QQ: %2s ' % (
                s['stuName'], s['stuTel'], s['stuQq']))
            break
    else:
        print('没有您要找的信息....')


# select_stumsg()

"""遍历学生信息"""


# 序号\t姓名\t\t手机号\t\tQQ
def select_all():
    global studengtMsg
    print('所有学生信息')
    for index, stuM in enumerate(studengtMsg):
        print('序号：%d姓名：%s  手机号：%2s QQ: %2s  ' % (index,
                                                 stuM['stuName'], stuM['stuTel'], stuM['stuQq']))


# select_all()
# 读取文件中的信息
def load_alltxt():
    global studengtMsg
    # 打开文件读取
    r = open('hf.txt', 'r')
    data = r.read()
    print(data)
    r.close()


# 保存到文件中
def write_txt():
    global studengtMsg
    # 创建文件夹
    # os.mkdir('hf.txt')
    # #获取当前目录
    # os.getcwd()
    # #获取目录列表
    # os.listdir("./")
    # os.listdir("hf.txt")
    # if os.path.exists('hf')==False:
    try:
        op = open('hf.txt', 'a', encoding='utf-8')
        # 把列表装换为str类型，写入文件
        writeStu = op.write(str(studengtMsg))
        op.close()
    except IOError:
        print('文件异常')


# 主程序

def stu_main():
    while True:
        #调用打印功能
        meinfo()
        choiceNuber = int(input('请输入要操作的数字:'))
        if (choiceNuber == 1):
             # 添加学生
            add_stuMsg()
            # 保存到文件
            write_txt()
        elif (choiceNuber == 3):
            # 修改学生信息
            update_stuMsg()
        elif (choiceNuber == 4):
            # 查询学生
            select_stumsg()
        elif (choiceNuber == 5):
            # 显示所有学生
            select_all()

        elif (choiceNuber == 6):
            # 保存数据
            write_txt()
            load_alltxt()
        elif (choiceNuber == 7):
            # 退出系统
            isEx = input('你确定要退出系统吗？yes or No   ')
            if isEx == "yes":
                print('tuic')
                break

    else:
        print('你输入的有误')
  
  
    input('按回车键继续')
    os.system("clear")
#调用主程序
stu_main()

