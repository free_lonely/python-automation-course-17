# --*--coding: utf-8 --*--
__project__ = 'pythonProject'
__fileName = 'test12'
__author = 'admin'
__time = '2021/4/15 17:27'


# 1、创建一个对象后默认调用(A)
# A、__init__    B、__str__    C、__add__    D、__and__
# 2、类是对象的_______、对象是类的_______.
# 3、对象是由____方法___、____类___两部分构成.
# 4、创建学生类：
# 	类名：Student
# 	属性：name（姓名）、age（年龄）、sex（性别）
# 方法：
# 	def info(self) # 打印学生的姓名、年龄、性别
# 	def draw(self) #打印”XX会画画呢”
# 描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.

class StudentMsg(object):
    def __init__(self, name, sex, age=18):
        self.name = name
        self.age = age
        self.sex = sex

    def info(self):
        print('姓名：%s 年龄：%s 性别：%s' % (self.name, self.age, self.sex))

    def dram(self):
        print('%s会画画呢' % self.name)


studentInfo = StudentMsg('Carry', '女')
# 调用打印，学生的姓名、年龄、性别
studentInfo.info()
# 打印”XX会画画呢”
studentInfo.dram()


# 4、创建动物类：
# 	类名：animal
# 	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
# 方法：
# 	def info(self) # 打印姓名、年龄、毛颜色
# 	def run（self）#打印“XX会跑呢”
# 描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.

# 创建动物类
class Animal(object):
    # 魔法方法定义类的属性
    #主要init不要写int
    def __init__(self,name,age,color):
        self.name=name
        self.age=age
        self.color=color

    # 打印属性
    def info(self):
        print('姓名：%s 年龄：%s 毛发：%s' % (self.name, self.age, self.color))

    def run(self):
        print('%s会跑' % self.name)


# 创建对象
animalMsg = Animal("东北的老虎", "1岁", "乌黑发亮")
# 打印
animalMsg.info()
# 打印跑跑
animalMsg.run()
