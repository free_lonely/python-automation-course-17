# --*--coding: utf-8 --*--
__project__ = 'TestHomeWork'
__fileName = 'test18_02'
__author = 'admin'
__time = '2021/4/27 14:46'
__product__ = 'PyCharm'
import  pymysql
#导入配置文件
import configparser
class TestStudents(object):
    def __init__(self):
        #ini
        #可以包含一个或多个节(section), 每个节可以有多个参数（键 = 值）。
        config=configparser.ConfigParser()
        config.read(r'config.ini.ini')
        host=config.get('db','db_host')
        port=eval(config.get('db','db_port'))
        user = config.get('db', 'db_user')
        password = config.get('db', 'db_pass')
        db=config.get('db', 'db_name')
        # 打开数据库连接
        self.db = pymysql.connect(host=host, user=user, port=port, password=password, database=db)
        print("数据库开始")

    def db_select(self, sql):
            try:
                # # 使用cursor方法创建游标对象
                # cursor = self.db.cursor()
                # # 使用execute执行sql查询语句
                # cursor.execute(sql)
                # # fatchall获取返回的所有数据
                # data = cursor.fetchall()
                # return data
                # 获得Cursor对象
                cs1 = self.db.cursor()
                # 执行select语句，并返回受影响的行数：查询一条数据
                count = cs1.execute(sql)
                # 打印受影响的行数
                print("查询到%d条数据:" % count)

                for i in range(count):
                    # 获取查询的结果
                    result = cs1.fetchone()
                    # 打印查询的结果
                    print(result)
                # 获取查询的结果

            except BaseException as e:
                print(e)

    def db_close(self):
            try:
                self.db.close()
                print("关闭数据库")
            except BaseException as e:
                print(e)

if __name__ == '__main__':
    a = TestStudents()
    sql = 'select * from students17;'
    a.db_select(sql)
    a.db_close()

