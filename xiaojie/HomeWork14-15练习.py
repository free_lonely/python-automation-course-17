# -*- coding: utf-8 -*-
'''
@Time    : 2021/4/19 22:17
@Author  : xiaojie
@File    : HomeWork14-15练习.py
'''

# try:
#     print('-----test--1---')
#     open('123.txt', 'r')
#     print('-----test--2---')
#
# except IOError:
#     print('IOError类型的错误，No such file or directory：123.txt ')


# try:
#     print(num)
# except NameError:
#     print('NameError: name ‘num’ is not defined')

'''
捕获多个异常
'''
# try:
#     print('-----test--1---')
#     open('123.txt', 'r')
#     print('-----test--2---')
#     print(num)
# except (IOError,NameError) as result:
#     print('哈哈，捕获到了异常')
#     print('异常的基本信息是：',result)

'''
捕获异常中使用else
'''
# try:
#     num = 100
#     print(num)
# except NameError as errorMsg:
#     print('产生错误了:%s'%errorMsg)
# else:
#     print('没有捕获到异常，真高兴')


import time
try:
    print('进入外层try模块')
    f = open('test.txt')
    try:
        print('打开文件成功，进入内层try')
        while True:
            content = f.readline()
            print(content)
            if len(content) == 0:
                print('文件为空跳出循环')
                break
            time.sleep(2)
            print(content)
    except:
        # 如果在读取文件的过程中，产生了异常，那么就会捕获到
        # 比如 按下了 ctrl+c
        pass
    finally:
        f.close()
        print('关闭文件')
except:
    print("没有这个文件")

import pymysql



