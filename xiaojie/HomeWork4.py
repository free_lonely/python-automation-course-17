'''
1、使用while，完成以下图形的输出
*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*
'''
#for循环实现
num1 = int(input('请输入要打印的行数：'))
#上半部分打印的图像
for i in range(num1):
    for j in range(0,i):
        print("*", end=" ")
    print("")
#下半部分打印的图像
for i in range(num1):
     for j in range(0, num1 - i):
       print("*", end=" ")
     print("")
#while循环实现
# hangshuju = int(input('请输入要打印的行数：'))
# i = 0
# while i <= hangshuju:
#     j = 0
#     while j <= i:
#         print('*',end='')
#         j += 1
#     print('')
#     i += 1
# while i <= hangshuju:
#     j = 0
#     while j <= hangshuju-i-1:
#         print('*',end='')
#         j += 1
#     print('')
#     i += 1

