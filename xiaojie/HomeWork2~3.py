# import keyword  #导入python中的特殊关键字
print(keyword.kwlist)  # 输出keywork的特殊关键字


'''1、	说出变量名可以由哪些字符组成'''
#变量由字母、数据和下划线组成
'''2、写出变量命名时的规则'''
#变量名不能以数字开头，不能出现除数字/字母/下划线之外的特殊符号
'''3、写出什么是驼峰法?'''
#小驼峰法：即myStudentCount第一个单词是全部小写，后面的单词首字母大写。
#大驼峰法：即DataBaseUser

'''
4、编写程序，完成以下要求：
•	提示用户进行输入数据
•	获取用户的数据（需要获取2个）
•	对获取的两个数字进行求和运行，并输出相应的结果
%s字符串占位符，，其中S=String，2、%d整数占位符 3、%f浮点数占位符，
'''
# 提示用户进行输入数据
a = input("请输入第一个数据：")
b = input("请输入第二个数据：")
# 打印变量a、b的类型
print(type(a))
print(type(b))
result = a + b
print(type(result))
# %S字符串占位符
print('两个数字进行求和运行：a+b=%s' % result)

# 将用户输入的数据强制转换为int类型
a = int(input("请输入第一个整数："))
b = int(input("请输入第二个整数："))
# 打印转换后的变量a、b的类型
print(type(a))
print(type(b))
result = a + b
print(type(result))
# %d整数占位符
print('两个数字进行求和运行：a+b=%d' % result)

# 将用户输入的数据强制转换为float类型
a = float(input("请输入第一个浮点数："))
b = float(input("请输入第二个浮点数："))
# 打印转换后的变量a、b的类型
print(type(a))
print(type(b))
result = a + b
print(type(result))
# %f浮点数占位符
print('两个数字进行求和运行：a+b=%f,' % result)

'''
5、编写程序，完成以下信息的显示:
•	==================================
•	=        欢迎进入到身份认证系统V1.0
•	= 1. 登录
•	= 2. 退出
•	= 3. 认证
•	= 4. 修改密码
•	==================================
'''
print('==================================')
print('=        欢迎进入到身份认证系统V1.0')
option = int(input('请输入您需要使用的功能：1.登录，2.退出，3.认证，4.修改密码：'))
if option == 1:
    print('= 1. 登录')
elif option == 2:
    print('= 2. 退出')
elif option == 3:
    print('= 3. 认证')
else:
    print('= 4. 修改密码')
print('==================================')

'''
6、编写程序，通过input()获取一个人的信息，然后按照下面格式显示
==================================
姓名: xxxxx
QQ:xxxxxxx
手机号:131xxxxxx
公司地址:北京市xxxx
==================================
'''
name=input('姓名：')
QQ=input('QQ:')
mobile=input('手机号：')
address=input('公司地址：')
print('==================================\n姓名: '+name+'\nQQ:'+QQ+'手机号:'+mobile+'\n公司地址:'+address+'\n==================================')


'''
7、使用if，编写程序，实现以下功能： 
•	从键盘获取用户名、密码
•	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
'''
username = input('请输入用户名：')
password = input('请输入密码：')
if username == 'admin' and password == '123456':
    print('欢迎进入xxx的世界')
else:
    print('密码或用户名错误')

'''
8、有能力的同学可以用循环的方式做出石头剪刀布的游戏并且输入中文
'''
# 随机数模块
import random

# 引导用户输入拳法(剪刀 石头 布)
# 玩家 或者 用户
player = int(input("请输入: 剪刀(0) 石头(1) 布(2):"))

# 电脑(随机出拳)
# randint(0, 2) == [0, 2](闭区间)
computer = random.randint(0, 2)

# 记住玩家赢了的情况如下：
# 用户->剪刀 电脑-> 布
# 用户->石头 电脑-> 剪刀
# 用户->布   电脑-> 石头

# 平局
# 用户的拳法==电脑的拳法

# 输了
# 除了赢得和平的 就是输的
print("玩家:%d--电脑:%d" % (player, computer))
# 赢了
if (player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print("玩家胜利!!!")
# 平局
elif player == computer:
    print("平局")
# 输了
else:
    print("玩家失败!!!")

