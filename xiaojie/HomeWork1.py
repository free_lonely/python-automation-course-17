
#学会使用git上传作业：
#第一次登录需要配置config，使用如下命令：
#git config.ini --int
#git config.ini --global --unset user.email

#git clone 远程服务器地址
#本地仓库新建文件
#文件上传到本地仓库：
# git add .
# git commit -m '第一次课堂作业提交'
#本地仓库上传到远程仓库：
# git push
#拉取远程仓库最新代码：
# git pull


'''
使用git上传作业到远程仓库遇到的问题：
remote: You do not have permission push to this repository
fatal: unable to access 'https://gitee.com/free_lonely/python-automation-course-17.git/': The requested URL returned error: 403
'''
#解决方法：控制面板\用户帐户\凭据管理器\编辑普通凭据，进这里面输入授权的git用户名和密码，然后再试试git push指令



