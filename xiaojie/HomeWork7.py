import random

# numbers = [12, 5, 3, 9, 7]
# numbers.sort(reverse=True)
# print(numbers[0])



# 1. 用任意语言循环输出整数0-10
i = 0
while i <= 100:
    if i % 2 == 0:
        print(i)
    i +=  1

# 2. 用任意语言逆序输出字符串，比如[欢乐逛欢迎你]，输出[你迎欢逛乐欢]
str1 = '欢乐逛欢迎你'
str2 = str1[::-1]
print(str2)

# 3. str = 'a,hello' ,在字符串中查找hello
str = 'a,hello'
print(str.find('hello'))

# 4. str = 'a, b, c, d',用逗号分割字符串，并保存在列表
str = 'a, b, c, d'
print(str.split(','))

# 5. 将'笔试题123A'中的123 换为‘进行中’
str = '笔试题123A'
print(str.replace('123','进行中'))

# -------------------——————————————————————————————————————————————————————--------------------------
# -----------------------------------------

# 1.下列将字符串"100"转换为数字100的正确的是( B )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
# 2.下列程序执行结果是( A )
# numbers = [1，5，3，9，7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1
# 3.如何在列表中添加一个元素      append()或extend()
# 4.对于列表什么是越界    下标不在列表值范围内即越界
# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型  列表可变，字符串/元组/字典不可变
"""
6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母。
"""
# name = input("请依次输入5个学生的名字：")
# name1 = name.split()
# students = []
# for i in range(5):
#     student = input('请输入第%d个学生的名字：' % i)
#     students.append(student)
#     i += 1
# print(students)
# for word in students:
#     print("%s的第二个字母为%s" % (word, word[1]))

'''
7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
'''
# counts = []
# for i in range(5):
#     count = random.randint(0, 100)
#     counts.append(count)
#     #counts.sort(reversed=True)
#     i += 1
# print("正向：" + str(counts))
# counts.reverse()
# print("反向：" + str(counts))


'''
8.将下列两个列表合并，将合并后的列表升序并输出.
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
'''
# list1 = [1, 3, 4, 5, 7]
# list2 = [0, 66, 8, 9]
# list1.extend(list2)
# print('打印合并后的列表'+str(list1))
# list1.sort(reverse=False)  #list1进行升序排序
# print('合并后的列表升序展示：'+str(list1))

'''
9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
'''
# name = input("姓名：")
# age = int(input("年龄(数字)："))
# stuno = input("学号：")
# dictionary = {'姓名': name, '年龄': age, '学号': stuno}
# print(type(dictionary))
# print(dictionary)
'''
10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}
'''
# dict1 = {'school': 'lebo', 'date': 2018, 'address': 'beijing'}
# print(dict1.keys()) #输出值为元组
# print(list(dict1.keys()))
# print(list(dict1.values()))
# print(list(dict1.keys())[list(dict1.values()).index("lebo")])

'''
11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
'''
# num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# fanzhuannum = num[::-1]
# print(fanzhuannum)
# result = 0
# for i in fanzhuannum:
#     if i % 2 == 0:
#         print(i)
#         result = i + result
# print('将翻转完后的列表中所有偶数位置的元素相加求和:%d' % result)
