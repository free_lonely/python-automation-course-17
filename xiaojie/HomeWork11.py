'''
                                                                综合应用: 学生管理系统(文件版)
'''
# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
stu_list = []
# 定义一个字典，用来存储用户的学生信息(这是一个字典)
stu_dict = {}


# 有如下7个方法
def menu():
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")


"""添加学生信息"""


def addstudent(x):
    for i in range(x):
        print('请输入第 %d 个要添加的学生信息：' % i)
        new_name = input("请输入姓名:")
        new_tel = input("请输入手机号:")
        new_qq = input("请输入QQ:")
        stu_dict = {'name': new_name, 'tel': new_tel, 'qq': new_qq}
        stu_list.append(stu_dict)
    print(stu_list)


# """删除学生信息"""
def del_student():
    print('学生列表的长度：', len(stu_list))
    # "请输入要删除的序号:"
    del_index = int(input('请输入要删除的序号'))
    if del_index >= 0 and del_index < len(stu_list):
        jueding = input('你确定要删除么？yes or no')
        if jueding == 'yes':
            stu_list.pop(del_index)
            print(stu_list)
    else:
        print('输入序号有误, 请重新输入')


# """修改学生信息"""
def update_student():
    print('打印列表的长度：', len(stu_list))
    # "请输入要修改的序号:"
    xunhao = int(input('请输入要修改的序号:'))
    if xunhao >= 0 & xunhao <= len(stu_list) - 1:
        # "你要修改的信息是:"
        print("你要修改的信息是:", stu_list[xunhao])
        stu_list[xunhao]['name'] = input("请输入新的姓名:")
        stu_list[xunhao]['tel'] = input("请输入新的手机号:")
        stu_list[xunhao]['qq'] = input("请输入新QQ:")
        print("修改后的学生数据为：", stu_list)
    else:
        print("输入序号有误,请重新输入")


# """查询学生信息"""
def select_student():
    # "请输入要查询的学生姓名:"
    name = input("请输入要查询的学生姓名:")
    count = 0
    for i in stu_list:
        for j in i.keys():
            print(i)
            print(j)
            if i[j] == name:
                print('查询到的信息如下:%s' % i[j])
                count += 1
                print('共查询到%d个学生信息：' % count)
            else:
                print("没有您要找的信息....")


# """遍历学生信息"""
def foreach_student():
    # 序号\t姓名\t\t手机号\t\tQQ
    # """加载之前存储的数据"""
    xunhao = 0
    for i in stu_list:
        xunhao += 1
        print('序号:%s\t\t姓名:%s\t\t手机号:%s\t\tQQ:%s' % (xunhao, i['name'], i['tel'], i['qq']))


def save_student():
    file = open('student.txt', 'w')
    file.writelines(str(stu_list))
    file.close()


# 作业：写出这个学生管理系统的后台，周日12点之前写完

for i in range(7):
    menu()
    index = int(input('请输入1-7中的一个数字：'))
    if index == 1:
        x = int(input('一共需要添加多少个学生信息：'))
        addstudent(x)
    elif index == 2:
        del_student()
    elif index == 3:
        update_student()
    elif index == 4:
        select_student()
    elif index == 5:
        foreach_student()
    elif index == 6:
        save_student
    else:
        break
