'''
4、用lambda函数实现两个数相乘
5、列表推导式求列表所有奇数并构造新列表，a =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
'''
f = lambda a, b: print(a * b)
f(2, 3)

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list = []
for i in a:
    if i % 2 == 1:
        print(i)
        list.append(i)
print(list)
print([x for x in a if x % 2 == 1])

l1 = [i * i for i in range(1, 11)]
print(l1)

l2 = [i for i in range(1, 101) if i % 2 == 0]
print(l2)

l3 = [f'python{i}期' for i in range(1, 101)]
print(l3)



# 2.  长度未知的元组a=(1,3,4,5,6,…),请随机输出n项
a = (1, 3, 4, 5, 6, 8, 9, 10, 200)
index = len(a)-1
print(a[index])



"""
3.有一个文件 test. Txt,里面有数据
id name click  date
1 test 100 2012-04-18
2 aaa  12  2012-04-19
3 bbb  333 2012-04-18
4 cc   211 2012-04-17
5 ddd  334 2012-04-16
共有5行4列数据,最后一列为日期。请按日期从小到大对数据进行排序一
假如数据以 key: value的形式存储格式为id1, name test, click100date:2012-04-18
现在有了一个月的数据,统计下平均一天内的某个name的cick值
# """


# a = 'lebo.txt'
# b = 'lebo.txt'
# old_f = open(a, 'w')
# old_f.write("lebo")
# old_f.close()
#
#
# old_f = open(a)
# ret = old_f.readlines()
# new_f = open(b, 'w')
# for i in ret:
#     new_f.write(i)
#
# old_f.close()
# new_f.close()

import os
# open('lebo.txt', 'w')
# # os.remove('lebo.txt')
# os.rename('lebo.txt', 'hahah.txt')
funFlag = 1
flodename= '../'
dirlist = os.listdir(flodename)
for name in dirlist:
    #print(name)
    if funFlag == 1:
        newName = '[lebo]-' + name
    print(newName)


file1 = open('lebo.txt', mode='wb')
file1.write('你好，乐博学院'.encode())
ret = file1.read()
for i in ret:
    print(i)
file1.close()
