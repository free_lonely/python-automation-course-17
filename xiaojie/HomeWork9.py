'''
1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
'''
'''答案：*args：不定长参数，接收单个出现的参数,接收后存成一个元组
**args：关键字参数，接收以键值对形式出现的参数,接收后存成一个字典'''
'''

2、（先自己想，然后再实验，最后查百度）
def f(x, l=[]):
    for i in range(x):
        l.append(x * 1)
    print(l)

f(2)  #[2,2]
f(3, [3, 2, 1])   #[3,2,1,3,3,3]
f(3)   #[3,3,3]
'''
def f(x, l=[]):
    for i in range(x):
        l.append(x * 1)
    print(l)


f(2)  #[2,2]
f(3, [3, 2, 1])  # [3,2,1,3,3,3]
f(3)  # [3,3,3]


'''
3、如何在一个函数内部修改全局变量
通过关键字改变局部变量为全局变量：globle num
'''
def fun():
    global num
    num = 10
    print(num)

def fun1():
    print(num)
fun()
fun1()

