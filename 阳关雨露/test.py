"""
__project__ = 'PythonProject'
__file_name__ = 'test'
__author__ = 'Sun'
__time__ = '2021/4/8 15:22'
__product_name = PyCharm

"""
"""
day01克隆提交代码
git clone ‘远端仓库的地址’ #首次把远端的内容拉到本地仓库
ll 查看文件目录
git status #查看本地仓库的状态
git add .  #增加到本地仓库
git commit -m '注释的内容'  #提交到本地仓库，注释的内容不可为空必须填写相应的描述
git push  #从本地仓库推送到远端仓库
git pull    #把远端仓库的代码拉到本地仓库
push之前需要先commit
输入用户名和密码
git config.ini --global user.email "you@example.com"
git config.ini --global user.name "Your Name"

注释的两种类型：单行注释和多行注释
单行注释表示用 # 代码的文字说明
多行注释用 格式: """""" 或者 '''注释说明'''
快捷键：Ctrl+/（添加注释或者取消注释）

变量是用来临时保存数据的，便于数据开发
变量的数据类型 int float bool string double
定义一个变量例如 num = 10
查看一个变量的类型 type 内置函数
格式：type(变量名)
print(type(变量名))

变量的命名
# 第一种 小驼峰命名法
myName = '老王'
# 第二种 大驼峰命名法
MyAge = 18
# 下划线 _连接单词
is_man = True

# 导入模块
import keyword 例如 import random
# 查看当前这个版本中有哪些关键字
print(keyword.kwlist)

# 格式化输出 %d整型  %s字符串  %f浮点型
%f保留的是小数点后6位，在实际开发中只需要两位即可 %.2f

myName = "小明"
print("我的名字叫做%s" % myName)

myAge = 20
print("我的年龄是%d岁" % myAge)

myHeight = 180.21
print("我的身高为%.2f" % myHeight)

isMan = False
print("是否是男人:%s" % isMan)
# 默认情况下 True -> 1  False -> 0
# 如果想输出bool 如果显示的为True 或者False 那么使用%s 如果显示 1 或者 0 那么使用%d
# 在python中 用 \n 代表是换行
# print("hello") 是输出的一个简写
# print("hello", end="\n") 和 print("hello") 等价的
# print("hello", end="")

# 班级90分以上的人数占了85%
# 如果想显示一个单纯的%号 需要写两个
print("班级90分以上的人数占了%d%%" % num)

# 如果为了输出一段文字 需要两个变量的配合 完成

my_name = "小明"

my_age = 20

# 名字:小明 年龄:20
print("名字:%s 年龄:%d" % (my_name, my_age))

# 在python3中 无论输入的是什么格式的数据 最终记录数据的变量的类型都是字符串类型
my_name = input("请输入您的名字:")
print(type(my_name))
print("输出结果:%s" % my_name)

a = 10
b = 20
# +	加	两个对象相加 a + b 输出结果 30
# -	减	得到负数或是一个数减去另一个数 a - b 输出结果 -10
# *	乘	两个数相乘或是返回一个被重复若干次的字符串 a * b 输出结果 200
# /	除	b / a 输出结果 2
# //取整除	返回商的整数部分 9//2 输出结果 4 , 9.0//2.0 输出结果 4.0
# %	取余	返回除法的余数 b % a 输出结果 0
"""
# ** 指数 a**b 为10的20次方， 输出结果 100000000000000000000

# +=	加法赋值运算符	c += a 等效于 c = c + a
# -=	减法赋值运算符	c -= a 等效于 c = c - a
# *=	乘法赋值运算符	c *= a 等效于 c = c * a
# /=	除法赋值运算符	c /= a 等效于 c = c / a
# %=	取模赋值运算符	c %= a 等效于 c = c % a
# **=	幂赋值运算符	c **= a 等效于 c = c ** a
# //=	取整除赋值运算符	c //= a 等效于 c = c // a
"""
# eval(str)	用来计算在字符串中的有效Python表达式,并返回一个对象
a = 3.14

my_str = "a"
ret5 = eval(my_str)
print(type(ret5))

# 安检
# # 判断是否有危险品 如果没有危险品 打印可以进入火车站
# # 定义一个变量来保存是否有危险品(True 没有危险品)
# is_flag = False
# # 使用if语句判断
# if is_flag:
#     print("可以进入火车站")
#
# print("测试")


# 网吧
# # 定义一个变量 保存年龄(年龄大于等于18才可以进入网吧)
# age = 19
# # ret = (age >= 18)
# # print(ret)
# if age >= 18:
#     print("可以进入网吧")
#     print("测试")

# ==	检查两个操作数的值是否相等，如果是则条件变为真。	如a=3,b=3，则（a == b) 为 True
# !=	检查两个操作数的值是否相等，如果值不相等，则条件变为真。	如a=1,b=3，则(a != b) 为 True
# >	检查左操作数的值是否大于右操作数的值，如果是，则条件成立。	如a=7,b=3，则(a > b) 为 True
# <	检查左操作数的值是否小于右操作数的值，如果是，则条件成立。	如a=7,b=3，则(a < b) 为 False
# >=	检查左操作数的值是否大于或等于右操作数的值，如果是，则条件成立。	如a=3,b=3，则(a >= b) 为 True
# <=	检查左操作数的值是否小于或等于右操作数的值，如果是，则条件成立。	如a=3,b=3，则(a <= b) 为 True
a = 111
b = 111

# ==
# 如果a 和 b 相等 我打印一句话 a 等于 b
# if a == b:
#     print("a 等于 b")

# !=
# if a != b:
#     print("a 不等于 b")

# >
# if a > b:
#     print("a 大于 b")

# <
# if a < b:
#     print("a 小于 b")

# >=
# if a >= b:
#     print("a 大于等于 b")

# <=
# if a <= b:
#     print("a 小于等于 b")

# and	x and y	布尔"与"：如果 x 为 False，x and y 返回 False，否则它返回 y 的值。	True and False， 返回 False。
# or	x or y	布尔"或"：如果 x 是 True，它返回 True，否则它返回 y 的值。	False or True， 返回 True。
# not	not x	布尔"非"：如果 x 为 True，返回 False 。如果 x 为 False，它返回 True。	not True 返回 False, not False 返回 True


# and
# 同真则真 一假则假
# 假设 用户名 mngr 密码 123456  告知用户您输入的用户名和密码正确 登录成功

# 引导用户输入用户名和密码
# user = input("请输入您的用户名:")
# passwd = input("请输入您的密码:")

# 判断
# if user == "mngr" and passwd == "123456":
#     print("您输入的用户名和密码正确 登录成功")


# or
# 一真则真 全假则假
# 您输入的用户名或者密码错误

# if user != "mngr" or passwd != "123456":
#     print("您输入的用户名或者密码错误")


# not(取反)
# 非真则假 非假则真
# 定义一个变量 判断是否是男人
# is_man = True
#
# if not is_man:
#     print("是男人")

is_man = not True

if is_man:
    print("是男人")


# 安检
# 定义一个变量 记录是否是危险品
# 如果为True 就代表没有危险品
# is_flag = not True
# if is_flag:
#     print("没有危险品 可以进入")
# else:
#     print("有危险品 不可以进入火车站")
#
# print("测试")

# 网吧
# 请输入您的年龄 如果年龄大于等于18岁 可以进入网吧吃鸡 如果年龄小于18 回去学习吧
# age = input("请输入您的年龄:")
# new_age = int(age)

# 简写
# age = int(input("请输入您的年龄:"))
# if age >= 18:
#     print("可以进入网吧吃鸡")
# else:
#     print("回去学习吧")

# 保险柜
# 引导用户输入密码 密码正确 打印 密码正确 密码错误 密码错误
# passwd = input("请输入密码:")
#
# if passwd == "12345":
#     print("密码正确")
# else:
#     print("密码错误")

# elif -> else if

# 定义一个变量 记录分数
score = 44
# 如果分数>=90 显示 优  如果分数 >= 80 良  分数 >= 60 中 分数 >= 0 差
# if score >= 90 and score <= 100:
#     print("优")
# elif score >= 80 and score < 90:
#     print("良")
# elif score >= 60 and score < 80:
#     print("中")
# elif score >= 0 and score < 60:
#     print("差")

# 简写
# if score >= 90:
#     print("优")
# elif score >= 80:
#     print("良")
# elif score >= 60:
#     print("中")
# elif score >= 0:
#     print("差")

# 也可以使用else
if score >= 90:
    print("优")
elif score >= 80:
    print("良")
elif score >= 60:
    print("中")
else:
    print("差")

# if语句多种组合
# if语句
# if-else
# if-elif-elif
# if-elif-elif-xx-else


# 进入火车站
# 火车票 刀具
"""
# 伪代码
# 进入火车站- 判断是否有火车票 - 如果有 -> 可以进入火车站- 判断刀具的长度- 如果刀具的长度 < 10cm -> 可以进入火车站-
# 如果刀具的长度 >= 10cm -> 不可以进入火车站 - 如果没有 -> 请买票后 再进入火车站
"""

# 定义一个变量判断是否有火车票(只有等于1的时候代表有火车票)
chePiao = 1
# 刀具的长度
daoLenght = 7
# 判断火车票
if chePiao == 1:
    # 有车票
    print("可以进入火车站")
    # 判断刀具的长度
    if daoLenght < 10:
        print("拿着您的小刀进入吧")
    else:
        print("不要拿着您的宝剑指着我")
    print("测试")
else:
    # 没有火车票
    print("请买票后 再进入火车站")


# 情节描述：上公交车，并且可以有座位坐下
# 要求：输入公交卡当前的余额，只要超过2元，就可以上公交车；如果车上有空座位，就可以坐下。

# 定义一个变量 记录余额
money = eval(input("请输入公交卡的余额:"))
# 定义一个变量 判断是否有空座
flag = not True

# 判断余额
if money >= 2:
    print("可以上车")
    # 判断是否有座位
    if flag:
        print("有座位可以坐下")
    else:
        print("没有座位")
else:
    print("卡中余额不足")


# 随机数模块
import random
# 引导用户输入拳法(剪刀 石头 布)
# 玩家 或者 用户
player = int(input("请输入: 剪刀(0) 石头(1) 布(2):"))

# 电脑(随机出拳)
# randint(0, 2)  == [0, 2](闭区间)
computer = random.randint(0, 2)

# 记住 用户 是第一视角
# 赢了
# 用户->剪刀 电脑-> 布
# 用户->石头 电脑-> 剪刀
# 用户->布   电脑-> 石头

# 平局
# 用户的拳法==电脑的拳法

# 输了
# 除了赢得和平的 就是输的
print("玩家:%d--电脑:%d" % (player, computer))
# 赢了
if (player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print("玩家胜利!!!")
# 平局
elif (player == computer):
    print("平局")
# 输了
else:
    print("玩家失败!!!")

# 定义一个变量
i = 0
# while循环
while i < 5:
    print("媳妇儿我错了! - 第%d次" % (i + 1))
    i += 1
    
    
# 计算1~100的累积和（包含1和100）
# 如果一个循环没有停止的条件 我们称之为死循环
# 定义一个变量
num = 1
# 定义一个变量 记录(1~100的和)
my_sum = 0
while num <= 100:
    # print(num)
    my_sum += num
    num += 1

print("计算1~100的累积和:%d" % my_sum)


# 计算1~100之间偶数的累积和（包含1和100）

# 定义一个变量
num = 1
# 定义一个变脸保存求和
my_sum = 0

while num <= 100:
    # 判断num是否是偶数(被2整除)
    if num % 2 == 0:
        # print(num)
        my_sum += num

    num += 1

print("计算1~100之间偶数的累积和:%d" % my_sum)

"""
# *
# **
# ***
# ****
# *****
"""

# 考虑行数
# i = 1
# while i <= 5:
#     print("*")
#     i += 1

# 考虑列数
# j = 1
# while j <= 5:
#     print("*", end="")
#     j += 1

# 整合
# 控制行数
i = 1
while i <= 5:
    # 控制列数
    j = 1
    while j <= i:
        print("*", end="")
        j += 1
    # 换行
    print()
    i += 1



# 行数
# i = 1
# while i <= 9:
#     print(i)
#     i += 1

# 列数
# j = 1
# while j <= 3:
#     print(j, end="")
#     j += 1


# 整合
i = 1
while i <= 9:
    # 列数
    j = 1
    while j <= i:
        print("%d * %d = %2d " % (j, i, (i * j)), end="")
        j += 1
    # 换行
    print()
    i += 1




# 字符串就是一个有序的字符序列

# for循环

# 特殊用法
# 定义一个字符串
# name = "itheima"
#
# for x in name:
#     print(x)

# - 0 1 2 3 4
# i = 0
# while i < 5:
#     print(i)
#     i += 1

# 等价于for循环
# 如果使用range(x) [0,x)
# for i in range(5):
#     print(i)

# 5 6 7 8 9 10
# i = 5
# while i <= 10:
#     print(i)
#     i += 1
# range(a, b) [a, b)
# for i in range(5, 11):
#     print(i)


# 直接定义一个死循环
# while True:
#     pass


# break和continue(肯定需要和循环配合使用)

# break

# while-break
# 在一个循环中如果某个条件成立后 执行了break 那么这个循环将停止(跳出循环)
# 而且在break后面的代码将不再执行

# i = 1
# while i <= 5:
#     print(i)
#     # 如果i=2 执行下break
#     if i == 2:
#         break
#         print("哈哈")
#     i += 1
#
# print("测试")

# while-continue
# 在一个循环中如果某个条件成立后 执行了continue 那么提前结束本次循环
# 而且在continue后面的代码将不再执行
# i = 1
# while i <= 5:
#     i += 1
#     # 如果i=2 执行下continue
#     if i == 3:
#         continue
#         print("哈哈")
#     print(i)
#
#
# print("测试")


# for-break
# 结论和while-break 一样的
# for i in range(5):
#     print(i)
#     if i == 2:
#         break


# for-continue
# 结论和while-break 一样的
for i in range(5):

    if i == 2:
        continue
    print(i)
    
"""
# tab ====> 四个空格
# 回退一个tab键====> shift tab
# (取消)注释的快捷键====> ctrl + ?
# 写错时回退 ctrl + z
# ctrl + a  =====  全选

# for-else
# # 当for循环循环完成以后 然后会执行else中的代码
# for i in range(5):
#     print(i)
# else:
#     print("进入else")

# 当for循环中执行了break 那么else中的代码将不会执行
# for i in range(5):
#     print(i)
#     if i == 2:
#         break
# else:
#     print("进入else")


# while-else
# while-else 的结论可以完全参考for-else结论
i = 0
while i < 5:
    print(i)
    if i == 2:
        break
    i += 1
else:
    print("执行了else")

# 字符串就是一个有序的字符序列(保存字符的)
# 输入引导的文字
# name = "名字"
# input("请输入您的%s" % name)

# 输入一段文字 我爱您中国
# result = "中国"
# 我爱您中国
# result1 = "我爱您%s" % result
# print(result1)


# 字符串的定义
# 01:格式
# my_str1 = "hello world"
# my_str2 = 'hello world'
# # 在python中 '' 和""意义是一样
# if my_str1 == my_str2:
#     print("等价的")

# 02 如果想保留文本内容格式
# 如果保留文本的格式 可以使用 """""" 或者 ''''''
# my_str = '''你好
# 中国
# 你好
# 龟叔"""
# print(my_str)


# 03 空字符串(一个字符都没有而且有"" 或者 '')
# 定义了一个空的字符串
my_str1 = ""
my_str2 = '111222'
# 类名创建一个空字符串
my_str3 = str()

# python内置函数(python直接提供的函数 服务于程序员)
# len函数 是来计算字符串中字符(元素)的个数
l = len(my_str2)
print(l)


# 字符串其实就是一个有序的字符序列(保存字符的)(字符串中最小单元就是字符)

my_str = "hello"
# 打印其my_str中e
# 下标索引 或者是 下标 或者 索引
# python中下标索引是从0开始的(顺序是从左到右)
# ret1 = my_str[1]
# print(ret1)
# 也可以通过从右到左获取e这个字符(从右到左是从-1开始)
# ret2 = my_str[-4]
# print(ret2)


# 依次的打印字符串中每个字符
# -for循环
# for c in my_str:
#     print(c)

# -while循环
# 定义一个变量
i = 0
# 计算下字符串的字符个数
l = len(my_str)
while i < l:
    # 通过下标获取对应的字符
    ret = my_str[i]
    print(ret)
    i += 1


# 切片的语法：[起始:结束:步长]

# python中的切片 其实可以理解为字符串的截取(得到字符串中的某个字符或者一个字符片段)
# >>> a = "abcdef"
#  'abc'
#  'ace'
#  'bd'
#  'fdb'
#  'fd'
# python中字符串是不可变(当执行切片的操作时候 其本身不会发生改变)

# 定义一个字符串
a = "abcdef"
#  'abc'
# 默认情况下 步长不写就是为1的
# ret1 = a[0:3]
# 简写
# ret1 = a[:3]
# print(ret1)

#  'ace'
# ret2 = a[:5:2]
# print(ret2)

#  'bd'
# ret3 = a[1:4:2]
# print(ret3)

#  'fdb'
# 倒序
# ret4 = a[::-2]
# print(ret4)

#  'fd'
# ret5 = a[-1:-4:-2]
# print(ret5)


# 条条大路通罗马

# 内置函数前面什么都没有 只有一个函数名而已
# 字符串中的使用的都是方法 方法前面是字符串
# len()

# 定义一个字符串
a = "abcdcecf"
# <1>find
# 检测 str 是否包含在 mystr中，如果是返回开始的索引值，否则返回-1
# index = a.find("1")
# print(index)

# <2>index
# 跟find()方法一样，只不过如果str不在 mystr中会报一个异常.
# index = a.index("q")
# print(index)

# <3>count
# 返回 str在start和end之间 在 mystr里面出现的次数
# count = a.count("1")
# print(count)

# <4>replace
# 把 mystr 中的 str1 替换成 str2,如果 count 指定，则替换不超过 count 次.
# ret1 = a.replace("c", "q", 1)
# print(ret1)

# <5>split
# 以 str 为分隔符切片 mystr，如果 maxsplit有指定值，则仅分隔 maxsplit 个子字符串
# ret = a.split("c")
# # ['ab', 'd', 'e', 'f']
# print(ret)



# 定义一个字符串
a = "AbCD"
# <6>capitalize
# 把字符串的第一个字符大写
# ret1 = a.capitalize()
# print(ret1)

# <7>title
# 把字符串的每个单词首字母大写
# ret2 = a.title()
# print(ret2)

# <8>startswith
# 检查字符串是否是以 hello 开头, 是则返回 True，否则返回 False
# flag = a.startswith("1ab")
# print(flag)

# <9>endswith
# 检查字符串是否以obj结束，如果是返回True,否则返回 False.
# flag = a.endswith(".mp4")
# print(flag)

# <10>lower
# 转换 mystr 中所有大写字符为小写Afre -> AFRE
# ret = a.lower()
# print(ret)

# <11>upper
# 转换 mystr 中的小写字母为大写
# ret = a.upper()
# print(ret)


# 定义一个字符串
a = "ppooabcdiii"
# <12>ljust
# 返回一个原字符串左对齐,并使用空格填充至长度 width 的新字符串
# ret = a.ljust(6, "x")
# print(ret, end="-")

# <13>rjust
# 返回一个原字符串右对齐,并使用空格填充至长度 width 的新字符串
# ret = a.rjust(6, "x")
# print(ret)

# <14>center
# 返回一个原字符串居中,并使用空格填充至长度 width 的新字符串
# ret = a.center(6, "x")
# print(ret)

# <15>lstrip
# 删除 mystr 左边的空白字符
# ret = a.lstrip("pob")
# print(ret)

# <16>rstrip
# 删除 mystr 字符串末尾的空白字符
# ret = a.rstrip("i")
# print(ret)

# <17>strip
# 删除mystr字符串两端的空白字符
# ret = a.strip("pio")
# print(ret)

# <18>rfind
# 类似于 find()函数，不过是从右边开始查找.

# <19>rindex
# 类似于 index()，不过是从右边开始.

a = "    "
# <20>partition
# 把mystr以str分割成三部分,str前，str和str后
# ret = a.partition("cd")
# print(ret)

# <21>rpartition
# 类似于 partition()函数,不过是从右边开始.
# ret = a.rpartition("f")
# print(ret)

# 22>splitlines
# 按照行分隔，返回一个包含各行作为元素的列表
# ret = a.splitlines()
# print(ret)

# <23>isalpha
# 如果 mystr 所有字符都是字母 则返回 True,否则返回 False
# flag = a.isalpha()
# print(flag)

# <24>isdigit
# 如果 mystr 只包含数字则返回 True 否则返回 False.
# flag = a.isdigit()
# print(flag)

# <25>isalnum
# 如果 mystr 所有字符都是字母或数字则返回 True,否则返回 False
# flag = a.isalnum()
# print(flag)

# <26>isspace
# 如果 mystr 中只包含空格，则返回 True，否则返回 False.
# flag = a.isspace()
# print(flag)


# <27>join -> 列表 -> 字符串
# 定义一个列表
# my_list = ["ab", "cd", "ef"]
# # 最终打印一个结果abxcdxef
# ret = "".join(my_list)
# print(ret)


# 技巧
a = "hello"
# str 数据类型
print(type(a))
# 内置函数(帮助查询某个类中的方法文档)
# 查看所有
help(str)
# 查看某一个
help(str.count)



# 列表中可以承载任意的数据类型
# 列表其实就是一个容器类型(承载数据是有序的) -> 支持下标索引
# 格式: 列表名 = [元素1, 元素2,...]
# my_list = [1, 3.14, True, "hello"]

# print(my_list)
# 打印3.14
# result = my_list[1]
# print(result)



# 定义一个字符串的标识符号 ""
# 定义一个列表的标识符号 就是一个[]

# 定义一个特殊的列表 -> 空列表 (列表中没有一个元素)
my_list1 = []
# <class 'list'>
print(type(my_list1))
# 计算列表中有多少个元素
# l = len(my_list1)
# print(l)
my_list2 = list()
print(len(my_list2))



# 定义一个列表

# my_list = ["a", "b", "c", "d"]

# 简写
my_list = list("abcd")
# print(my_list)


# 循环遍历

# -for循环
# for value in my_list:
#     print(value)

# -while
i = 0
while i < len(my_list):
    # 通过下标获取元素
    # value = my_list[i]
    # print(value)
    print(my_list[i])
    i += 1



# 列表是可变的 改变其本身

# 定义一个列表
my_list = ["a", "b"]


# append 是添加元素是一个整体 可以是任意类型
# my_list.append(["c", "d"])
#
# print(my_list)


# extend 一般添加的是 字符串 或者是 列表
# my_list.extend([1, 2, 3])
# print(my_list)


# insert 插入数据
my_list.insert(1, "hello")
print(my_list)


# 定义一个列表
my_list = ["1", "张三", "李四", "张三"]


# 修改元素
# 把张三改成双击666
# my_list[1] = "双击666"
# print(my_list)


# 查找元素
# in
# 判断一个元素是否存在一个列表中
# if "张三" in my_list:
#     print("张三是列表的元素")

# not in
# if "张三1" not in my_list:
#     print("张三1不再列表中")

# index
# 判断一个元素在列表中的位置(下标索引)
# index = my_list.index("李四1")
# print(index)

# 先判断李四1是否存在
# if "李四" in my_list:
#     # 如果存在
#     index = my_list.index("李四")
#     print(index)
# else:
#     print("李四不存在")


# count
count = my_list.count("张三1")
print(count)


# 删除元素("删"del, pop, remove)
# 定义一个列表
my_list = ["a", "3.14", "hello"]

# del
# 格式: del 列表名[下标索引]
# IndexError: list assignment index out of range 索引越界
# del my_list[10]
# print(my_list)

# pop
# 格式: 列表名.pop(下标索引)
# my_list.pop(11)
# print(my_list)
#如果pop()里面不添加索引 默认删除列表中的最后一个
# my_list.pop()
# my_list.pop()
# print(my_list)

# remove
# 格式: 列表名.remove(元素的值)
# my_list.remove("a")
# print(my_list)

# 如果有元素的值3.14 再通过pop删除
# 判断3.14是否是my_List的元素
# if "3.14" in my_list:
#     index = my_list.index("3.14")
#     my_list.pop(index)


# if my_list.count("3.14"):
#     index = my_list.index("3.14")
#     my_list.pop(index)
#     print(my_list)


# 定义一个列表
my_list = [8, 6, 10, 1, 2]


# 排序(sort, reverse)
# sort 升序(从小到大)
# my_list.sort(reverse=False) 等价于 my_list.sort()
# my_list.sort()
#
# print(my_list)


# 降序(从大到小)
my_list.sort(reverse=True)
print(my_list)


# schoolNames = [['北京大学', '清华大学'], ['南开大学', '天津大学', '天津师范大学'], ['山东大学', '中国海洋大学']]
# # 得到天津大学
# my_list = schoolNames[1]
# # ['南开大学', '天津大学', '天津师范大学']
# # print(my_list)
# # 得到结果
# result = my_list[1]
# print(result)

# print(schoolNames[1][1])
# print(schoolNames[2][0])

# 导入模块
import random

# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
school_list = [[], [], []]
# school_list[1].append("A")
# print(school_list)

# 定义8个老师
teacher_list = list("ABCDEFGH")
# print(teacher_list)

#for循环
for name in teacher_list:
    # print(name)
    # 产生随机数(代表的是school_list的下标索引)
    index = random.randint(0, 2)
    # 给随机产生的下标索引获取的元素进行添加数据
    school_list[index].append(name)

# 查看数据
print(school_list)

"""
总结:
    - "" 或者 ''  或者 三"  -> 字符串
    - [] -> 列表
    - () -> 元组
"""
# 元组是不可变的 因为他的元素是不可以修改的(程序员不可以对元组做增删改的操作)

# 定义一个元组
# 格式: 元组名 = (元素1, 元素2, ...)
# my_tuple = (1, 3, 5)
# TypeError: 'tuple' object does not support item assignment
# 测试元组的元素数据不可以修改
# my_tuple[1] = 10

# 定义一个空的元组
# my_tuple = tuple()
#len函数中的o -> object -> 对象
# l = len(my_tuple)
# print(l)
# my_tuple = ()
# <class 'tuple'> 元组的类型
# print(type(my_tuple))

# 定义一个特殊的元组
# 当程序员定义了一个元组 有且只有一个元素 (元素,)
# my_tuple = (3.14,)
# print(type(my_tuple))

# in not index count
my_tuple = (1, 3, 5)

# in 存在
#  if 3 in my_tuple:
#     print("存在")

# not in 不存在
# if 33 not in my_tuple:
#     print("不存在")

# index
# index = my_tuple.index(33)
# print(index)

# count
# count = my_tuple.count(5)
# print(count)

# 得到元组中的3这个值
# value = my_tuple[1]
# print(value)

# 循环
# for循环
# for value in my_tuple:
#     print(value)

# 数据类型转换
# my_tuple = tuple([1, 3, 5])
# print(my_tuple)
# my_list = list(my_tuple)
# print(my_list)
# 分析: 为什么出现了元组这个类型?
# 数据安全


# 字典的的key和value是一一对应的
# 字典中的key是唯一的
# 注意注意注意: 但是这个数据类型必须是不可变的
# (不能作为key的数据类型列表 字典)
# 字典是无序的
# 定义一个字典
#
# my_dict = {"name": "小明", "age": 20, "name": "小红"}
# 查看类型
# <class 'dict'>
# print(type(my_dict))
# "name" -> 键值 key
# "小明" -> 实值 value
# "name": "小明" -> 键值对 -> key-vale -> 元素
# print(my_dict)
# my_dict1 = {1: "哈哈", 3.14: "hello"} 实际开发中用的很少 一般情况下都是使用字符串作为key


# 定义一个空的字典
# my_dict = dict()
# print(type(my_dict))
# my_dict = {}
# print(type(my_dict))

# 通过key获取value (字典是无序的-> 不支持下标索引获取数据)
# my_dict = {"name": "小明", "age": 20}
# # 打印小明
# my_name = my_dict["name"]
# print(my_name)



# 字典是可变
my_dict = {'name':'吴彦祖','age':50}

# <1>查看元素
# 通过key获取value 如果key存在得到对应的value 如果key不存在 将报错
# my_name = my_dict["name1"]
# print(my_name)

# <2>修改元素 -添加元素
# 把age的value改成70
#如果通过key修改字典中的value  如果key存在于字典中 那么直接修改对应的value
# 如果key不存在 那么系统会自动添加一个新的元素(key-value)
# my_dict["age1"] = 70
#
# print(my_dict)

# <4>删除元素
# del
# 格式: del 字典名[key]
# del my_dict["age"]
# print(my_dict)

# clear -> 清空 - 等价于 my_dict = {}
# my_dict.clear()
# print(my_dict)


# 定义一个字典
# my_dict = {"name": "小明", "age": 20, "no": "007"}

# <1>len() -> 计算字典中的键值对的个数
# l = len(my_dict)
# print(l)

# <2>keys
# keys = my_dict.keys()
# dict_keys(['name', 'age', 'no']) 就是一个列表
# 我就想把dict_keys作为列表使用 -> 解决方案 把dict_keys 转成list
# print(list(keys)[0])

# <3>values
# values = my_dict.values()
# print(values)

# <4>items
# items = my_dict.items()
# [('name', '小明'), ('age', 20), ('no', '007')]
# print(items)

my_dict = {"name": "小明", "age": 20, "no": "007"}
## 判断一个key是否存在
# 通过in 判断某个key是否存在于一个字典中
# if "小明" in my_dict:
#     print("哈哈")

# setdefault
# 通过key获取value 如果key存在 那么会返回这个value 而且dict没有任何影响
# ret = my_dict.setdefault("name", "老王")
# print(ret)
# print(my_dict)

# 通过key获取value 如果key不存在 那么会返回设置的默认值(老王) 然后把这个键值对添加到字典中
# ret = my_dict.setdefault("name1", "老王")
# print(ret)
# print(my_dict)

# get
# get 等价于setdefault (name是存在的)
# ret = my_dict.get("name", "老王")
# print(my_dict)
# print(ret)

# 通过key获取value 如果key不存在 那么会返回设置的默认值(老王) 不会对字典做任何操作
# ret = my_dict.get("name1", "老王")
# print(my_dict)
# print(ret)

# get 和setdefault区别是在于这个key不存在然后对字典是否造成影响(是否添加新的键值对)



# 字符串
# my_str = "abed"
#
# for c in my_str:
#     print(c)

# 列表
# my_list = [1, 3, 5, 7]
# for value in my_list:
#     print(value)

# 元组
# my_tuple = (2, 4, 6, 8)
#
# for value in my_tuple:
#     print(value)

# 定义一个字典
my_dict = {"name": "小明", "age": 20, "no": "007"}

# 遍历key
# for value in my_dict.keys():
#     print(value)

# 遍历value
# for value in my_dict.values():
#     print(value)

# 遍历item
# for value in my_dict.items():
#     # print(value)
#     print("key=%s value=%s" % (value[0], value[1]))

# 直接获取到字典中的key 和value
# for key, value in my_dict.items():
#     # print(value)
#     print("key=%s value=%s" % (key, value))

# 列表
my_list = ["小明", "小红", "小刚"]

# for name in my_list:
#     print(name)
#     # 获取对应的下标索引
#     index = my_list.index(name)
#     print(index)

# i = 0
# for name in my_list:
#     print(name)
#     # 获取对应的下标索引
#     print(i)
#     i += 1

# 使用enumerate 可以吧列表中的元素和元素的对应的索引都可以得到
for i, name in enumerate(my_list):
    # 通过,隔开打印对应的变量  会有一个空格
    print(i, name)



# 集合的定义
# 集合是无序的(不可以通过下标获取元素) 而且集合中的元素是不重复的 -> 列表中的元素去重

# 定义一个正常的集合
# my_set = {1, 3, 5, 7, 20}
# 查看数据类型
# <class 'set'>
# print(type(my_set))
# print(my_set)

# 定义一个空的集合
# my_set1 = set()
# 不可以 {} 是空的字典
# my_set2 = {}


# 01 作用 去重使用
# my_list = [1, 3, 5, 3, 5, 7]
# my_set = set(my_list)
# print(my_set)
# my_list = list(my_set)
# print(my_list)


my_set = {1, 3, 5}
# 02 添加元素(add，update)
# my_set.add(10)
# print(my_set)

# 和列表中的extend相似
# my_set.update("abcd")
# print(my_set)


# 删除元素(remove，pop，discard)
# remove
# 格式:集合名.remove(元素值)
# my_set.remove(33)
# print(my_set)

# pop 任意的删除一个(因为集合是无序的)
# my_set.pop()
# print(my_set)

# discard
# 格式: 集合名.discard(元素值) (不同于remove 如果元素值不存在 不会报错)
# my_set.discard(33)
# print(my_set)


# 交集和并集( & 和 | )
# 定义两个列表
my_list1 = [1, 2, 3, 4]
my_list2 = [3, 4, 5, 6]

# 两个列表中相同的元素
my_set1 = set(my_list1)
my_set2 = set(my_list2)

# 得到交集
# new_set1 = my_set1 & my_set2
# ret = list(new_set1)
# # 进行排序
# ret.sort(reverse=True)
# print(ret)


# 得到并集
new_set2 = my_set1 | my_set2
print(new_set2)




# +
list1 = [1, 2]
list2 = [3, 4]

# extend
# list1.extend(list2)
# print(list1)

# 集合
# set1 = set(list1)
# set2 = set(list2)
# ret = set1 | set2
# print(ret)

# +
# ret = list1 + list2
# print(ret)

# ret = """*
# *
# **
# ***
# ****
# *****"""

# for i in range(1, 6):
#     print("*"*i)


my_list = [(2,3),(4,5), ({"name": [1, 33, 5]}, 11)]
# print(my_list)
# my_tuple = my_list[2]
# # ({'name': [1, 33, 5]}, 11)
# print(my_tuple)
# my_dict = my_tuple[0]
# # {'name': [1, 33, 5]}
# print(my_dict)
# value = my_dict["name"]
# # [1, 33, 5]
# print(value)
# ret = value[1]
# print(ret)
# result = my_list[2][0]["name"][1]
# print(result)

# 单元划分(python)
# 变量 -> 函数 -> 类 -> 模块 -> 包 -> 项目
def my_print():
    print("                            _ooOoo_  ")
    print("                           o8888888o  ")
    print("                           88  .  88  ")
    print("                           (| -_- |)  ")
    print("                            O\\ = /O  ")
    print("                        ____/`---'\\____  ")
    print("                      .   ' \\| |// `.  ")
    print("                       / \\||| : |||// \\  ")
    print("                     / _||||| -:- |||||- \\  ")
    print("                       | | \\\\\\ - /// | |  ")
    print("                     | \\_| ''\\---/'' | |  ")
    print("                      \\ .-\\__ `-` ___/-. /  ")
    print("                   ___`. .' /--.--\\ `. . __  ")
    print("                ."" '< `.___\\_<|>_/___.' >'"".  ")
    print("               | | : `- \\`.;`\\ _ /`;.`/ - ` : | |  ")
    print("                 \\ \\ `-. \\_ __\\ /__ _/ .-` / /  ")
    print("         ======`-.____`-.___\\_____/___.-`____.-'======  ")
    print("                            `=---='  ")
    print("  ")
    print("         .............................................  ")
    print("                  佛祖镇楼                  BUG辟易1  ")
    print("          佛曰:  ")
    print("                  写字楼里写字间，写字间里程序员；  ")
    print("                  程序人员写程序，又拿程序换酒钱。  ")
    print("                  酒醒只在网上坐，酒醉还来网下眠；  ")
    print("                  酒醉酒醒日复日，网上网下年复年。  ")
    print("                  但愿老死电脑间，不愿鞠躬老板前；  ")
    print("                  奔驰宝马贵者趣，公交自行程序员。  ")
    print("                  别人笑我忒疯癫，我笑自己命太贱；  ")
    print("                  不见满街漂亮妹，哪个归得程序员？")

# 函数的调用
my_print()

#函数的作用:
# 提高开发效率 减少重复代码的出现
# 函数是对某个功能点的解释


# 定义一个变量
num = 10
# 使用num
print(num)

# 函数定义
# def -> define 定义的意思(定义函数或者方法的关键字)
# 格式: def 函数名():
#           代码实现
def my_print():
    print("你好中国")

# 函数的调用(执行)
# 格式: 函数名()
my_print()


#
# # 求和 -> 函数 (10 + 5)
# def my_func():
#     # 定义两个变量
#     a = 10
#     b = 5
#     ret = a + b
#     print("和:%d" % ret)
#
#
# # 调用函数
# my_func()
#
# # 求和 -> 函数 (11 + 6)
# def my_func1():
#     # 定义两个变量
#     a = 11
#     b = 6
#     ret = a + b
#     print("和:%d" % ret)
#
#
# # 调用函数
# my_func1()

# # 定义一个有参数的函数(有多少个形参看的是业务需求)
# # 格式: def 函数名(参数1(形参1), 参数2(形参2),...):
# #          代码逻辑
# def my_func(num1, num2):
#     ret = num1 + num2
#     print("和:%d" % ret)
#
# # 调用一个有参数的函数(有多少个实参要看函数有多少个形参)
# # 格式: 函数名(参数1(实参1), 参数2(实参2),...)
# my_func(10, 5)
# my_func(11, 6)





# 定义一个有参数的函数(有多少个形参看的是业务需求)
# 格式: def 函数名(参数1(形参1), 参数2(形参2),...):
#          代码逻辑
def my_func(num1, num2):
    ret = num1 + num2
    print("和:%d" % ret)

# 调用一个有参数的函数(有多少个实参要看函数有多少个形参)
# 格式: 函数名(参数1(实参1), 参数2(实参2),...)
a = 10
b = 5
my_func(a, b)


# 函数的定义
# def add2num(a, b):
#     ret = a + b
#     return ret
#
# # 函数的调用
# # a = 10
# # b = 11
# # add2num(a, b)
# result = add2num(10, 11)
# print(result)

# 定义一个函数 计算列表的的元素个数 -> 模拟python内置函数的len函数
# 格式: 函数名(形参1, 形参2,...):
#       代码逻辑
#       return 返回值
def hm_len(my_list):
    #定义一个变量记录循环次数
    index = 0
    # 遍历my_list
    for value in my_list:
        index += 1

    return index

# 函数调用
# 格式: 变量名 = 函数名(实参1, 实参2,...)
# ret = hm_len([1, 3, 5])
# print(ret)

# 4种函数的类型
# 无参数无返回值
# 无参数有返回值
# 有参数无返回值
# 有参数有返回值

# 无参数无返回值
# 通过函数打印你好龟叔
# 格式: def 函数名():
#           代码逻辑
# def my_print():
#     print("你好龟叔")
# # 函数的调用
# # 格式: 函数名()
# my_print()


# 无参数有返回值
# 通过调用函数得到一个3.1415926
# 格式: def 函数名():
#           return 返回值
# def get_pi():
#     return 3.1415926
#
# # 格式: 变量名 = 函数名()
# pi = get_pi()
# print(pi)


# 有参数无返回值
# 格式: def 函数名(形参1,...):
#           代码逻辑
# def my_print(s):
#     print("你好%s" % s)
#
# # 格式: 函数名(实参1,...)
# my_print("中华")


# 有参数有返回值
# 格式: def 函数名(形参1,...):
#           return 返回值
def my_print(s):
    ret = "你好" + s
    return ret

# 格式: 变量名 = 函数名(实参1,...)
result = my_print("共产党")
print(result)


# 定义三个函数
def my_func1():
    print("最后结果")


def my_func2():
    print("func2开始")
    my_func1()
    print("func2结束")


def my_func3():
    print("func3开始")
    my_func2()
    print("func3结束")

# 调用my_func3
print("开始-")
my_func3()
print("结束-")
"""
print("开始-")
print("func3开始")
print("func2开始")
print("最后结果")
print("func2结束")
print("func3结束")
print("结束-")
"""

# 特列
# def my_func111():
#
#     def my_func222():
#         print("haha")
#
#     my_func222()
#
#
# my_func111()

# 如果两个函数名相同 那么最后一个函数会覆盖前面的函数
# def aaa():
#     print("小样")
#
# def aaa():
#     print("大洋")
#
# aaa()

num = 10
num = 20




# #  打印一条横线
# def printOneLine():
#     print("-"*30)
#
# # 打印自定义行数的横线
# def printLines(count):
#     for i in range(count):
#         printOneLine()
#
# printLines(10)


# 写一个函数求三个数的和
def add3num(a, b, c):
    return a + b + c

# ret = add3num(10, 11, 12)
# print(ret)

# 写一个函数求三个数的平均值
def average3Number(num1, num2, num3):
    # ret = num1 + num2 + num3
    ret = add3num(10, 11, 12)
    return ret/3

print(average3Number(10, 11, 12))




# 定义一个函数
def add2num(a, b):
    ret = a + b
    print(ret)

# 函数调用之位置参数
# 实参的位置要和形参的位置一一对应
# 如果实参的位置发生改变 那么形参的值也就发生了改变
add2num(10, 11)


#day05
# 定义一个函数
def add2num(a, b):
    print(a)
    print(b)
    ret = a + b
    print(ret)

# 函数调用之关键字参数
# 在调用函数的时候找到函数形参的名字 直接赋值
# 关键字参数特点: 不需要考虑当前参数的位置
add2num(b=11, a=10)

"""
__project_ = '第六次课'
__file_name__ = '函数'
__author__ = 'wang'
__time__ = '2020/9/9 20:39'
__product_name = PyCharm
# code is far away from bugs with the god animal protecting
    I love animals. They taste delicious.
              ┏┓      ┏┓
            ┏┛┻━━━┛┻┓
            ┃        ┃
            ┃  ┳┛  ┗┳  ┃
            ┃      ┻      ┃
            ┗━┓      ┏━┛
                ┃      ┗━━━┓
                ┃  神兽保佑    ┣┓
                ┃　永无BUG！   ┏┛
                ┗┓┓┏━┳┓┏┛
                  ┃┫┫  ┃┫┫
                  ┗┻┛  ┗┻┛
"""

a = """
┏┓      ┏┓
            ┏┛┻━━━┛┻┓
            ┃        ┃
            ┃  ┳┛  ┗┳  ┃
            ┃      ┻      ┃
            ┗━┓      ┏━┛
                ┃      ┗━━━┓
                ┃  神兽保佑    ┣┓
                ┃　永无BUG！   ┏┛
                ┗┓┓┏━┳┓┏┛
                  ┃┫┫  ┃┫┫
                  ┗┻┛  ┗┻┛
"""

# day07
# 变量 -- 函数 -- 类 -- 模块 -- 包 -- 项目
# def my_print():
#     """
#     这是打印神兽
#     :return:
#     """
#     print(a)
#
#
# my_print()
# print(help(my_print))


# def sum_num():
#
#     print(1)
#
#
# def sum_num():
#     print(2)
#
#
# sum_num()

# 打印自定义行数的横线

# def print_line():
#     print('-' * 50)
#
#
# def print_lines(count):
#     for i in range(count):
#         print_line()
#
#
# print_lines(3)

def add_three(num1, num2, num3):
    ret = num1 + num2 + num3
    return ret


def avg_add(a, b, c):
    ret = add_three(a, b, c)
    return ret/3


print(3.14)

# a = "asdasdafsafds"
#
# b = a.rindex('a')
# print(b)
# print(a[-3:])

# # break
# a = """
# asdas
# asd
# """


# a = "CD EFG"
#
# b = a.istitle()
# print(b)

# count/replace/rfind/rindex/split/startswith/endswith/rjust/strip/join
# lower/upper//capitalize/title
# ljust//rstrip/lstrip/center/partition//rpartition
# istitle/isupper/islower/isspace/isdigit/...

# my_list = [1, 3.14, "haha"]
# print(my_list[2])

# my_list = list('abcd')
# print(my_list)
# a = 'hahaha'
# print(id(a))
# b = a.replace('a', '')
# print(id(b))
# my_list = [12, 212, 21, 321, 31, 2, 12]
# print(id(my_list))
# my_list.append(['1'])
# print(id(my_list))
# print(my_list)
# for i in my_list:
#     print(i)
# i = 0
# while i < len(my_list):
#     print(my_list[i])
#     i += 1

# my_list = ["haha", "heihei"]
# my_list.insert(1, "hehe")
# print(my_list)

# my_list = [12, 212, 21, 321, 31, 2, 12]
#
# my_list.clear()
# print(my_list)

# my_list[1] = 666
# print(my_list)
# if 12 not in my_list:
#     print("haha")
# else:
#     print("heieh")

# find


# 增：extend,append,insert
# 删：del, pop, clear, remove
# 改：下标
# 查：index，count，in，not in

# my_list = ['123', "12", '312', 'ASDF', "ZXCV", '124', '134', '35443']
#
# # 正序，倒序，逆序
# my_list.sort()
# print(my_list)
# schoolNames = [['北京大学', '清华大学'],
#                ['南开大学', ['天津大学'], '天津师范大学'],
#                ['山东大学', '中国海洋大学']]
# print(schoolNames[1][1][0])
# a = [[[1, [0]]]]
# print(a[0][0][1][0])

# 一个学校，三个办公室，8位老师，随机分配工位
# import random
# school_list = [[], [], []]
# teacher_list = list('ABCDEFGH')
# for name in teacher_list:
#     index = random.randint(0, 2)
#     school_list[index].append(name)
# print(school_list)

# 不可变的  ==== 没有增删改
# my_tuple = ("1", 3, 3, 4, 5)
# my_list = list(my_tuple)
# print(my_list)
# for i in my_tuple:
#     print(i)
#
# my_dict = {"name": "lebo", "age": 18}
#
# print(my_dict)

# encoding=utf-8

# import random
#
# # 定义一个列表用来保存3个办公室
# offices = [[], [], []]
# num1 = int(input("请输入第一个房间进几位老师:\n"))
# # 定义一个列表用来存储8位老师的名字
# names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
# while True:
#     # 利用老师随机生成下表
#     index = random.randint(0, len(names)-1)
#     # 随机删除下标老师，并赋值
#     teacher_temp = names.pop(index)
#     # 增加第一个办公室的老师
#     offices[0].append(teacher_temp)
#     if len(offices[0]) == num1:
#         break
# i = 0
# for name in names:
#     index = random.randint(1, 2)
#     offices[index].append(name)
#
# i = 1
# for tempNames in offices:
#     print('办公室%d的人数为:%d' % (i, len(tempNames)))
#     i += 1
#     for name in tempNames:
#         print("%s" % name, end='')
#     print("\n")
#     print("-" * 20)

# key
# key
# key
# del clear

my_dict = {
    "name": "lebo",
    "age": 18
}
# ret = my_dict.items()
# print(ret)
# print(my_dict)

# for key in my_dict.keys():
#     print(key)
# for values in my_dict.values():
#     print(values)
# for key, value in my_dict.items():
#     print(key, value)

# my_list = [111, 1, 1, 11, 1, 10, 1, 1]
# my_set = set(my_list)
# # my_list = list(my_set)
# # print(my_list)
# # print(my_set[0])
# # my_set.discard()
# print(my_set)

# my_list = [1, 2, 3, 4]
# my_list2 = [3, 4, 5, 6]
# my_set = set(my_list)
# my_set2 = set(my_list2)
# new_set = my_set | my_set2
# new_list = list(new_set)
# print(new_list)

# a = ["12", '123', "adsa", "xcv"]
#
# print(min(a))
# my_demo = [(1, (2, 3), [(4, 5), {"name": [6, [7], 8], "age": 10}], 9)]
# print(my_demo[0][2][1]['name'][1][0])


# print(sum(range(1, 101)))

# a = 1

# def num():
#     return [1,2,3]
#
#
# print(type(num()))
# my_list = [1,2,3,4,5]
# def add_num(b, a=4, *args, **kwargs):
#     print(b)
#     print(a)
#     print(args)
#     print(kwargs)
#
# add_num(1,2,3,5,6,7,8, n=10)

# def num_1(name, age):
#     return name,age
#
# name,age = num_1("lebo", 16)
# print(name)
# print(age)

# name, age= ["lebo", "hahah"]
#
# print(name)
# print(age)
# a = 3
# b = 4
# a = a + b # 7
# b = a - b # 3
# a = a - b # 4
# print(a)
# print(b)

# a = 4
# b = 5
# a = b
# b = a
# print(a)
# print(b)

# a, b = 4, 5
# a, b = b, a
# print(a,b)

# print(my_dict)
# print(id(my_dict))
# my_dict.clear()
# print(my_dict)
# print(id(my_dict))

# 1!
def my_func5(num):
    return 1

# 2!
def my_func4(num):
    return num * my_func5(num-1)

# 3!
def my_func3(num):
    return num * my_func4(num-1)

# n！
# def my_func(num):
#     if num > 1:
#         print("%d x " % num, end="")
#         return num * my_func(num-1)
#     else:
#         print("1")
#         return 1
#
# print(my_func(4))

# def test():
#     test()
#
# test()

# def num():
#     print("lebo")
# num

# num = lambda: print("lebo")
# num()

# def num():
#     return 1
# print(num())
# num = lambda: 1
# print(num())

# def num(num):
#     print(num)
# num(1)
# num = lambda num: print(num)
# num(1)

# def num(num):
#     return num
# print(num(1))
# num = lambda num: num
# print(num(1))

# def num():
#     return 1

# num = lambda : 1
#
# def num1(a, b):
#     return (a,b)
#
# print(num1(num(), 10))

# 列表推导式
# my_list = []
# for num in range(5, 10):
#     for num1 in range(1, 5):
#         my_tuple = 'num, num1'
#         my_list.append(my_tuple)
# print(my_list)

# print([(num,num1) for num in range(5,10) for num1 in range(1, 5)])

# 列表中[1,2,3,4,5,...100],分成[[1,2,3], [4,5,6], .....]

my_list = [i for i in range(1, 101)]
# for x in range(0, len(my_list), 3):
#     my_kong.append(my_list[x: x+3])

print([my_list[x:x+3] for x in range(0, len(my_list), 3)])


# a = "asdasdafsafds"
#
# b = a.rindex('a')
# print(b)
# print(a[-3:])

# # break
# a = """
# asdas
# asd
# """


# a = "CD EFG"
#
# b = a.istitle()
# print(b)

# count/replace/rfind/rindex/split/startswith/endswith/rjust/strip/join
# lower/upper//capitalize/title
# ljust//rstrip/lstrip/center/partition//rpartition
# istitle/isupper/islower/isspace/isdigit/...

# my_list = [1, 3.14, "haha"]
# print(my_list[2])

# my_list = list('abcd')
# print(my_list)
# a = 'hahaha'
# print(id(a))
# b = a.replace('a', '')
# print(id(b))
# my_list = [12, 212, 21, 321, 31, 2, 12]
# print(id(my_list))
# my_list.append(['1'])
# print(id(my_list))
# print(my_list)
# for i in my_list:
#     print(i)
# i = 0
# while i < len(my_list):
#     print(my_list[i])
#     i += 1

# my_list = ["haha", "heihei"]
# my_list.insert(1, "hehe")
# print(my_list)

# my_list = [12, 212, 21, 321, 31, 2, 12]
#
# my_list.clear()
# print(my_list)

# my_list[1] = 666
# print(my_list)
# if 12 not in my_list:
#     print("haha")
# else:
#     print("heieh")

# find


# 增：extend,append,insert
# 删：del, pop, clear, remove
# 改：下标
# 查：index，count，in，not in

# my_list = ['123', "12", '312', 'ASDF', "ZXCV", '124', '134', '35443']
#
# # 正序，倒序，逆序
# my_list.sort()
# print(my_list)
# schoolNames = [['北京大学', '清华大学'],
#                ['南开大学', ['天津大学'], '天津师范大学'],
#                ['山东大学', '中国海洋大学']]
# print(schoolNames[1][1][0])
# a = [[[1, [0]]]]
# print(a[0][0][1][0])

# 一个学校，三个办公室，8位老师，随机分配工位
# import random
# school_list = [[], [], []]
# teacher_list = list('ABCDEFGH')
# for name in teacher_list:
#     index = random.randint(0, 2)
#     school_list[index].append(name)
# print(school_list)

# 不可变的  ==== 没有增删改
# my_tuple = ("1", 3, 3, 4, 5)
# my_list = list(my_tuple)
# print(my_list)
# for i in my_tuple:
#     print(i)
#
# my_dict = {"name": "lebo", "age": 18}
#
# print(my_dict)

# encoding=utf-8

# import random
#
# # 定义一个列表用来保存3个办公室
# offices = [[], [], []]
# num1 = int(input("请输入第一个房间进几位老师:\n"))
# # 定义一个列表用来存储8位老师的名字
# names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
# while True:
#     # 利用老师随机生成下表
#     index = random.randint(0, len(names)-1)
#     # 随机删除下标老师，并赋值
#     teacher_temp = names.pop(index)
#     # 增加第一个办公室的老师
#     offices[0].append(teacher_temp)
#     if len(offices[0]) == num1:
#         break
# i = 0
# for name in names:
#     index = random.randint(1, 2)
#     offices[index].append(name)
#
# i = 1
# for tempNames in offices:
#     print('办公室%d的人数为:%d' % (i, len(tempNames)))
#     i += 1
#     for name in tempNames:
#         print("%s" % name, end='')
#     print("\n")
#     print("-" * 20)

# key
# key
# key
# del clear

my_dict = {
    "name": "lebo",
    "age": 18
}
# ret = my_dict.items()
# print(ret)
# print(my_dict)

# for key in my_dict.keys():
#     print(key)
# for values in my_dict.values():
#     print(values)
# for key, value in my_dict.items():
#     print(key, value)

# my_list = [111, 1, 1, 11, 1, 10, 1, 1]
# my_set = set(my_list)
# # my_list = list(my_set)
# # print(my_list)
# # print(my_set[0])
# # my_set.discard()
# print(my_set)

# my_list = [1, 2, 3, 4]
# my_list2 = [3, 4, 5, 6]
# my_set = set(my_list)
# my_set2 = set(my_list2)
# new_set = my_set | my_set2
# new_list = list(new_set)
# print(new_list)

# a = ["12", '123', "adsa", "xcv"]
#
# print(min(a))
# my_demo = [(1, (2, 3), [(4, 5), {"name": [6, [7], 8], "age": 10}], 9)]
# print(my_demo[0][2][1]['name'][1][0])


# print(sum(range(1, 101)))

# a = 1

# def num():
#     return [1,2,3]
#
#
# print(type(num()))
# my_list = [1,2,3,4,5]
# def add_num(b, a=4, *args, **kwargs):
#     print(b)
#     print(a)
#     print(args)
#     print(kwargs)
#
# add_num(1,2,3,5,6,7,8, n=10)

# def num_1(name, age):
#     return name,age
#
# name,age = num_1("lebo", 16)
# print(name)
# print(age)

# name, age= ["lebo", "hahah"]
#
# print(name)
# print(age)
# a = 3
# b = 4
# a = a + b # 7
# b = a - b # 3
# a = a - b # 4
# print(a)
# print(b)

# a = 4
# b = 5
# a = b
# b = a
# print(a)
# print(b)

# a, b = 4, 5
# a, b = b, a
# print(a,b)

# print(my_dict)
# print(id(my_dict))
# my_dict.clear()
# print(my_dict)
# print(id(my_dict))

# 1!
def my_func5(num):
    return 1

# 2!
def my_func4(num):
    return num * my_func5(num-1)

# 3!
def my_func3(num):
    return num * my_func4(num-1)

# n！
# def my_func(num):
#     if num > 1:
#         print("%d x " % num, end="")
#         return num * my_func(num-1)
#     else:
#         print("1")
#         return 1
#
# print(my_func(4))

# def test():
#     test()
#
# test()

# def num():
#     print("lebo")
# num

# num = lambda: print("lebo")
# num()

# def num():
#     return 1
# print(num())
# num = lambda: 1
# print(num())

# def num(num):
#     print(num)
# num(1)
# num = lambda num: print(num)
# num(1)

# def num(num):
#     return num
# print(num(1))
# num = lambda num: num
# print(num(1))

# def num():
#     return 1

# num = lambda : 1
#
# def num1(a, b):
#     return (a,b)
#
# print(num1(num(), 10))

# 列表推导式
# my_list = []
# for num in range(5, 10):
#     for num1 in range(1, 5):
#         my_tuple = 'num, num1'
#         my_list.append(my_tuple)
# print(my_list)

# print([(num,num1) for num in range(5,10) for num1 in range(1, 5)])

# 列表中[1,2,3,4,5,...100],分成[[1,2,3], [4,5,6], .....]

my_list = [i for i in range(1, 101)]
# for x in range(0, len(my_list), 3):
#     my_kong.append(my_list[x: x+3])

print([my_list[x:x+3] for x in range(0, len(my_list), 3)])


# 打开一个文件
# 所谓的open 打开一个文件 和面的w 或者 r 或者其他模式 只是给执行者一个权限
# w 是写的意思
# open("lebo.txt", "w")
# r 是读的意思
# open("lebo.txt", "r")


# 读文件
# 如果使用r模式打开一个文件
# 如果文件存在直接打开
# 如果文件不存在 将报错
# FileNotFoundError: [Errno 2] No such file or directory: 'hm.txt'
# open("lebo.txt", "r")


# 写文件
# 如果使用w模式打开一个文件
# 如果文件存在 直接打开
# 如果文件不存在 先创建一个文件 然后打开
# 当文件使用完成后 记得关闭文件（释放内存）
f = open("lebo.txt", "w")
# 关闭文件
f.close()

# <1>写数据(write)
# 打开一个文件
# f = open("lebo.txt", "w")
# 写入数据
# f.write("hello python")
# 关闭文件
# f.close()

# <2>读数据(read)
# # 打开一个文件
# f = open("lebo.txt", "r")
# # 读取数据
# ret = f.read()
# print(ret)
# # 关闭文件
# f.close()


# <3>读数据（readlines）
# 读取文件中的所有行的内容 使用列表进行保存
# # 打开文件
# f = open("lebo.txt", "r")
# # ret 是一个列表
# ret = f.readlines()
# print(ret)
# f.close()

# <4>读数据（readline）
# 打开文件
# f = open("lebo.txt", "r")
# ret = f.readline()
# print(ret)
# ret1 = f.readline()
# print(ret1)
# f.close()

# 写入数据
#如果使用w模式写入数据
# 打开文件 然后把文件中的内容清空 然后再写入
# f = open("lebo.txt", "w", encoding="utf-8")
# 在mac下或者linux 不需要考虑编码格式
# # 在windows系统下如果写入中文 需要执行编码格式 utf-8
# # 默认windows下为gbk编码格式 (中国的电脑)
# f.write("你好世界")
# f.close()

# 读取数据
# f = open("lebo.txt", "r", encoding="utf-8")
# print(f.read())
# f.close()


"""
# 美国人发明了电脑
# 26英文字母 -> ascii码 字符集 0-127 (300多个符号)
# 引入其他国家 gbk (6000 + 14000) -> 简体
# 繁体(中国台湾) -> BIG5
# 美国中97-> a
# 中国人 97 ->哈
# ISO(国际标准化组织)
# unicode 万国码 2个字节(8位)
# utf-8 互通的编码格式
"""

# a -> append
# f = open("lebo.txt", "a", encoding="utf-8")
# f.write("哈哈")
# f.close()


# 了解****
# f = open("lebo1.txt", "wb")
# # 写入二进制数据 编码
# f.write("哈哈".encode(encoding="utf-8"))
# f.close()

# f = open("lebo1.txt", "rb")
# 写入二进制数据 解码
# print(f.read().decode(encoding="utf-8"))
# f.close()


# f = open("lebo.txt", "r", encoding="utf-8")
# # 文本格式下 一个字符就是一个字节
# print(f.read(1))
# f.close()


# f = open("lebo.txt", "rb")
# 写入二进制数据 解码
# 在二进制文件下(utf-8) 一个汉字占三个字节
# print(f.read(3).decode(encoding="utf-8"))
# f.close()

# 制作文件的备份：

# # 准备一个源文件
# old_f = open("lebo.txt", "w")
# # 写入数据
# old_f.write("nihao\nnihao\nhello\n")
# # 关闭文件
# old_f.close()

# 准备一个名字 新文件
new_file_name = "lebo[复件].txt"

# 01 打开源文件读取数据
old_f = open("lebo.txt", "r")
# 去读行数
ret = old_f.readlines()
# 02 创建一个新文件吧数据写入
new_f = open(new_file_name, "w")
# 遍历列表的数据
for value in ret:
    new_f.write(value)

# 03 分别关闭两个文件
old_f.close()
new_f.close()

import os

# 1. 文件重命名
#
# os模块中的rename()可以完成对文件的重命名操作
#
# rename(需要修改的文件名, 新的文件名)
# os.rename("lebo.txt", "lebo1.txt")

# 2. 删除文件
#
# os模块中的remove()可以完成对文件的删除操作
#
# remove(待删除的文件名)
# os.remove("lebo.txt")

# 3. 创建文件夹
# os.mkdir("文件夹")

# 4. 获取当前目录(在执行os.getcwd时候 看执行这个代码的模块属于哪个路径)
# print(os.getcwd())

# 5. 改变默认目录
# 目前的路径
# print(os.getcwd())
# # ../ 或者./ 都是属于相对路径(看到一个. 或者两个.)
# # Z:\Desktop\第九天的代码 绝对路径(可以看到盘符)
# os.chdir("../")
# print(os.getcwd())

# 6. 获取目录列表
# .idea 如果一个文件是以.开头 就代表是一个隐藏文件
# 在我们创建一个工程时候 默认会有这个.idea名字的隐藏文件
# my_list = os.listdir()
# print(my_list)


# 7. 删除文件夹
# ./文件夹 或者 文件夹 一样的
# os.rmdir("./文件夹")


















