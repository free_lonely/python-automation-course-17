"""
__project__ = 'PythonProject'
__file_name__ = 'class_06'
__author__ = 'Sun'
__time__ =  '2021/4/1 9:02'
__product_name = PyCharm

"""
# class_06

# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
import random
# 定义一个空的列表，存放3个办公室
offices = [[], [], []]
teachnames = (input("请输入8位老师的名字："))
# 将8位老师放在一个列表中
teach_list = list(teachnames)
# 打印这个列表
print("teach_list =" % teach_list)
# 随机3个办公室，循环遍历每一位老师，将老师加入到办公室中
i = 0
for teachname in teachnames:
    office_index = random.randint(0, 2)
    offices[office_index].append(teachname)
i = 1
for tempname in offices:
    print("办公室%d的人数为：%d,分别是" % (i, len(tempname)))
    i += 1
    for teachname in tempname:
        print("%s" % teachname, end="")
    print("\n")

