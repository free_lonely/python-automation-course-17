"""
__project__ = 'PythonProject'
__file_name__ = 'class_13'
__author__ = 'Sun'
__time__ = '2021/4/26 15:01'
__product_name = PyCharm

"""

"""
1、下列程序结果是______.
class A:

    def __init__(self):
        print("初始化")

    def __del__(self):
        print("销毁")

    def __str__(self):
        return "类A"
a = A()
print(a)
# 初始化
# 类A
# 被销毁

2. 下列程序执行结果______.
class Animal(object):

    def shut(self):
        print("动物在叫")
class Dog(Animal):

    def shut(self):
        super().shut()
        print("汪汪汪")
dog = Dog()
dog.shut()

# 动物在叫
# 汪汪汪

3. 创建猫类：
	类名：Cat
	属性：无
描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
class Cat(object):
    def __str__(self):
        return '喵？喵？喵？'


cat = Cat()
print(cat)



4.创建计算器类：
	类名：Calculator
	属性：number_1（数字一）、number_2（数字二） 
方法：
	def __init__(self,number_1,number_2): 
# 类的初始化方法
	def add(self) # 返回数字一加数字二的值 
	def sub(self) # 返回数字一减去数字二的值
	def div(self) # 返回数字一除以数字二的值
	def mul(self) # 返回数字一乘以数字二的值
描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.

class Calculator(object):
    def __init__(self, number_1, number_2):
        self.number_1 = number_1
        self.number_2 = number_2

    def add(self):
        return self.number_1 + self.number_2

    def sub(self):
        return self.number_1 - self.number_2

    def div(self):
        return self.number_1 / self.number_2

    def mul(self):
        return self.number_1 * self.number_2

calc = Calculator(100, 25)
print(calc.add())
print(calc.sub())
print(calc.div())
print(calc.mul())


5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
class Animal:

    def shut(self):
		# 打印叫声
        pass

    def eat(self):
		# 打印爱吃的食物
        pass



1、	面向对象三大特性是___封装___、_继承_____、_多态_____.
2、	单例模式创建保证实例只创建__1____次.

"""
# class A:
#
#     def __init__(self):
#         print("初始化")
#
#     def __del__(self):
#         print("销毁")
#
#     def __str__(self):
#         return "类A"
# a = A()
# print(a)
#
#
# class Animal(object):
#
#     def shut(self):
#         print("动物在叫")
# class Dog(Animal):
#
#     def shut(self):
#         super().shut()
#         print("汪汪汪")
# dog = Dog()
# dog.shut()
#
#
#
# # 3. 创建猫类：
# # 	类名：Cat
# # 	属性：无
# # 描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
# class Cat(object):
#     def __init__(self):
#         print("喵？喵？喵？")
# cat = Cat()
# print(cat)
#
#
# class Cat(object):
#     def __str__(self):
#         return '喵？喵？喵？'
#
# cat = Cat()
# print(cat)


# 4.创建计算器类：
# 	类名：Calculator
# 	属性：number_1（数字一）、number_2（数字二）
# 方法：
# 	def __init__(self,number_1,number_2):
# # 类的初始化方法
# 	def add(self) # 返回数字一加数字二的值
# 	def sub(self) # 返回数字一减去数字二的值
# 	def div(self) # 返回数字一除以数字二的值
# 	def mul(self) # 返回数字一乘以数字二的值
# 描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.
# class Calculator(object):
#     def __init__(self, number_1, number_2):
#         self.number_1 = number_1
#         self.number_2 = number_2
#
#     def add(self):
#         return self.number_1 + self.number_2
#
#     def sub(self):
#         return self.number_1 - self.number_2
#
#     def div(self):
#         return self.number_1 / self.number_2
#
#     def mul(self):
#         return self.number_1 * self.number_2
#
# calc = Calculator(100, 25)
# print(calc.add())
# print(calc.sub())
# print(calc.div())
# print(calc.mul())

# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
class Animal(object):
    def __init__(self, name, sound, food):
        self.name = name
        self.sound = sound
        self.food = food

    def shut(self):
		# 打印叫声
        print("%s叫声%s"% (self.name, self.sound))

    def eat(self):
		# 打印爱吃的食物
        print("%爱吃的食物%s" % (self.name, self.food))

class Cat(Animal):
    def shut(self):
        # 打印叫声
        print("%s叫声%s" % (self.name, self.sound))

    def eat(self):
        # 打印爱吃的食物
        print("%s爱吃的食物%s" % (self.name, self.food))

class Dog(Animal):
    def shut(self):
        # 打印叫声
        print("%s叫声%s" % (self.name, self.sound))

    def eat(self):
        # 打印爱吃的食物
        print("%s爱吃的食物%s" % (self.name, self.food))
cat = Cat("小花猫", "喵喵喵", "鱼")
cat.shut()
cat.eat()
dog = Dog("小狗", "汪汪汪", "骨头")
dog.shut()
dog.eat()
