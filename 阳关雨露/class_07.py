"""
__project__ = 'PythonProject'
__file_name__ = 'class_07'
__author__ = 'Sun'
__time__ = '2021/4/1 19:54'
__product_name = PyCharm

"""
# class_07
"""
1. 用任意编程语言输出0-10
for i in range(0, 11):
    print(i)
    
2.用任意编程语言实现逆序输出一个字符串，比如[欢乐逛欢迎您],输出[您迎欢逛乐欢]
my_str = "[欢乐逛欢迎您]"
my_str2 = my_str[::-1]
print(my_str2)

3.str = "a,hello",在字符串str中查找hello
str = "a,hello"
print(str.find("hello"))
print(str.index("hello"))

4.str = 'a,b,c,d'用逗号分割str字符串，并保存到列表
str2 = 'a,b,c,d'
myList = list(str2.split(","))
print(myList)

5. ‘笔试题123A'将中123替换为'进行中'
str3 = "笔试题123A"
str4 = str3.replace("123A", "进行中")
print(str4)

"""

"""
1.	下列将字符串"100"转换为数字100的正确的是(A)                                     
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

2.	下列程序执行结果是(A)                                                 
numbers = [1，5，3，9，7]              
numbers.sort(reverse=True)                                                                              
print(numbers)                                                                                      
A、[9，7，5，3，1] 
B、[1，3，5，7，9]                                                                                      
C、1，3，5，7，9                                                                                        
D、9，7，5，3，1

3.	如何在列表中添加一个元素
# 定义一个列表
my_list = [1, 3, 5, 90, 78, 56]
# 通过append可以向列表添加元素是整个一股脑的添加
my_list.append(88)
print(my_list)
# 通过extend是将另一个集合中的元素逐一添加到列表中一个一个的添加
my_list1 = [55, 87, 33]
my_list.extend(my_list1)
print(my_list)
# insert(index, object) 在指定的index位置插入元素
my_list.insert(1, 22)
print(my_list)

4.	对于列表什么是越界
下标超出列表的索引值即为越界

5.	说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
可变数据类型：字典dict 列表list 
不可变数据类型：int str float double tuple

6.	从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
# 定义一个空的列表，将5位学生添加到列表中
stu_list = []
for i in range(5):
    stu_name = input("请输入第%d学生名字:"%(i+1))
    stu_list.append(stu_name)
print(stu_list)
#  使用循环，利用切片输出每个学生姓名的第二个字母
for i in range(len(stu_list)):
    stu_name = stu_list[i]
    print("第%d个学生的姓名是：%s,第二个字母是：%s"%(i+1, stu_name, stu_name[1]))
7.	随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
# 导入随机数模块
import random
# 定义一个空的列表并随机生成5个数，并最后添加到列表中
num_list = []
for i in range(5):
    num = random.randint(0, 100)
    num_list.append(num)
print(num_list)
# 定义一个空的列表，循环输出并得到相反数，最后输出到列表中
num_list1 = []
for i in range(len(num_list)):
    num = -num_list[i]
    num_list1.append(num)
print(num_list1)

8.	将下列两个列表合并，将合并后的列表升序并输出.
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
list1 = [1, 3, 4, 5, 7]
list2 = [0, 66, 8, 9]
list1.extend(list2)
list1.sort(reverse=False)
print(list1)

9.	使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
# 定义一个空的字典
my_dict = {}
name = input("请输入您的姓名：")
age = int(input("请输入您的年龄："))
stu_no = input("请输入您的学号：")
my_dict['姓名'] = name
my_dict['年龄'] = age
my_dict['学号'] = stu_no
print(my_dict)

10.	有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}

dict1 = {"school": "lebo", "date": 2018, "address": "beijing"}
for i, j in dict1.items():
    if j == 'lebo':
        print(i)
11.	使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
um = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
num1 = num[::-1]
print(num1)
num2 = num1[::2]
print(num2)
sum = 0
for num in num2:
    sum += num
print(sum)
"""

