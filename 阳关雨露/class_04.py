"""
__project__ = 'PythonProject'
__file_name__ = 'class_04'
__author__ =  'Sun'
__time__ =  '2021/3/29 9:23'
__product_name = PyCharm

"""
# class_04
# i = 1  # i控制行数
# while i <= 5:
#     j = 1  # j控制列数
#     while j <= i:
#         print('* ', end='')
#         while j > i:
#             print('* ', end='')
#             j -= 1
#         j += 1
#     print()
#     i += 1
#
# #输出图形下半部分
#
# m = 1 # m控制行数
# while m <= 4:
#     n = 4  # n控制列数
#     while n >= m:
#         print('* ', end='')
#         n -= 1
#     print()
#     m += 1


"""
1、使用while，完成以下图形的输出
*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*
"""
"""
打印上半部分
*
* *
* * *
* * * *
* * * * *
"""
# i控制行数，j控制列数
i = 1
while i <= 5:
    j = 1
    while j <= i:
        print("*",end = "")
        j += 1
    print()
    i += 1

"""
打印下半部分
* * * *
* * *
* *
*
"""
x = 0
while x <= 4:
    y = 1
    while y <= 4 - x:
        print("*",end = "")
        y += 1
    print()
    x += 1


