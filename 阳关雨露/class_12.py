"""
__project__ = 'PythonProject'
__file_name__ = 'class_12'
__author__ = 'Sun'
__time__ = '2021/4/13 19:15'
__product_name = PyCharm

"""

# schoolNames = [['北京大学', '清华大学'],
#                 ['南开大学', ['天津大学'], '天津师范大学'],
#                 ['山东大学', '中国海洋大学']]
# print(schoolNames[1][1][0])

"""
1、创建一个对象后默认调用(A)
A、__init__    B、__str__    C、__add__    D、__and__
2、类是对象的_抽象______、对象是类的__实例具体表现形式_____.
3、对象是由_____属性__、_方法______两部分构成.
4、创建学生类：
	类名：Student
	属性：name（姓名）、age（年龄）、sex（性别） 
方法：
	def info(self) # 打印学生的姓名、年龄、性别
	def draw(self) #打印”XX会画画呢”
描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法和draw方法.
"""
class Student(object):

    def info(self):
        print("学生的姓名：%s,学生的年龄:%d,学生的性别:%s" % (self.name, self.age, self.sex))

    def draw(self):
        print("%s会画画呢" % self.name)
        print("-"*100)
xiaoming = Student()
xiaoming.name = "小明"
xiaoming.age = 18
xiaoming.sex = "男"
xiaoming.info()
xiaoming.draw()


"""
4、创建动物类：
	类名：animal
	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色） 
方法：
	def info(self) # 打印姓名、年龄、毛颜色
	def run（self）#打印“XX会跑呢”
描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.

"""
class Animal(object):
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color
    def info(self):
        print("动物：%s,年龄:%d,颜色:%s" % (self.name, self.age, self.color))

    def run(self):
        print("%s会跑呢呀！" % self.name)
dog = Animal("旺财", 5, "金色")
dog.info()
dog.run()

