"""
__project__ = 'PythonProject'
__file_name__ = 'test3'
__author__ = 'Sun'
__time__ = '2021/4/13 16:08'
__product_name = PyCharm

"""


# 有参数的__init__()方法

# class Dog(object):
#
#     def __init__(self):
#         # 设置属性
#         self.name = "旺财"
#         self.color = "白色"
#
#     def info(self):
#         print("名字:%s" % self.name)
#         print("毛色:%s" % self.color)
#
#
#
# # 创建一个对象
# wangcai1 = Dog()
# wangcai1.info()

#
class Dog(object):

    def __init__(self, new_name, new_color, new_age=5):
        # 设置属性
        self.name = new_name
        self.color = new_color
        self.age = new_age

    def info(self):
        print("名字:%s" % self.name)
        print("毛色:%s" % self.color)
        print("年龄:%d" % self.age)


wangcai = Dog("旺财", "白色")
wangcai.info()

#
zangao = Dog("藏獒", "黑色", 6)
zangao.info()



class Dog(object):

    def __init__(self, new_name, new_color, new_age=5):
        # 设置属性
        self.name = new_name
        self.color = new_color
        self.age = new_age

    # 在实际开发中不会定义一个方法 来追踪对象属性信息变化
    # def info(self):
    #     print("名字:%s" % self.name)
    #     print("毛色:%s" % self.color)
    #     print("年龄:%d" % self.age)

    # 作用: 是追踪对象属性信息变量 一般用于程序员调试代码
    # 特点: 有且只有一个形参 那就是self
    # 他必须有返回值 返回值类型为字符串
    # 什么时候被python调用: 在当前监听到程序员打印这个类创建的对象的时候
    def __str__(self):
        return "名字:%s 毛色:%s 年龄:%d" % (self.name, self.color, self.age)


# 旺财
wangcai = Dog("旺财", "白色")
# <__main__.Dog object at 0x000000000218EB38>
# 在我们没有实现__str__方法的时候 打印对象 会输出十六进制地址
# 如果我们实现了__str__方法 打印对象 会输出str方法中的字符串
print(wangcai)
# wangcai.info()

# 藏獒
zangao = Dog("藏獒", "黑色", 8)
# zangao.info()
print(zangao)



# # 自定义一个英雄类
# class Hero(object):
#
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     def __str__(self):
#         return "名字:%s 年龄:%d" % (self.name, self.age)
#
#     # 监听对象销毁的
#     def __del__(self):
#         print("再见!!!")
#
# huangzhong = Hero("黄忠", 500)
# print(huangzhong)
# # 当程序的代码执行完成后 将要结束 那么会执行del huanghzong 杀死对象 系统释放内存



#（单继承）

# 故事情节：煎饼果子老师傅在煎饼果子界摸爬滚打几十年，拥有一身精湛的煎饼果子技术，
# 并总结了一套"古法煎饼果子配方"。

class Master(object):

    def __init__(self):
        # 属性
        self.kongfu = "古法配发"

    # 会制作煎饼果子
    def make_cake(self):
        print("会制作古法煎饼果子")


# 自定义一个李师傅
lishifu = Master()
print(lishifu.kongfu)
lishifu.make_cake()

print("-" * 30)


# 在python类中 子类继承了父类 子类就拥有了父类的属性和方法
# 格式: class 子类名(父类名):
# 单继承 就是只有一个父类
class Prentice(Master):
    pass


damao = Prentice()
print(damao.kongfu)
damao.make_cake()





# 李师傅类
class Master(object):

    def __init__(self):
        print(self)
        self.kongfu = "古法配方"

    def make_cake(self):
        print("古法煎饼果子")

    # 大烟袋
    def dayandai(self):
        print("大烟袋")


# 新东方
class School(object):

    def __init__(self):
        self.kongfu = "现代配方"

    def make_cake(self):
        print("现代煎饼果子")

    def xiaoyandai(self):
        print("小烟袋")


# 大猫 继承了李师傅 继承新东方
# 多继承格式: class 子类名(父类1, 父类2, ...):
class Prentice(Master, School):
    pass


# 创建一个大猫
damao = Prentice()
print(damao)
print(damao.kongfu)
damao.make_cake()
damao.dayandai()
damao.xiaoyandai()
"""
# 如果子类继承了多个父类 如果父类的方法名相同 那么子类会继承第一个父类方法
# 如果子类继承了多个父类 如果父类的方法名不同 那么子类会全部继承方法

# 在使用类创建一个对象的时候就会执行init方法
# 如果子类实现了init方法 会走自己子类中的init方法
# 如果子类没有实现init方法 会找继承父类的init方法

# 为什么子类继承了父类 子类就拥有了父类的属性?
# 前提是继承了父类的init方法 才使用父类的init 给自己设置属性
# 先继承方法的 然后继承了父类的属性
"""


# 古法师傅类
class Master(object):

    def __init__(self):
        self.kongfu = "古法配方"

    def make_cake(self):
        print("古法煎饼果子")


# 现代师傅类
class School(object):
    def __init__(self):
        self.kongfu = "现代配方"

    def make_cake(self):
        print("现代煎饼果子")


# 徒弟类多继承
# 大猫 猫氏配方
# 会做 猫氏煎饼果子
class Prentice(Master, School):

    # 当前类中的无论init方法 还是 make_cake 方法都叫做重写
    # 重写: 子类继承了父类 在子类中实现了父类已有的方法或者属性 就叫做重写
    # 子类继承父类 重写父类的方法 做自己特有的事情
    # 特有事情: init 设置kongfu的属性值为猫氏配方
    # make_cake 打印 猫氏煎饼果子
    def __init__(self):
        self.kongfu = "猫氏配方"

    def make_cake(self):
        print("猫氏煎饼果子")

    # 自定义方法
    def hello_python(self):
        print("你好python")


damao = Prentice()
print(damao.kongfu)
damao.make_cake()
damao.hello_python()
"""
子类继承了父类 
子类重写了父类的同名的属性和方法
那么子类会使用自己的属性 和 方法
"""
#-----------------------------------------------------------


# 古法师傅类
class Master(object):

    def __init__(self):
        self.kongfu = "古法配方"

    def make_cake(self):
        print("古法煎饼果子")


# 现代师傅类
class School(object):
    def __init__(self):
        self.kongfu = "现代配方"

    def make_cake(self):
        print("现代煎饼果子")


# 徒弟类多继承
# 大猫 猫氏配方
# 会做 猫氏煎饼果子
class Prentice(Master, School):

    def __init__(self):
        self.kongfu = "猫氏配方"
        School.__init__(self)

    # def set_kongfu(self):
    #     self.kongfu = "古法配方"

    # 猫氏煎饼果子
    def make_cake(self):
        print("猫氏煎饼果子")

    # 古法
    def make_old_cake(self):
        print("制作古法煎饼果子-大猫")
        # 格式: 父类名.同名方法名(self)
        Master.make_cake(self)

    # 现代
    def make_new_cake(self):
        print("制作现代煎饼果子-大猫")
        School.make_cake(self)


damao = Prentice()
print(damao.kongfu)
damao.make_cake()
damao.make_old_cake()
damao.make_new_cake()



# 古法师傅类
class Master(object):

    def __init__(self):
        self.kongfu = "古法配方"

    def make_cake(self):
        print("古法煎饼果子")


# 现代师傅类
class School(object):
    def __init__(self):
        self.kongfu = "现代配方"

    def make_cake(self):
        print("现代煎饼果子")


# 徒弟类多继承
# 大猫 猫氏配方
# 会做 猫氏煎饼果子
class Prentice(Master, School):

    def __init__(self):
        self.kongfu = "猫氏配方"

    # 猫氏煎饼果子
    def make_cake(self):
        print("猫氏煎饼果子")

    # 古法
    def make_old_cake(self):
        # 子类调用父类的同名方法 一共有三种方法:

        # 01:
        # 格式: 父类名.同名方法名(self)
        # Master.make_cake(self)

        # 02:
        # 格式: super(子类名, self).同名方法名()
        # super(Prentice, self).make_cake()

        # 03:
        # 格式: super().同名方法名()
        # 03格式是02格式的简写

        # 02 和 03 只适用于新式类
        super().make_cake()

    # 现代
    def make_new_cake(self):
        # 01:
        # School.make_cake(self)
        # 02:
        # super(Prentice, self).make_cake()
        # 03:
        super().make_cake()


damao = Prentice()
# print(damao.kongfu)
# damao.make_cake()
damao.make_old_cake()
damao.make_new_cake()

"""
如果子类继承了父类 子类重写了父类的同名方法 而且子类想调用父类的这个同名方法一共有三种方式
# 01 父类名.父类方法名(self)
# 02 super(子类名, self).父类方法名()
# 03 super().父类方法名()

# 02 和 03 只适用于新式类
# 02 和 03 一般用于单继承  也可以是多继承 但是只能调用第一个父类的方法
# 01 适用于单继承或者多继承都可以
"""


# # 古法
# class Master(object):
#
#     def __init__(self):
#         # 配方
#         self.kongfu = "古法配方"
#         # 钱
#         # 如果一个属性是以两个下划线开始 就标识这个这个属性是一个私有属性
#         self.__money = 1000000
#
#
#     def make_cake(self):
#         print("古法煎饼果子")
#
#     # 如果一个方法是以两个下划线开始 也代表已经私有
#     def __hello_python(self):
#         print("你好python")
#
# # 徒弟类
# class Prentice(Master):
#     pass
#
#
# damao = Prentice()
# print(damao.kongfu)
# # 子类继承了父类 如果父类的属性私有 将不会被子类继承
# # print(damao.money)
# damao.make_cake()
# # 子类继承了父类 如果父类的属性私有 将不会被子类继承
# damao.hello_python()


# 古法
class Master(object):

    def __init__(self):
        # 配方
        self.kongfu = "古法配方"
        # 钱
        # 如果一个属性是以两个下划线开始 就标
        self.__money = 10000

    def make_cake(self):
        print("古法煎饼果子")
        # 私有属性和私有方法可以在类的里面使用
        print(self.__money)
        self.__hello_python()

    # 如果一个方法是以两个下划线开始 也代表已经私
    def __hello_python(self):
        print("你好python")


lishifu = Master()
# print(lishifu.kongfu)
# 自定义类中 如果一个属性进行了私有 在类的外面不能调用
# print(lishifu.money)
# 自定义类中 如果一个方法进行了私有 在类的外面不能调用
# lishifu.hello_python()


# 测试
lishifu.make_cake()



class Person(object):

    def __init__(self):
        self.name = "小明"
        self.__age = 20

    # 获取私有属性的值
    def get_age(self):
        return self.__age

    # 设置私有属性的值
    def set_age(self, new_age):
        self.__age = new_age


# 定义一个对象
p = Person()
# 强行获取私有属性
# 崇尚一切靠自觉
print(p._Person__age)
print(p.name)
# 想在类的外面获取对象的属性
# ret = p.get_age()
# print(ret)

# 想在类的外面修改对象私有属性的值
p.set_age(30)
print(p.get_age())


class Person(object):

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def eat(self):
        print("吃饭")


# 小明
xm = Person("小明", 22)
# print(id(xm.name))
print(id(xm.eat()))
xh = Person("小红", 33)
# print(id(xh.name))
print(id(xh.eat()))
print(id(Person))
# 通过地址可以看到  通过相同的类创建的对象不同

# 小明和小红的name id不同
# 因为需要保存各自的属性值
# 不同的对象调用相同的方法 方法id相同
# 因为实例方法中有一个self 可以知道是哪个对象调用的 没有必要开辟内存

# # 自定义一个类
# class Person(object):
#
#     # 类属性
#     # 类属性定义在类的里面 而且在方法的外面 就称之为类属性
#     country = "中国"
#
#     def __init__(self):
#         # 实例属性 对象属性
#         self.name = "小明"

# 类属性
# 取值
# 01: 类名.类属性名
# print(Person.country)
# 02: 对象名.类属性名
# p = Person()
# print(p.country)

# 赋值 只有一种方式
# 01: 类名.类属性名 = 值
# Person.country = "荷兰"
# print(Person.country)
# 02: 对象名.类属性名 其实是 对象名.实例属性名(这种方式不存在)
# p = Person()
# 是给p这个对象添加了一个实例属性(只是和类属性名相同而已)
# p.country = "河南"
# print(Person.country)
# 打印实例属性
# print(p.country)
# 但是我们就想使用p.country 打印 中国
# 删除实例属性
# del p.country
# print(p.country)


# 不同的对象调用这个类属性 这个类属性的值相同me id相同me
# id相同啊 值相同
# 类属性值有一个 只会开辟一份内存
# 他是类的属性 类的特征
# p1 = Person()
# print(id(p1.country))
#
# p2 = Person()
# print(id(p2.country))
#
# p3 = Person()
# print(id(p3.country))
#
# print(id(Person.country))


# 实例属性(取值和赋值)
# 取值
# p = Person()
# # print(p.name)
# # 赋值
# p.name = "小小明"
# print(p.name)


# 类属性的作用
# 自定义一个类
# 对象属性
# class Person(object):
#
#     def __init__(self, name, country="中国"):
#         # 实例属性 对象属性
#         self.name = name
#         self.country = country
#
#
# p1 = Person("小明")

class Person(object):
    country = "中国"

    def __init__(self, name):
        # 实例属性 对象属性
        self.name = name

# p1 = Person("小明")
# print(p1.country)


# def hehe(a=None, B, **kwargs):
#     pass

# a = {1, 2, 3, 4}
# # b = (1, 23)
# # c = a + b
# print(id(a))
#
# a.pop()
# print(a)
# print(id(a))

# a = 5
# b = 10
# a = a + b # 15
# b = a + b # 25
# print(a)
# print(b)
# a = 1
# b = 2
#
# a, b = b, a
#
# print(a)
# print(b)

# a = ("lebo", "18")
# # print(a[0])
# # print(a[1])
# name, age = a
# print(name)
# print(age)

# def get_my_info():
#     high = 178
#     weight = 100
#     age = 18
#     return high, weight, age

# result = get_my_info()
# print(result[0])

# my_high, my_weight, my_age = get_my_info()
# print(my_high)
# print(my_weight)
# print(my_age)
# def print_func(a):
#     return a
# a = 1
# b = 2
# c = 3
# f = lambda a, b: print(a + b)
#
# f(1, 2)

# def f(x, l={}):
#     for i in range(x):
#         l[key]=value
#     print(l)
#
# 使用函数，创建一个空字典，每一次增加一个新的键值对时，将结果增加到字典中。
# if __name__ == "__main__":
# f(2)
# # f(3, [3, 2, 1])
# f(3)
# f(4)
#
# f = lambda a, b: a*b
#
# print(f(1, 2))
#
# my_list = []
#
# for x in range(1, 3):
#     for y in range(2, 4):
#         my_tuple = (x, y)
#         my_list.append(my_tuple)
#
# print(my_list)

# print([(x, y) for x in range(1, 3) for y in range(2, 4)])

# 请写出一段 Python 代码实现分组一个 list 里面的元素,
# 比如 [1,2,3,...100]变成 [[1,2,3],[4,5,6]....]

# a = [x for x in range(1, 101)]


# print(a)
# list1 = []
# for x in range(0, len(a), 3):
#     # a.append(x)
#     # 0,3,6,9,12
#     # print(x)
#     list1.append(a[x:x+3])
#
# print(list1)
#
# print([a[x:x+3] for x in range(0, len(a), 3)])

# # 1!  --- 1
# def my_func1(num):
#     return num
#
#
# # 2!  --- 2 * 1!
# def my_func2(num):
#     return num * my_func1(num - 1)
#
#
# # 3!  --- 3 * 2!
# def my_func3(num):
#     return num * my_func2(num - 1)

# n!
# def my_func(num):
#     if num > 1:
#         return num * my_func(num - 1)
#     else:
#         return 1
#
#
# print(my_func(3))

# def aaa():
#     aaa()
#
# aaa()
#
# f = open("qasd.txt", mode="wb")
# f.write("asdasd".encode())
# ret = f.
# print(ret)
# f.close()
#
# a = 'lebo.txt'
#
# b = '[附件leb.txt]'
#
# old_f = open(a, 'w')
# old_f.write("lebo")
# old_f.close()
#
# old_f = open(a)
# ret = old_f.readlines()
# new_f = open(b, 'w')
# for i in ret:
#     new_f.write(i)
#
# old_f.close()
# new_f.close()

import os


#
# os.rename('lebo.txt', "haha.txt")
#
# os.remove('[附件leb.txt]')
# a = os.listdir(r'../')
# print(a)
# coding=utf-8
# 批量在文件名前加前缀
# import os
#
# funFlag = 1 # 1表示添加标志
# folderName = './renameDir/'
#
# # 获取指定路径的所有文件名字
# dirList = os.listdir(folderName)
#
# # 遍历输出所有文件名字
# for name in dirList:
#     # print(name)
#
#     if funFlag == 1:
#         newName = '[lebo]-' + name
#     # print(newName)
#
#     os.rename(folderName+name, folderName+newName)
# 基类
# class Animal(object):
#     pass
# class Dog(object):
#
#     # 魔法方法
#     def __init__(self, name, tui, age):
#         self.name = name
#         self.tui = tui
#         self.age = age
#
#     def __str__(self):
#         return "name:%s ,tui:%d， age：%d" % (self.name, self.tui, self.age)
#
#     def __del__(self):
#         """
#         监听对象是否被销毁
#         :return:
#         """
#         print("del")

#     def jump(self):
#         print("jump")
#
# mro_
# dahuang = Dog(name='11', age=1,tui=2)
# haha = Dog(name='10', age=1, tui=2)
# print(dahuang)
# print(haha)
# del dahuang
# del haha
# input()
# if __name__ == '__main__':
#     dahuang = Dog(name='11', age=1,tui=2)
#     haha = Dog(name='10', age=1, tui=2)
#     print(dahuang)
#     print(haha)
#     del dahuang
#     del haha
#     input()
# 故事情节：煎饼果子老师傅在煎饼果子界摸爬滚打几十年，拥有一身精湛的煎饼果子技术，并总结了一套"古法煎饼果子配方"。
#
# 可是老师傅年迈已久，在嗝屁之前希望把自己的配方传承下去，于是老师傅把配方传给他的徒弟大猫...
# 大猫掌握了师傅的配方，可以制作古法煎饼果子。但是大猫是个爱学习的好孩子，
# # 他希望学到更多的煎饼果子的做法，于是通过百度搜索，找到了一家煎饼果子培训学校。（多继承）
# 大猫掌握了 师傅的配方 和 学校的配方，通过研究，大猫在两个配方的基础上，创
# 建了一种全新的煎饼果子配方，称之为 "猫氏煎饼果子配方"。（子类重写父类同名属性和方法）
# 大猫的新配方大受欢迎，但是有些顾客希望也能吃到古法配方和
# 现代配方 的煎饼果子...（子类调用父类的同名属性和方法）
# 大猫的煎饼果子店非常红火，终于有一天，他成了世界首富！！
# 但是他也老了，所以他希望把 师傅的配方 和 学校的配方 以及自己的配方 继续传承下去...（多层继承）
# 大猫觉得配方传承下去没问题，但是钱是辛辛苦苦挣得血汗钱，不想传给徒弟。（私有权限）
# class Master(object):
#     def __init__(self):
#         self.peifang = '古法煎饼果子配方'
#
#     def make_cake(self):
#         print('古法煎饼果子')
#
#     def master(self):
#         print("master")
#
#
# class School(object):
#     def __init__(self):
#         self.__peifang = '现代配方'
#
#     def make_cake(self):
#         print("现代煎饼果子")
#
#     def school(self):
#         print("school")
#
#
# class tudi(Master, School):
#
#     is_flag = True
#
#     def __init__(self):
#         self.__peifang = '猫氏煎饼果子配方'
#         School.__init__(self)
#
#     def make_cake(self):
#         print("猫氏煎饼果子")
#
#     def make_old_cake(self):
#         Master.make_cake(self)
#
#     def make_new_cake(self):
#         # School.make_cake(self)
#         super().make_cake()
#
#     def tudi(self):
#         print("tudi")
#
#     def __money(self):
#         print("10000")
#
#     def haha(self):
#         self.__money()
#
#     def set_haha(self, new_money):
#         self.__money = new_money
#
# class ersun(tudi):
#     pass
#
# tudi = tudi()
# # tudi.money()
# # tudi.haha()
# ersun = ersun()
# print(ersun.peifang)
# ersun.haha()
# class Pro(object):
#
#     def __str__(self):
#         return "喵~喵~喵~"
#
# cat = Pro()
# print(cat)
#
# class Animal(object):
#     def shut(self):
#         pass
#
#     def eat(self):
#         pass
#
#
# class Dog(Animal):
#     def shut(self):
#         print("dog")
#
#     def eat(self):
#         print("dog")
#
#
# class Cat(Animal):
#     def shut(self):
#         print('cat')
#
#     def eat(self):
#         print('cat')
#
# cat = Cat()
# dog = Dog()
# cat.eat()
# cat.shut()
# dog.shut()
# dog.eat()



# class Animal(object):
#     __instance = None
#     is_first = True
#     def __new__(cls, *args, **kwargs):
#         if not cls.__instance:
#             cls.__instance = object.__new__(cls)
#         return cls.__instance
#     def __init__(self, name):
#         if Animal.is_first:
#             self.name = name
#             Animal.is_first = not Animal.is_first
# p = Animal("haha")
# print(p.name)
#
# v = Animal("heiehie")
# print(v.name)
# print(Animal().animal)

# class Animal(object):
#     def run(self):
#         print("run")
#
# class Dog(Animal):
#     def run(self):
#         print("DOG RUN")
#
# a = list()
# print(isinstance(a, list))
# LLL = Dog()
# haha = Animal()
# print(isinstance(LLL, Animal))
# try:
# print(hahahah)
# open("hahaha.txt")
# except NameError:
#     print("没有被定义")

# try:
#     print('aha')
# except:
#     print("except")
# else:
#     print("else")
# finally:
#     print("finally")

# try:
#     try:
#         print(haha)
#     finally:
#         print("f")
# except:
#     print("haha")

# def haha():
#     print(aaaaa)
# def hehe():
#     try:
#         haha()
#     except:
#         pass
# hehe()

# class AgeError(BaseException):
#     def __init__(self, age):
#         self.age = age
#
#     def __str__(self):
#         return "0---150之间，现在输入的是%d，不对！！！！！" % self.age
#
# class Person(object):
#     def __init__(self, age):
#         if age > 0 and age < 150:
#             self.age = age
#         else:
#             raise AgeError(age)
#
#
# xm = Person(2222)

# class haha(object):
#     def __init__(self, switch):
#         self.switch = switch
#     def hehe(self, a, b):
#         try:
#             return a/b
#         except Exception as e:
#             if self.switch:
#                 print("开启捕获", e)
#             else:
#                 raise
#
# a = haha(True)
# a.hehe(10, 0)
# print("-"*1000)
# a.switch = False
# a.hehe(10, 0)
