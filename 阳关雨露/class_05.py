"""
__project__ = 'PythonProject'
__file_name__ = 'class_05'
__author__ = 'Sun'
__time__ = '2021/3/29 22:09'
__product_name = PyCharm

"""
# class_05

"""
2、break和continue的区别：
break是结束break当前所在的循环，后续的代码不再执行
continue是结束当次循环，继续进行下一次循环
"""
"""
3、按要求处理字符串： 
现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
1.	截取字符串的第一位到第三位的字符
str1 = '1234567890'
str2 = str1[:3]
print(str2)
2.	截取字符串最后三位的字符
str1 = '1234567890'
str3 = str1[-3:]
print(str3)
3.	截取字符串的全部字符
str1 = '1234567890'
str4 = str1[:]
print(str4)
4.	截取字符串的第七个字符到结尾
str1 = '1234567890'
str5 = str1[-4:]
print(str5)
5.	截取字符串的第一位到倒数第三位之间的字符
str1 = '1234567890'
str6 = str1[1:-3:]
print(str6)
6.	截取字符串的第三个字符
str1 = '1234567890'
str7 = str1[2:]
print(str7)
7.	截取字符串的倒数第一个字符
str1 = '1234567890'
str8 = str1[-1]
print(str8)
8.	截取与原字符串顺序相反的字符串
str1 = '1234567890'
str9 = str1[::-1]
print(str9)
9.	截取字符串倒数第三位与倒数第一位之间的字符
str1 = '1234567890'
str10 = str1[-2:-1]
print(str10)
10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
str1 = '1234567890'
str11 = str1[1:-1:2]
print(str11)
"""
str1 = '1234567890'
str11 = str1[1:-1:2]
print(str11)
"""
定义一个字符串，打印出字符串及其字符串所在的下标
"""
str1 = '1234567890'
i = 0
while i < len(str1):
    ret = str1[i]
    print("字符串是：%s,字符串对应的下标是：%d"%(ret,i))
    i += 1










