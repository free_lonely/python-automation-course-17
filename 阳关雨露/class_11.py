"""
__project__ = 'PythonProject'
__file_name__ = 'class_11'
__author__ = 'Sun'
__time__ = '2021/4/13 12:33'
__product_name = PyCharm

"""
# 老师的
import os

# 综合应用: 学生管理系统(文件版)
# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
stu_list = []
# 有如下7个方法
def print_menu():
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")

def add_new_info():
    """添加学生信息"""
    global stu_list
    new_name = input("请输入姓名:")
    new_tel = input("请输入手机号:")
    new_qq = input("请输入QQ:")
    # 添加学生可能出现：此用户名已经被占用, 请重新输入
    for temp_info in stu_list:
        if temp_info["name"] == new_name:
            print("此用户名已经被占用, 请重新输入!")
            return # 结束函数
    # 定义一个字典，用来存储用户的学生信息(这是一个字典)
    stu_dict = {}
    # 向字典中添加数据
    stu_dict["name"] = new_name
    stu_dict["tel"] = new_tel
    stu_dict["qq"] = new_qq
    # 向列表中添加这个字典
    stu_list.append(stu_dict)

def del_info():
    """删除学生信息"""
    global stu_list
    # "请输入要删除的序号:"
    del_num = int(print("请输入要删除的序号:"))
    if 0 <= del_num< len(stu_list):
        # 你确定要删除么?yes or no
        del_flag = input("你确定要删除么?yes or no")
        if del_flag == "yes":
            del stu_list[del_num]
    # 输入序号有误, 请重新输入
    else:
        print("您输入的序号有误请重新输入！")

def modify_info():
    """修改学生信息"""
    global stu_list
    # "请输入要修改的序号:"
    modify_num = int(input("请输入要修改的序号"))
    if 0 <= modify_num < len(stu_list):
        # "您要修改的信息是："
        print("您要修改的信息是：name：%s,tel:%s,qq:%s" % (stu_list[modify_num]["name"], stu_list[modify_num]["tel"],
                                                 stu_list[modify_num]["qq"]))

        stu_list[modify_num]["name"] = input("请输入新的姓名:")
        stu_list[modify_num]["tel"] = input("请输入新的手机号:")
        stu_list[modify_num]["qq"] = input("请输入新QQ:")
    else:
        print("您输入的序号有误请重新输入！")

def search_info():
    """查询学生信息"""
    search_name = input("请输入要查询的学生姓名:")
    for temp_info in stu_list:
        if temp_info["name"] == search_name:
            print("查询的信息如下：")
            print("name:%s,tel:%s,qq:%s" % (temp_info["name"], temp_info["tel"], temp_info["qq"]))
            break
    else:
        print("没有您要找的信息！")

def print_all_info():
    """遍历学生信息"""
    print("序号\t姓名\t\t手机号\t\tqq")
    i = 1
    for temp in stu_list:
        # temp是一个字典
        print("%d\t%s\t\t%s\t\t%s" % (i, temp["name"], temp["tel"], temp["qq"]))
        i += 1

def save_data():
    """加载之前存储的数据"""
    file = open('test.txt', 'w')
    file.write(str(stu_list))
    file.close()

def load_data():
    """加载之前存储的数据"""
    global stu_list
    file = open("test.txt")
    # 读取内容
    content = str(file.readlines())
    stu_list = eval(content)
    file.close()


def main():
    """用来控制整个流程"""
    load_data()
    while True:
        # 1.调用打印菜单功能
        print_menu()

        # 2.获取用户的选择
        num = input("请输入要进行的操作（数字）")

        # 3.根据用户选择，做相应的事情
        if num == "1":
            # 添加学生
            add_new_info()
        elif num == "2":
            # 删除学生
            del_info()
        elif num == "3":
            # 修改学生
            modify_info()
        elif num == "4":
            # 查询学生
            search_info()
        elif num == "5":
            # 显示所有学生
            print_all_info()
        elif num == "6":
            # 保存数据
            save_data()
        elif num == "7":
            exit_flag = input("您确定要退出系统吗？（yes or no）")
            if exit_flag == "yes":
                break
        else:
            print("您输入有误，请重新输入")

        input("\n\n\n按回车键继续")
        # 调用Linux命令clear完成清屏
        os.system("clear")

# 程序的开始
main()
