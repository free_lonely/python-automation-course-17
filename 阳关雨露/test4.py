"""
__project__ = 'PythonProject'
__file_name__ = 'test4'
__author__ = 'Sun'
__time__ = '2021/4/13 16:11'
__product_name = PyCharm

"""
# 打开一个文件
# 所谓的open 打开一个文件 和面的w 或者 r 或者其他模式 只是给执行者一个权限
# w 是写的意思
# open("lebo.txt", "w")
# r 是读的意思
# open("lebo.txt", "r")


# 读文件
# 如果使用r模式打开一个文件
# 如果文件存在直接打开
# 如果文件不存在 将报错
# FileNotFoundError: [Errno 2] No such file or directory: 'hm.txt'
# open("lebo.txt", "r")


# 写文件
# 如果使用w模式打开一个文件
# 如果文件存在 直接打开
# 如果文件不存在 先创建一个文件 然后打开
# 当文件使用完成后 记得关闭文件（释放内存）
f = open("lebo.txt", "w")
# 关闭文件
f.close()

# <1>写数据(write)
# 打开一个文件
# f = open("lebo.txt", "w")
# 写入数据
# f.write("hello python")
# 关闭文件
# f.close()

# <2>读数据(read)
# # 打开一个文件
# f = open("lebo.txt", "r")
# # 读取数据
# ret = f.read()
# print(ret)
# # 关闭文件
# f.close()


# <3>读数据（readlines）
# 读取文件中的所有行的内容 使用列表进行保存
# # 打开文件
# f = open("lebo.txt", "r")
# # ret 是一个列表
# ret = f.readlines()
# print(ret)
# f.close()

# <4>读数据（readline）
# 打开文件
# f = open("lebo.txt", "r")
# ret = f.readline()
# print(ret)
# ret1 = f.readline()
# print(ret1)
# f.close()

# 写入数据
#如果使用w模式写入数据
# 打开文件 然后把文件中的内容清空 然后再写入
# f = open("lebo.txt", "w", encoding="utf-8")
# 在mac下或者linux 不需要考虑编码格式
# # 在windows系统下如果写入中文 需要执行编码格式 utf-8
# # 默认windows下为gbk编码格式 (中国的电脑)
# f.write("你好世界")
# f.close()

# 读取数据
# f = open("lebo.txt", "r", encoding="utf-8")
# print(f.read())
# f.close()


"""
# 美国人发明了电脑
# 26英文字母 -> ascii码 字符集 0-127 (300多个符号)
# 引入其他国家 gbk (6000 + 14000) -> 简体
# 繁体(中国台湾) -> BIG5
# 美国中97-> a
# 中国人 97 ->哈
# ISO(国际标准化组织)
# unicode 万国码 2个字节(8位)
# utf-8 互通的编码格式
"""

# a -> append
# f = open("lebo.txt", "a", encoding="utf-8")
# f.write("哈哈")
# f.close()


# 了解****
# f = open("lebo1.txt", "wb")
# # 写入二进制数据 编码
# f.write("哈哈".encode(encoding="utf-8"))
# f.close()

# f = open("lebo1.txt", "rb")
# 写入二进制数据 解码
# print(f.read().decode(encoding="utf-8"))
# f.close()


# f = open("lebo.txt", "r", encoding="utf-8")
# # 文本格式下 一个字符就是一个字节
# print(f.read(1))
# f.close()


# f = open("lebo.txt", "rb")
# 写入二进制数据 解码
# 在二进制文件下(utf-8) 一个汉字占三个字节
# print(f.read(3).decode(encoding="utf-8"))
# f.close()

# 制作文件的备份：

# # 准备一个源文件
# old_f = open("lebo.txt", "w")
# # 写入数据
# old_f.write("nihao\nnihao\nhello\n")
# # 关闭文件
# old_f.close()

# 准备一个名字 新文件
new_file_name = "lebo[复件].txt"

# 01 打开源文件读取数据
old_f = open("lebo.txt", "r")
# 去读行数
ret = old_f.readlines()
# 02 创建一个新文件吧数据写入
new_f = open(new_file_name, "w")
# 遍历列表的数据
for value in ret:
    new_f.write(value)

# 03 分别关闭两个文件
old_f.close()
new_f.close()

import os

# 1. 文件重命名
#
# os模块中的rename()可以完成对文件的重命名操作
#
# rename(需要修改的文件名, 新的文件名)
# os.rename("lebo.txt", "lebo1.txt")

# 2. 删除文件
#
# os模块中的remove()可以完成对文件的删除操作
#
# remove(待删除的文件名)
# os.remove("lebo.txt")

# 3. 创建文件夹
# os.mkdir("文件夹")

# 4. 获取当前目录(在执行os.getcwd时候 看执行这个代码的模块属于哪个路径)
# print(os.getcwd())

# 5. 改变默认目录
# 目前的路径
# print(os.getcwd())
# # ../ 或者./ 都是属于相对路径(看到一个. 或者两个.)
# # Z:\Desktop\第九天的代码 绝对路径(可以看到盘符)
# os.chdir("../")
# print(os.getcwd())

# 6. 获取目录列表
# .idea 如果一个文件是以.开头 就代表是一个隐藏文件
# 在我们创建一个工程时候 默认会有这个.idea名字的隐藏文件
# my_list = os.listdir()
# print(my_list)


# 7. 删除文件夹
# ./文件夹 或者 文件夹 一样的
# os.rmdir("./文件夹")


