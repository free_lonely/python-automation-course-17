"""
__project__ = 'PythonProject'
__file_name__ = 'test1'
__author__ = 'Sun'
__time__ = '2021/4/10 19:26'
__product_name = PyCharm

"""
# 分析当执行到第二行的时候需要开辟内存保存哈哈
# 当第二行代码执行完成后 后面没有可执行的代码 标识这程序结束
# 当程序结束后 python就会干掉my_str (作用使用它持有的内存)
my_str = "哈哈"

# 定义一个函数
def my_func1():
    # 定义一个变量
    # 局部变量(定义在函数内部的变量就叫做局部变量)
    # 局部变量的作用域(使用范围)是在函数的内部
    # 局部变量 内存使用时调用函数执行第13行代码
    num = 10
    print(num)
    # 当执行完第14行标识这函数执行完成 也就是会吧变量内存释放

# 调用函数
my_func1()



# 对于以下两个函数中分别有一个局部变量 名字叫做num 但是是两个不同的变量
# def my_func1():
#     num = 10
#     print(num)
#
# def my_func2():
#     num = 10
#     print(num)


# 全局变量
# 定义一个变量(全局变量就是定义在函数外面的变量)
# 全局变量作用域是整个模块内部
# 在函数的内部可以使用全局变量
# num = 10
#
# def my_func1():
#     print(num)
#
# def my_func2():
#     print(num)
#
# my_func1()
# my_func2()


# 定义两个名字相同的变量(全局变量和局部变量)
# 定义一个全局变量
#
# # 全局变量
# num = 10
#
# # 函数的内部
# def my_func():
#     # 局部变量
#     num = 20
#     # num = 20
#     # 在函数的内部使用一个变量
#     # 遵循一个规则: 先在函数内部找 找到直接使用 如果找不到到函数外部找 如果还是找不到 就报错(由内到外)
#     print("2",num)
#
# # num = 10
# print("1", num)
# my_func()
# # num = 10
# # 局部变量的作用域是在函数内部 (只能使用全局变量)
# print("3",num)



# 函数内部对全局变量的值进行修改 global
# 全局变量
num = 10

# 函数的内部
def my_func():
    # 标识下函数内部的num不是一个局部变量 而是对全局变量重新赋值
    global num
    # 可以对全局变量的值进行修改
    num = 20
    print("2", num)


my_func()
# 当打印的时候 必须打印的是 3, 20
print("3", num)








# 不可变 重新赋值
# a = 10
# b = a
#
# #在python中都是引用的方式
# print(id(a))
# print(id(b))
# a = 20
# print(id(a))
# print(id(b))


# 可变 操作本身
a = [1, 2]

b = a

print(id(a))
print(id(b))
a.append(3)
print(id(a))
print(id(b))




# 定义一个一个函数
# def my_func(b):
#     # b = b + b
#     b += b
#     print(id(b))
#
#
# # 定义一个全局变量 (a 是不可变的)
# a = 10
# #python中不是值的传递 是引用传递
# my_func(a)
# print(id(a))


# # 定义一个一个函数
# def my_func(b):
#     # 如果b =  b + b 就等于 b = [1, 2, 1, 2] 需要重新开辟内存保存[1, 2, 1, 2]
#     # b = b + b
#     # b += b 认为是 b.extend(b) (和append相似)
#     b += b
#     # print(id(b))
#     print(b)
#
#
# # 定义一个全局变量
# a = [1, 2]
# #python中不是值的传递 是引用传递
# my_func(a)
# # print(id(a))
# print(a)


# 小整数缓存池 -5到256之间 内存地址已经开辟好 我们程序员直接使用就好
# 字符串或者元组 或者列表等等 遵循的2kb大小 缓存机制

a = 100
b = 100
c = 100
print(id(a),id(b),id(c))
