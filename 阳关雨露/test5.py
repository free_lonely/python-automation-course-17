"""
__project__ = 'PythonProject'
__file_name__ = 'test5'
__author__ = 'Sun'
__time__ = '2021/4/15 11:26'
__product_name = PyCharm

"""
# 如果我想保存数据 10 3.14 "hello"
# my_list1 = [10, 3.14, "hello"]
#
# 如果查看一个类 可以按住ctrl + 鼠标左键
# my_list2 = list()

# 创建一个孙悟空英雄对象
# 游戏的英雄类
# 定义类 -> 自定义类(在python内部根本就没有一个Hero类来创建一个孙悟空对象)

# 自定义类
# 类的写法一共分为三种
# 在python2.x中 三种创建类的方式不同 1 和 2 相同 (经典类) 3 叫做新式类
# 在python3.x中 这三种创建类的方式 是相同的 都是继承父类object
# object 是在python中 所有类的父类(基类)
# 01
class Hero:
    pass

# 02
class Hero():
    pass

# 03(建议大家都要写这种方式)
class Hero(object):
    pass

# 格式:
# 类名的规则: 遵循大驼峰命名法
# class 类名(object):
#     pass

# 一个函数定义在模块内 叫做函数
# 一个函数定义在类中 就叫做方法

# 创建一个人 叫做小明
# 小明会走(行为 方法)
# 自定义一个人类
class Person(object):

    # 人会走
    # 定义在类中的方法
    # 实例方法 也可以叫做对象方法
    # 实例方法 默认有一个形参 默认形参名叫做self
    # 默认情况下 调用对象方法 self这个参数不需要传递
    # 因为python中在使用对象调用这个方法的是 会吧小明这个对象直接传递给形参self
    def move(self):
        print("人会走")


# 通过自定义类创建一个对象
# 自定义对象格式: 变量名 = 类名()
xiaoming = Person()
# 只要创建一个对象 系统就要开辟内存
# <__main__.Person object at 0x00000000021DF0F0>默认情况下打印一个自定义对象
# 输出的是一个16进制的地址
# print(id(xiaoming))
# 想调用一个类中的方法
# 需要通过这个类创建一个对象
# 然后使用这个对象调用其这个类中的方法
# 对象方法调用格式: 对象名.对象方法()
xiaoming.move()


# 面向过程开发
# def my_func():
#     print("函数")
#
# my_func()

# 自定义一个游戏英雄类
class Hero(object):

    # 会走
    def move(self):
        print("会走")

    # 会跑
    def run(self):
        print("会跑")



# 自定义一个黄忠
huangzhong = Hero()
# print(huangzhong)
# huangzhong.move()
# huangzhong.run()
# wukong = Hero()
# print(wukong)

# 有名字 年龄 血量 攻击力 属性
# 给黄忠这个对象添加属性(赋值)
huangzhong.name = "黄忠"
huangzhong.age = 500
huangzhong.hp = 300
huangzhong.atk = 100

# 获取对象的属性值(取值)
print("名字:%s" % huangzhong.name)
print("年龄:%d" % huangzhong.age)
print("血量:%d" % huangzhong.hp)
print("攻击力:%d" % huangzhong.atk)


# 自定义小明这个对象
# 会python
class Person(object):

    # 自定义实例方法
    # 命名方式: 使用_ 或者是小驼峰
    def hello_python(self):
        print("会python")


    # 在类的里面打印小明的属性值
    def print_info(self):
        # 如果在类的内部打印一个对象的属性值格式: self.属性名
        print(self.name)
        print(self.age)


# 自定义一个对象
xiaoming = Person()
# 名字 年龄
xiaoming.name = "小明"
xiaoming.age = 20
# 打印属性值
# 如果在类的外面打印对象的属性格式: 对象名.属性名
# print(xiaoming.name)
# print(xiaoming.age)
# 调用类中的print_info
xiaoming.print_info()



# # 自定义一条狗(对象) 名字 旺财 年龄5岁 毛色 白色
#
# # 自定义一个犬类
# class Dog(object):
#
#     # 啃骨头
#     def eat(self):
#         print("肯骨头")
#
#     # 方法-> 打印对象的属性值
#     def print_info(self):
#         print("名字:%s" % self.name)
#         print("年龄:%d" % self.age)
#         print("毛色:%s" % self.color)
#         print(self)
#
#
# # 旺财
# wangcai1 = Dog()
# # 名字 旺财 年龄5岁 毛色 白色
# wangcai1.name = "旺财"
# wangcai1.age = 5
# wangcai1.color = "白色"
# # 调用方法
# wangcai1.print_info()
#
# print("-"*40)
# # 另一条旺财
# wangcai2 = Dog()
# # 名字 旺财 年龄5岁 毛色 白色
# wangcai2.name = "旺财"
# wangcai2.age = 5
# wangcai2.color = "白色"
# # 调用方法
# wangcai2.print_info()
#
# print("-"*40)
# # 另一条旺财
#
# wangcai3 = Dog()
# # 名字 旺财 年龄5岁 毛色 白色
# wangcai3.name = "旺财"
# wangcai3.age = 5
# wangcai3.color = "白色"
# # 调用方法
# wangcai3.print_info()



