"""
__project__ = 'PythonProject'
__file_name__ = 'test2'
__author__ = 'Sun'
__time__ = '2021/4/13 16:03'
__product_name = PyCharm

"""
# 阶乘 %! 函数
# 现在要完成5的阶乘，可以完成
# def my_func(num):
#     ret = 1
#     while num > 0:
#         ret *= num
#         num -= 1
#
#     print(ret)
#
#
# my_func(5)
#
# # 5 * 4 * 3 * 2 * 1
# # 5 * 4 * 3 * 2 * 1
# def my_func1(num):
#     if num > 1:
#         return num * my_func2(num - 1)
#     else:
#         return 1
#
# # 4 * 3 * 2 * 1
# def my_func2(num):
#     if num > 1:
#         return num * my_func3(num - 1)
#     else:
#         return 1
#
# # 3 * 2 * 1
# def my_func3(num):
#     if num > 1:
#         return num * my_func4(num - 1)
#     else:
#         return 1
#
# # 2 * 1
# def my_func4(num):
#     if num > 1:
#         return 2 * my_func5(num - 1)
#     else:
#         return 1
#
# # 1
# def my_func5(num):
#     if num > 1:
#         return num * (num - 1)
#     else:
#         return 1
#
# # print(my_func4(2))
# print(my_func1(5))
# print("测试")

def my_func(num):
    if num > 1:
        return num * my_func(num - 1)
    else:
        return 1

print(my_func(5))



# # n!
# def my_func(num):
#     if num > 1:
#         return num * my_func(num - 1)
#     else:
#         return 1
#
# print(my_func(5))
#
# # 使用递归函数要注意 肯定要有一个停止递归调用的条件
# # RecursionError: maximum recursion depth exceeded
# # 不能停止的递归函数
def tete():
    tete()
# #
tete()
#