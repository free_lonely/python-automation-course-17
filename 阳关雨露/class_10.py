"""
__project__ = 'PythonProject'
__file_name__ = 'class_10'
__author__ = 'Sun'
__time__ = '2021/4/11 22:09'
__product_name = PyCharm

"""
# 4、用lambda函数实现两个数相乘
num = lambda a, b: a * b
ret = num(20, 5)
print(ret)

# 5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
my_list = [i for i in range(1, 11) if i % 2 != 0]
print(my_list)