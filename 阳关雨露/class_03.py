"""
__project__ = 'PythonProject'
__file_name__ = 'class_03'
__author__ =  'Sun'
__time__ = '2021/3/28 15:38'
__product_name = PyCharm

"""
# class_03
"""
情节描述：上公交车，并且可以有座位坐下

要求：输入公交卡当前的余额，只要超过2元，就可以上公交车；如果车上有空座位，就可以坐下。

"""
"""
#  定义1个变量接收公交卡的余额
amount = int(input("请输入公交卡的余额："))
seat = 0  # 1代表有座位，0代表没有座位
if amount >= 2:
    print("卡里有钱，可以出去浪了")
    if seat == 1:
        print("有座位了，可以坐了")
    else:
        print("没有座位了，只能站着了")
else:
    print("卡里余额不足，请充值，只能下次出去嗨了！")

"""


# 写出石头剪刀布的游戏
import random
# 定义一个变量来接收用户随机输入的数，而电脑随机的需要用到random.randint函数
while True:
    player = int(input("请随机输入：石头（0），剪刀（1），布（2）"))
    computer = random.randint (0,2)
# 分析用户赢得几种情况进行比较：石头（0）——剪刀（1）剪刀（1）-布（2） 布（2）—石头（0）
# 平局的情况为：两个出的一样的 其他的情况为输错了
    if (player == 0 and computer == 1) or (player == 1 and computer == 2) or(player == 2 and computer ==0):
        print("用户出的是：%d,电脑出的是：%d,用户赢了"%(player,computer))
    elif player == computer:
        print("用户出的是：%d,电脑出的是：%d，平局"%(player,computer))
    else:
        print("用户出的是：%d,电脑出的是：%d,用户输了"%(player,computer))
# eval返回一个字符串中原本的数据类型
num1 = eval("10")
print(type(num1))
