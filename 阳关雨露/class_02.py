"""
__project__ = 'PythonProject'
__file_name__ = 'class_02'
__author__ = 'Sun'
__time__ = '2021/3/25 9:52'
__product_name = PyCharm

"""
# class_02
"""
1、	说出变量名可以由哪些字符组成
    变量名由字母、数字、下划线组成，且不能以数字开头
    
2、	写出变量命名时的规则 
    变量命名的规则：
    1.见名知意 如：age
    2.使用小驼峰命名法,第一个单词的首字母小写，之后的每一个单词的首字母都大写，例如myName
    3.使用大驼峰命名法，每一个单词的首字母都大写，例如FirstName
    4.使用下划线命名法，例如student_age
    5.变量命名不能以数字开头，不能包含特殊字符
    6.不能使用关键字命名
    
3、	写出什么是驼峰法
    使用小驼峰命名法,第一个单词的首字母小写，之后的每一个单词的首字母都大写，例如myName
    使用大驼峰命名法，每一个单词的首字母都大写，例如FirstName
"""


"""
# 4、编写程序，完成以下要求：
# •	提示用户进行输入数据
# •	获取用户的数据（需要获取2个）
# •	对获取的两个数字进行求和运行，并输出相应的结果
num1 = int(input("请输入第一个数："))
num2 = int(input("请输入第一个数："))
result = num1 + num2
print("您输入的两数相加的和为：%d"%result)


# 5、编写程序，完成以下信息的显示:
# •	==================================
# •	=        欢迎进入到身份认证系统V1.0
# •	= 1. 登录
# •	= 2. 退出
# •	= 3. 认证
# •	= 4. 修改密码
# •	==================================
print("=        欢迎进入到身份认证系统V1.0")
print("= 1. 登录")
print("= 2. 退出")
print("= 3. 认证")
print("= 4. 修改密码")

"""


# 6、编写程序，通过input()获取一个人的信息，然后按照下面格式显示
# ==================================
# 姓名: xxxxx
# QQ:xxxxxxx
# 手机号:131xxxxxx
# 公司地址:北京市xxxx
# ==================================
print("==================================")
name = input("请输入您的姓名：")
QQ = input("请输入您的QQ号：")
tel_num = input("请输入您的手机号：")
address = input("请输入您的公司地址：")
print("姓名：%s\nQQ：%s\n手机号：%s\n公司地址：%s"%(name,QQ,tel_num,address))

"""
7、使用if，编写程序，实现以下功能： 
•	从键盘获取用户名、密码
•	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
"""
# 假设用户名为：18607143979 密码为：654387
usr_name = int(input("请输入用户名："))
pwd = int(input("请输入密码："))
if usr_name == 18607143979 and pwd ==654387:
    print("欢迎进入XXX的世界")
else:
    print("密码或者用户名错误")



age1 = 20
print(type(age1))

# 数据类型的转换
str1 = "10"
print(int(str1))




# 8.写出石头剪刀布的游戏
import  random
#player = int(input("请输入：石头（0），剪刀（1），布（2）"))
# 玩家赢得几种情况：玩家出石头，电脑出剪刀  玩家出剪刀，电脑出布 玩家出布，电脑出石头
# 电脑出的随机
# computer = random.randint(0,2)
# print("玩家出的是：%d,电脑出的是：%d"%(player,computer))

while True:
    player = int(input("请输入：石头（0），剪刀（1），布（2）"))
    computer = random.randint(0, 2)
    print("玩家出的是：%d,电脑出的是：%d" % (player, computer))
    if(player == 0 and computer == 1) or(player == 1 and computer == 2) or (player == 2 and computer == 0):
        print("玩家赢了")
    elif player == computer:
        print("平局")
    else:
        print("玩家输了")







