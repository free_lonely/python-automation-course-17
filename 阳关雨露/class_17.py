"""
__project__ = 'PythonProject'
__file_name__ = 'class_17'
__author__ = 'Sun'
__time__ = '2021/4/26 15:01'
__product_name = PyCharm

"""
'''
from pymysql import *
# 创建数据库连接
db = connect(host="49.233.39.160", user="user", password="leboAa!#$123",
             database="lebo16", port=3306, charset="utf8")
# 使用cursor()方法获取操作游标
# cursor = db.cursor()
# 定义一个sql进行查询所有信息，并执行sql
# sql = "select * from students_test1"
# cursor.execute(sql)

# 查询返回的是一个字典
cur = db.cursor(cursor=cursors.DictCursor)
# name = input("请输入姓名：")
# sql = "select * from students_test1 where name ='%s';" % name
sql = "select * from students_test1;"
cur.execute(sql)

# fetchone查询一条数据
# result1 = cursor.fetchone()
# print(result1)
# fetchall查询所有数据
# result2 = cursor.fetchall()
# print(result2)
# fetchmany(2)查询两条数据
result3 = cur.fetchmany(2)
print(result3)

'''



from pymysql import *
# 创建数据库连接
db = connect(host="49.233.39.160", user="user", password="leboAa!#$123",
             database="lebo16", port=3306, charset="utf8")
# 使用cursor()方法获取操作游标
# cursor = db.cursor()
# 定义一个sql进行查询所有信息，并执行sql
# sql = "select * from students_test1"
# cursor.execute(sql)

# 查询返回的是一个字典
cur = db.cursor(cursor=cursors.DictCursor)
name = input("请输入序号：")
sql = "select * from students_test1 where studentNo = '%s';" % name
print(sql)
# age = input("请输入年龄:")
# 防止sql注入
# sql = "select * from students_test1 where name = %s and age = %s;"
# sql = "select * from students_test1 where name = ''or 1=1 or '';"

# 数据库增删改都需要进行commit操作，如果失败需要进行rollback操作
# sql = "update students_test1 set name = '张三' where name = '王昭君';"

# sql = "select * from students_test1;"
# count = cur.execute(sql, [name, age])
count = cur.execute(sql)
print("查询到%d条数据" % count)
# fetchone查询一条数据
# result1 = cur.fetchone()
# print(result1)
# # fetchall查询所有数据
# result2 = cur.fetchall()
# print(result2)
# fetchmany(2)查询两条数据
result3 = cur.fetchmany(5)
print(result3)
# print("查询到%d条数据" %count)
# db.commit()
# sql1 = 'select * from students_test1;'
# cur.execute(sql1)
# ret = cur.fetchall()
# print(ret)
cur.close()
db.close()


# 'or 1=1 or'  sql注入

