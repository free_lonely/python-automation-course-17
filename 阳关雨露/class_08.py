"""
__project__ = 'PythonProject'
__file_name__ = 'class_08'
__author__ = 'Sun'
__time__ = '2021/4/7 19:14'
__product_name = PyCharm

"""
"""
1.python2.x和Python3.x默认的编码格式分别是什么？
 Python2.x的编码格式为asscll码，而Python3中使用的是utf-8

2.将“hello_new_world”按“—”进行切割
str1 = "hello_new_world"
print(str1.split("_"))

3.将数字1以“0001”的格式输出到屏幕
第一种
str2 = "1"
print(str2.rjust(4, "0"))

第二种
c = 1
c1 = "%04d" % c
print(c1)

"""
"""
1.字典a = {1: 1}，是否正确？
正确
a = {1: 1}
print(type(a))

2.请合并列表a = [1,2,3,4]和列表b = [5,6,7,8]
# 第一种方法
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
list1 = a + b
print(list1)
# 第二种方法
a.extend(b)
print(a)
# 第三种方法
a.append(b)
print(a)
#用循环的方式添加
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
for i in range(len(b)):
    a.append(b[i])
print(a)

3.列表a,请写出正序排列、倒序排列、逆序排列的内置方法
a = (1, 0, 78, 89, 980, 56, 34, 32)
b = list(a)
print(type(b))
a1 = [33, 45, 76, 2, 5, 90, 65, 32, 12, 0, 23]
# 逆序排列
# a1.reverse()
# print(a1)

# 利用切片逆序排列
b1 = a1[::-1]
print(b1)

# 升序排列
# a1.sort()
# print(a1)

# 降序排列
# a1.sort(reverse=True)
# print(a1)

# 不反转升序排列reverse=False
a1.sort(reverse=False)
print(a1)

4.字典d ={"k ": 1, "v": 2}，请写出d.iterms的结果
d ={"k ": 1, "v": 2}
print(d.items())  # 由列表、元祖、字典组成 dict_items([('k ', 1), ('v', 2)])

# 遍历字典中的项（元素）
for iterm in d.items():
    print(iterm)

# 遍历字典中key-value
for key, value in d.items():
    print("key = %s,value = %s"%(key, value))

5、复杂列表[{"k":1,"v":2},{"k":12,"v":22},{"k":13,"v":32}],请用内置方法写出按k的倒序排列的代码。
# 第一种方法
list1 = [{"k": 1, "v": 2}, {"k": 12, "v": 22}, {"k": 13, "v": 3}]
list2 = list1[::-1]
print(list2)

# 第二种方法
list3 = [{"k": 1, "v": 2}, {"k": 12, "v": 22}, {"k": 13, "v": 3}]
list3.reverse()
print(list3)

6.集合s = set([1, 2, 3, 4]),d =set([2, 4, 9, 0, 3])请用内置方法写出他们的交集，并集，对称差
s = set([1, 2, 3, 4])
d = set([2, 4, 9, 0, 3])
# 交集
set1 = s & d
print(set1)

# 并集
set2 = s | d
print(set2)

# 差集
set3 = d - s
print(set3)  # {0， 9}
set4 = s - d
print(set4)  # {1}

#对称差集
set5 = s ^ d
print(set5)

7.如何把列表a = ["a", "b"]里的各项，转为字符串并用逗号“，”连接
a = ["a", "b"]
str1 = ",".join(a)
print(str1)

"""

"""
1.判断下列描述是否正确，如果有错误，请指出错误的地方并改正
字典：具有键值映射关系，无序，字典的键不能重复并且可以是任意数据类型
答：错，字典的键key的类型必须是不可变的数据类型
元组：不能修改，无序，不能索引切片，当元组中只有一个元素时，需要在元素的后面加逗号
答：错，元组是有序的有下标，可以索引切片
列表：元素可以重复，有序，不可以反向索引，元素可以是任意类型
答：错，列表可以反向索引
④	集合：元素不可以重复，可以索引，a={}声明了一个空集合
答：错，集合是无序的，不可以索引
空集合的声明为： a = set()

2.一行代码实现求1到100的和
print(sum(range(1, 101)))

3.按下面要求写出完整代码：
 使用random.random方法实现随机输出范围在[25, 60]中的浮点数
 import random
num = random.randint(25, 60) + random.random()
print(num) 

"""

"""
1.一个list对象：a = [1,2,4,3,2,2,4],需要去掉里面的重复值
a = [1, 2, 4, 3, 2, 2, 4]
b = set(a)
a = list(b)
print(a)
"""
