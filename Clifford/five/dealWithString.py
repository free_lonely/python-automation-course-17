str1 = '1234567890'
str2 = str1[0:3]  # 左闭右开
print(str1, "截取字符串的第一位到第三位的字符:%s"%str2)
str2 = str1[-3:]
print(str1, "截取字符串最后三位的字符:%s"%str2)
str2 = str1[:]
print(str1, "截取字符串的全部字符:%s"%str2)
str2 = str1[6:]
print(str1, "截取字符串的第七个字符到结尾:%s"%str2)
str2 = str1[1:-3]
print(str1, "截取字符串的第一位到倒数第三位之间的字符:%s"%str2)
str2 = str1[2:3]
print(str1, "截取字符串的第三个字符:%s"%str2)
str2 = str1[-1:]
print(str1, "截取字符串的倒数第一个字符:%s"%str2)
str2 = str1[::-1]
print(str1, "截取与原字符串顺序相反的字符串:%s"%str2)
str2 = str1[-2:-1]
print(str1, "截取字符串倒数第三位与倒数第一位之间的字符:%s"%str2)
str2 = str1[1:-1:2]
print(str1, "截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次:%s"%str2)