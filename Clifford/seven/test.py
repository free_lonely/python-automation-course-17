"""
#1.下列将字符串"100"转换为数字100的正确的是( A )
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
#2.下列程序执行结果是( B )
numbers = [1，5，3，9，7]
numbers.sort(reverse=True)
print(numbers)
A、[9，7，5，3，1]
B、[1，3，5，7，9]
C、1，3，5，7，9
D、9，7，5，3，1
#3.如何在列表中添加一个元素
list1 = [1, 5, 3, 9, 7]
list1.append(4)
print(list1)
list1.extend([2, 8, 6])
print(list1)
list1.insert(4, 0)
print(list1)
#4.对于列表什么是越界
#下标超过列表长度
#5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
#可变数据变量：List、Dic、Set
#不可变数据变量：Number、String、Tuple
#6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
list2 = []
for i in range(5):
    strTemp = input()
    list2.append(strTemp)
print(list2)
for item in list2:
    print(item[1])
#7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
import random
list3 = []
for i in range(5):
    strTemp = random.randint(0, 100)
    list3.append(strTemp)

for item in list3:
    print(int(item)*(-1))
#8.将下列两个列表合并，将合并后的列表升序并输出.
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
list1.extend(list2)
list1.sort()
print(list1)
#9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
dict1 = {}
name = input("please input your name")
age = input("please input your age")
id = input("please input your id")
dict1["name"] = name
dict1["age"] = int(age)
dict1["id"] = id
print(dict1)
#10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={"school": "lebo", "date": 2018, "address": "beijing"}
key_list = list(dict1.keys())
values_list = list(dict1.values())
value_index = values_list.index("lebo")
print(key_list[value_index])
#11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
num1 = num[::-1]
count = 0
for i in range(1, len(num1)):
    if (i+1) % 2 == 0:
        count += num1[i]
print(count)
"""
