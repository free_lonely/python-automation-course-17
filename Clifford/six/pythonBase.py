# python基础
# 第一题
str1 = 'a,hello'
if 'hello' in str1:
    print("True")
else:
    print("False")

print(str1.find('hello'))
print(str1.index('hello'))

# 第二题
str2 = 'a,b,c,d'
list2 = str2.split(',')
print(list2)

# 第三题
str3 = "笔试题123A"
str4 = str3.replace('123A', '进行中')
print(str4)