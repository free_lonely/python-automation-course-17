# -*- coding: utf-8 -*-
# @Time    : 2021/4/1 9:55
# @Author  : zj
# @FileName: class_hw06.py
# @Software: PyCharm

import re

# 1.循环输出整数0到10
for i in range(0, 11):
    print(i)

# 2.逆序输出一个字符串，比如【欢乐逛欢迎您】，输出【您迎欢逛乐欢】

# ——切片
text = "欢乐逛欢迎您abc"
print(text[::-1])

# ——反转元素的排序
str1 = list(text)
str1.reverse()
print("".join(str1))

# 3.str = 'a,hello' 在字符串str中查找hello

str2 = 'a,hello'
print(str2.find('hello'))  # ——查找，返回从左第一个指定字符的索引，找不到返回-1
print(str2.count('l'))  # ——计数，返回指定字符在字符串中的个数,找不到返回0

# 4.str = 'a,b,c,d' 用逗号分隔str字符串，并保存到列表

str3 = 'a,b,c,d'
list1 = re.split(r'[,]', str3)  # ——re.split()允许为分割符指定多个正则匹配模式
print(list1)

# 5.将"笔试题 123A"中 "123"替换为"进行中"

str4 = "笔试题 123A"
print(str4.replace("123", "进行中"))
