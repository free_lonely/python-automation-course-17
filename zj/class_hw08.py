# -*- coding: utf-8 -*-
# @Time    : 2021/4/8 13:33
# @Author  : zj
# @FileName: class_hw08.py
# @Software: PyCharm
import random

"""
    1.python2.x 3.x 默认的编码格式
    ——python2.x
        ——默认编码是ASCII
        ——源码重复，不规范
        ——长整形long
        ——打印方式：print[空格][打印内容]
        ——raw_input() input()
    ——python3.x
        ——默认编码为UTF-8
        ——整合源码，清晰简单
        ——无long，统一为int
        ——打印方式：print([打印内容])
        ——保留input()
        
"""

"""
    2.将“hello_new_world” 按“_”符进行切割
"""
str1 = "hello_new_world"
print(str1.split("_"))

"""
    3.将数字1以“0001”的格式输出到屏幕
"""
print(str(1).zfill(4))

"""
    4.字典 a={1: 1},是否正确
    ——正确，不建议这样定义字典类型变量, 建议键名使用字符串类型
    ——字典中key为不可变数据类型
"""

"""
    5.请合并列表a={1,2,3,4}和列表b={5,6,7,8}
"""
a = {1, 2, 3, 4}
b = {5, 6, 7, 8}
print(a | b)

a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
print(a + b)

"""
    6.列表a，请写出实现正序排列，倒序排列，逆序排列的内置方法
"""
a = [2, 3, 1, 4]
# 正序
a.sort()
print(a)  # [1,2,3,4]
# 倒序
a.reverse()
print(a)  # [4,1,3,2]
#  sort()+reverse() ——[4, 3, 2, 1]
# 逆序
a.sort(reverse=True)
print(a)  # [4,3,2,1]

"""
    7.字典d={“k”:1,"v":2}，请写出d.items()结果
    ——print(d.items()) # dict_items([('k', 1), ('v', 2)])
    ——print(d.keys()) # dict_keys(['k', 'v'])
    ——print(d.values()) # dict_values([1, 2])
"""

"""
    8.复杂列表[{"k"：1,"v":2},{"k":12,"v":22},{"k":13,"v":32}],请用内置方法写出按k倒序排列的代码
"""
list1 = [{"k": 1, "v": 2}, {"k": 12, "v": 22}, {"k": 13, "v": 32}]
list1.sort(key=lambda x: x["k"], reverse=True)
print(list1)

"""
    9.集合s=set([1,2,3,4]),d=set([2,4,9,0,3]),请用内置方法写出他们的并集，交集，对称差
"""
s = set([1, 2, 3, 4])
d = set([2, 4, 9, 0, 3])

# 并集
print(s | d)
print(s.union(d))
# 交集
print(s & d)
print(s.intersection(d))
# 对称公差
print(s ^ d)
print(s.symmetric_difference(d))

"""
    10.如何把列表a=["a","b"]里的各项，转为字符串并用逗号‘，’连接
"""
# 方法1：
d = ','.join(a)
print(d)

# 方法2：
a = ["a", "b"]
c = ''
j = 1
for i in a:
    if j < len(a):
        c = c + str(i) + ","
    elif j == len(a):
        c = c + str(i)
    j = j + 1
print(c)

"""
    11.判断：
    ——1.字典：具有键值映射关系，无序，字典的键不能重复并且可以是任意数据类型
        ——字典的键类型不可变
    ——2.元组：不能修改，无序，不能索引切片，当元组中只有一个元素时，需要在元素的后面加逗号 
        ——元组有序，可以索引切片
    ——3.列表：元素可以重复，有序，不可以反向索引，元素可以是任意类型
        ——可以反向索引
    ——4.集合：元素不可以重复，可以索引，a={}声明了一个空集合
        ——集合无序，不可索引 ;a = set()
"""

"""
    12.一行代码实现求1到100的和
"""
print(sum(range(1, 101)))

"""
    13.使用random.random方法实现随机输出范围在[25,60)中的浮点数
"""
a = random.random()
print(a)
if 0.25 <= a < 0.6:
    print("随机数：", a * 100)
else:
    print("不在范围内")

"""
    14.一个list对象：a=[1,2,4,3,2,2,4],需要去掉里面的重复值
"""
a = [1, 2, 4, 3, 2, 2, 4]
print(list(set(a)))
