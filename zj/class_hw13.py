# -*- coding: utf-8 -*-
# @Time : 2021-04-17 9:36
# @Author : zj
# @File : class_hw13.py
# @Software: PyCharm

"""
    1.
"""


class A(object):
    def __init__(self):
        print("__init__")

    def __str__(self):
        return "__str__"

    def __del__(self):
        print("销毁")


a = A()  # __init__
print(a)  # __str__
print(id(a))  # 1602674779232
del a  # 销毁

"""
    2.
"""


class Animal(object):

    def shut(self):
        print("动物在叫")


class Dog(Animal):
    def shut(self):
        super().shut()
        print("汪汪")


dog = Dog()  # 动物在叫
dog.shut()  # 汪汪

"""
    3.描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
"""


class Cat(object):

    def __str__(self):
        return "miao?"


cat = Cat()
print(cat)  # 该方法需要 return 一个数据，并且只有self一个参数，当在类的外部 print(对象) 则打印这个数据

"""
    4.描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.
"""


class Calculator(object):

    def __init__(self, number_1, number_2):
        self.number_1 = number_1
        self.number_2 = number_2

    def add(self):
        return self.number_1 + self.number_2

    def sub(self):
        return self.number_1 - self.number_2

    def div(self):
        return self.number_1 / self.number_2

    def mul(self):
        return self.number_1 * self.number_2


cal = Calculator(1, 2)
cal.add()
cal.sub()
cal.div()
cal.mul()

"""
    5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
"""


class Animal1(object):
    def shut(self):
        pass

    def eat(self):
        pass


class Dog1(Animal1):

    def shut(self):
        print("dog的叫声")

    def eat(self):
        print("dog吃什么")


class Cat1(Animal1):

    def shut(self):
        print("cat的叫声")

    def eat(self):
        print("cat吃什么")


dog1 = Dog1()
dog1.shut()
dog1.eat()

cat1 = Cat1()
cat1.shut()
cat1.eat()

"""
    面向对象三大特征：封装、继承、多态
    单列模式创建保证实例只创建1次
"""
