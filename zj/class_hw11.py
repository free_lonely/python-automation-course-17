# -*- coding: utf-8 -*-
# @Time : 2021-04-15 21:59
# @Author : zj
# @File : class_hw11.py
# @Software: PyCharm

import os

students = []


def menu():
    print("=" * 30)
    print("*" * 10 + "学生信息管理" + "*" * 10)
    print("1.添加学生信息")
    print("2.删除学生信息")
    print("3.指定学号查询学生信息")
    print("4.查询全部学生信息")
    print("5.保存信息")
    print("6.修改信息")
    print("0.退出系统")
    print("=" * 30)


def load_data():
    """加载信息"""
    global students
    flag = os.path.exists("info_data.data")
    # 如果目标文件不存在 创建文件
    if not flag:
        print("创建文件")
        f = open("info_data.data", "w", encoding="utf-8")
        # 写入一个{}
        f.write("[]")
        # 关闭文件
        f.close()
        return
    # 目标文件已存在
    f = open("info_data.data", "r", encoding="utf-8")
    # 读取数据
    ret = f.read()
    f.close()

    students = eval(ret)
    print("读取到的信息：", students)
    return students


def save_data():
    """保存学生列表"""
    global students
    # 打开
    f = open("info_data.data", "w", encoding="utf-8")
    # 写入
    f.write(str(students))
    # 关闭
    f.close()
    print("保存成功")


def add_new_info():
    """添加学生信息"""
    global students
    # students = load_data()
    print("--------添加学生信息--------")
    stu_dic = {}
    new_name = input("请输入姓名:")
    new_tel = input("请输入手机号:")
    new_qq = input("请输入qq:")

    students = load_data()
    for stu in students:
        if stu["new_name"] == new_name:
            new_name_again = input("姓名已存在,请重新输入")
            stu_dic["new_name"] = new_name_again
            break
        else:
            stu_dic["new_name"] = new_name

    stu_dic["new_tel"] = new_tel
    stu_dic["new_qq"] = new_qq

    students.append(stu_dic)
    print("此时的list为：", students)
    print("添加至临时list成功")


def print_all_info():
    """查询学生列表"""
    for temp in load_data():
        print("姓名:%s,手机号:%s,qq:%s" % (temp['new_name'], temp['new_tel'], temp['new_qq']))


def delete_info():
    """删除学生信息"""
    global students
    students = load_data()
    del_index = int(input("请输入需要删除的学生序号(从1开始):"))
    if del_index <= len(students):
        del_name = students[del_index - 1]["new_name"]
        print("找到需要删除的学生，姓名：", del_name)
        exit_flag = input("你确定要删除吗？(yes or no)")
        if exit_flag == "yes":
            del students[del_index - 1]
            print(students)
            print("临时删除成功")
        else:
            print("删除取消")
    else:
        print("超出查询范围")


def modify_info():
    """修改学生信息"""
    global students
    students = load_data()
    modify_index = int(input("请输入需要修改的学生序号(从1开始):"))
    if modify_index <= len(students):
        modify_name = students[modify_index - 1]["new_name"]
        print("找到需要修改的学生，姓名：", modify_name)

        new_name = input("修改姓名：")
        new_tel = input("修改手机号：")
        new_qq = input("修改qq:")

        # for stu in students:
        #     if stu["new_name"] == new_name:
        #         new_name_again = input("姓名重复，请重新输入：")
        #         students[modify_index-1]["new_name"] = new_name_again
        #         break
        #     else:
        #         students[modify_index-1]["new_name"] = new_name

        students[modify_index - 1]["new_name"] = new_name
        students[modify_index - 1]["new_tel"] = new_tel
        students[modify_index - 1]["new_qq"] = new_qq

        print("此时的list为：", students)
        print("修改至临时list成功")

    else:
        print("超出查询范围")


def search_info():
    """查询学生信息"""
    search_name = input("请输入要查询的学生姓名：")
    for temp in load_data():
        if temp["new_name"] == search_name:
            print("找到目标学生 —— 姓名:%s,手机号:%s,qq:%s" % (temp['new_name'], temp['new_tel'], temp['new_qq']))
        else:
            print("查询失败")


def main():
    load_data()
    while True:
        # 1.打印工程
        menu()
        # 2.获取用户的选择
        key = input("请输入要进行的操作）：")

        if key == "1":
            add_new_info()
        elif key == "2":
            del_info()
        elif key == "3":
            search_info()
        elif key == "4":
            print_all_info()
        elif key == "5":
            save_data()
        elif key == "6":
            modify_info()
        elif key == "0":
            exit_flag = input("你确定要退出吗？(yes or no)")
            if exit_flag == "yes":
                break
            else:
                print("输入有误，请重新输入。。。")
                input("\n\n\n按回车键可以继续。。。")
                continue


if __name__ == '__main__':
    main()
