# -*- coding: utf-8 -*-
# @Time    : 2021/4/2 9:03
# @Author  : zj
# @FileName: class_hw07.py
# @Software: PyCharm

import random

"""
    1.字符串转为数字
"""
print(type(int("1000")))

"""
    2.列表排序sort(),reverse()
"""
numbers = [1, 5, 3, 9, 7]

# reverse() 对列表进行反转
numbers.reverse()
print(numbers)  # print——[7, 9, 3, 5, 1]

# sort()对列表进行排序
numbers.sort()
print(numbers)  # print——[1, 3, 5, 7, 9]

# sort(reverse=True) 对列表进行逆序排序
numbers.sort(reverse=True)
print(numbers)  # print——[9, 7, 5, 3, 1]
numbers.sort(reverse=False)
print(numbers)  # print——[1, 3, 5, 7, 9]

# reverse() 对列表进行反转，不排序
numbers.reverse()
print(numbers)  # print——[9, 7, 5, 3, 1]，在最近一次排序规则基础上再次进行排序

"""
    3.列表添加元素方法
"""
# ——list:append():只接受一个参数，支持任何数据类型，被追加的元素在list中保持原结构类型
list1 = []
print(list1)
list1.append('a')
print(list1)
list1.append(['a', 'b', 'c', 'd'])  # print——['a', ['a', 'b', 'c', 'd']]
print(list1)

# ——list.extend()：将一个列表中每个元素分别添加到另一个列表中，只接受一个参数
list2 = ['a', 'b', 'c']
list2.extend(['1', '2', '4', 'd'])  # print——['a', 'b', 'c', '1', '2', '4', 'd']
print(list2)

# ——list.insert(index,obj):(对象obj需要插入的索引位置,要插入到列表的对象）
list3 = [23, 'sdf', 'abc']
list3.insert(1, 'zj')  # print——[23, 'zj', 'sdf', 'abc']
print(list3)

"""
    4.对于列表什么是越界
    ——list[index],index超出范围
    ——list本身为空没有元素，list[0]即越界
    ——对列表切片的时候可以越界
"""
data = [1, 8, 5, 9, 7, 4, 5]
print(data[:100])  # 遍历到结束为止
print(data[7:])  # 返回空
# print(data[7])  # 出错，越界 IndexError: list index out of range

"""
    5.变量类型
    ——可变数据类型：
        List(列表),Dictionary(字典),Set(集合)
    ——不可变数据类型：
        Number(数字),String(字符串),Tuple(元组),Bool(布尔值)
"""

"""
    6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
"""

# 方式1.依次输入5个名字，存入list
listName = []
for i in range(0, 5):
    name = input("请输入第%d个学生姓名:" % (i + 1))
    listName.insert(i, name)
    print("第%d个学生名字中的第2个字母是:%s" % (i + 1, listName[i][1:2]))

for j in listName:
    print("%s学生名字的第2个字母：%s" % (j, j[1:2]))
    print("%s学生名字的第2个字母：%s" % (j, j[1]))

for h in range(len(listName)):
    print("第%d个学生名字的第2个字母：%s" % (h, listName[h][1:2]))

# 方式2：一次性输入5个名字保存为一个字符串，split()切割为list
names = input("请依次输入5个学生的名字，空格隔开：")
name = names.split()  # 切割符默认为所有的空格符，如果字符串中有\n也会被切割

print(names)
print(name)
print(type(name))
print(name[1])
print(name[1][1])

for n in name:
    print("%s的第二个字母为：%s" % (n, n[1]))

"""
    7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
"""
numbers = []
for num in range(0, 5):
    shu = random.randint(0, 100)
    numbers.insert(num, shu)
print(numbers)

for i in range(len(numbers)):
    numbers[i] = 100 - numbers[i]

print(numbers)

"""
    8.将下列两个列表合并，将合并后的列表升序并输出.
    list1 = [1,3,4,5,7]
    list2 = [0,66,8,9]
"""
list1 = [1, 3, 4, 5, 7]
list2 = [0, 66, 8, 9]

list3 = list1 + list2
list3.sort(reverse=False)
print(list3)

"""
    9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
"""
dic = {'姓名': input("请输入姓名："), '年龄[数字]': int(input("请输入年龄[数字]：")), '学号': input("请输入学号：")}
print(dic)

"""
    10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}
"""
dict1 = {'school': "lebo", 'date': 2018, 'address': "beijing"}

# 字典中的value不保证唯一性，因此根据值查出来的是一个list

# 方法1：
key_list = []
value_list = []
for key, value in dict1.items():
    # print(key, value)
    key_list.append(key)
    value_list.append(value)

chose_index = value_list.index('lebo')
chose_key = key_list[chose_index]
print("根据值查找对应的key为:", chose_key)

# 方法2：
dict2 = {'school': "lebo", 'date': 2018, 'address': "beijing", 'test': "lebo"}

for x in dict2:
    if dict2[x] == "lebo":
        print("key:", x)
        print("value:", dict2[x])

"""
    11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
"""
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
num1 = num[::-1]
he = 0
for i in range(len(num1)):  # i从0开始
    if (i % 2) == 1:
        he = he + num1[i]
    else:
        continue
print(he)

num2 = num[::-2] # 取从后向前（相反）的元素，每次索引位置+2
num2 = num[-2::-2] # 取从后第二位向前（相反）的元素，每次索引位置+2
