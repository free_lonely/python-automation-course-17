# -*- coding: utf-8 -*-
# @Time : 2021-04-15 22:06
# @Author : zj
# @File : class_hw12.py
# @Software: PyCharm

"""
    1.创建一个对象后默认调用__init__
    2.类是对象的抽象，对象是类的实例
    3.对象是由属性、方法组成
"""

"""
    创建学生类，通过学生类创建一个学生对象，分别调用学生的info,draw方法.
"""


class Student(object):

    def info(self):
        print("name：%s,age：%s,sex：%s" % (self.name, self.age, self.sex))

    def draw(self):
        print("%s会画画" % self.name)


stu = Student()
stu.name = "zj"
stu.age = 18
stu.sex = "男"
stu.info()
stu.draw()

"""
    描述：创建动物类，属性(使用魔法方法实现),通过动物类创建一个动物对象，分别调用动物的info和run方法.
    
"""


class Animal(object):
    # 实例属性
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def info(self):
        print("姓名:%s,年龄:%s,color:%s" % (self.name, self.age, self.color))

    def run(self):
        print("%s会跑" % self.name)


animal = Animal("cat", 8, "white")
animal.info()
animal.run()
