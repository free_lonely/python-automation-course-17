# -*- coding: utf-8 -*-
# @Time : 2021-04-11 10:18
# @Author : zj
# @File : class_hw09.py
# @Software: PyCharm

"""
    1.*args,**kwargs
    ——当含糊的参数不确定时，可以使用*args和**kwargs
    ——*args 将参数打包成list/tuple给函数体调用,没有key值
    ——**kwargs 将参数打包成dict给函数体调用，有key值
    ——使用顺序（arg,*args,*kwargs）
"""

"""
    2.运行结果
   def f(x, l=[]):
    for i in range(x):
        l.append(i * i)
    print(l) 
    
    ——f(2) 输出：l = [0,1]
    ——f(3) 输出：l = [0,1,4]
    ——f(2) + f(3,[3,2,1]) 输出：l = [3,2,1,0,1,4] ，l被f(3,[3,2,1])重新赋值为[3,2,1] 
    ——f(2) + f(3) 输出：l = [0,1,0,1,4]，f(3)执行前，l经执行f(2)已被赋值为[0,1]
"""

"""
    3.如何在一个函数内部修改全局变量
    ——global关键字声明全局变量，系统就会去找对应的全局变量
"""

"""
    4、用lambda函数实现两个数相乘
"""
f = lambda x, y: x * y
print(f(1, 5))

"""
    5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
"""
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
new_a = [x for x in a if x % 2 == 1]  # 要生成的元素放到关键字for的前面，把生成元素的表达式放在for关键字后面
print(new_a)
