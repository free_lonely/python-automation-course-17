"""
__project__ = 'wuyun'
__file_name__ = 'day6'
__author__ = 'lanting'
__time__ = '2021/4/4 17:46'
__product_name = PyCharm"""
# 1.循环0到10
# i=0
# while i<=10:
#     print(i)
#     i=i+1
# 2.实现逆序输出字符串
# name=input("请输入欢迎词")
# print(name[::-1])

# 3.python基础
#  str='a,hello'在字符串str中查找hello
# str='a,hello'
# a=str.find("hello")
# print(a)

# str='a,b,c,d'用逗号分隔str字符串，并保存到列表
str='abcd'
my_list = list(str)
print(my_list)


a='笔试题123A'
my_list1=list(a)
my_list1[3]="进"
my_list1[4]="行"
my_list1[5]="中"
print(my_list1)
ret = "".join(my_list1)
print(ret)
