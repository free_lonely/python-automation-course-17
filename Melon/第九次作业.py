# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/19 0019 11:13"
# @FileName :第九次作业.Set.py

# 1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
# *args：不定长参数之元组
# **kwargs：不定长参数之字典
# 为什么使用不定长参数：有时可能需要一个函数能处理比当初声明时更多的参数(即传的实参数不确定有多长), 这些参数叫做不定长参数

# 2、（先自己想，然后再实验，最后查百度）
def f(x, l=[]):
    for i in range(x):
        l.append(i * i)
    print(l)
f(2)               # 默认参数不填，取默认值 ，--》[0, 1]
f(3, [3, 2, 1])    # 默认参数添，取添加的值 --》[3, 2, 1, 0, 1, 4]
f(3)               # 默认参数不填，取默认值，因为f(2)在列表已经加了[0,1] --》[0, 1, 0, 1, 4]

# 3、如何在一个函数内部修改全局变量
c = 50
def test():
    global c       # 将局部变量修改为全局变量
    print('修改前值：%d' % c)
    c = 100        # 修改全局变量
    print('修改后的全局变量：%d' % c)
def test1():
    print('局部变量修改为全局变量，所有函数都可调用，值为%d' % c)
test()
test1()
# 4、用lambda函数实现两个数相乘
f = lambda a, b: a * b
print(f(2, 2))

# 5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
my_list = [val for val in a if val % 2 == 1]
print(my_list)
# for循环
my_list1 = []
for i in a:
    if i % 2 == 1:
        print(i)
        my_list1.append(i)
    else:
        print('%d--为偶数，不符合' % i)
print(my_list1)

if __name__ == "__main__":
    TheApp = 0
