# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/1 0001 9:42"
# @FileName :第六次作业.py

import random
# 1、用任意变成语言循环输出整数0到10
# a = int(input('请输入一个正整数：'))
# 方法1
# for i in range(a + 1):
#     print(i)
# 方法2
# i = 0
# while i < a + 1:
#     print(i)
#     i += 1

# 2、str = 'a,hello',在字符串中查找hello
str = 'a,hello'
str4 = 'hello'
for val in str4:
    i = str.find(val)
    print('%s字符串下标是：%-6d' % (val, i), end='')
# 3、str = 'a,b,c,d',用逗号分割str字符串并保存到列表
# str1 = 'a,b,c,d'
# str2 = str1.split('')
# print(str2)

# 4、将'笔试题123A'中123替换为'进行中';
# str3 = '笔试题123A'
# print(str3.replace('123', '进行中', str3.count('123')))

# 5、用任意语言实现逆序输出一个字符串，比如'欢乐逛欢迎您'，输出'您迎欢逛乐欢'
str5 = '欢乐逛欢迎您'
print(len(str5))
# 方法一
i = len(str5)
print(i)
while i > 0:
    print(str5[i - 1], end='')
    i -= 1
# 方法二：
str6 = str5[::-1]
print()
print(str6)
# 方法三
for i in range(len(str5), 0, -1):
    print(str5[i - 1], end='')
print()

# 6、一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
schools = [[], [], []]                                  # 定义一个列表来存储3个教室
teachers = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']     # 定义一个列表来存储8个老师
# 遍历8个老师
for teacher in teachers:
    index = random.randint(0, 2)
    schools[index].append(teacher)
# print(schools, end='\n')
for school in schools:
    print(school, end='')
    print(len(school))
    print('%s----教室的教师人数为：%d' % (school, len(school)))


