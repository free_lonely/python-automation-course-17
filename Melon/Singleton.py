# _*_ utf-8 _*_
__author__ = "LiuJian"
__date__ = "2021/7/5 0005 15:38"
# @FileName :Singleton.py

class Singleton(object):
    # 为了保护单例类，类属性可以定义为私有属性
    __instance = None
    __is_first = True

    def __new__(cls, *args, **kwargs):

        if not cls.__instance:
            cls.__instance = object.__new__(cls)
        return cls.__instance

    def __init__(self, uname):
        print('__init__')
        if Singleton.__is_first is True:
            self.name = uname
            Singleton.__is_first = False    # Singleton.is_first = not Singleton.is_first

    def __str__(self):
        print('__str__')
        return '名字为：%s' % self.name

    def __del__(self):
        print('__del__')

xiaoli = Singleton('小李')
print(xiaoli)
xiaoming = Singleton('小明')
print(xiaoming)


if __name__ == "__main__":
    TheApp = 0
