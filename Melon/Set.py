# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/9 0009 10:40"
# @FileName :Set.py

# 1、集合的定义    无序，可变数据类型
# my_set = set()
# print(type(my_set))

# 2、集合的添加
my_set = {1, 2, 3, 4}
# 添加元素
# my_set.add(666)
# 是把要传入的元素拆分，做为个体传入到集合中（重复的元素只存一个）
# my_set.update('04556666')
# print(my_set)     # ------>{1, 2, 3, 4, '5', '0', '4', 666, '6'}

# 3、集合删除
# (1)remove：使用remove删除集合中的元素 如果有 直接删除 如果没有 程序报错
# my_set.remove(0)
# (2)pop:使用pop删除是随机删除集合中的元素 如果set没有元素讲程序报错
# my_set.pop()
# (3)discard:使用discard删除 如果元素存在 直接删除 如果元素不存在 不做任何操作
my_set.discard(3)
# print(my_set)

# 交集和并集( & 和 | ),自动去重
set1 = {1, 2, 3, 4}
set2 = {3, 4, 5, 6}
# 交集
my_set1 = set1 & set2
# 并集
my_set2 = set1 | set2
# print(my_set2)
print(set1 & set2)  # 使用  "&"  运算求a与b的交集，输出：{3, 4}
print(set1 | set2)  # 使用  "|"  运算求a与b的并集，输出：{1, 2, 3, 4, 5, 6}
print(set1 - set2)  # 使用  "-"  运算求a与b的差(补)集： 求b中有而a中没有的元素，输出：{1, 2}
print(set2 - set1)  # 使用  "-"  运算求a与b的差(补)集： 求a中有而b中没有的元素，输出：{5, 6}
print(set1 ^ set2)  # 使用  "^"  运算求a与b的对称差集，输出：{1, 2, 5, 6}

# 集合自动去重
my_list = [3123, 4124, 414, 41, 414, 414, 41, 414, 4, 4, 45]
my_set3 = set(my_list)
print(my_set3)
my_list1 = list(my_set3)
print(my_list1)

if __name__ == "__main__":
    TheApp = 0
