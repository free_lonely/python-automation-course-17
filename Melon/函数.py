# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/13 0013 15:10"
# @FileName :函数.py

# def my_func():
#     print("                            _ooOoo_  ")
#     print("                           o8888888o  ")
#     print("                           88  .  88  ")
#     print("                           (| -_- |)  ")
#     print("                            O\\ = /O  ")
#     print("                        ____/`---'\\____  ")
#     print("                      .   ' \\| |// `.  ")
#     print("                       / \\||| : |||// \\  ")
#     print("                     / _||||| -:- |||||- \\  ")
#     print("                       | | \\\\\\ - /// | |  ")
#     print("                     | \\_| ''\\---/'' | |  ")
#     print("                      \\ .-\\__ `-` ___/-. /  ")
#     print("                   ___`. .' /--.--\\ `. . __  ")
#     print("                ."" '< `.___\\_<|>_/___.' >'"".  ")
#     print("               | | : `- \\`.;`\\ _ /`;.`/ - ` : | |  ")
#     print("                 \\ \\ `-. \\_ __\\ /__ _/ .-` / /  ")
#     print("         ======`-.____`-.___\\_____/___.-`____.-'======  ")
#     print("                            `=---='  ")
#     print("  ")
#     print("         .............................................  ")
#     print("                  佛祖镇楼                  BUG辟易  ")
#     print("          佛曰:  ")
#     print("                  写字楼里写字间，写字间里程序员；  ")
#     print("                  程序人员写程序，又拿程序换酒钱。  ")
#     print("                  酒醒只在网上坐，酒醉还来网下眠；  ")
#     print("                  酒醉酒醒日复日，网上网下年复年。  ")
#     print("                  但愿老死电脑间，不愿鞠躬老板前；  ")
#     print("                  奔驰宝马贵者趣，公交自行程序员。  ")
#     print("                  别人笑我忒疯癫，我笑自己命太贱；  ")
#     print("                  不见满街漂亮妹，哪个归得程序员？")
# for i in range(1, 11):
#     print(i)
#     my_func()
# 1、函数定义
def my_func(a, b):
    '''
    '''''':回车，函数注释
    这个是加法运算
    :param a: 
    :param b: 
    :return: 
    '''
    ret = a + b
    return ret


# help(my_func)     # 函数帮助
print(my_func(10, 20))


# 小总结
# 定义时小括号中的参数，用来接收参数用的，称为 “形参”
# 调用时小括号中的参数，用来传递给函数用的，称为 “实参”

# 2、参数
# 默认参数(缺省参数)：调用函数时，缺省参数的值如果没有传入，则取默认值，sex缺省值为：男
def my_func1(name, sex='男'):
    print('name对应的值是：%s' % name)
    print('sex对应的值是：%s' % sex)


# my_func1('哈哈', '女')

# 不定长参数之元组
def my_func2(a, b, *args):
    print('a的值是：%d ' % a)
    print('b的值是：%d ' % b)
    print(args)


my_func2(1, 2, 3, 5, 6, 55, 54)


# 不定长参数之字典
def my_func3(a, b, **kwargs):
    print('a的值是：%d ' % a)
    print('b的值是：%d ' % b)
    print(kwargs)


my_func3(1, 2, m=3, n=5, f=6, t=55, r=54)


# 小结：(1)如果很多个值都是不定长参数，那么这种情况下，可以将缺省参数放到 *args的后面， 但如果有**kwargs的话，**kwargs必须是最后的
#      (2)加了星号（*）的变量args会存放所有未命名的变量参数，args为元组
#      (3)而加**的变量kwargs会存放命名参数，即形如key=value的参数， kwargs为字典.

# 3、变量
# 局部变量：  (1)局部变量，就是在函数内部定义的变量
#           (2)其作用范围是这个函数内部，即只能在这个函数中使用，在函数的外部是不能使用的
#           (3)因为其作用范围只是在自己的函数内部，所以不同的函数可以定义相同名字的局部变量
#           (4)局部变量的作用，为了临时保存数据需要在函数中定义变量来进行存储
#           (5)当函数调用时，局部变量被创建，当函数调用完成后这个变量就不能够使用了
def test1():
    num = 1  # --->局部变量
    print('变量修改前的值：%d' % num)
    num = 10  # --->局部变量修改
    print('变量修改后的值：%d' % num)


def test2():
    num = 11  # --->局部变量
    print(num)


# 调用函数
# test1()
# test2()

# 全局变量：  (1)在函数外边定义的变量叫做全局变量
#           (2)全局变量能够在所有的函数中进行访问
num1 = 100  # --->全局变量


def test3():
    print(num1)  # 虽然没有定义变量num1但是依然可以获取其数据


def test4():
    print(num1)  # 虽然没有定义变量num1但是依然可以获取其数据


# 调用函数
# test3()
# test4()

# (3)全局变量和局部变量名字相同问题：当函数内出现局部变量和全局变量相同名字时，函数内部中的 变量名 = 数据 此时理解为定义了一个局部变量，而不是修改全局变量的值
num2 = 100  # --->全局变量


def test5():
    num2 = 150
    print('test5修改前的输出结果:%d' % num2)
    num2 = 200
    print('test5修改后的输出结果:%d' % num2)


def test6():
    print('test6的输出结果:%d' % num2)


# 调用函数
# test5()
# test6()
# 修改全局变量
num3 = 66  # --->全局变量

def test7():
    global num3  # --->将局部变量修改为全局变量
    print('test7修改前的输出结果:%d' % num3)
    num3 = 88
    print('test7修改后的输出结果:%d' % num3)


def test8():
    print('test8的输出结果:%d' % num3)

# 调用函数
test7()
test8()


# (4)如果在一个函数中需要对多个全局变量进行修改，那么可以使用
#      1)可以使用一次global对多个全局变量进行声明
#      global a, b
#      2)还可以用多次global声明都是可以的
#      global a
#      global b

# 4、拆包、交换变量的值
# <1> 对返回的数据直接拆包  拆包时要注意，需要拆的数据的个数要与变量的个数相同，否则程序会异常
# (1)对元组进行拆包
def get_info():
    height = 183
    weight = 90
    age = 30
    return height, weight, age


result = get_info()
print(result)
my_height, my_weight, my_age = get_info()
print(my_height)
print(my_weight)
print(my_age)


# (2)对列表进行拆包
def get_info1():
    height = 183
    weight = 90
    age = 30
    return [height, weight, age]


result = get_info1()
print(result)
[my_height, my_weight, my_age] = get_info1()
print(my_height)
print(my_weight)
print(my_age)


# (3)对字典进行拆包
def get_info2():
    height = 183
    weight = 90
    age = 30
    return {'height': 183, 'weight': 90, 'age': 30}


result = get_info2()
print(result)
my_height, my_weight, my_age = get_info2().items()
print(my_height)
print(my_weight)
print(my_age)
# <2>交换变量的值
# 第1种方式
# a = 4
# b = 5
# c = 0
#
# c = a
# a = b
# b = c
#
# print(a)
# print(b)

# 第2种方式
# a = 4
# b = 5
# a = a+b  # a=9, b=5
# b = a-b  # a=9, b=4
# a = a-b  # a=5, b=4
# print(a)
# print(b)

# 第3种方式
a, b = 4, 5  # a=4, b=5
a, b = b, a  # a=b=5, b=a=4
print(a)
print(b)

# 5、匿名函数
# (1)匿名函数定义
f = lambda: print('hello')  # 无参无返回值
f()  # 匿名函数需要用变量来调用
f1 = lambda m, n: m - n  # 有参数有返回值
print(f1(n=1, m=4))

# 6、列表推导式
my_list = [i for i in range(0, 11)]
print(my_list)

my_list1 = [(i, j) for i in range(0, 3) for j in range(1, 4)]
print(my_list1)

my_list3 = [i for i in range(0, 11) if i % 2 == 0]
print(my_list3)


# 7、递归函数
def factorial(num):
    if num > 1:
        result = num * factorial(num - 1)
    else:
        result = 1
    return result
print(factorial(3))

# 8、文件操作
# 默认文件权限为只读：r:以只读方式打开文件。文件的指针将会放在文件的开头。这是默认模式 ;
# w:打开一个文件只用于写入。如果该文件已存在则将其覆盖。如果该文件不存在，创建新文件
# a:打开一个文件用于追加。如果该文件已存在，文件指针将会放在文件的结尾。也就是说，新的内容将会被写入到已有内容之后。如果该文件不存在，创建新文件进行写入
# 文件写入
# f = open('lebo.py', 'a', encoding='utf-8')
# f.write('乐搏')
# f.close()
# 文件读取
# f = open('lebo1.py', 'r', encoding='utf-8')
# ret = f.read()         # 读取全部，字符串类型
# ret = f.readline()     # 读取第一行
# ret = f.readlines()      # 读取全部，列表类型
# print(ret)
# f.close()
# rb:以二进制格式打开一个文件用于只读。文件指针将会放在文件的开头。这是默认模式。
# wb:以二进制格式打开一个文件只用于写入。如果该文件已存在则将其覆盖。如果该文件不存在，创建新文件。
# ab:以二进制格式打开一个文件用于追加。如果该文件已存在，文件指针将会放在文件的结尾。也就是说，新的内容将会被写入到已有内容之后。如果该文件不存在，创建新文件进行写入。
# f = open('lebo1.py', 'ab')
# f.write('乐搏学院'.encode(encoding='utf-8'))
# f.close()

# f = open('lebo1.py', 'rb')
# ret1 = f.read().decode(encoding='utf-8')
# print(ret1)
# f.close()

# 文件备份操作
# new_file = 'lebo[副件]lebo.py'
# old_f = open('lebo.py', 'r', encoding='utf-8')
# ret2 = old_f.readlines()
# new_f = open('lebo[副件]lebo.py', 'w', encoding='utf-8')
# for val in ret2:
#     new_f.write(val)
# old_f.close()
# new_f.close()

# 对文件夹的操作 ,需要先导入os模块
import os

print(os.listdir())
print('-' * 100)
# 修改文件夹名称
# os.rename('lebo.py', 'lebo666.py')
# 创建文件夹
# os.mkdir('test')
# 查看当前目录
print(os.getcwd())
# 修改当前目录
os.chdir(r'C:\Users\Administrator\Desktop')  # 为防止’\‘被转义，需在修改的目录前加 r
print(os.getcwd())
# 在修改后的目录下创建文件夹
# os.mkdir('00000')
# 删除文件夹   在路径后加文件名
os.chdir(r'E:\python自动化\python-automation-course-17\Melon')
print(os.getcwd())
# os.remove(r'E:\python自动化\python-automation-course-17\Melon\Module\test2.py')
# os.rmdir(r'E:\python自动化\python-automation-course-17\Melon\test1111')

# 对文件操作，自动关闭对象，无需再手动关闭
# with open('lebo1.py', 'r', encoding='utf-8') as ff:
#     for val in ff.readlines():
#         # .strip():去除字符串空格
#         print(val.strip())

# 批量修改文件名
# (1)指定批量修改的文件夹
folder = r'E:\python自动化\python-automation-course-17\Melon\Module'
print(folder)
# (2)获取指定文件夹下所有的文件名
dir_list = os.listdir(folder)
print(dir_list)
os.chdir(r'E:\python自动化\python-automation-course-17\Melon\Module')
# (3)遍历出指定文件夹下所有的文件名
funFlag = 3  # 1：添加   2：删除
for name in dir_list:
    print(name + ',')
    if funFlag == 1:
        new_name = '[改]-' + name
        os.rename(name, new_name)
    elif funFlag == 2:
        num_1 = len('[改]-')
        num_2 = len(name)
        new_name = name[num_1:num_2:1]
        os.rename(name, new_name)


if __name__ == "__main__":
    TheApp = 0
