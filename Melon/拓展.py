# _*_ utf-8 _*_
__author__ = "LiuJian"
__date__ = "2021/7/6 0006 10:44"
# @FileName :拓展.Set.py

from decimal import Decimal
from decimal import getcontext

from timeit import Timer

a = 3.14
b = 2.56

c = a + b
print(c)

getcontext().prec = 4      # 保留位数设置
c = Decimal(a) + Decimal(b)

print(c)


def t1():
    l = []
    for i in range(0, 101):
        l.append(i)

def t2():
    l2 = [i for i in range(0, 101)]

def t3():
    l3 = []
    for i in range(0, 101):
        l3 = l3 + [i]

def t4():
    l4 = list(range(0,101))

def t5():
    l5 = []
    for i in range(0, 101):
        l5.insert(0, i)

t1()
t3()
t5()

time1 = Timer('t1()', 'from __main__ import t1')
print('T1', time1.timeit(number=1000), 'second')

time2 = Timer('t2()', 'from __main__ import t2')
print('T2', time2.timeit(number=1000), 'second')

time3 = Timer('t3()', 'from __main__ import t3')
print('T3', time3.timeit(number=1000), 'second')

time4 = Timer('t4()', 'from __main__ import t4')
print('T4', time4.timeit(number=1000), 'second')

time5 = Timer('t5()', 'from __main__ import t5')
print('T5', time5.timeit(number=1000), 'second')

if __name__ == "__main__":
    TheApp = 0

