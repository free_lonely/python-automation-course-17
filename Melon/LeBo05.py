# _*_ utf-8 _*_
__author__ = "LiuJian"
__data__ = "2021/4/1 0001 9:41"
# @FileName :LeBo05.py

# 列表格式： 列表是可变数据类型、  列表元素可以是不同数据类型
myList = [123, 131, 23243, 41224, 323]
# myList1 = [123, '131', 23.243, True, 323]
# print(myList[0])
# print(myList[1])
# print(myList[2])
# 列表循环遍历：for循环   while循环
# for value in myList:
#     print(value)
# print(len(myList))
# i = 0
# while i < len(myList):
#     print(myList[i])
#     i += 1

# 列表的相关操作 增删改查
# 添加元素 append()   extend()   insert()
myList2 = [1213, 313, 131, 4141, 1, 134]
# append():如果添加的元素也是一个列表，那么把这个列表当做一个整体添加到原列表中 list.append()
# temp = input("请输入:")
# myList2.append(temp)
# for value in myList2:
#     print(value)

# extend():如果添加的元素也是一个列表，那么把这个列表中元素逐一添加到原列表中   list.extend()
# temp = ['haha', 'hehe', 456, True]
# myList2.extend(temp)
# 输出：1213 313 131 4141 1 134 haha hehe 456 True

# insert():在列表指定元素位置前插入元素                                  list.insert()
# myList2.insert(0, 'hehe')
# 输出：hehe 1213 313 131 4141 1 134 haha hehe 456 True

# 修改元素
# myList2[0] = 'hehhe'

# 查询元素
# in：存在
if 123 in myList2:
    print('zai')
else:
    print("buzai")
# not in：不存在
if 123 not in myList2:
    print('shide')
else:
    print('buzai')

# index, count
# myList2.index('1', 0, len(myList2))    # 在0 - 最后一下标之间查找’1‘，不存在输出报错
# print(myList2.index('hehe', 0, len(myList2)))    # 在0 - 最后一下标之间查找’1‘，不存在输出报错
# print(myList2.count(1))   # 查找1的个数

# 删除元素(del, pop, remove)
# del:根据下标进行删除
del myList2[0]

# pop:默认删除最后一个元素
print(myList2.pop(3), end='\n')

# remove:根据元素的值进行删除
myList2.remove(313)

for value in myList2:
    print(value)

# 排序元素(sort, reverse)
# sort方法是将list按特定顺序重新排列，默认为由小到大，参数reverse=True可改为降序，由大到小。
# reverse方法是将list逆置

testList = [1, 2, 3, 15, 4, 5, 6]
testList.sort(reverse=True)             # --->[15, 6, 5, 4, 3, 2, 1]
# testList.sort()                       # --->[1, 2, 3, 4, 5, 6, 15]
# testList.reverse()                    # --->[6, 5, 4, 15, 3, 2, 1]
print(testList)






