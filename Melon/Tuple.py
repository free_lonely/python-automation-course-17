# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/6 0006 15:17"
# @FileName :Tuple.py


# 1、元组：不可变数据类型  Tuple,元组的出现是为了数据的安全
# 元组定义：
# a = tuple()
# a = (123, )
a = ()
print(type(a))
# 2、元组查询
b = (1, 2, 3, 45, 6)
print(len(b))

if 1 in b:
    print('在')
else:
    print('不在')

print(b.index(2))  # 查询其下标
print(b[1])  # 查询某一元素

# 3、元组修改(需要将元组强转为list)
my_list = list(b)
my_list[3] = 33
print(my_list)
my_tup = tuple(my_list)
print(my_tup)

'''
字典：1、key 和 value一一对应
     2、key是唯一的
     3、key的数据类型：不可变数据类型
     4、字典的数据类型：可变数据类型(增删改查)
     5、字典是无序的
'''
# 字典类型定义：
my_dict = {'key': 'value', 'name': '123'}
# dict = {}
dict = dict()
print(dict)
print(type(dict))
# 1、字典增加/修改
# my_dict1 = my_dict['name'] = 'xiaoliu'      # key存在，修改value
# my_dict2 = my_dict['password'] = '1234'     # key不存在，添加新键值对
# print(my_dict2)
# print(my_dict)

# 2、查询字典
len = len(my_dict)
print(len)
print(my_dict['name'])
# 查询所有的key键
print(my_dict.keys())
# 查询所有的值value
print(my_dict.values())
# 查询所有的键值对
print(my_dict.items())
# ---> dict_items([('key', 'value'), ('name', '123')])   # 包含列表、元组、字符串数据类型，通过元组类型表示key,value
# 默认遍历所有的key
for key in my_dict:
    print(key)
# 遍历所有的key, value
for key, value in my_dict.items():
    print(key, value)

# 3、删除某个元素和删除整个字典
# del my_dict['name']              # 根据键key删除某个元素
# my_dict3 = my_dict.clear()         # 删除整个字典
# print(my_dict3)

# 4、etdefault和get函数使用
# my_dict4 = my_dict.setdefault('key', '6666')   # 字典中有对应的key值，设置value值无效，仍显示原来的value值
# print(my_dict4)
# my_dict5 = my_dict.setdefault('key1', '6666')   # 字典中没有对应的key值，想字典中插入新的key-value元素
# print(my_dict5)

print(my_dict.get('key1'))           # 通过key获取对应的value值，key不存在，输出None


# 函数(遍历列表的值和下标)
# my_list = ['a', 'b', 'c', 'd']
# for val, index in enumerate(my_list):
#     print('%d 下标对应的值为：%s' % (val, index))


if __name__ == "__main__":
    TheApp = 0
