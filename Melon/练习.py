# _*_ utf-8 _*_
__author__ = "LiuJian"
__data__ = "2021/3/31 0031  8:46"
# @FileName :练习.Set.py

name = 'lebozbidonghua17肥嘟嘟17'
# 切片：
# 语法：[起始:结束:步长]
# print(name[2:9:3])    # 从索引为2开始，为9结束，每隔3个打印出变量d
# print(name[:])        # 全部打印
# print(name[:-3])      # 索引从0开始到-3结束，步长为1打印
# print(name[-1:-4:-2])    # 索引从-1开始到索引为-4结束，步长为-2打印
# print(name[::-1])     # 从右往左全部打印

# 内置函数：
# 切记：查找 find()、rfind()、index()、rindex()
# print(name.find('a'))           # 从左到右查找name中字符为a的下标
# print(name.rfind('a'))          # 从右到左查找name中字符为a的下标
# print(name.find('b', 2, 6))       # 从左到右查找name中下标从2到6之间的字符‘b’
# print(name.rfind('b', 2, 6))      # 从右到左查找name中下标从2到6之间的字符‘b’
# print(name.find('0'))            # 查找0，name中没有0，会输出-1
# print(name.index('0'))           # 查找0，name中没有0，会报错不存在找不到
# print(name.index('b', 2, 6))      # 从左到右查找name中下标从2到6之间的字符‘s’
# print(name.rindex('b', 2, 6))     # 从右到左查找name中下标从2到6之间的字符‘s’

# 计数 count()
# print(name.count('o'))  # 查询name中字符o出现过几次
# print(name.count('2'))  # 查询不存在的字符时，输出0
# 替换 replace()
# print(name.replace('17', '17班'))  # 把name中17换成17班
print(name.replace('17', 'ban', name.count('17')))   # 把name中的17替换成ban，替换两次（从左到右）
# 切割 split()
# print(name.split('o'))    # 把name按o切割

# 了解：
# print(name.capitalize())  # 把第一个字母大写
# print(name.title())       # 每个单词的首字母大写
# print(name.swapcase())    # 大小写字母转换后生成的新字符串
# print(name.startswith('ab'))   # 查询以‘ab’开头的
print(name.endswith('ab'))     # 查询以‘ab’结尾的
# print(name.upper())     # 所有单词大写
# print(name.lower())     # 所有单词小写
# print(name.isupper())   # 是否都大写
# print(name.islower())   # 是否都小写

# 常用：just() 填充
str = 'haha'
# print(str.ljust(4, '1'))      # 从左到右填充，填充的位数小于等于字符串原位数，字符串不变
# print(str.rjust(4, '1'))     # 从右到左填充，填充的位数小于等于字符串原位数，字符串不变
# print(str.ljust(5, '1'))      # 从左到右填充
print(str.rjust(8, '1'))     # 从右到左填充

# center() 填充两边,原字符串在中间
# print(str.center(5, '1'))  # 打印出1haha1
# print(str.center(6, '1'))  # 打印出1haha1（先填充左边再填充右边）
# print(str.center(7, '1'))  # 打印出11haha1（先填充左边再填充右边）

# strip() 去除空格
str1 = ' he he '
print(str1.lstrip())          # 去除左边空格
print(str1.rstrip())          # 去除右边空格
print(str1.replace(' ', ''))  # 去除中间空格(即replace()方法)

# join()： 连接字符串数组。将字符串、元组、列表中的元素以指定的字符(分隔符)连接生成一个新的字符串
str3 = 'nihaoaxiexieha'
print('-'.join(str3))

# （面试题）给定一个字符串aStr，返回使用空格或者'\t'分割后的倒数第二个子串
str2 = 'haha nihao a \t heihei  \t  woshi  nide  \t hao \npengyou'
print(str2.split('\t\n'))  # 错误的处理
print(str2.split()[6])

if __name__ == "__main__":
    TheApp = 0
