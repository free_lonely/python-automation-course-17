# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/12 0012 13:29"

# @FileName :第八次作业.Set.py
import random

# 1、python2.x和python3.x默认的编码格式分别是什么？
# Python 2.x 默认使用 ASCII 编码格式
# Python 3.x 默认使用 UTF-8 编码格式，也有说是unicode的

# 2、将'hello_new_world'按'_'切割
# str = 'hello_new_world'
# str1 = str.split('_')
# print(str1)

# 3、将数字1以'0001'的格式输出到屏幕
num = 1
print('%04d' % num)

# 4、字典a={1:1},是否正确?
# 正确
# a = {1: 1}
# print(type(a))

# 5、请合并列表a=[1,2,3,4]和列表b=[5,6,7,8]
a = [1, 2, 3, 4]
b = [5, 6, 7, 8]
# # 方法一
# c = a + b
# print(c)
# # 方法二
# a.extend(b)   # 逐一加入列表后
# print(a)
# # 方法三
# a.append(b)    # 整体加入列表
# print(a)
# # 方法四
a.insert(3, 10)  # insert(下标, 插入的数据)
print(a)  # --->[1, 2, 3, 10, 4]

# 6、列表a,请写出实现正序排列,倒序排列,逆序排列的内置方法。
a = [1, 5, 3, 4, 2]
# 正序
# a.sort(reverse=False)
# print(a)      # -->[1, 2, 3, 4, 5]
# 倒序
# a.sort(reverse=True)
# print(a)      # -->[5, 4, 3, 2, 1]
# 逆序
a.reverse()
print(a)  # -->[2, 4, 3, 5, 1]

# 7、字典d={“k”:1,“v:2},请写出 d.items结果。
# d = {'k': 1, 'v': 2}
# d.items()
# print((d.items()))             # --->dict_items([('k', 1), ('v', 2)])
# print(type(d.items()))
# for val in d.items():
#     print(type(val), end='')   # ---》<class 'tuple'>
#     print(val)                 # ---》('k', 1) ('v', 2)

# 8、复杂列表[{'k': 1, 'v': 2}, {'k': 12, 'v': 22}, {'k': 13, 'v': 32}],请用内置方法写出按k倒序排列的代码
li = [{'k': 1, 'v': 2}, {'k': 12, 'v': 22}, {'k': 13, 'v': 32}]
li.sort(key=lambda x: x['k'], reverse=True)  # lambda x: x['k']相当于一个参数，针对于sort方法，key后不可以接函数
print(li)
# 9、集合s=set(1,2,3,4),d=set(2,4,9,0,3),请用内置方法写出他们的并集,交集,差集
# s = set([1, 2, 3, 4])
# d = set([2, 4, 9, 0, 3])
# # 交集
# set1 = s & d
# print(set1)
# # 并集
# set1 = s | d
# print(set1)
# # 差集
# set1 = s - d
# print(set1)

# 10、如何把列表l=[“a”,“b”]里的各项,转为字符串并用逗号,连接。
l = ['a', 'b', 'c', 'd', 'e']
# 方法一
# str = ''
# for val in l:
#     str += val
#     print(str)
# print(','.join(str))
# 方法二
print(','.join(l))
# 请随机输出大于0小于1的数;请随机输出一个100以内的整形。
# 随机输出大于0小于1的数
print(random.random())  # 随机输出大于0小于1的数
# 随机输出一个100以内的整形
print(random.randint(0, 100))  # 随机输出一个100以内的整形

# 11.  长度未知的元组a=(1,3,4,5,6,…),请随机输出n项
a = (1, 3, 4, 5, 6, 7, 8, 9)
list_a = list(a)
print(a)
for val in range(0, len(a)):
    print('下标%d:' % val, end='')
    print('值%d' % (a[random.randint(0, len(a) - 1)]))


# 说说map、filter的作用
# map
def func(x):
    return x + 1
print(list(map(func, [1, 2, 3, 4])))
# filter
def func1(x):
    return x > 4
print(list(filter(func1, [1, 2, 33, 4, 5])))

# 12、 判断下列描述是否正确,如果有错误,请指出错误的地方并改正
# ①字典:具有键值映射关系,无序,字典的键不能重复并且可以是任意数据类型。
#  字典的键不能重复，只能是不可变数据类型
# ②元组:不能修改,无序,不能索引切片,当元组中只有一个元素时,需要在元素的后面加逗号。
# 元组有序，可以索引切片
# ③列表:元素可以重复,有序,不可以反向索引,元素可以是任意类型。
# 可以反向索引
# ④集合:元素不可以重复,可以索引,a={}声明了一个空集合。
# 无序，没有下标，不能索引。a = {} 是空字典 ，空集合是 a = set()

# 13.  一行代码输出1-100和
print(sum(range(1, 101)))

# 14. 使用random.random方法实现随机输出范围在[25, 60)中的浮点数
# my_mum = random.randint(25, 60) + random.random()
# print(my_mum)
# 方法二
my_num = random.random()
print(my_num)
if (my_num >= 0.25) and (my_num < 0.6):
    print(my_num * 100)
else:
    print('%d  不在25-60之间' % (my_num * 100))

# 15.一个list对象a=[1,2,4,3,2.2.4],需要去掉里面的重复值
my_list = [1, 2, 4, 3, 2, 2, 4, 6, 8, 0]
my_set = set(my_list)
my_list = list(my_set)
print(my_list)

if __name__ == "__main__":
    TheApp = 0
