# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/7 0007 10:15"

# @FileName :第七次作业.py

import random

'''
1.下列将字符串"100"转换为数字100的正确的是( A )                                      
 A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
2.下列程序执行结果是( A s)                                                 
 numbers = [1，5，3，9，7]                                                                         
 numbers.sort(reverse=True)                                                                               
 print(numbers)                                                                                      
 A、[9，7，5，3，1]                                                                                                      
 B、[1，3，5，7，9]                                                                                      
 C、1，3，5，7，9                                                                                        
 D、9，7，5，3，1
'''
my_list = ['a', 'b', 'c', 'd']
# 3.如何在列表中添加一个元素
# my_list.append(['1', '2'])
# print(my_list)
# my_list.extend(['3', '4'])
# print(my_list)
# my_list.insert(0, '666')
# print(my_list)

# 4.对于列表什么是越界
# 列表下标索引超出列表长度

# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 可变数据类型：list、dict
# 不可变数据类型：str、tuple、int、float、bool

# 6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
# my_list1 = ['', '', '', '', '']
# for i in range(0, len(my_list1)):
#     my_list1[i] = input('请输入名字：')
#     # print(my_list1)
# print(my_list1)
# for name in my_list1:
#     print(name[1])

# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
# 1)先定义一个空列表
my_list2 = []
# 2)随机生成五个数字
# for i in range(0, 5):
#     shu = random.randint(0, 100)
#     shu1 = - shu
#     my_list2.append(shu1)
#     print(shu)
# print(my_list2)
# 8.将下列两个列表合并，将合并后的列表升序并输出.
# list1 = [1, 3, 4, 5, 7]
# list2 = [0, 66, 8, 9]
# list1.extend(list2)
# print(list1)
# # 默认reverse=False升序，reverse=True降序
# list1.sort()
# print(list1)
# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
# my_dict = {}
# # print(my_dict)
# for i in range(0, 1):
#     name = input('请输入姓名信息：')
#     age = input('请输入年龄信息：')
#     number = input('请输入学号信息：')
#     my_dict.setdefault('name', name)
#     my_dict.setdefault('age', age)
#     my_dict.setdefault('number', number)
# print(my_dict)

# 10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1 = {'school': 'lebo', 'date': 2018, 'address': 'beijing'}
for key, value in dict1.items():
    print('key:%s------value:%s' % (key, value))
    if value == 'lebo':
        print(key)

# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
num1 = num[::-1]
# print(len(num1))
print(num1)
sum = 0
for i in range(0, len(num1)):
    # print(i)
    if i % 2 == 0:  # 遍历偶数下标位置
        print(i)
        sum += num[i]
print(sum)

if __name__ == "__main__":
    TheApp = 0
