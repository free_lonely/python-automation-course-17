# _*_ utf-8 _*_
__author__ = "LiuJian"
__data__ = "2021/3/28 0028  23:22"
# @FileName :LeBo04.py

# 1、使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
# * *
# *
# i = 1
# while i <= 9:
#     if i <= 5:
#         j = 1
#         while j <= i:
#             print('*', end='')
#             j += 1
#     else:
#         j = 9 - i + 1
#         while j >= 1:
#             print('*', end='')
#             j -= 1
#     print("")
#     i += 1
'''
#九九乘法表
#1、i代表行，j 代表列
#2、由行控制列
i = 1
while i <= 9:
    j = 1
    while j <= i:
        print('%d * %d = %-4d' % (i, j, i * j), end='')
        j += 1
    print()
    i += 1
'''

# 2、break和continue的区别：
#     break/continue:只能用在循环中，除此以外不能单独使用
#     break/continue:在嵌套循环中，只对最近的一层循环起作用
#     break:用来结束break所在的循环
#     continue:用来结束本次循环，紧接着执行下次循环
# 3、按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
str1 = '1234567890'
# 1.	截取字符串的第一位到第三位的字符
print(str1[0:3:1])
# 2.	截取字符串最后三位的字符
print(str1[7::1])
# 3.	截取字符串的全部字符
print(str1[::1])
# 4.	截取字符串的第七个字符到结尾
print(str1[6::])
# 5.	截取字符串的第一位到倒数第三位之间的字符
print(str1[:8:])
# 6.	截取字符串的第三个字符
print(str1[2])
# 7.	截取字符串的倒数第一个字符
print(str1[-1])
# 8.	截取与原字符串顺序相反的字符串
print(str1[::-1])
# 9.	截取字符串倒数第三位与倒数第一位之间的字符
print(str1[-3:-1:])
# 10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
print(str1[1:-1:2])

# 如何既打印出字符串内容，又显示下标
str = 'A乐搏学院17期自动化'
for i in range(len(str)):
    print('%s--下标是--%-4d' % (str[i], i), end='')
    print(str[i])
print(str.startswith('乐'))  #
print(str.swapcase())
