# _*_ utf-8 _*_
__author__ = "LiuJian"
__date__ = "2021/7/26 0026 16:38"
# @FileName :OperationDataBase.py

import pymysql
import configparser

class OperationDataBase(object):

    def __init__(self,db='db'):
        # 创建配置文件对象
        self.config = configparser.ConfigParser()
        # 读取配置文件内容
        self.config.read(r'E:\python自动化\python-automation-course-17\Melon\Module\config.ini')
        # 获取配置文件里连接属性
        self.host = self.config.get(db, 'db_host')
        self.user = self.config.get(db, 'db_user')
        self.password = self.config.get(db, 'db_password')
        self.port = eval(self.config.get(db, 'db_port'))
        self.database = self.config.get(db, 'db_database')
        self.charset = self.config.get(db, 'db_charset')
        # 创建连接
        self.conn = pymysql.connect(host=self.host,
                                        user=self.user,
                                        password=self.password,
                                        port=self.port,
                                        database=self.database,
                                        charset=self.charset)
        print(self.conn)
    def check_sql(self, sql):

        try:

            self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

            count = self.cur.execute(sql)

            print('查询出%d条数据' % count)

            for i in range(0, count):

                ret = self.cur.fetchone()
                print(ret)

        except Exception as e:
            print('查询出错：%s', e)
        finally:
            self.cur.close()

if __name__ == "__main__":

    od = OperationDataBase('db1')

    sql = 'select * from melon;'

    od.check_sql(sql)

# xiugai-hebingfenzhi
# 修改