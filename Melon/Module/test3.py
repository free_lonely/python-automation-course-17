# _*_ utf-8 _*_
__author__ = "LiuJian"
__date__ = "2021/7/26 0026 14:23"
# @FileName :test3.py

import Melon.Module.DataBase as DB


if __name__ == "__main__":

    conn = DB.DataBase()

    print(conn.check_sql('select * from melon where uname = %s;', '小刘'))
    print(conn.check_sql('select * from melon where uname = %s and uage = %s;', ['小刘', 30]))