# _*_ utf-8 _*_
__author__ = "LiuJian"
__date__ = "2021/7/23 0023 13:32"
# @FileName :test2.py

# 第一种应用模块的方法
import Melon.Module.test1

# print(Melon.Module.test1.Person(100))

# 第二种应用模块的方法
from Melon.Module.test1 import Person,add2num

print(add2num(2, 3))
print(Person(30))

if __name__ == "__main__":
    TheApp = 0
