# _*_ utf-8 _*_
__author__ = "LiuJian"
__time__ = "2021/4/20 0020 10:57"
# @FileName :MyClass.Set.py

# 定义一个类   （class test():  /  class test:   /   class test(object):）三种方式
class Person(object):
    # 魔法方法，在魔法方法里定义对象属性，在创建对象完成后自动调用此方法
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def getInfo(self):
        print('名称是：%s' %(self.name))
        print('年龄是：%d' %(self.age))

    # (1)魔法方法，如果没有__str__ 则默认打印 对象在内存的地址。监听对象属性信息变化
    # (2)当类的实例化对象 拥有 __str__ 方法后，那么打印对象则打印 __str__ 的返回值。
    def __str__(self):
        return ('名称是：%s, 年龄是：%d' % (self.name, self.age))

    # (1)魔法方法，当对象被删除时，会自动被调用   监听对象是否被销毁
    # (2)当使用del() 删除变量指向的对象时，则会减少对象的引用计数。如果对象的引用计数不为1，
    # 那么会让这个对象的引用计数减1，当对象的引用计数为0的时候，则对象才会被真正删除（内存被回收）
    def __del__(self):
        print("__del__方法被调用")
        print('对象：%s 被监听了' % (self.name))


# 创建对象
xiaoming = Person('小明', 20)
xiaoli = Person('小李', 25)
xiaoli1 = xiaoli
xiaoli2 = xiaoli
# 打印对象
# print(xiaoli)

# 删除对象
# print('%d被删除1次' % (id(xiaoli)))
del xiaoli
del xiaoli1
del xiaoli2
input()
# 调用方法
# xiaoming.getInfo()
print('-'*100)
# xiaoli.getInfo()
print('-'*100)

if __name__ == "__main__":
    TheApp = 0
