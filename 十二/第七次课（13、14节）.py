# 作业：
# 1、用任意编程语言循环输出整数0到10。
for i in range(0, 11):
    print(i)

# 2、用任意语言实现逆序输出一个字符串，比如[欢乐逛欢迎您]，输出[您迎欢逛乐欢]。
str1 = '欢乐逛欢迎您'
print(str1[::-1])

# 3、python基础：
# 1） str = 'a,hello'在字符串str中查找hello。
str2 = 'a,hello'
print(str2.find('hello'))

# 2）str = 'a,b,c,d'用逗号分割str字符串，并保存到列表。
str3 = 'a,b,c,d'
list1 = str3.split(',')
print(list1)

# 1.下列将字符串"100"转换为数字100的正确的是( A )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

# 2.下列程序执行结果是( A )
# numbers = [1, 5, 3, 9, 7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1

# 3.如何在列表中添加一个元素。
list2 = ['ii', 'uu', 'vv']
list2.extend(['yy'])
print(list2)

# 4.对于列表什么是越界。
# 下标超出元素个数

# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型。
# 可变：列表，集合，字典
# 不可变：整型，元组，字符串

# 6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母。
list3 = []
i = 1
while i <= 5:
    name = input('请输入第%d位学生的姓名:' % i)
    list3.extend([name])
    i += 1
print(list3)
for i in list3:
    print(i[1])

# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)。
import random

i = 1
list4 = []
while i <= 5:
    a = random.randint(0, 100)  # randint产生的随机数区间是包含左右极限的，也就是说左右都是闭区间的[1, n]，能取到1和n。
    list4.extend([a])
    i += 1
print(list4)
list5 = []
for i in list4:
    i = i * -1
    list5.extend([i])
print(list5)

# 8.将下列两个列表合并，将合并后的列表升序并输出。
list6 = [1, 3, 4, 5, 7]
list7 = [0, 66, 8, 9]
list8 = list6 + list7
list8.sort(reverse=False)  # reverse=False时，reverse=False可以省略
print(list8)

# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端。
name = input('请输入姓名：')
age = int(input('请输入年龄：'))
number = input('请输入学号：')
dict1 = {'姓名': name, '年龄': age, '学号': number}
print(dict1)

# 10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)。
dict1 = {'school': 'lebo', 'date': 2018, 'address': 'beijing'}
print(list(dict1.keys()))  # 先把所有key转换为列表
print([list(dict1.values()).index('lebo')])  # 转换为列表后，找到value对应的索引值
print(list(dict1.keys())[list(dict1.values()).index('lebo')])  # 根据索引值找到key列表中的字符串

# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端。
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
num = num[::-1]
print(num)
num1 = num[::2]
print(num1)
sum1 = 0
for i in num1:
    sum1 = i + sum1
print(sum1)

'''
# 笔记
##############################################3
1、元组：
1）查询：


2、字典：
1）key和value一一对应；
2）key是唯一的；
3）key的数据类型必须是不可变的数据类型
4）字典的数据类型是可以变的，可以进行增删改查；
5）字典是无序的，意味着没有下标（实际上是按照ASCII码排序）
6）空字典：
# 方法一
my_dict = {}
print(type(my_dict))
# 方法二
my_dict = dict()

7)my_dict = {'key': 'value', 123: '123'}
key和value之间之间用： 隔开，冒号后面要有空格
8）查找：
print(my_dict['name'])# name是key，查到它的value

#查找key是否存在
fi 'key' in my_dict:
    print('存在')
else:
    print('不存在')

ret=my_dict.setdefault('key', '666')#通过key找value,如果key不存在则新增键值对'key':'666'
print(ret)
print(my_dict)

ret=my_dict.get('key', '666')#这个和上面的区别是，不会改变其本身，不存在也不会新增
print(ret)
print(my_dict)

9）修改：
my_dict['name'] = 'lebo'# 把key为name的value修改成‘lebo’
10）删除：
del my_dict  # my_dict这个变量删掉了
del my_dict['name']
清空字典：my_dict.clear()# 字典还在，清空了
11）计算键值对个数：
print(len(my_dict))
print(my_dict.keys())# 查看所有的key出来的是dict_keys数据类型，可以print(list(my_dict.keys()))这样转换为list
print(my_dict.value())# 查看所有的value，数据类型和转换河上面一样
print(my_dict, items())  # 以元组的方式返回所有键值对，数据类型为dict_items
'''
