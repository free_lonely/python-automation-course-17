# 应用：
# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
import random

school_list = [[], [], []]  # 三个办公室
teacher_list = list('ABCDEFGH')  # 八位老师
for name in teacher_list:  # 遍历每一位老师
    index = random.randint(0, 2)  # 随机一个办公室的下标，index是下标
    school_list[index].append(name)  # 把遍历的老师放到随机得到的办公室里面
print(school_list)


''' 笔记
##########################################
1、 把列表里面的字符串合并成一个字符串
my_list = ['a', 'b', 'c']
print(my_list)
ret = ''.join(my_list)  # 单纯拼接起来
# ret = '6'.join(my_list)#用6来合起来
print(ret)

2、列表（列表每个元素后面的逗号后面要加一个空格）
1）列表也是支持下标索引的；
2）列表支持空列表，有两种写法：list()、list[]
3）查列表长度：len(my_list)
4）如果列表每个元素都是一个字符，可以简写成：my_list = list('abcde') ———和这个相同——— > my_list = list['a', 'b', 'c', 'd', 'e']
5）打印列表中某一个元素中的某个字符：
my_list = ['abc', 'qwe']
a = my_list[0]  # 先根据下标拿到某个元素
print(a[2])  # 再根据下标拿到要的字符
6）列表是可变的，可以往里面加元素：
my_list = ['abc', 'qwe']
my_list.append(['a', 'b'])  # 把['a', 'b']这个列表加到my_list里面
my_list.extend(['a', 'b'])  # 把列表里面的元素一个个加到my_list里面
my_list.insert([2, 'b'])  # 把'b'加到下标为2的元素的后面
7）修改元素：
my_list[1] = '666qwe'#把下标为1的元素修改为'666qwe'
8）查找元素:
index = my_list.index(666)#查找666的下标
print(index)
方法二：
if 666 in my_list:#判断存在字符666，not in就是判断不在列表中
    print('存在')
else：
    print('不存在')

a=my_list.count('666')#查询666出现了几次
9）删除：
del my_list[1]#删除下标为1的元素，如果下标超过元素的数量，就会报错“下标越界”
print(my_list)

my_list.pop(1)#删除下标为1的元素，不填下标就默认删除列表中最后一个元素
print(my_list)

my_list.remove(666)#删除列表中666，如果有多个，就只删除第一个，如果要删掉多个，就可以用循环一个个删，或者用下标的方式

10）排序：
#升序、降序
my_list = [1, 2, 4, 5, 6, 8, 2]
my_list.sort(reverse=False)#reverse是问你是否降序排序，False是升序，reverse=False可以省略
print(my_list)
#逆序
my_list.reverse()
print(my_list)

my_list = [1, 2, 4, 5, 6, 8, '2']# 不同类型不能排序
my_list = ['2', 'sdf', '都是']#字符串类型能排序，按照ASCII码排序

11）列表的嵌套：
schoolnames=['北京大学', ['广州大学', ['天津大学', '清华大学']],['天津师范大学']]
print(schoolnames[1][1][0])#能拿到天津大学:[1]是第一层，[1]是第二层，[0]是第三层'''
