students_list = []


class Student_system(object):
    def __init__(self, new_name=None, new_tel=None, new_qq=None, num=None, say_yes_no=None):
        self.new_name = new_name
        self.new_tel = new_tel
        self.new_qq = new_qq
        self.num = num
        self.say_yes_no = say_yes_no

    def tianjia(self):
        global students_list
        student_dict = {}
        self.new_name = input("请输入姓名：")
        self.new_tel = input("请输入手机号：")
        self.new_qq = input("请输入QQ：")
        student_dict['name'] = self.new_name
        for i in students_list:
            if i['name'] == self.new_name:
                print("此用户名已经被占用，请重新输入")
        student_dict['tel'] = self.new_tel
        student_dict['qq'] = self.new_qq
        students_list.append(student_dict)

    def shanchu(self):
        global students_list
        self.num = int(input("请输入要删除的序号："))
        if 0 <= self.num < len(students_list):
            self.say_yes_no = input("你确定要删除么？yes or no")
            if self.say_yes_no == 'yes':
                del students_list[self.num]
        else:
            print("输入序号有误，请重新输入")

    def xiugai(self):
        global students_list
        self.num = int(input("请输入要修改的序号："))
        if 0 <= self.num < len(students_list):
            print("你要修改的信息是：")
            print("name:%s, tel:%s, qq:%s" % self.new_name, self.new_tel, self.new_qq)
            students_list[self.num]['name'] = input("请输入新姓名：")
            students_list[self.num]['tel'] = input("请输入手机号：")
            students_list[self.num]['qq'] = input("请输入新QQ：")
        else:
            print("输入序号有误，请重新输入")

    def chaxun(self):
        self.new_name = input("请输入要查询的学生姓名：")
        for i in students_list:
            if i['name'] == self.new_name:
                print("查询到的信息如下:")
                print("%s,%s,%s" % (i['name'], i['tel'], i['qq']))
            else:
                print("输入的序号有误")

    def bianli(self):
        for i in students_list:
            print("%s,%s,%s" % (i['name'], i['tel'], i['qq']))

    def baocun(self):
        shuju = open("xinxi.txt", 'w')
        shuju.write(str(students_list))
        shuju.close()

    def jiazai(self):
        global students_list
        shuju = open('xinxi.txt')
        duqushuju = shuju.read()
        students_list = eval(duqushuju)
        shuju.close()


    def kaiji(self):
        self.jiazai()
        while True:
            print("      学生管理系统 V1.0")
            print(" 1:添加学生")
            print(" 2:删除学生")
            print(" 3:修改学生")
            print(" 4:查询学生")
            print(" 5:显示所有学生")
            print(" 6:保存数据")
            print(" 7:退出系统")
            print("---------------------------")
            kaiji_num = input("请输入要进行操作的数字：")
            if kaiji_num == '1':
                self.tianjia()
            elif kaiji_num == '2':
                self.shanchu()
            elif kaiji_num == '3':
                self.xiugai()
            elif kaiji_num == '4':
                self.chaxun()
            elif kaiji_num == '5':
                self.bianli()
            elif kaiji_num == '6':
                self.baocun()
            else:
                tuichu = input("亲，你确定要退出么？yes or no")
                if tuichu == 'yes':
                    break
                else:
                    print("欢迎回来")


xiaoming = Student_system()
xiaoming.kaiji()
