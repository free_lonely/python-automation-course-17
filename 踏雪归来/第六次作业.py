
# 第一题：任意编程语言循环输出0到10
# i = 0
# for i in range(0, 11):
#     if i <= 10:
#         print(i)

# i = 0
# while i <= 10:
#     print(i)
#     i += 1
#
# 第二题：任意编程语言实现逆序输出一个字符串，比如【欢乐逛欢迎您】，
# 输出【您--欢】

# my_str = '欢乐逛欢迎您'
# my_str1 = my_str[::-1]
# print(my_str1)
# print(my_str[::-1])
# python基础
# 1、str = 'hello' ,在字符串中查找hello
# my_str = 'hello'
# if 'hello'in my_str:
#     print(1)
# else:
#     print(0)
# print(my_str.find('hello'))
# print(my_str.index('hello'))
# 第二题：str = 'a, b ,c ,d'，用逗号分割str字符，并保存到列表
# my_str = 'a, b ,c ,d'
# print(list(my_str.split(' ')))
# 第三题：将‘笔试题A123’中的123替换为进行中
# my_str = '笔试题A123'
# print(my_str.replace('123','进行中'))
