# 第一页4.5.6题
# python 2.x 3.x 默认的编码格式分别是什么？
#python2默认的编码是accii编码
# python默认是utf-8编码
# 打印方式不同： 2版本是print 加空格 加打印内容
# 将hello_new_world用_分割
# str = 'hello_new_world'
# print(str.split('_', 2))

# 将数字1以0001的格式输出到屏幕
# my_str = '1'
# my_str1 = my_str.rjust(4,'0')
# print(my_str1)
# 第二页1-07题
# 1、
# a = {1: 1} 对
# 2、
# a = [1, 2, 3, 4]
# b = [5, 6,7, 8]
# print(a + b)
# 3、到序
# b = [54, 65, 73432, 844, 43, 3, 44]
# b.sort(reverse=True)
# print(b)
# 正序
# b.sort(reverse=False)
# print(b)
# 逆序
# b.reverse()
# print(b)
# 4、
# d ={'k':1, 'v':2}
# print(d.items())
# ([('k', 1), ('v', 2)])
# 5、
# my_list = [{'k': 2, 'v': 2}, {'k': 12, 'v': 22}, {'k': 13, 'v': 32}]
# # my_list.reverse()
# # print(my_list)
# my_list.sort(key=lambda x: x['k'], reverse=True)
# print(my_list)
# 6/
# s = set([1, 2, ([2, 4, 9, 0, 3])3, 4])
# d = set
# a = s.intersection(d)
# b = s.union(d)
# print(a)
# print(b)

# a = s & d
# print(a)
# b = s|d
# print(b)
# c = d - s
# print(c)
# c = d ^ s
# print(c)
# 7/
# a = ['a', 'b']
# b = ','.join(a)
# print(b)

# 第三页1-3
# 1、
# 1.1错误，键值对的可以只能是不可变数据类型
# 1.2错误，元组是有序的
# 1.3错误，可以反向索引
# 1.4不能索引，无序的，a = {}错误，这是dict
# 2、
# sum = 0
# for i in range(101):
#     sum = sum + i
#     i += 1
# print(sum)

# print(sum(range(0, 101)))

# 3/
# import random
# num = random.random()
# if 0.25<num < 0.6:
#    print(num * 100)
#
# print(num)

# 第四页
# a = [1, 2, 3, 4, 2, 2, 4]
# a1 = set(a)
# print(a1)