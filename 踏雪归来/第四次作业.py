# 使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
# * *
# *
#
n = int(input('请输入行数：'))
i = 1
while i <= n:
    if i < (n/2 + 1):
        j = 1
        while j <= i:
            print('*', end=' ')
            j += 1
    elif i >= (n/2 + 1):
        m = n-i+1
        while m > 0:
            print('*', end=' ')
            m = m - 1
    print()
    i += 1

