#
# 1.下列将字符串"100"转换为数字100的正确的是( A )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
# 2.下列程序执行结果是(  A )
# numbers = [1，5，3，9，7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1
# 3.如何在列表中添加一个元素
# my_list = [1, 3,'asd']
# my_list.insert(1, 'sads')
# my_list.append(['asdsa', 'dasd'])
# my_list.extend('11')
# print(my_list)
# 4.对于列表什么是越界
# 超出了下标
# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 可变的list、dict 、set ；不可变的str 、tuple 、int 、folat 、布尔
# 6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
# students_list = []
# for i in range(5):
#     name = input('请输入学生姓名：')
#     students_list.append(name)
# for name in students_list:
#     print(name[1])

# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
# import random
# my_list = []
# for i in range(5):
#     num = random.randint(0, 100)
#     my_list.append(-num)
# print(my_list)

# 8.将下列两个列表合并，将合并后的列表升序并输出.
# list1 = [1,3,4,5,7]
# list2 = [0,66,8,9]
# my_list = list1 + list2
# my_list.sort(reverse=False)
# print(my_list)

# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，
# 这些信息来自键盘的输入，储存完输出至终端.
# my_name = input('请输入你的姓名:')
# my_age = input('请输入你的年龄:')
# my_xuehao = input('请输入你的学号:')
# my_dict = {'姓名':my_name, '年龄':my_age,'学号':my_xuehao}
# print(my_dict)

# 10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
# dict1 = {'school':'lebo','date':'2018','address':'beijing'}
# for value in dict1:
#     if dict1[value] == 'lebo':
#         print(value)
#     else:
#         print(2)


# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0, 1, 2, 3,4,5,6,7,8,9]
num1 = num[::-1]
num2 = num1[1::2]
print(num2)
sum = 0
for i in num2:
    sum = sum + i
print(sum)







