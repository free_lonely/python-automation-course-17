# 1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
# *args是不定长参数之元组，在不知道参数的个数时使用；
# **kwargs是不定长参数之字典，必须放到参数的最后面，键值对，一一对应；
# 2、（先自己想，然后再实验，最后查百度）

# f(2) ,i = 2,输出[0, 1, ]
# f(3,[3, 2, 1]) ,i = 3,输出[3, 2, 1, 0, 1 ,4]
# f(3) ,i = 3,输出[0, 1, 0, 1, 4]
def f(x,  l=[]):
    for i in range(x):
        l.append(i * i)
    print(l)


f(2)
f(3,[3, 2, 1])
f(3)
#
# print(f(2))
# 3、如何在一个函数内部修改全局变量
# 使用global修改全局变量
# num = 10
# def my_func():
#     global num
#     num =100
#     print(num)
#
#
# my_func()
# 4、用lambda函数实现两个数相乘
# def my_func(a, b):
#     return a*b
#
#
# print(my_func(11, 22))

# f =lambda a, b:a*b
# print(f(121, 22))


# 5、列表推导式求列表所有奇数并构造新列表，
# # a =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# my_list = []
# for i in a:
#     if i % 2 ==1:
#         my_list.append(i)
# print(my_list)

# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# print([i for i in a if i % 2 == 1] )

