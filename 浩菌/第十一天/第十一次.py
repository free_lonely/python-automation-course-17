import time

"""
综合应用:学生管理系统(文件版)
"""
# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
Student_list = []
"""
有如下7个方法
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")
"""


# 定义菜单
def print_interface():
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")


"""添加学生信息"""


def add_student():
    global Student_list
    new_name = input("请输入姓名:")
    new_tel = input("请输入手机号:")
    new_qq = input("请输入QQ:")
    # 此用户名已经被占用,请重新输入
    for ret_info in Student_list:
        if ret_info == new_name:
            print("此学生已存在，请重新输入")
            return

    # 定义一个字典，用来存储用户的学生信息(这是一个字典)
    info = {}
    # 向字典中添加数据
    info['name'] = new_name
    info['tel'] = new_tel
    info['qq'] = new_qq
    # 向列表中添加这个字典
    Student_list.append(info)


#  删除学生信息
def del_student():
    global Student_list

    show_student()
    # "请输入要删除的序号:"
    del_num = int(input("请输入要删除的序号："))
    # 判断输入的序列号是否存在
    # 你确定要删除么?yes or no
    if 0 <= del_num < len(Student_list):
        del_enquire = input('您确定要删除么？yes or no：')
        if del_enquire == 'yes':
            del Student_list[del_num]
    else:
        print("输入的序号有误，请重新输入")


"""修改学生信息"""


def alter_student():
    global Student_list

    # "请输入要修改的序号:"
    alter_num = int(input("请输入要修改的序号："))
    # 你确定要删除么?yes or no
    # "你要修改的信息是:"
    # input("请输入新的姓名:")
    # input("请输入新的手机号:")
    # input("请输入新QQ:")
    # print("输入序号有误,请重新输入")
    if 0 <= alter_num < len(Student_list):
        print("你要修改的信息是: ")
        print("姓名：%s,手机号：%s,qq:%s" %
              (Student_list[alter_num]['name'],
               Student_list[alter_num]['tel'],
               Student_list[alter_num]['qq']))
        Student_list[alter_num]['name'] = input("请输入新的姓名：")
        Student_list[alter_num]['tel'] = input("请输入新的手机号：")
        Student_list[alter_num]['qq'] = input("请输入新QQ：")
    else:
        print("输入的序号有误，请重新输入")


"""查询学生信息"""


def sel_student():
    global Student_list
    sel_name = input("请输入要查询的学生姓名：")
    for sel_ret in Student_list:
        if sel_ret['name'] == sel_name:
            print("该学生信息如下：")
            print("name:%s,tel:%s,qq:%s" % (sel_ret['name'],
                                            sel_ret["tel"],
                                            sel_ret['qq']))
            break
        else:
            print("没有您要找的信息....")


"""遍历学生信息"""


# 序号\t姓名\t\t手机号\t\tQQ
# 查看学生序列号
def show_student():
    global Student_list
    # 遍历出学生的信息
    for i, student_info in enumerate(Student_list):
        print("序列号：%s,学生信息：%s" % (i, student_info))


"""存储数据"""

import os


def save_data():
    f = open('Student_info.data', 'w')
    f.write(str(Student_list))
    f.close()


# 加载之前存储的数据
def load_data():
    global Student_list
    f = open('Student_info.data')
    content = f.read()
    Student_list = eval(content)
    f.close()


# 主流程
def main():
    # 加载
    load_data()

    # 用死循环无限运行知道关闭
    while True:

        # 打印功能
        print_interface()
        # 让用户选择功能
        num = input("请输入您要进行的操作：")

        # 判断并执行相应的功能
        if num == "1":
            # 添加学生信息
            add_student()
        elif num == '2':
            # 删除学生信息
            del_student()
        elif num == '3':
            # 修改学生信息
            alter_student()
        elif num == '4':
            # 查询学生信息
            sel_student()
        elif num == '5':
            # 显示学生信息
            show_student()
        elif num == '6':
            # 保存数据
            save_data()
        elif num == '7':
            # 退出系统
            exit_enquire = input("您确定要退出系统吗？yes or no：")
            if exit_enquire == 'yes':
                break
            else:
                print("您输入有误")

    input('输入回车继续....')


main()
