"""
4、用lambda函数实现两个数相乘
"""
f = lambda x, y: x * y
print(f(5, 8))

"""
5、列表推导式求列表所有奇数并构造新列表，a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
"""
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list_a = [x for x in a if x % 2 != 0]
print(list_a)
