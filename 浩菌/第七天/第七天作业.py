'''
1.	下列将字符串"100"转换为数字100的正确的是( A )                                      
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)
2.	下列程序执行结果是( A )                                                
 numbers = [1，5，3，9，7]                                                                          
 numbers.sort(reverse=True)                                                                              
 print(numbers)                                                                                      
 A、[9，7，5，3，1]                                                                                                        
 B、[1，3，5，7，9]                                                              
 C、1，3，5，7，9                                                                                        
 D、9，7，5，3，1
3.如何在列表中添加一个元素
（1）使用append()方法
（2）使用extend()方法
（3）使用insert()方法
4.对于列表什么是越界
列表存储的元素超越了最大极限
按下表访问列表内容时，超过了目前的元素个数
删除列表元素超过最大元素个数
5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
（1）可变的：列表
（2）不可变的：字符串，元组
'''
'''
6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
'''
import random

# stuNameList = []
# for i in range(5):
#     stuName = input("请输入姓名：")
#     stuNameList.insert(i,stuName)
#
# for j in range(len(stuNameList)):
#
#     str_name = stuNameList[j]
#     c = str_name[1:2]
#     print(c)
# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)

# i = 0
# number_list = []
# newNumberList = []
# while i < 5:
#     numbers = random.randint(0, 100)
#     number_list.extend([numbers])
#     i += 1
# print("随机数得到的列表为：", number_list)
#
# for i in range(5):
#     print("循环遍历列表中的值[%d]: %d" % (i, ~number_list[i]))

'''
8.	将下列两个列表合并，将合并后的列表升序并输出.
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
'''
# list1 = [1, 3, 4, 5, 7]
# list2 = [0, 66, 8, 9]
#
# for i in range(len(list2)):
#     list1.extend([list2[i]])
#
# list1.sort()
#
# print(list1)

'''
9.	使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，
这些信息来自键盘的输入，储存完输出至终端
'''
# name = input("请输入姓名:")
# while True:
#     age1 = input("请输入年龄:")
#     if not age1.isdigit():
#         print("请输入数字")
#     else:
#         age = int(age1)
#         break
# sno = input("请输入学号:")
# my_dict = {'name': name, 'age': age, 'son': sno}
# print(my_dict)
'''
10.	有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}
'''
# dict1 = {'date': '2018', 'school': 'lebo', 'address': 'beijing'}
# for i in dict1:
#     if dict1[i] == 'lebo':
#         print(i)

'''
11.	使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
'''

# sum = 0
# num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# newNum = num[::-2]
# print(newNum)
#
# for i in newNum:
#     sum += i
# print(sum)

for i in range(10):
    print(i)
