"""
1、下列程序结果是______.
class A:

    def __init__(self):
        print("初始化")

    def __del__(self):
        print("销毁")

    def __str__(self):
        return "类A"
a = A()
print(a)

"""
# 结果：类A 销毁
"""
2. 下列程序执行结果______.
class Animal(object):

    def shut(self):
        print("动物在叫")
class Dog(Animal):

    def shut(self):
        super().shut()
        print("汪汪汪")
dog = Dog()
dog.shut()

"""
# 结果：动物在叫 汪汪汪
"""
3. 创建猫类：
	类名：Cat
	属性：无
描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
"""

# class Cat(object):
#     def __str__(self):
#         return '喵？喵？喵？'
#
#
# cat = Cat()
# print(cat)
"""
4.创建计算器类：
	类名：Calculator
	属性：number_1（数字一）、number_2（数字二） 
方法：
	def __init__(self,number_1,number_2): 
# 类的初始化方法
	def add(self) # 返回数字一加数字二的值 
	def sub(self) # 返回数字一减去数字二的值
	def div(self) # 返回数字一除以数字二的值
	def mul(self) # 返回数字一乘以数字二的值
描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.
"""


# class Calculator(object):
#     def __init__(self, number_1, number_2):
#         self.number1 = number_1
#         self.number2 = number_2
#
#     # 返回number1加number_2的值
#     def add(self):
#         return print('%d+%d=%d' % (self.number1, self.number2, (self.number1 + self.number2)))
#
#     # 返回number_2减去number_2的值
#     def sub(self):
#         return print('%d-%d=%d' % (self.number1, self.number2, (self.number1 - self.number2)))
#
#     # 返回number_2除以number_2的值
#     def div(self):
#         return print('%d/%d=%d' % (self.number1, self.number2, (self.number1 / self.number2)))
#
#     # 返回number_2乘以number_2的值
#     def mul(self):
#         return print('%d*%d=%d' % (self.number1, self.number2, (self.number1 * self.number2)))
#
#
# num = Calculator(35, 15)
# num.add()
# num.sub()
# num.div()
# num.mul()

"""
5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
class Animal:

    def shut(self):
		# 打印叫声
        pass

    def eat(self):
		# 打印爱吃的食物
        pass
"""


# class Animal:
#     def shut(self):
#         # 打印叫声
#         pass
#
#     def eat(self):
#         # 打印爱吃的食物
#         pass
#
#
# class Cat(Animal):
#     def shut(self):
#         # 打印叫声
#         print('喵喵喵？')
#
#     def eat(self):
#         # 打印爱吃的食物
#         print('虾皮子')
#
#     def animal_shut(self):
#         Animal.shut(self)
#
#     def animal_eat(self):
#         Animal.eat(self)
#
#
# class Dog(Animal):
#     def shut(self):
#         # 打印叫声
#         print('汪汪汪？')
#
#     def eat(self):
#         # 打印爱吃的食物
#         print('红烧肉')
#
#     def animal_shut(self):
#         Animal.shut(self)
#
#     def animal_eat(self):
#         Animal.eat(self)
#
#
# cat = Cat()
# dog = Dog()
# # 1调用重写的方法
# cat.shut()
# cat.eat()
# # 1调用父类的方法
# cat.animal_shut()
# cat.animal_eat()
# # 2调用重写的方法
# dog.shut()
# dog.eat()
# # 2调用父类的方法
# dog.animal_shut()
# dog.animal_eat()
"""
1、面向对象三大特性是__多态____、__继承____、___封装___.
2、单例模式创建保证实例只创建___1___次.
"""
