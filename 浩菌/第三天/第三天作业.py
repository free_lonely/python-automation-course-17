'''
7、使用if，编写程序，实现以下功能：
•	从键盘获取用户名、密码
•	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
'''

user = 'haojun'
password = 123456
print("请输入账号密码：")
inputUser = input('请输入账号：')
inputPassword = int(input('请输入密码：'))
if inputUser == user and inputPassword == password:
    print('欢迎进入浩菌的世界')
else:
    print('您输入的密码或者用户名错误')
'''
8、有能力的同学可以用循环的方式做出石头剪刀布的游戏并且输入中文
'''
# 调用随机数
import random
# 石头剪刀布游戏说明
print('石头剪刀布史诗对抗赛')
# 让用户输入一个数并赋值给用户变量
player = (input('请输入你要出的手势：剪刀、石头、布：'))
# 给出一个0到2的随机数赋值给电脑变量
computer = random.randint(0, 2)
# 将电脑的随机数变为具体手势
if computer == 0:
    computer = '剪刀'
elif computer == 1:
    computer = '石头'
else:
    computer = '布'
# 查看双方的手势
print('用户出的是：%s ，电脑出的是：%s ' % (player, computer))
# 判断用户赢
if player == '剪刀' and computer == '布' or player == '石头' and computer == '剪刀' or player == '布' and computer == '石头':
    print('用户胜利')
# 判断平局
elif player == computer:
    print('平局')
# 否则电脑赢
else:
    print('电脑赢')
