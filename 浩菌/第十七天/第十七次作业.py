import pymysql
while True:
    db = pymysql.connect(host='localhost', user='root',
                         db='test', port=3306,
                         passwd='root', charset='utf8')

    cursor = db.cursor(pymysql.cursors.DictCursor)
    pd = int(input('------------学生信息查询系统------------\n 1注册，2登录，3退出：'))
    if pd == 1:
        print('————————————————————注册————————————————————')
        s_name = input('请输入姓名：')
        s_sex = input('请输入性别：')
        s_hometown = input('请输入地址：')
        s_age = int(input('请输入年龄：'))
        s_class = input('请输入班级：')
        s_card = input('请输入密码：')
        sql = "select * from students where name = %s"
        count = cursor.execute(sql, s_name)
        if count == 1:
            print('您输入的用户名已存在，请从新输入')
        else:
            sql = "insert into students(name,sex,hometown,age,class,card) " \
                  "values (%s, %s, %s, %s, %s, %s);"
            cursor.execute(sql, [s_name, s_sex, s_hometown, s_age, s_class, s_card])
            db.commit()
            print("恭喜您注册成功，以下是您的注册信息，按回车继续")
            sql = "select * from students where name = %s"
            cursor.execute(sql, s_name)
            ret = cursor.fetchall()
            print(ret)
            input('----按回车继续----')
            continue
    elif pd == 2:
        print('————————————————————登录————————————————————')
        s_name = input('请输入姓名：')
        s_card = input('请输入密码：')
        sql = "select * from students where name = %s and card = %s;"
        print(sql)
        count = cursor.execute(sql, [s_name, s_card])
        if count == 1:
            print('您的信息如下:')
            ret = cursor.fetchone()
            print(ret)
            input('----按回车继续----')

        else:
            print('您输入的姓名或密码错误')
    elif pd == 3:

        pd2 = int(input("您确定退出程序吗？\n 1、退出，2、回到主菜单:"))
        if pd2 == 1:
            print('程序已经退出，感谢您的使用')
            cursor.close()
            db.close()
            break
        else:
            print('请您继续使用')
            continue
    else:
        print('您输入的数字有误请从新输入')
        continue