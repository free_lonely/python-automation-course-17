"""
__project__ = '浩菌'
__file_name__ = 'connection_mysql'
__author__ = '86176'
__time__ = '2021/4/30 22:06'
__product_name = PyCharm
"""
import pymysql
from file.dbinfo import dbinfo


class DataBases(object):
    def __init__(self,
                 user=dbinfo['user'],
                 password=dbinfo['password'],
                 host=dbinfo['host'],
                 database=dbinfo['database'],
                 port=dbinfo['port'],
                 charset=dbinfo['charset']):
        self.user = user
        self.password = password
        self.host = host
        self.database = database
        self.port = port
        self.charset = charset

    def __get_connection(self):
        if not self.database:
            raise RuntimeError('数据库没有找到')
        try:
            self.conn = pymysql.connect(user=self.user,
                                        password=self.password,
                                        host=self.host,
                                        database=self.database,
                                        port=self.port,
                                        charset=self.charset)
        except Exception as e:
            raise RuntimeError('初始化数据库失败，原因是：', e)
        self.cursor = self.conn.cursor()
        return self.cursor

    def execute_sql(self, sql, params=None):
        # 链接数据库
        self.__get_connection()
        # 发送语句
        self.cursor.execute(sql, params)
        # 事务提交
        self.conn.commit()
        print('提交成功')

    def __close_sql(self):
        try:
            if self.cursor:
                self.cursor.close()
        except Exception as e:
            print('关闭游标异常', e)

    def select_sql(self, sql, params=None):
        self.__get_connection()
        self.execute_sql(sql, params)
        data = self.cursor.fetchall()
        self.__close_sql()
        return data


if __name__ == '__main__':
    db1 = DataBases()
    sql = "select * from students;"
    print(db1.select_sql(sql))
