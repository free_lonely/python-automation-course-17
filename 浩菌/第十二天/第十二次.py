'''
1、创建一个对象后默认调用(A)
A、__init__    B、__str__    C、__add__    D、__and__
'''
# 2、类是对象的：抽象化、对象是类的：实例化.
# 3、对象是由：属性、方法两部分构成.
'''
4、创建学生类：
	类名：Student
	属性：name（姓名）、age（年龄）、sex（性别）
方法：
	def info(self) # 打印学生的姓名、年龄、性别
	def draw(self) #打印”XX会画画呢”
描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.
'''


class Student(object):
    def info(self):
        print('姓名：%s' % self.name)
        print('年龄：%s' % self.age)
        print('性别：%s' % self.sex)

    def draw(self):
        print('%s会画画呢！' % self.name)


student1 = Student()
student1.name = '张韩旭'
student1.age = '21'
student1.sex = '女'
student1.info()
student1.draw()
'''
5、创建动物类：
	类名：animal
	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
方法：
	def info(self) # 打印姓名、年龄、毛颜色
	def run（self）#打印“XX会跑呢”
描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.
'''


class Animal(object):
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def info(self):
        print('姓名：%s' % self.name)
        print('年龄：%s' % self.age)
        print('毛颜色：%s' % self.color)

    def run(self):
        print('%s会跑呢！' % self.name)


animal1 = Animal('汤姆', '4', '蓝色')
animal1.info()
animal1.run()
