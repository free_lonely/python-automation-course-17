'''
1、使用while，完成以下图形的输出
*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*
'''
# 假设行数是i
i = 1
# 假设需要5行
while i <= 5:
    j = 1
    while j <= i:
        print("*", end='')
        j += 1
    print()
    i += 1
i = 1
while i <= 4:
    j = 4
    while j >= i:
        print("*", end='')
        j -= 1
    print()
    i += 1




'''
2、break和continue的区别：
'''
# break 直接结束循环体
# continue 跳过本次循环，继续之后的循环

'''
3、按要求处理字符串： 
现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
'''
str1 = '1234567890'
# 1.截取字符串的第一位到第三位的字符
str2 = str1[:3]
print(str2)
# 2.截取字符串最后三位的字符
str2 = str1[7::]
print(str2)
# 3.截取字符串的全部字符
str2 = str1
print(str2)
# 4.截取字符串的第七个字符到结尾
str2 = str1[6::]
print(str2)
# 5.截取字符串的第一位到倒数第三位之间的字符
str2 = str1[:8]
print(str2)
# 6.截取字符串的第三个字符
str2 = str1[2]
print(str2)
# 7.截取字符串的倒数第一个字符
str2 = str1[-1]
print(str2)
# 8.截取与原字符串顺序相反的字符串
str2 = str1[::-1]
print(str2)
# 9.截取字符串倒数第三位与倒数第一位之间的字符
str2 = str1[7:]
print(str2)
# 10.截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
str2 = str1[::2]
print(str2)

#4、如何既打印出字符串，有显示下标

str1 = 'abcdefgaabbccd'

for i in range(len(str1)):
    str3 = str1[i]
    print(str3 + '下标为：' + str(i))
