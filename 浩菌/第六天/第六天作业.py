# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配
# # 引用随机
# import random
# # 一个学校有3个办公室
# school_list = [[], [], []]
# # 现有8位老师
# teacher_list = list('ABCDEFGH')
# # 使用for遍历出每个老师赋予给name，再每次遍历中安排一个随机的办公室
#
# for name in teacher_list:
#     index = random.randint(0, 2)
#     school_list[index].append(name)
#
# print(school_list)
'''
# 1、用任意变成语言循环输出整数0到10
'''
# for i in range(11):
#     print(i)

'''
# 2、用任意语言实现逆序输出一个字符串，比如“欢乐逛欢迎您”，输出“你迎欢逛乐欢”
'''

# str1 = input('请输入一段句子：')
# print('顺序输出长这样：', str1)
# str2 = str1[::-1]
# print('逆序输出长这样：', str2)



'''
 3、str = 'a,hello'在字符串str中查找hello
'''
# str1 = 'a,hello'
# print(str1.find('hello'))
# print(str1.rfind('hello'))
# print(str1.rindex('hello'))
# print(str1.index('hello'))
# print(str1.count('hello'))
# 4、str='a,b,c,d'用逗号分割str字符串，并保存到列表

str1 = 'a,b,c,d'
list1 = str1.split(',')
print(list1)
# 将list1 变灰字符串str2
str2 = ''.join(list1)
print(str2)

# 5、将“笔试题123A”中的123替换位“进行中”

str1 = '笔试题123A'
str2 = str1.replace('123', '进行中')
print(str2)




