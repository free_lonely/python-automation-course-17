#定义一个空列表
studentList = []

#将学生信息存储到列表中
def addStudent():
    name = input("请输入学生姓名：")
    mobile = input("请输入学生手机号：")
    qq = input("请输入学生QQ号：")
    studentDict = {'name':name,'mobile':mobile,'qq':qq}
    return studentDict

#删除学生信息
def delStudent():
    print(studentList)
    flag = True
    while flag == True:
        id = int(input("请输入要删除的学生ID:"))
        tips = input("确定要删除：yes or no：")
        if tips =="yes" or tips == "no":
            if tips == "yes":
                del studentList[id]
                break
            elif tips == "no":
                flag == False
                print("不执行删除操作")
                break
        else:
            flag = True
            print("请重新输入")
            continue

#更新学生信息
def updateStudent():
    id = int(input("请输入要修改学生的ID:"))
    name = input("请输入学生新姓名：")
    mobile = input("请输入学生新手机号：")
    qq = input("请输入学生新QQ号：")
    studentList[id]=[{'name':name,'mobile':mobile,'qq':qq}]
    return studentList

#查询系统中的值
def searchStudent(name):
    for studentDict in studentList:
        if studentDict.get('name') == name:
            print("查询到的信息如下:")
            print("学生姓名：%s; 学生手机号：%s; 学生QQ号：%s" %(studentDict.get('name') ,studentDict.get('mobile') ,studentDict.get('qq')))
        else:
            print("没有您要找的信息....")

#显示系统中的数据
def showStudentInfo():
    for studentDict in studentList:
        print("学生姓名：%s; 学生手机号：%s; 学生QQ号：%s" %(studentDict.get('name') ,studentDict.get('mobile') ,studentDict.get('qq')))

#保存列表
def save():
    studentinfo = addStudent()
    studentList.append(studentinfo)

#退出系统，清空列表
def loginOut():
    studentList.clear()
    print(studentList)

save()
save()
loginOut()
