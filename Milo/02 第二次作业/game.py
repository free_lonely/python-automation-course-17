# 石头剪刀布游戏
# 假设用户A赢则退出游戏
flag = False
while flag == False:
	userA = input("请用户A出手势：")
	userB = input("请用户B出手势：")
	if userA not in['石头','剪刀','布'] or userB not in['石头','剪刀','布']:
		print("用户A或者用户B出错了手势，请重新出~~")
		flag = False
	else:
		if userA == userB:
			print("你俩出的一样的手势，打成了平手，继续...")
			flag = False
		else:
			if userA == "石头" and userB == "剪刀":
				print("恭喜用户A赢了，游戏结束！")
				flag = True
				break
			if userA == "石头" and userB == "布":
				print("用户A输了，继续...")
				flag = False
			if userA == "剪刀" and userB == "石头":
				print("用户A输了，继续...")
				flag = False
			if userA == "剪刀" and userB == "布":
				print("恭喜用户A赢了，游戏结束！")
				flag = True
				break
			if userA == "布" and userB == "石头":
				print("恭喜用户A赢了，游戏结束！")
				flag = True
				break
			if userA == "布" and userB == "剪刀":
				print("用户A输了，继续...")

