
#1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
  #*args: 不定长参数之元组，返回的数据类型是元组
  #**kwargs 不定长参数之字典，返回的数据类型是字典，传值必须是关键字参数
  #因为不知道要传多少个参数，所以使用不定长参数
  #**kwargs 必须放在最后
'''
2、下面代码会输出什么
def f(x,1=[]):
    for i in range(x):
        i.append(i*i)
    print(i)
'''

def f(x,l=[]):
    for i in range(x):
        l.append(i*i)
    print(l)
f(2)  # 返回值是[0,1]
f(3,[3,2,1])#[3,2,1,0,1,4]
f(3) #[0,1,4]

#3、如何在一个函数内部修改全局变量
# 使用 global 修饰变量

#4、用lambda 函数实现两个数相乘
sum = lambda x,y : x*y
print(sum(4,5))



#5、列表推导式求列表所有奇数并构造新列表，a=[1,2,3,4,5,6,7,8,9,10]

a=[1,2,3,4,5,6,7,8,9,10]
b = [a[i] for i in range(len(a)) if a[i]%2==1]
print(b)
