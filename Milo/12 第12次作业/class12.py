'''
1、创建一个对象后默认调用(A)
A、__init__    B、__str__    C、__add__    D、__and__
2、类是对象的__抽象_____、对象是类的___实例____.
3、对象是由___方法____、_属性__两部分构成.
'''

'''
4、创建学生类：
	类名：Student
	属性：name（姓名）、age（年龄）、sex（性别）
方法：
	def info(self) # 打印学生的姓名、年龄、性别
	def draw(self) #打印”XX会画画呢”
描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.

class Student(object):

    def __init__(self):
        pass

    def info(self,name,age,sex):
       print("我的名字是：%s，年龄是：%d，性别：%s" % (name,age,sex))

    def draw(self,name):
        print(name,"会画画")

if __name__ == '__main__':

    student = Student()
    student.info("王强",22,"男")
    student.draw("张三")
'''

'''
4、创建动物类：
	类名：animal
	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
方法：
	def info(self) # 打印姓名、年龄、颜色
	def run（self）#打印“XX会跑呢”
描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.

'''

class Animal:

    def __init__(self):
        self.name = "猴子"
        self.age = 2
        self.color = '土黄色'

    def info(self):
        print(self.name,self.age,self.color)

    def run(self,name):
        print(name ,"会跑")

if __name__ == '__main__':

    animal = Animal()
    animal.info()
    #animal2 = Animal()
    animal.run("兔子")


