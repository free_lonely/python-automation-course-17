#1. 创建猫类：
# 	类名：Cat
# 	属性：无
# 描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.

# class Cat(object):
#     def cat(self):
#         cat = '喵？喵？喵？'
#         print(cat)
#
# cat = Cat()
# cat.cat()

# 4.创建计算器类：
# 	类名：Calculator
# 	属性：number_1（数字一）、number_2（数字二）
# 方法：
# 	def __init__(self,number_1,number_2):
# # 类的初始化方法
# 	def add(self) # 返回数字一加数字二的值
# 	def sub(self) # 返回数字一减去数字二的值
# 	def div(self) # 返回数字一除以数字二的值
# 	def mul(self) # 返回数字一乘以数字二的值
# # 描述：创建计算器类，通过计算器类创建一个计算器对象
class Calculator(object):
    def __init__(self,number1,number2):
        self.number1 = number1
        self.number2 = number2

    def add(self):
        sum = self.number1+self.number2
        print('和为: %d' %sum)

    def sub(self):
        sub = self.number1-self.number2
        print("差为: %d" %sub)

    def div(self):
        div = self.number1/self.number2
        print("商为： %d" %div)

    def mul(self):
        print('积为：%d' % (self.number1*self.number2))

he = Calculator(1,2)
he.add()

ca = Calculator(12,10)
ca.sub()

shang = Calculator(12,2)
shang.div()

ji = Calculator(2,2)
ji.mul()

# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
# class Animal:
#
#     def shut(self):
# 		# 打印叫声
#         pass
#
#     def eat(self):
# 		# 打印爱吃的食物
#         pass
class Animal(object):

    def shut(self):
        print('叫声')

    def eat(self):
        print('爱吃的食物')

class Cat(Animal):

    def shut(self):
        print('喵喵喵')

    def eat(self):
        print('爱吃鱼')
# 重写
cat = Cat()
cat.shut()
cat.eat()

class Dog(Animal):
    
    def shut(self):
        print('汪汪汪')

    def eat(self):
        print('爱吃骨头')

# 调用
dog = Animal()
dog.shut()
dog.eat()

# 1、面向对象三大特性是__封装____、___继承___、___多态___.
# 2、单例模式创建保证实例只创建___1___次.