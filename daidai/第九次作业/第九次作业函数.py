# 作业
#1.这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
   # *args,不定长参数之元组；**kwargs,不定长参数之字典。用处：不确定输入参数有多少个，不定长参数可以输出多个元素来
# 2.下面代码会输出什么
def f(x,l=[]):
    for i in range(x):
        l.append(i*i)
    print(l)

f(2)
f(3,[3,2,1])
f(3)
# 结果为：
'''
[0,1]
[3,2,1,0,1,4]
[0,1,4]
'''
#3、如何在一个函数内部修改全局变量
# global

# 局部变量和全局变量
# num = 1
# def my_fun():
#     global num    #  将局部变量声明为全局变量 global
#     num = 2
#     print(num)
#
# def my_fun1():
#     print(num)
#
# my_fun()
# my_fun1()

# 缺省值参数
# def my_fun(age,sex='中'):  # sex='中'叫做缺省值参数
#     print(age,sex)
#
# my_fun(18)

# 不定长参数
