# -*- coding: utf-8 -*-
# @Time : 2021/4/18
# @Author : sunpengfei
# @File : 学生管理系统

# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)

def printMenu():
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")

"""添加学生信息
new_name = input("请输入姓名:")
new_tel = input("请输入手机号:")
new_qq = input("请输入QQ:")
添加学生可能出现：
此用户名已经被占用,请重新输入
定义一个字典，用来存储用户的学生信息(这是一个字典)
向字典中添加数据
向列表中添加这个字典"""
student_list = []
def add_info():
    new_name = input("请输入姓名:")
    new_tel = input("请输入手机号:")
    new_qq = input("请输入QQ:")
    for temp in student_list:
        if temp['name'] == new_name:
            print('此用户名已经被占用,请重新输入')
            return
    student_dict = {}
    student_dict['name'] = new_name
    student_dict['tell'] = new_tel
    student_dict['qq'] = new_qq
    student_list.append(student_dict)
    print(student_list)


"""删除学生信息
"请输入要删除的序号:"
你确定要删除么?yes or no
输入序号有误, 请重新输入"""
def del_info():
    delID = int(input("请输入要删除的序号:"))
    if 0 <= delID < len(student_list):
        anwer = input("你确定要删除么?yes or no")
        if anwer == 'yes':
            del student_list[delID]
        else:
            print("输入序号有误, 请重新输入")

'''
修改学生信息
"请输入要修改的序号:"
"你要修改的信息是:"
input("请输入新的姓名:")
input("请输入新的手机号:")
input("请输入新QQ:")
print("输入序号有误,请重新输入")
'''
def modify_info():
    modify = int(input("请输入要修改的序号:"))
    if 0 <= modify <len(student_list):
        print("你要修改的信息是:")
        # print('姓名：%s,电话：%s,qq号：%s' % (stdent_list[modify]['name'], stdent_list[modify]['tell'], stdent_list[modify]['qq']))
        name = input("请输入新的姓名:")
        tel = input("请输入新的手机号:")
        QQ = input("请输入新QQ:")
        student_list[modify]['name'] = name
        student_list[modify]['tel'] = tel
        student_list[modify]['QQ'] = QQ
    else:
        print("***输入序号有误,请重新输入")


'''
查询学生信息
"请输入要查询的学生姓名:"
查询到的信息如下:
"没有您要找的信息...."
'''
def query_info():
    query_name = input("请输入要查询的学生姓名:")
    for temp in student_list:
        if temp['name'] == query_name:
            print("姓名：%s\n手机号：%s\nQQ：%s " % (temp['name'], temp['tel'] ,temp['QQ']))
        else:
            print("没有您要找的信息....")


'''
遍历学生信息
序号\t姓名\t\t手机号\t\tQQ
'''
def display_info():
    print("接下来进行遍历所有的学生信息...")
    for temp in student_list:
        print("姓名:%s     手机号：%s     QQ：%s" % (temp['name'], temp['tell'], temp['qq']))


'''保存学生信息'''
def save_data():
    f = open('student.txt', 'w', encoding='utf-8')
    f.write(str(student_list))
    f.close()

'''加载信息'''
def load_data():
    global student_list
    f = open('student.txt')
    content = f.read()
    student_list = eval(content)
    f.close()

# 主流程
def main():
    # # 加载
    # load_data()

    while True:

        # 打印功能
        printMenu()
        # 让用户选择功能
        num = input("请输入您要进行的操作：")

        # 判断并执行相应的功能
        if num == "1":
            # 添加学生信息
            add_info()
        elif num == '2':
            # 删除学生信息
            del_info()
        elif num == '3':
            # 修改学生信息
            modify_info()
        elif num == '4':
            # 查询学生信息
            query_info()
        elif num == '5':
            # 显示学生信息
            display_info()
        elif num == '6':
            # 保存数据
            save_data()
        elif num == '7':
            # 退出系统
            exit_flag = input("您确定要退出系统吗？yes or no：")
            if exit_flag == 'yes':
                break
            else:
                print("您输入有误")

    input('输入回车继续....')


main()






