#1.循环输出0-1
for i in range(0,11):
    print(i)

#2.逆序输出“欢乐逛欢迎您”为“您迎欢逛乐欢”
str = "欢乐逛欢迎您"
for i in reversed(str):
    print(i,end = '')

#3.str='a,hello'在字符串中查找hello;
str = 'a,hello'
print(str.find('hello'))

#4.str = 'a,b,c,d',用逗号分割str字符串，并保存到列表
str = 'a,b,c,d'
my_list = str.split(',')
for value in my_list:
    print(value)

#5.将“笔试题123A”中123替换为“进行中”
str = "笔试题123A"
print(str.replace('123','进行中'))

