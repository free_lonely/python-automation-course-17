# -*- coding: utf-8 -*-
# @Time : 2021/4/11
# @Author : sunpengfei
# @File : task9

# 1、这两个参数是什么意思：*args，**kwargs？我们为什么要使用它们？
# *args:不定长参数之元祖
# **kwargs：不定长参数之字典
# 当参数中有很多个参数不确定多长的时候

# 2、（先自己想，然后再实验，最后查百度）
def f(x, l=[]):
    for i in range(x):
        l.append(i * i)
    print(l)

f(2)
# x = 2,l = []
#     range(2) -- 0,1
#     i * i  -- 0,1
#     l = [0,1]
f(3, [3, 2, 1])
# x = 3 , l = [3,2,1]
#     range(3) -- 0,1,2
#     i * i --0,1,4
#     l = [3,2,1,0,1,4]
f(3)
# 不明白

# 3、如何在一个函数内部修改全局变量
# global