# -*- coding: utf-8 -*-
# @Time : 2021/4/5
# @Author : sunpengfei
# @File : task7

'''
1.下列将字符串"100"转换为数字100的正确的是( A )
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

2.下列程序执行结果是(  A )
numbers = [1，5，3，9，7]
numbers.sort(reverse=True)
print(numbers)
A、[9，7，5，3，1]
B、[1，3，5，7，9]
C、1，3，5，7，9
D、9，7，5，3，1
'''

#3.如何在列表中添加一个元素(append、extend、insert)
# my_list = [1, 2, 3, 4, 5]
# my_list.append(6)
# print(my_list)

#4.对于列表什么是越界
#超出索引范围

#5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
#可变：list、dict
#不可变：str、bool、tuple、int、float

#6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
# name_list = []
# for i in range(1,6):
#     stdent_name = input("请输入第%d学生名字："% i)
#     name_list.append(stdent_name)
# print(name_list)
# for name in name_list:
#     print(name[1])

#7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
# import random
# number_list = []
# for i in range(5):
#     number = random.randint(0, 100)
#     number_list.append(number)
# print(number_list)

#8.将下列两个列表合并，将合并后的列表升序并输出
# list1 = [1,3,4,5,7]
# list2 = [0,66,8,9]
# list1.extend(list2)
# list1.sort()
# print(list1)

#9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
# personal_info = {}
# name = input("请输入姓名：")
# age = input("请输入年龄：")
# student_number = input("请输入学号：")
# personal_info.setdefault("name" , name)
# personal_info.setdefault("age" , age)
# personal_info.setdefault("student_number" ,student_number)
# print(personal_info)

#10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
#dict1 = {"school": "lebo", "date": 2018, "address": "beijing"}
# for key, value in dict1.items():
#     if value == 'lebo':
#         print(key)

#11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
list = num[::-1]
print(list)
res = 0
for num in list:
    if list.index(num) % 2 == 0:
         res += num
         print(num)
print(res)