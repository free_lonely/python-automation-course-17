"""
__project__='代码'
__file_name__='class5'
__author__='videt'
__time__ = '2021/4/1 16:26'
__product_name =PyCharm
"""
import random

# 定义一个列表用来保存3个办公室
offices = [[],[],[]]

# 定义一个列表用来存储8位老师的名字
names = ['乐老师', '好威老师', '王老师', '小贺老师', '流珂老师', '玲玲老师', '贤老师', 'lydia老师']
# 办公室下标
index = 0;
# 遍历办公室
for i in offices:
    # 判断是否为最后一个办公室 是则将剩下所有未分配老师塞入 反之进入随机分配
    if index >= len(offices)-1:
        i = names
    else :
        # 随机出该办公室老师人数
        teacherNum = random.randint(0,len(names)-1)
        nameIndex = 0
        # 循环取出老师并追加到当前办公室列表中
        while nameIndex < teacherNum:
            # 取出随机的老师下标
            tmpIndex = random.randint(0,len(names)-1)
            # 将随机出的老师追加到当前办公室
            i.append(names[tmpIndex])
            # 将随机出的老师移除待分配老师列表
            names.remove(names[tmpIndex])
            nameIndex += 1
    # 办公室下标自增
    index += 1
    print('办公室%d的人数为:%d' % (index, len(i)))
    # 遍历打印当前办公室老师
    for name in i:
        print("%s" % name, end=' ')
    print("\n")
    print("-" * 20)