"""
__project__='代码'
__file_name__='class7(第七次作业)'
__author__='videt'
__time__ = '2021/4/6 14:44'
__product_name =PyCharm
"""
# 1、下列将字符串"100"转换为数字100的正确的是( A )
# 2、下列程序执行结果是( A  )
# 3、如何在列表中添加一个元素
# 定义初始值
l = ['A', 'B', 'C']
# 调用添加append方法
l.append('A')
# 输出
print(l)

# 4、对于列表什么是越界
# 是用、访问了列表不存在的元素

# 5、说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 不可变数据(Number、String、Tuple)和可变数类型(List、Dictionary、Set)

# 6、从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
print('第六题：')
students = ['abc','def','ghi','jgk','lmn']
for i in students:
    print(i[1])

# 7、随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
print('第七题：')
import random
seven  = []
index = 0
while index <5:
    seven.append(str(random.randint(0,100) * -1))
    index +=1
print(seven)
# 8将下列两个列表合并，将合并后的列表升序并输出.
# list1 = [1,3,4,5,7]
# list2 = [0,66,8,9]
print('第八题：')
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
list1.extend(list2)
list1.sort(reverse=False)
print(list1)
# 9使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘
# 的输入，储存完输出至终端.
""""
print('第九题：')
dict1 = {'name', 'age','no'}
name = input('请输入姓名')
dict1['name'] = name
age = input('请输入年龄')
dict1['age'] = age
no = input('请输入学号')
dict1['no'] = no
print('姓名:%d'%dict1['name'],'年龄:%d'%dict1['age'],'学号:%d'%dict1['no'])
"""

# 10 有下列字典 dict1,查找值为“lebo”对应的 key 并输出到终端.(结果应该是输出
# school)
print('第十题：')
dict10 = {'school':'lebo','date':2018,'address':'beijing'}
for k,v in dict10.items():
    if v == 'lebo':
        print(k)

# 11 使用切片翻转列表 num，将翻转完后的列表中所有偶数位置的元素相加求和并
# 输出至终端.
# num = [0,1,2,3,4,5,6,7,8,9 ]
print('第十一题：')
num = [0,1,2,3,4,5,6,7,8,9 ]
num.sort(reverse=True)
numTotal = index = 0
for k in num:
    index += 1
    if (index % 2) == 0:
        numTotal += k
print(numTotal)