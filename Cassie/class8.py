"""
__project__='代码'
__file_name__='class8'
__author__='videt'
__time__ = '2021/4/9 16:09'
__product_name =PyCharm
"""
# 第一张：4,5,6
# 4、python2、python3默认的编码格式分别是什么？
#   Python2 中默认把脚步文件使用 ASCII 来处理
#   python3 中默认把脚步文件使用 UTF-8 来处理

# 5、将"hello_new_werld"按“_”符进行切割
# my_str = 'hello_new_werld'
# print(my_str.split('_'))

# 6、将数字1以“001”的格式输出到屏幕
# for i in range(1,2):
#     m="%03d" % i
#     print(m)

# 第二张：第1到第7题
#  1、字典a={1: 1},是否正确？
#  不正确 结尾有逗号

# 2、请合并列表a=[1,2,3,4]和列表b=[5,6,7,8]
# a=[1,2,3,4]
# b=[5,6,7,8]
# c = a + b
# print(c)

# 3、列表a,请写出实现正序排列，倒序排列，逆序排列的内置方法
#  正序：list.sort()
#  倒序：list[::-1]
#  逆序：list.reverse()

#  4、字典d={"k":1,"v"2},请写出d.items()结果
# dict_items([('k', 1), ('v', 2)])

# 5、复杂列表[{"k":1,"v";2},{"k":12,"v":22},{"k":13,"v":32}请用内置方法写出按k倒序排列的代码
# sorted(a, key=lambda keys:keys['k'])

# 6、集合s=set([1,2,3,4])),d=set([2,4,9,0,3]),请用内置方法写出他们的并集，交集，对集差
#   s & d
#   s | d
#   s - d

# 7、如何反列表a=["a","b"]里的各项，转为字符串并用逗号‘，’连接
#  ','.join(a)

# 第三张：1,2,3
#  2、一行代码实现示1到100的和
#  sum(rang(1,101))

#  3、使用random,random方法实现随机输出范围在（25，60）中的浮点数
# import random
# a = random.uniform(25, 60)
# print(a)


# 第四张：2
# 2、一个list对象：a=[1,2,4,3,2,2,4],需要去掉里面的重复值
# a = [1, 2, 4, 3, 2, 2, 4]
# a = list(set(a))
# print(a)