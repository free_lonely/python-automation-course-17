# 1、下列程序结果是______.
# 结果是输出所有结果
# class A:
#
#     def __init__(self):
#         print("初始化")
#
#     def __del__(self):
#         print("销毁")
#
#     def __str__(self):
#         return "类A"
# a = A()
# print(a)



# 2. 下列程序执行结果______.
# 结果是动物在叫---汪汪汪
#
# class Animal(object):
#
#     def shut(self):
#         print("动物在叫")
# class Dog(Animal):
#
#     def shut(self):
#         super().shut()
#         print("汪汪汪")
# dog = Dog()
# dog.shut()

# 3. 创建猫类：
# 	类名：Cat
# 	属性：无
# 描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.

# class Cat(object):
#
#     def __init__(self):
#         print( '喵？喵？喵？')
# cat = Cat()
# print(cat)

4.
# 创建计算器类：
# 类名：Calculator
# 属性：number_1（数字一）、number_2（数字二）
# 方法：

# def __init__(self, number_1, number_2):

# 类的初始化方法
#      def add(self)  # 返回数字一加数字二的值
#      def sub(self)  # 返回数字一减去数字二的值
#      def div(self)  # 返回数字一除以数字二的值
#      def mul(self)  # 返回数字一乘以数字二的值
# 描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.

# class Calculator(object):
#     def __init__(self, number_1, number_2):
#         self.number1 = number_1
#         self.number2 = number_2
#     # 返回数字一加数字二的值
#     def add(self):
#         print('%d 加上 %d等于 %d' % (self.number1, self.number2, (self.number1 + self.number2)))
#     def sub(self): # 返回数字一减去数字二的值
#         print('%d 减去 %d等于 %d' % (self.number1, self.number2, (self.number1 - self.number2)))
#     def div(self): # 返回数字一除以数字二的值
#         print('%d 除以 %d等于 %d' % (self.number1, self.number2, (self.number1 / self.number2)))
#     def mul(self): # 返回数字一乘以数字二的值
#         print('%d 乘以 %d等于 %d' % (self.number1, self.number2, (self.number1 * self.number2)))
# cal = Calculator(4, 2)
# cal.add()
# cal.sub()
# cal.div()
# cal.mul()

# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
# class Animal:
#
#     def shut(self):
# 		# 打印叫声
#         pass
#
#     def eat(self):
# 		# 打印爱吃的食物
#         pass
# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
# class Animal(object):
#
#     def shut(self):
#      print('%s的叫声' % self.name)
#
#     def eat(self):
#         print('%s爱吃的食物' % self.name)
# 5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
# class Cat(Animal):
#     def shut(self):
#         print("%s的叫声：喵， 喵， 喵喵" % self.name)
#     def eat(self):
#         print('%s爱吃的食物是鱼' % self.name)
#
# class Dog(Animal):
#     def shut(self):
#         print("%s的叫声：汪， 汪， 汪汪" % self.name)
#     def eat(self):
#         print('%s爱吃的食物是肉' % self.name)
#
# cat = Cat()
# cat.name = '小花'
# cat.eat()
# cat.shut()
# dog = Dog()
# dog.name = '小白'
# dog.eat()
# dog.shut()



# 1、面向对象三大特性是（封装、继承、多态）
# 2、单例模式创建保证实例只创建___1___次.













