# 第一张作业
# [python] python2x 和 python3x默认的编码格式分别是什么
# python3x 默认的编码格式是UTF-8
# python2x 默认的编码格式是US-ASCLL

# [python] 将'hello_new_world'按'_'符进行切割
# str = 'hello_new_world'
# list = str.split('_')
# print(list)

# [python] 将数字 1 以 '0001'的格式输出到屏幕上
# i = 1
# print('000' + str(1))


# 第二张作业
# 字典 a = {1: 1}，是否正确
# 是正确的

# 请合并列表 a = [1, 2, 3, 4]和列表b = [5, 6, 7, 8]
# a = [1, 2, 3, 4]
# b = [5, 6, 7, 8]
# c = a + b
# print(c)

# 列表a， 请写出实现正序排列，倒序排列，逆序排列的内置方法
# a = [1, 2, 3, 4]
# a.sort(reverse=False)
# a.sort(reverse=True)
# a.reverse()
# print(a)

# 字典d = {"k": 1, "v": 2} 请写出d.items()结果
# d = {"k": 1, "v": 2}
# list = d.items()
# [("k", 1),("v", 2)]

# 复杂列表[{"k": 1, "v": 2}, {"k": 12, "v": 22}, {"k": 13, "v": 32}],请用内置方法写出按'k'倒序排列的代码
#list = [{"k": 1, "v": 2}, {"k": 13, "v": 32}, {"k": 12, "v": 22}]
list = [{"k": 1, "v": 2}, {"k": 12, "v": 22}, {"k": 13, "v": 32}]
list.reverse()
# list.sort(key=lambda x : x['k'], reverse=True)
print(list)


# print(list[::-1])
#
# z = 0
# while z < len(list) - 1:
#     for i in range(len(list)):
#         if i - 1 < 0 :
#             continue
#         if list[i].get('k') > list[i-1].get('k'):
# #            print(list[i].get('k'),'---------',list[i-1].get('k'))
#             list[i], list[i-1] = list[i-1], list[i]
# #            print(list)
#     z += 1
# print(list)


# 集合s = set([1, 2, 3, 4]), d = ([2, 4, 9, 0, 3]),请用内置方法写出他们的并集，交集，对  差
# s = set([1, 2, 3, 4])
# d = set([2, 4, 9, 0, 3])
# y = s & d
# y 输出的结果是{2, 3, 4}
# z = s | d
# z 输出的结果是{0, 1 ,2, 3, 4, 9}

# 如何把列表 a = ["a", "b"]里的各项，转为字符串并用逗号","连接
# a = ["a", "b"]
# z = ''
# for i in a:
#     if z == '':
#         z += i
#     else:
#         z += ',' + i
# print(z)




# 第三张作业
"""
1.判断下列描述是否正确，如果有错误，请指出错误的地方并改正
    ①。字典：具有键值映射关系，无序，字典的键不能重复并可以是任意数据类型
    字典的减是不能重复的，但是数据类型只能是不可变数据类型
    ②。元组：不能修改，无序，不能索引切片，当元组中只有一个元素时，需要在元素的后面加逗号
    元组是可以所有和切片的,当元组中只有一个元素时，需要在元素的后面加英文逗号
    ③。列表：元素可以重复，有序，不可以反向索引，元素可以是任意类型
    列表是可以反向索引的
    ④。集合：元素不可以重复，可以索引，a = {} 声明了一个空集合
    集合的空集合是 set()
    
"""

# 2.一行代码实现求1 到 100 的和
# print((1+100)*100/2)
print(sum(range(1, 101)))


# 3.按照下面要求写出完整代码：使用random.random方法实现随机输出范围在[25， 60]中的浮点数。
# import random
# fla = 25 + random.random() * 35
# print(fla)




# 第四张作业
# 一个list对象：a = [1, 2, 4, 3, 2, 2, 4],需求去掉里面的重复值
# a = [1, 2, 4, 3, 2, 2, 4]
# b = set(a)
# b = list(b)
# print(b)


























