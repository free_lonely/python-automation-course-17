import time
import os

# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
info_list = []

def print_menu():
    print("---------------------------")
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")


# -----添加学生信息-----
def add_new_info():
    print("-----添加学生信息-----")
    # global info_list   # 把局部变量变成全局变量
    # 定义需要输入的内容，使用input方法
    new_name = input('请输入学生姓名：')
    new_tel = input("请输入手机号:")
    new_qq = input("请输入QQ:")

    # 这里是循环列表对比是否存在这个学员
    for i in info_list:
        if i['name'] == new_name:
            print("该名字已存在")
            return    # 这里加上return 主要是用于判断如果 存在这名学员就结束这个操作

    # 定义一个字典来存储学生信息
    info = {}
    info['name'] = new_name
    info['tel'] = new_tel
    info['qq'] = new_qq
    # 把学员的字典存入列表中
    info_list.append(info)
    print(info_list)


# -----删除学生信息-----
# "请输入要删除的序号:"
# 你确定要删除么?yes or no
# 输入序号有误, 请重新输入
def del_info():
    print("-----删除学生-----")
    number = int(input('请输入要删除的序号:'))
    for i in info_list:
        print(type(i['id']),i['id'])
        if number == i['id']:
            dec = input('你确定要删除什么？yes or no：')
            if dec == 'yes':
                info_list.remove(i)
                print(info_list)
                return
    print("输入序号有误，请重新输入")




# -----修改学生-----
def modify_info():
    print("-----修改学生-----")
    number = int(input("请输入需要修改的学生的学号："))
    for i in info_list:
        if number == i['id']:
            new_name = input('请输入学生姓名：')
            new_tel = input("请输入手机号:")
            new_qq = input("请输入QQ:")
            i['name'] = new_name
            i['tel'] = new_tel
            i['qq'] = new_qq
            print(info_list)
            return
    print("学号不存在")




# -----查询学生-----
def search_info():
    print("-----查询学生-----")
    sear_name = input("请输入需要查询的额学生信息：")
    for i in info_list:
        if sear_name == i['name']:
            print("%s学生的信息是（姓名：%s, 手机号：%s， qq号：%s" %  (i['name'], i['name'], i['tel'], i['qq']))
            return
    print("该学生不存在")




# -----遍历所有学生-----
# 这里是把文件中的数据进行遍历，并把数据进行编号
def print_all_info():
    global info_list
    i = 0
    info_list_add = []
    for info in info_list:
        info['id'] = i
        info_list_add.append(info)
        print('%d, %s, %s, %s' % (i, info['name'], info['tel'], info['qq']))
        i += 1
    info_list = info_list_add      #通过赋值把新的列表赋值给学习表
    print(info_list)


#-----保存数据到文件-------
# 这里是把数据保存到我的文件里面
def save_data():
    add_info = open('info_data.data','w',encoding='UTF-8')
    add_info.write(str(info_list))
    add_info.close()


# 先定义一个列表,查询列表中信息
# 这里是从定义的文件里面去读取数据
def load_data():
    global info_list
    info_data = open('info_data.data','r',encoding='UTF-8')
    info_list = eval(info_data.read())
    print(type(info_list),info_list)
    info_data.close()




def main():
    """用来控制整个流程"""

    # 加载数据（1次即可）
    load_data()

    while True:
        # 1. 打印功能
        print_menu()

        # 2. 获取用户的选择
        num = input("请输入要进行的操作(数字):")

        # 3. 根据用户选择,做相应的事情
        if num == "1":
            # 添加学生
            add_new_info()
        elif num == "2":
            # 删除学生
            del_info()
        elif num == "3":
            # 修改学生
            modify_info()
        elif num == "4":
            # 查询学生
            search_info()
        elif num == "5":
            # 遍历所有的信息
            print_all_info()
        elif num == "6":
            # 保存数据到文件中
            save_data()
        elif num == "7":
            # 退出系统
            exit_flag = input("亲,你确定要退出么?~~~~(>_<)~~~~(yes or no) ")
            if exit_flag == "yes":
                break
        else:
            print("输入有误,请重新输入......")


        input("\n\n\n按回车键继续....")
        os.system("clear")  # 调用Linux命令clear完成清屏

# 程序的开始
main()






































