
import  random
"""

str1 = "1234567890"
# 截取字符串的第一位到第三位的字符
str2 = str1[:3:1]
print(str2)
# 截取字符串最后三位的字符
str3 = str1[-3::1]
print(str3)
# 截取字符串的全部字符
str4 = str1[::1]
print(str4)
# 截取字符串的第七个字符到结尾
str5 = str1[6::1]
print(str5)
# 截取字符串的第一位到倒数第三位之间的字符
str6 = str1[2:-3:1]
print(str6)
# 截取字符串的第三个字符
str7 = str1[2]
print(str7)
# 截取字符串的倒数第一个字符
str8 = str1[-1]
print(str8)
# 截取与原字符串顺序相反的字符串
str9 = str1[::-1]
print(str9)
# 截取字符串倒数第三位与倒数第一位之间的字符
str10 = str1[-3:-1]
print(str10)
# 截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。(结果应该为2468)
str11 = str1[1:-1:2]
print(str11)
# 注意：如果对于第几位有歧义 属于正常 按照自己的意愿得到结果即可
"""

# 列表练习
name_list = ['赵一','钱二','张三','李四','王五']
for i in name_list:
    print(i, end= '')
print()
j = 0
while j < len(name_list):
    print(name_list[j], end= '')
    j += 1
print()
"""
常用函数
len()    len这个函数主要的作用是一个计算长度的方法
添加或新增
append()   append向列表里面添加参数，同时append添加参数没有类型限制
    name_list.append('孙六')    name_list.append(1)    name_list.append([1])
extend()   通过extend可以将另一个集合中的元素逐一添加到列表中,
           如果括号里面的是一个字符串，会把字符串按照下标一个一个的当成参数添加进列表中
    name_list.extend('孙六')    lists = [1,[1]]    name_list.extend(lists)
    lists = [1,[1]]
    a = ['孙六']
    name_list.extend('孙六')
    name_list.extend(lists)
    print(name_list)
"""


# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序，完成随机的分配

school = [[],[],[]]
ther_list = ['赵','钱','孙','李','周','吴','郑','王']
for a in ther_list:
    intt = random.randint(0,2)
    school[intt].append(a)
print(school)


























