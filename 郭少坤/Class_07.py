
import  random

"""

# 五 用任意编程语言循环输出整数0到10

for i in range(11):
    print(i ,' ', end= '')
j = 0
print()
while j <= 10:
    print(j, ' ', end='')
    j += 1

# 六 使用任意语言实现逆序输出一个字符串 ，比如 |欢乐逛欢迎您|，输出|您迎欢逛乐欢|

str = '|欢乐逛欢迎您|'
print(str[::-1])
"""


"""

# python  基础
# 1 str = ‘a,hello’ 在字符串str中查到hello
str = 'a,hello'
print(type(str.count('hello')))
print(str.endswith('hello'))
print(str.find('hello'))


# 2 str = ‘a,b,c,d’ 用逗号分割str字符串，并保存到列表
str_list = 'a,b,c,d'
my_list = str_list.split(',')
print(my_list)

# 3 将‘笔试题123A’中 123 替换为 ‘进行中’
strti =  '笔试题123A'
print(strti.replace('123','进行中'))

"""





"""

# 列表练习
name_list = ['赵一','钱二','张三','李四','王五']
for i in name_list:
    print(i, end= '')
print()
j = 0
while j < len(name_list):
    print(name_list[j], end= '')
    j += 1
print()

"""

"""
常用函数
len()    len这个函数主要的作用是一个计算长度的方法
添加或新增
append()   append向列表里面添加参数，同时append添加参数没有类型限制
    name_list.append('孙六')    name_list.append(1)    name_list.append([1])
extend()   通过extend可以将另一个集合中的元素逐一添加到列表中,
           如果括号里面的是一个字符串，会把字符串按照下标一个一个的当成参数添加进列表中
    name_list.extend('孙六')    lists = [1,[1]]    name_list.extend(lists)
lists = [1,[1]]
a = ['孙六']
name_list.extend('孙六')
name_list.extend(lists)
name_list.insert()
print(name_list)

"""







# 1.下列将字符串"100"转换为数字100的正确的是( A )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

# 2.下列程序执行结果是( A  )
# numbers = [1，5，3，9，7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1

# 3.如何在列表中添加一个元素
# name_list.append(‘王五’)
# name_list.insert(1,’张三’)
# name_list.extend(‘李四’)

# 4.对于列表什么是越界
# 不在列表下标范围内的下标都是越界

# 5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 可变类型： list , dict
# 不可变类型：int , string , bool, tuple

# 6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
# i = 0
# my_list1 = []
# while i < 5:
#     my_list = input('请输入学生姓名：')
#     my_list1.append(my_list)
#     i += 1
# print(my_list1)
#
# for j in my_list1:
#     print(j[1],' ',end='')

# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
# int_list = []
# i = 1
# while i <= 5:
#     index = random.randint(0,100)
#     int_list.append(-index)
#     i += 1
# print(int_list)



# 8.将下列两个列表合并，将合并后的列表升序并输出.
# list1 = [1,3,4,5,7]
# list2 = [0,66,8,9]
# for i in list1:
#     list2.append(i)
# print(list2)
# list2.sort(reverse=False)
# print(list2)


# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
# i = int(input('请输入次数：'))
# info = {}
# j = 0
# while j < i:
#     na = input("请输入学生姓名：")
#     info['name']  = na
#     ag = input('请输入学生年龄：')
#     info['age'] = ag
#     nu = input('请输入学生学号：')
#     info['num'] = nu
#     j += 1
# print(info)



# 10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
# dict1={"school":"lebo","date":2018,"address":"beijing"}
# value = dict1.items()
# for i in value:
#     j = i.count('lebo')
#     if j > 0:
#         print(i[0])

# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
# num = [0,1,2,3,4,5,6,7,8,9]
# num1 = num[::-1]
# nu = num1[1::2]
# j = 0
# for i in nu:
#     j += i
# print(j)





















