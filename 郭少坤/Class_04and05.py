

"""
# tab ====> 四个空格
# 回退一个tab键====> shift tab
# (取消)注释的快捷键====> ctrl + ?
# 写错时回退 ctrl + z
# ctrl + a  =====  全选
"""

"""
（1）for 循环练习
name = 'student'
for i in name:
    print('%-2s' % i , end = '')
print()
for j in range(1, 5):
    print('%-2s' % j , end = '')
print()
"""


"""
（2）while 循环练习
九九乘法表
1x1=1    
1x2=2    2x2=4    
1x3=3    2x3=6    3x3=9    
1x4=4    2x4=8    3x4=12    4x4=16    
1x5=5    2x5=10    3x5=15    4x5=20    5x5=25    
1x6=6    2x6=12    3x6=18    4x6=24    5x6=30    6x6=36    
1x7=7    2x7=14    3x7=21    4x7=28    5x7=35    6x7=42    7x7=49    
1x8=8    2x8=16    3x8=24    4x8=32    5x8=40    6x8=48    7x8=56    8x8=64    
1x9=9    2x9=18    3x9=27    4x9=36    5x9=45    6x9=54    7x9=63    8x9=72    9x9=81

a = 1
while a <= 9:
    i = 1
    while i <= a:
        print('%d*%d=%-4d' % (i, a, i * a), end='')
        i += 1
    print()
    a += 1

"""


"""
①.作业练习
1、使用while，完成以下图形的输出
*
* *
* * *
* * * *
* * * * *
* * * *
* * *
* *
*

# 第一种方法
a = 1
b = '*'
c = 4
while a <= 5:
    print('%s ' % b * a)
    a += 1

while c > 0:
    print('%s ' % b * c)
    c -= 1

#第二种方法
a = 1
c = 4
while a <= 5:
    b = 1
    while b <= a:
        print("* ", end='')
        b += 1
    print() 
    a += 1
while c > 0:
    b = 1
    while b <= c:
        print("* ", end='')
        b += 1
    print()
    c -= 1
    
#第三种方法
a = 1
while a <= 9:
    if a <= 5 :
        b = 1
        while b <= a:
            print("* ", end='')
            b += 1
    else:
        b = 9 - a + 1
        while 1 <= b:
            print("* ", end='')
            b -= 1
    print()
    a += 1

"""


"""
②.作业练习
# 字符串是不可变的数据类型
# 作业1. 如何及打印出内容，又显示下标的编号
name = 'hadlajlldjlajdl'
i= 0
while i < len(name):
    my_Str = name[i]
    print("输出的内容：%s-----对应的下标：%d" % (my_Str, i))
    i += 1
print("退出-----name长度%d" % len(name))

f = 0
for j in name:
    print("输出的内容：%s-----对应的下标：%d" % (j, f))
    f += 1
print('退出')
"""


"""
③.作业练习
break和continue的区别
break和continue都是只能存在于循环语句中
break的作用是只要条件符合后执行break就会终止break最近的这一层循环
continue的作用是条件符合后执行continue就会跳过这一次循环，进入下一次循环之中
name = 'abcdefg12345'
j = 0

第一种练习
while j < 5 :
    for i in name:
       # print(i)          # 这里 最后输出的结果是 a b c d e f
        if  i == 'f':
           # break         # 这里没有到break 的时候都会跳出最近的for循环进入到外层的while
            continue       # 这里如果条件符合使用的是continue 就会跳出这一次循环，进入到下一次循环中
            print("输出的结果%s" % i)
        print(i)           # 这里 最后输出的结果是 a b c d e
    j += 1                 # while 循环中 循环的变量如果没有自增的化就会陷入无限死循环之中
print("退出")

第二种练习
for i in name:
     # print(i)            # 这里 最后输出的结果是 a b c d e f 1 2 3 4 5
     if  i == 'f':
        break              # 这里如果条件符合使用的是continue 就会跳出这一次循环，进入到下一次循环中
        print("输出的结果%s" % i)
     print(i)              # 这里 最后输出的结果是 a b c d e 1 2 3 4 5
else:                      # 这里 for循环后面是可以使用else的，它们相当于是一个循环，如果循环中途停止了就不会在执行else 
    print("退出")


name = 'abcdef12345'
j = 0
while j < len(name):
    # j += 1
    my_str = name[j]
    i = name.find('f')
    print(j , i)
    if  i == j:
        print('中途停止')
        break
    j += 1           # 记住 while 里面的变量要自增或自检符合判断条件 不然就会进入死循环
else:                # 这里 while 循环后面是可以使用else的，它们相当于是一个循环，如果循环中途停止了就不会在执行else
    print("退出")


 
"""


"""
④.按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
str1 = '1234567890'
# 1.	截取字符串的第一位到第三位的字符
str2 = str1[:3]
print('1.	截取字符串的第一位到第三位的字符:%s ' % str2)
# 2.	截取字符串最后三位的字符
str3 = str1[-3::]
print('2.	截取字符串最后三位的字符:%s' % str3)
# 3.	截取字符串的全部字符
str4 = str1[:]
print('3.	截取字符串的全部字符:%s' % str4)
# 4.	截取字符串的第七个字符到结尾
str5 = str1[6:]
print('4.	截取字符串的第七个字符到结尾:%s' % str5)
# 5.	截取字符串的第一位到倒数第三位之间的字符
str6 = str1[1:-3]
print('5.	截取字符串的第一位到倒数第三位之间的字符:%s' % str6)
# 6.	截取字符串的第三个字符
str7 = str1[2]
print('6.	截取字符串的第三个字符:%s' % str7)
# 7.	截取字符串的倒数第一个字符
str8 = str1[-1]
print('7.	截取字符串的倒数第一个字符:%s' %str8)
# 8.	截取与原字符串顺序相反的字符串
str9 = str1[::-1]
print('8.	截取与原字符串顺序相反的字符串:%s' % str9)
# 9.	截取字符串倒数第三位与倒数第一位之间的字符
str10 = str1[-3:-1]
print('9.	截取字符串倒数第三位与倒数第一位之间的字符:%s' % str10)
# 10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。(结果应该为2468)
str10 = str1[1:-1:2]
print('10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。:%s' % str10)

# 1.截取2 - 5位置的字符

# 2.截取2 - 末尾的字符

# 3.截取从开始 -5 位置的字符串

# 5.从开始位置，每隔一个字符截取字符串

# 6.从索引1开始，每隔一个取一个

"""







