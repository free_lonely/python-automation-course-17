# 1、创建一个对象后默认调用(A)
# 这里当调用方法的时候会默认自动调用init
# A、__init__    B、__str__    C、__add__    D、__and__
# 2、类是对象的_______、对象是类的_______.
#    类是对象的抽象，对象是类的实例
# 3、对象是由_______、_______两部分构成.
#    对象又属性和方法组做
# 4、创建学生类：
# 	类名：Student
# 	属性：name（姓名）、age（年龄）、sex（性别）
# 方法：
# 	def info(self) # 打印学生的姓名、年龄、性别
# 	def draw(self) #打印”XX会画画呢”
# 描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.

# class Student(object):
#
#     def info(self):
#         # 第二种输入方式
#         print('姓名：%s' % xiaoming.name)
#         print('年龄：%s' % xiaoming.age)
#         print('性别：%s' % xiaoming.sex)
#     def draw(self):
#         print("%s会画画呢" % self.name)
#
# xiaoming = Student()
# xiaoming.name = '小明'
# xiaoming.age = 18
# xiaoming.sex = '男'
# # 第一种输入方式
# print('姓名：%s' % xiaoming.name)
# print('年龄：%s' % xiaoming.age)
# print('性别：%s' % xiaoming.sex)
#
# # 第二种输入方式
# xiaoming.info()
# xiaoming.draw()


# 4、创建动物类：
# 	类名：animal
# 	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
# 方法：
# 	def info(self) # 打印姓名、年龄、毛颜色
# 	def run（self）#打印“XX会跑呢”
# 描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.

# class Animal(object):
#     def __init__(self, names, ages, colors):
#         self.name = names
#         self.age = ages
#         self.color = colors
#
#     def info(self):
#         print(self.name)
#         print(self.age)
#         print(self.color)
#
#     def run(self):
#         print('%s会跑呢' % self.name)
# dog = Animal('小狗', 2, '白色')
# dog.info()
# dog.run()