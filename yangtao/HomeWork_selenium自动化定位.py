# #导入selenium web驱动包
# from selenium import webdriver
# # 导入时间包,web页面可以有几秒时间
# from time import sleep
# # 实例化谷歌浏览器
# driver = webdriver.Chrome()
#
# # 注册页面地址
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
#
# # 打开注册页面
# driver.get(url)
#
# # 通过id定位账号
# username = driver.find_element_by_id('userA')
# # 发送关键字
# username.send_keys('admin')
# # 定位密码
# password = driver.find_element_by_id("passwordA").send_keys("pass")
#
#
# # 暂停3秒
# # sleep(3)
# # 关闭浏览器
# # driver.quit()

# 导入selenium包
# from selenium import webdriver
# from time import sleep
# # 实例化谷歌浏览器
# driver = webdriver.Chrome()
# # 地址
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# # 获取地址打开注册页面
# driver.get(url)
# # 通过name定位账号
# driver.find_element_by_name("userA").send_keys('admin')
# # 定位密码
# driver.find_element_by_name("passwordA").send_keys('123')
# # 暂停3秒
# sleep(3)
# # 关闭浏览器
# driver.quit()

# 单击
# 导包
# from selenium import webdriver
# from time import sleep
# # 实例化浏览器
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# # 打开注册页面
# driver.get(url)
# # click()单击
# driver.find_element_by_link_text("访问 新浪 网站").click()
# sleep(3)
# driver.quit()

# 模糊点击超链接
# 导包
# from selenium import webdriver
# from time import sleep
# # 实例化浏览器
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# # 获取地址
# driver.get(url)
# # 点击
# driver.find_element_by_partial_link_text("访问").click()
# sleep(3)
# driver.quit()

# 通过name定位
# from selenium import webdriver
# from time import sleep
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# driver.get(url)
# driver.find_element_by_class_name("telA").send_keys("183xxxxxxx")
# sleep(6)
# driver.quit()

# 通过标签名定位
# from selenium import webdriver
# from time import sleep
# # 实例化浏览器
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# driver.get(url)
# # driver.find_element_by_tag_name:返回符合条件的第一个元素
# driver.find_element_by_tag_name("input").send_keys("hello")
# sleep(6)
# # 关闭浏览器
# driver.quit()

# 通过列表下标定位elements
# from selenium import webdriver
# from time import sleep
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# driver.get(url)
# # driver.find_elements_by_tag_name 返回数据格式列表，访问列表数据格式必须要指定下标，默认从0开始
# driver.find_elements_by_tag_name("input")[0].send_keys("admin")
# driver.find_elements_by_tag_name("input")[2].send_keys("1234")
# sleep(5)
# driver.quit()

# 使用xpath绝对路径定位
# from selenium import webdriver
# from time import sleep
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# driver.get(url)
# driver.find_element_by_xpath("//*[@id='telA']").send_keys('1235555')
# driver.find_element_by_xpath("//*[@id='userA']").send_keys('admin')
# sleep(5)
# driver.quit()

# 使用css元素定位
# from selenium import webdriver
# from time import sleep
# driver = webdriver.Chrome()
# url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
# driver.get(url)
# driver.find_element_by_css_selector("#userA").send_keys("admin")
# driver.find_element_by_css_selector("#passwordA").send_keys("123")
# driver.find_element_by_css_selector("#telA").send_keys('189xxxxxx')
# driver.find_element_by_css_selector("#emailA").send_keys("email")
# sleep(3)
# driver.find_element_by_css_selector("button").click()
# driver.quit()

# 使用By定位元素
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
# 实例化对象
driver = webdriver.Chrome()
url = "file:///F:/%E4%B9%90%E5%8D%9A%E5%AD%A6%E9%99%A2%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99/%E4%B9%90%E6%90%8F%E8%87%AA%E5%8A%A8%E5%8C%96%E8%AF%BE%E7%A8%8B/%E7%AC%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E8%8A%82%E8%AF%BE/source/%E6%B3%A8%E5%86%8CA.html"
driver.get(url)
# 使用find_element()定位元素
driver.find_element(By.NAME,"userA").send_keys('admin')
driver.find_element(By.ID,'telA').send_keys('198xxxxx')

sleep(5)
driver.quit()


