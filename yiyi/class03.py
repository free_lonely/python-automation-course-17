# 1、使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
# * *
# *

i = 1
while i <= 5:
    j = 1
    while j <= i:
        print('* ', end='')
        j += 1
    print()
    i += 1
i = 4
while i >= 1:
    j = 1
    while j <= i:
        print('* ', end='')
        j += 1
    i -= 1
    print()


# 2、break和continue的区别：
# break:结束本层本次循环，continue：跳过本层本次循环，进入下一次循环



