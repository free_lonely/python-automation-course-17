class04_work
# 1、使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
# * *
# *

#打印出
# *
# * *
# * * *
# * * * *
# * * * * *
for i in range(0,5):
    for j in range(0,i + 1):
        print('*',end = ' ')
    print('')
#打印出
# * * * *
# * * *
# * *
# *
for i in range(0,4):
    for j in range(0,4-i):
        print('*',end = ' ')
    print('')

print('===============================')
#打印九九乘法表：
i = 1
while i <= 9:
    # print(i)
    j = 1
    while j <= i:
        print(str(i) + '*' + str(j) + '=' + str(i * j),end = ' ')
        j += 1
    i += 1
    print()