# -*- coding:utf-8 -*-
# @time : 2021/4/30 13:09
# @Author : DingTingTing
# @Email : 1830188186@qq.com
# @Fole : class17.py
"""
python 操作mysql
- commit 成功时提交
- rollback 失败时回滚
- sql注入：excute方法中写  excute 中有多个参数时，使用的是列表
- 字典：使用import pymysql导入时变成字典(原本的是元组)----pymysql.cursors.distcursor
- scroll：移动
"""
import pymysql
from practice.dbinfo import dbinfo
# 连接数据库的第一种方法：
# class DataBase(object):
#     def __init__(self, host, port, user, password, database,charset):
#         self.host = host
#         self.port = port
#         self.user = user
#         self.password = password
#         self.database = database
#         self.charset = charset # 编码格式
# if __name__ == '__main__':
#     db1 = DataBase('49.233.39.160', 3306, 'user', 'leboAa!#$123', 'lebo16', 'utf8')

# 连接数据库的第二种方法：
class DataBase(object):
    def __init__(self, host=dbinfo['host'], port=dbinfo['port'], user=dbinfo['user'], password=dbinfo['password'], database=dbinfo['database'], charset=dbinfo['charset']):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.charset = charset

    def getConnection(self):
        

if __name__ == '__main__':
    db1 = DataBase()

