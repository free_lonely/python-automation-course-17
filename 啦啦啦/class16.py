# -*- coding:utf-8 -*-
# @time : 2021/4/30 11:06
# @Author : DingTingTing
# @Email : 1830188186@qq.com
# @Fole : class16_work.py

import pymysql
def create_table():
    # 连接数据库
    db = pymysql.connect(host='49.233.39.160',
                    user='user',
                    database='lebo16',
                    port='3306',
                    passwd='leboAa!#$123'
    )
    # 获取由标（征求同意，要求对数据库进行操作）
    cursor = db.cursor()
    # 征求同意后，对数据库进行操作
    cursor.execute('drop table if exists students')
    # 操作之后，关闭由标
    cursor.close()
    # 关闭数据库
    db.close()
