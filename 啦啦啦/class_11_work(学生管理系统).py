class StudentSystem():

    def __init__(self):
        self.stu_list = []

    # 添加学生信息
    def add(self):
        stu_dict = {}
        stu_dict['name'] = input('请输入姓名：')
        stu_dict['tel'] = input('请输入电话：')
        stu_dict['qq'] = input('请输入QQ：')
        if len(self.stu_list) == 0:
            # 若学生列表为空，则取学生id为1
            stu_dict['id'] = 1
        else:
            # 若学生列表中有值，则按列表中最后一个学生的id+1
            stu_dict['id'] = self.stu_list[len(self.stu_list) - 1]['id'] + 1

        # 判断输入的姓名存在
        if stu_dict['name'] in [item['name'] for item in self.stu_list]:
            print('您输入的用户名已存在，请重新输入')
        else:
            self.stu_list.append(stu_dict)
            print('学生信息添加成功！')

    # 删除学生信息
    def remove(self):
        id = int(input('请输入要删除的学生id：'))
        # 找出学生id
        id_list = [item['id'] for item in self.stu_list]

        # 判断id是否存在
        if id in id_list:
            answer = input('你确定要删除么? (yes or no)')
            if answer == 'yes':
                del self.stu_list[id_list.index(id)]
        else:
            print('您输入的学生id不存在')

    # 修改学生信息
    def edit(self):
        id = int(input('请输入要修改的学生id：'))
        # 找出列表中存在的学生id
        id_list = [item['id'] for item in self.stu_list]

        if id in id_list:
            # 根据学生id的查找学生字典
            student = self.stu_list[id_list.index(id)]
            # 更新学生信息
            student['name'] = input('请输入新的姓名：')
            student['tel'] = input('请输入新的电话：')
            student['qq'] = input('请输入新的QQ：')
        else:
            print('您输入的学生id有误，请重新输入')

    # 查询学生信息
    def query(self):
        name = input('请输入要查询的学生姓名：')
        # 找出列表中存在的学生姓名
        name_list = [item['name'] for item in self.stu_list]
        # 判断姓名是否存在
        if name in name_list:
            stu = self.stu_list[name_list.index(name)]
            print('id：%s \n姓名：%s \n电话：%s \nQQ号：%s' % (stu['id'], stu['name'], stu['tel'], stu['qq']))
        else:
            print('没有找到您要找的学生信息')

    # 遍历所有学生信息
    def show(self):
        for stu in self.stu_list:
            print('id：%s \n姓名：%s \n电话：%s \nQQ号：%s' %(stu['id'],stu['name'],stu['tel'],stu['qq']))

    # 保存
    def save(self):
        print('上面已保存了数据，这块不晓得写啥？')

    # 退出学生系统：
    def exit(self):
        res = input("是否确认退出本系统？(yes or no)")
        if res == 'yes':
            print('已成功退出本系统！欢迎再次使用~')
        return res


print('欢迎进入学生管理系统V1.0')
# 是否再次循环，退出学生系统时将其置为False
sys_stu = StudentSystem()
loop = True
while loop:
    print(" \n 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7:退出系统")
    print("---------------------------")
    num = input('请您选择需要的服务：')
    if num == '1':
        sys_stu.add()
    elif num == '2':
        sys_stu.remove()
    elif num == '3':
        sys_stu.edit()
    elif num == '4':
        sys_stu.query()
    elif num == '5':
        sys_stu.show()
    elif num == '6':
        sys_stu.save()
    elif num == '7':
        loop = sys_stu.exit() != 'yes'













