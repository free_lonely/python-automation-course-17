# 1、用任意编程语言输出整数0到10
i = 1
while i <=10:
    print(i)
    i+=1
# 2、用任意编程语言实现逆序输出一个字符串，比如（欢乐逛欢迎您）输出（您迎欢逛乐欢）
str = 'abcdefg'
print(str[::-1])
# 3_1、str = 'a,hello' 在字符串中查找‘hello’
str1 = 'a,hello'
print(str1[2::1])
# 3_2、str = 'a,b,c,d' 用逗号分割str字符串，并保存到列表
str2 = 'a,b,c,d'
print(list(str2.split(',')))
# 3_3、将‘笔试题 123A’中123替换为‘进行中’
str3 = '笔试题 123A'
print(str3.replace('123','进行中'))

'''
1.下列将字符串"100"转换为数字100的正确的是( A ) 
A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

2.下列程序执行结果是(A)    #打印出来的是列表                                            
numbers = [1，5，3，9，7]                                                                          
numbers.sort(reverse=True)                                                                               
print(numbers)                                                                                      
A、[9，7，5，3，1]                                                                                                        
B、[1，3，5，7，9]                                                                                      
C、1，3，5，7，9                                                                                        
D、9，7，5，3，1

3.如何在列表中添加一个元素
答：列表名.append('值')  列表名.extend(['值'])  列表名.insert(下标索引,'值')

4.对于列表什么是越界
答：超出列表范围长度的是列表越界，比如查找一个下标索引超出了列表中下标索引的长度

5.说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
答：字符串、元组是不可变数据类型；列表、字典是可变的数据类型
'''

# 6.从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
name_1 = input('请输入一个学生的名字：')
name_2 = input('请输入一个学生的名字：')
name_3 = input('请输入一个学生的名字：')
name_4 = input('请输入一个学生的名字：')
name_5 = input('请输入一个学生的名字：')
list_name1 = [name_1,name_2,name_3,name_4,name_5]
print(list_name1[0][1])
print(list_name1[1][1])
print(list_name1[2][1])
print(list_name1[3][1])
print(list_name1[4][1])

print('==============')
i = 0
list_name = []
while i < 5:
    name_i = input('请输入一个学生的名字：')
    list_name.append(name_i)
    print(list_name[i][1])
    i += 1

# 7.随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
import random
num_1 = random.randint(0,100)
num_2 = random.randint(0,100)
num_3 = random.randint(0,100)
num_4 = random.randint(0,100)
num_5 = random.randint(0,100)
num_list = [num_1,num_2,num_3,num_4,num_5]
print(-num_list[0])
print(-num_list[1])
print(-num_list[2])
print(-num_list[3])
print(-num_list[4])

print('==============')
num_list1 = []
for i in range(0,5):
    num_i = random.randint(0,100)
    num_list1.append(num_i)
    print(-num_list1[i])

# 8.将下列两个列表合并，将合并后的列表升序并输出.
list1 = [1,3,4,5,7]
list2 = [0,66,8,9]
list = list1 + list2
list.sort(reverse = False) #(相等于list.sort())
#sort()默认升序
print(list)

# 9.使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
name = input('请输入姓名：')
age = input('请输入年龄：')
Sno = input('请输入学号：')
dict = {'name':name,'age':age,'Sno':Sno}
for ret in dict.items():
    print(ret)

# 10.有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
dict1 = {'school':'lebo','date':2018,'address':'beijing'}
keys = list(dict1.keys())
values = list(dict1.values())
i = values.index('lebo')
print(keys[i])

# 11.使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
num = [0,1,2,3,4,5,6,7,8,9]
num_1 = num[::-1]
num_2 = num_1[:10:2]
i = 0
sum = 0
while i < len(num_2):
    sum += num_2[i]
    i += 1
print(sum)

num1 = num[::-2]
sum = 0
for i in num1:
    sum += i
    print(sum)