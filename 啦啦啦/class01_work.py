# -*- coding:utf-8 -*-
# @time : 2021/4/15 22:15
# @Author : DingTingTing
# @Email : 1830188186@qq.com
# @Fole : class01_work.py
# print('hello world')
# file.write('我成功用代码创建了一个文件')
# file.close()

class Cat():
    def say(self):
        print('喵？喵？喵？')

cat_01 = Cat()
cat_01.say()