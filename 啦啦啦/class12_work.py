'''
1、创建一个对象后默认调用(A)
A、__init__    B、__str__    C、__add__    D、__and__
2、类是对象的模板、对象是类的实例
3、对象是由变量、类名两部分构成
'''
'''
4、创建学生类：
	类名：Student
	属性：name（姓名）、age（年龄）、sex（性别） 
方法：
	def info(self) # 打印学生的姓名、年龄、性别
	def draw(self) #打印”XX会画画呢”
描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.
'''
class Student():
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex

    # def __str__(self):
    #     return '姓名：%s \t年龄：%d \t性别：%s' %(self.name,self.age,self.sex)

    def info(self):
        print('姓名：%s \n年龄：%d \n性别：%s' %(self.name,self.age,self.sex))

    def draw(self):
        print('%s会画画呢' %(self.name))

stu_01 = Student('小乔',8,'女')
stu_02 = Student('鲁班',10,'男')
stu_03 = Student('凯',15,'未知')

stu_01.info()
stu_01.draw()
# print(stu_01)

print('~'*10)
stu_02.info()
stu_02.draw()
# print(stu_01)

stu_03.info()
stu_03.draw()
# print(stu_01)

print('='*15)
'''
4、创建动物类：
	类名：animal
	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
方法：
	def info(self) # 打印姓名、年龄、毛颜色
	def run（self）#打印“XX会跑呢”
描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.
'''
class Animal():
    def __init__(self,name,age,color):
        self.name = name
        self.age = age
        self.color = color

    # def __str__(self):
    #     return '名字：%s \t年龄：%d \t颜色：%s' %(self.name,self.age,self.color)

    def info(self):
        print('名字：%s \n年龄：%d \n颜色：%s' %(self.name,self.age,self.color))

    def run(self):
        print('%s会跑呢' %(self.name))

cat = Animal('小猫', 2, '灰白色')
dog = Animal('小狗', 3, '橘黄色')
rabbit = Animal('小白兔', 1, '雪白色')

cat.info()
cat.run()
# print(cat)

print('~'*10)
dog.info()
dog.run()
# print(dog)

print('~'*10)
rabbit.info()
rabbit.run()
# print(rabbit)