# 1.python2.x、3.x 默认的编码格式分别是什么？
# - 编码：python2.X默认编码格式为ASCII码；python3.X默认编码格式为UTF-8编码
# - 大环境：python2.X源码重复不规范；python3.X正整合源码，更清晰优美简单
# - python2.X有长整型long；python3.X没有长整型long，被废弃，统一为int
# - 打印方式不同：python2.X的print语句：print空格+打印内容；
#   ython3.Xprint语句：print()函数，print(打印内容)
# - 交互函数不同：python2.X ：raw_input()和input
#               ① raw_input()函数的输入内容类型为字符串
#               ② input()函数的输入类型为输入字符的类型
#               python3.X ：raw_input()和input
#               ① raw_input()被废除，统一使用input()
#               ② input()函数的输入类型为字符串
# 2.将‘hello_new_world’ 按“_” 进行分割
res = 'hello_new_world'
# res.split('_')
print(res.split('_',2))
# print(res)

# 3.将数字1以0001的格式输出到屏幕
# 方法一：
num = '1'
print(num.rjust(4,'0'))

# 方法二：
a1 = 1
a2 = '%04d' % a1
print(a2)

# 方法三：
j = 1
print(j.zfill(4))
# 1.字典a = {1:1} 是否正确？
# 正确

# 2.请合并列表a = [1,2,3,4] 和列表 b = [5,6,7,8]
a = [1,2,3,4]
b = [5,6,7,8]
print(a + b)

# 3.列表a,请写出实现正序排列，倒序排列，逆序排列的内置方法。
list_a = [1,5,6,2,96,54,3]
print(list_a[::-1])  #逆序排列
list_a.sort()  #正序排列
print(list_a)
list_a.sort(reverse=True)  #倒序排列
print(list_a)

# 4.字典d = {"k":1,"v":2},请写出d.items的结果
# 答：([('k', 1), ('v', 2)])

# 5.复杂列表[{“k”:1,"v":2},{“k”:12,"v":22},{“k”:13,"v":32}],请用内置方法写出按k倒序排列的代码
list = [{"k":1,"v":2}, {"k":12,"v":22}, {"k":13,"v":32}]
list.sort(reverse=True,key=lambda o: o['k'])
print(list)

# 6.集合s = set([1,2,3,4]),d = set([2,4,9,0,3]),请用内置方法写出他们的并集、交集，对称公差
s = set([1,2,3,4])
d = set([2,4,9,0,3])
s_d = s & d  #交集
print(s_d)
sd = s | d  #并集
print(sd)

# 对称公差(对应没有的)：
res = s ^ d
print(res)

# 差极(s在前面，取s对比d没有的)：
res1 = s - d  #取s的差极
print(res1)
res2 = d - s  #取d的差极
print(res2)


# 7.如何把列表a = ["a","b"]里的各项转为字符串并用逗号‘,’连接
a = ["a","b"]
a = str(a)
print(a)
# 使用join方法
a = ["a","b"]
b = ','
print(b.join(a))

# 1.  判断下列描述是否正确，如果有误，请指出错误的地方并改正
# ①字典：具有键值映射关系、无序，字典的键不能重复并且可以是任意数据类型
  # 答：字典的键不可以是任意数据类型，只能是不可变的数据类型
# ②元组：不能修改，无序，不能索引切片，当元组中只有一个元素时，需要在元素的后面加逗号
  # 答：元组不是无序的，元组有下标索引，可以索引切片
# ③列表：元素可以重复，有序，不可以反向索引，元素可以是任意类型
  # 答：列表可以反向索引
# ④集合：元素不可以重复，可以索引，a = {} 声明了一个空集合
  # 答：集合是无序的不可以索引，a = {} 不是空集合是一个空字典，声明一个空集合是a = set()

# 2.一行代码实现求1到100的和
sum1 = 0
for i in range(1,101):
    sum1 += i
print(sum1)

#sum方法求和
print(sum(range(1,101)))

# 3.按照下面要求写出完成代码：使用random.random方法实现随机输出范围在[25,60)中的浮点数
import random
for i in range(0,10):
    print(random.random() * 35 + 25)

# 2.一个list对象：a = [1,2,4,3,2,2,4]，需要去掉里面的重复值
a = [1,2,4,3,2,2,4]
a = set(a)
a = list(a)
print(a)