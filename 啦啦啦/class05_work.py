name = '12asdf56asd89'
#用循环打印出下标
# print(len(name))
# i = 0
# while i <= len(name):
#     print(name[i])
#     i += 1
#1、用循环既打印出内容，又显示下标
# i = 0
# while i < len(name):
#     print(name[i] + '的下标是：' + str(i))
#     i += 1

i = 0
for i in len(name):
    res = name[i]
    print('下标为%d的值是：%s' %(i,res))
    i += 1



# 2、break和continue的区别：
# break是结束、终止循环；continue是跳出本次循环进入到下一个循环
# 3、按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
str = '1234567890'
# 1.截取字符串的第一位到第三位的字符
str1 = str[0:3:1]
print(str1)
# 2.截取字符串最后三位的字符
str2 = str[:-4:-1]
print(str2)
# 3.截取字符串的全部字符
str3 = str[:]
print(str3)
# 4.截取字符串的第七个字符到结尾
str4 = str[6:10:1]
print(str4)
# 5.截取字符串的第一位到倒数第三位之间的字符
str5_1 = str[1:-3:1]
print(str5_1)   #不包含第一位和倒数第三位
str5_2 = str[0:-2:1]
print(str5_2)  #包含第一位和倒数第三位
# 6.截取字符串的第三个字符
str6 = str[2]
print(str6)
# 7.截取字符串的倒数第一个字符
str7 = str[-1]
print(str7)
# 8.截取与原字符串顺序相反的字符串
str8 = str[:-11:-1]
print(str8)
# 9.截取字符串倒数第三位与倒数第一位之间的字符
str9 = str[:-4:-1]
print(str9)
# 10.截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
str10 = str[0:11:2]
print(str10)