# -*- coding:utf-8 -*-
# @time : 2021/4/18 15:24
# @Author : DingTingTing
# @Email : 1830188186@qq.com
# @Fole : class13_work.py
'''
1、下列程序结果是（初始化、类A、销毁）.
class A:

    def __init__(self):
        print("初始化")

    def __del__(self):
        print("销毁")

    def __str__(self):
        return "类A"
a = A()
print(a)

2. 下列程序执行结果（动物在叫、汪汪汪）
class Animal(object):

    def shut(self):
        print("动物在叫")
class Dog(Animal):

    def shut(self):
        super().shut()
        print("汪汪汪")
dog = Dog()
dog.shut()

1、面向对象三大特性是：封装、继承、多态.
2、单例模式创建保证实例只创建  1  次.
'''

'''
3. 创建猫类：
	类名：Cat
	属性：无
描述：创建一个Cat类，通过Cat类创建一个对象cat，执行print(cat)输出“喵？喵？喵？”.
'''
class Cat():

    def say(self):
        print('喵？喵？喵？')

cat_01 = Cat()
cat_01.say()

'''
4.创建计算器类：
	类名：Calculator
	属性：number_1（数字一）、number_2（数字二） 
方法：
	def __init__(self,number_1,number_2): 
# 类的初始化方法
	def add(self) # 返回数字一加数字二的值 
	def sub(self) # 返回数字一减去数字二的值
	def div(self) # 返回数字一除以数字二的值
	def mul(self) # 返回数字一乘以数字二的值
描述：创建计算器类，通过计算器类创建一个计算器对象，在创建对象时需要传入数字一和数字二，分别调用计算器的四种方法.
'''
class Calculator():

    def __init__(self,number_1,number_2):
        self.number_1 = number_1
        self.number_2 = number_2

    def add(self):
        print(self.number_1 + self.number_2)

    def sub(self):
        print(self.number_1 - self.number_2)

    def div(self):
        print(self.number_1 / self.number_2)

    def mul(self):
        print(self.number_1 * self.number_2)

cal_01 = Calculator(24,8)
cal_01.add()
cal_01.sub()
cal_01.div()
cal_01.mul()


'''
5.创建Cat和Dog类分别继承Animal类，分别重写shut和eat方法，创建Cat类对象cat和Dog类对象dog，分别调用cat和dog的shut和eat方法
class Animal:

    def shut(self):
		# 打印叫声
        pass

    def eat(self):
		# 打印爱吃的食物
        pass
'''
class Animal():

    def shut(self):
        print('动物的叫声')

    def eat(self):
        print('动物爱吃的食物')

class Cat(Animal):
    def shut(self):
        print("喵？喵？喵？")

    def eat(self):
        print("小猫爱吃鱼")

smallCat = Cat()
smallCat.shut()
smallCat.eat()

class Dog(Animal):
    def shut(self):
        print("汪!汪!汪!")

    def eat(self):
        print("小狗爱啃骨头")

smallDog = Dog()
smallDog.shut()
smallDog.eat()