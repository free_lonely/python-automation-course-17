# 4、用lambda函数实现两个数相乘
def calc(a,b):
    c = a * b
    return c
print(calc(10,3))

def calc(a,b):
    c = lambda a, b: a * b
    return c(a,b)
print(calc(10,3))

c = lambda a, b: a * b
print(c(10,3))

print((lambda a, b: a * b)(10,3))


# 5、列表推导式求列表所有奇数并构造新列表,
# a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#普通方法：
b = []
for i in a:
    if i % 2 != 0:
        b.append(i)
print(b)

#列表推导式：
list_new = [i for i in a if i%2 !=0]
print(list_new)

#列表推导式：
print([i for i in a if i%2 !=0])
