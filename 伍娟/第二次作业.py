"""
# coding=utf-8
__project__='伍娟'
__file_name__=='第二次作业'
__author__='wwj'
__time__='2021/4/5 16:14'
__product_name__=PyCharm
# code is far away from bugs with the god animal protecting
    I love animals. They taste delicious.
              ┏┓      ┏┓
            ┏┛┻━━━┛┻┓
            ┃        ┃
            ┃  ┳┛  ┗┳  ┃
            ┃      ┻      ┃
            ┗━┓      ┏━┛
                ┃      ┗━━━┓
                ┃  神兽保佑    ┣┓
                ┃　永无BUG！   ┏┛
                ┗┓┓┏━┳┓┏┛
                  ┃┫┫  ┃┫┫
                  ┗┻┛  ┗┻┛
"""
"""
1. 说出变量名可以由哪些字符组成 
标识符由字母、下划线和数字组成，且数字不能开头
"""
"""
2. 写出变量命名时的规则 
小驼峰，大驼峰，下划线命名
"""
"""
3. 说出什么是驼峰法（大驼峰、小驼峰） 
大驼峰：MyName，一般采用
小驼峰：myName
"""
"""
4. 编写程序，完成以下要求： 
•	提示用户进行输入数据
•	获取用户的数据（需要获取2个）
•	对获取的两个数字进行求和运行，并输出相应的结果
"""
a = int(input("请输入数字a:"))
b = int(input("请输入数字b:"))
ret = a + b  # 求 a 和 b 的和
print("a + b = %d" % ret)

"""
5. 编写程序，完成以下信息的显示: 
==================================
=        欢迎进入到身份认证系统V1.0
= 1. 登录
= 2. 退出
= 3. 认证
= 4. 修改密码
==================================
"""
print("==================================")
print("=       欢迎进入到身份认证系统V1.0")
print("= 1. 登录")
print("= 2. 退出")
print("= 3. 认证")
print("= 4. 修改密码")
print("==================================")

"""
6. 编写程序，通过input()获取一个人的信息，然后按照下面格式显示 
==================================
姓名: xxxxx    
QQ:xxxxxxx
手机号:131xxxxxx
公司地址:北京市xxxx
==================================
"""
my_name = input("请输入您的姓名")
my_qq = input("请输入您的qq号码")
my_tel = input("请输入您的手机号")
my_addr = input("请输入您的公司地址")
print("=========================")
print("姓名：%s" '\n' "QQ:%s"  '\n' "手机号：%s"  '\n' "公司地址：%s" % (my_name, my_qq, my_tel, my_addr))
print("=========================")
"""
7. 使用if，编写程序，实现以下功能： 
•	从键盘获取用户名、密码
•	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
# 假设 用户名为admin 密码为12345
"""
myName = input("请输入用户名:")
myPassWd = input("请输入密码:")
if myName == "admin" and myPassWd == "12345":
    print("欢迎进入xxx的世界")
else:
    print("密码或者用户名错误")
"""
10、（这个后面讲列表的时候说）
# import random
# guess_list = ["石头", "剪刀", "布"]
# rule = [["布", "石头"], ["石头", "剪刀"], ["剪刀", "布"]]
#
# while True:
#     # 方法返回一个列表，元组或字符串的随机项。
#     computer = random.choice(guess_list)
#     people = input('请输入：石头,剪刀,布\n')
#
#     if people not in guess_list:
#         people = input('重新请输入：石头,剪刀,布\n')
#
#         continue
#     if computer == people:
#         print("平手，再玩一次！")
#
#     elif [computer, people] in rule:
#         print("电脑获胜！")
#     else:
#         print("人获胜！")
#         break 
"""