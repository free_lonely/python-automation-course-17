"""
__project__='伍娟'
__file_name__=='第五次作业'
__author__='wwj'
__time__='2021/4/7 22:12'
__product_name__=PyCharm
# code is far away from bugs with the god animal protecting
    I love animals. They taste delicious.
              ┏┓      ┏┓
            ┏┛┻━━━┛┻┓
            ┃        ┃
            ┃  ┳┛  ┗┳  ┃
            ┃      ┻      ┃
            ┗━┓      ┏━┛
                ┃      ┗━━━┓
                ┃  神兽保佑    ┣┓
                ┃　永无BUG！   ┏┛
                ┗┓┓┏━┳┓┏┛
                  ┃┫┫  ┃┫┫
                  ┗┻┛  ┗┻┛
"""
"""
break和continue区别：
break指的是立即跳出循环。只针对最近邻的循环有效，嵌套的外部循环不生效。
continue指的是立即跳出当前的循环，进行下一个循环。只针对最近邻的循环有效，嵌套的外部循环不生效。
"""
str1 = '1234567890'
new = str1[0:3]
print(new)
new = str1[0:-3]
print(new)
new = str1[:]
print(new)
new = str1[6:]
print(new)
new = str1[0:-3]
print(new)
new = str1[2]
print(new)
new = str1[-1]
print(new)
new = str1[::-1]
print(new)
new = str1[-3:-1]
print(new)
new = str1[::2]
print(new)

"""
字符串中加上索引号一起显示
"""

for i in range(len(str1)):
    print("字符串 %s的索引下标为 %d" % (str1[i], i))

