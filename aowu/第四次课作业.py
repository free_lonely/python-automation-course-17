# 1题
i = 1
j = 1
while i < 5:
    j = 1
    while j <= i:
        print('*  ', end='')
        j += 1
    print()
    i += 1
while i > 0:
    j = 1
    while j <= i:
        print('*  ', end='')
        j += 1
    print()
    i -= 1

# 2题：break和continue的区别：
# break是跳出整个这一层的循环，包括跳出else。continue是跳出本次的循环。

# 3题:按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
# 变量名自己有改动

str = '1234567890'
# 1.	截取字符串的第一位到第三位的字符
str1 = str[:3]
print(str1)

# 2.	截取字符串最后三位的字符
str2 = str[-3:]
print(str2)

# 3.	截取字符串的全部字符
str3 = str
print(str3)

# 4.	截取字符串的第七个字符到结尾
str4 = str[6:]
print(str4)

# 5.	截取字符串的第一位到倒数第三位之间的字符
str5 = str[1:-3]
print(str5)

# 6.	截取字符串的第三个字符
str6 = str[2]
print(str6)

# 7.	截取字符串的倒数第一个字符
str7 = str[-1]
print(str7)

# 8.	截取与原字符串顺序相反的字符串
str8 = str[::-1]
print(str8)

# 9.	截取字符串倒数第三位与倒数第一位之间的字符
str9 = str[-2:-1]
print(str9)

# 10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
str10 = str[1:-2:2]
print(str10)

# 4题.字符串中加上索引一块显示。
sr = 'abcdefg'
i = 0
while i < len(sr):
    print(sr[i], end='')
    print('下标为%s' % i)
    i += 1

