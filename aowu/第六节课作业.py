# one
# 1.用任一编程语言输出整数0-10.
i = 0
while i <= 10:
    print(i)
    i += 1

# 2. 用任一语言实现逆序输出一个字符串。
str = '乐搏欢迎您'
print(str[::-1])


# two python基础
# 1.str = 'a,hello'在字符串str中查找hello
str1 = 'a,hello'
a = str1.index('hello')
print('查找内容下标为%d' % a)

# 2. str = 'a,b,c,d'用逗号分割字符串，并保存到列表
# 不会做
str2 = 'a,b,c,d'
ret = ' '.join(str2)
print(ret)

# 3. 将‘笔试题 123A’中123替换为‘进行中’
str3 = '笔试题 123A'
print(str3.replace('123', '进行中'))

# 
