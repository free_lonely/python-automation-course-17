# 循环。不能用i++。可使用debug断点调试
# i = 0
# while i < 3:
#         print("哈")
#         i += 1


# 求十以内的偶数和
num = 0
sum0 = 0
while num <= 10:
    if num % 2 == 0:
        sum0 += num
        print(sum0)
    num += 1

# 求十以内的奇数和
num1 = 0
sum1 = 0
while num1 <= 10:
    if num1 % 2 == 1:
        sum1 += num1
        print(sum1)
    num1 += 1


# 输出5*5矩阵
# i = 1
# j = 1
# while i <= 5:
#     while j <= 5:
#         print('*', end=' ')
#         j += 1
#     print('')
#     j = 1
#     i += 1

# 输出三角矩阵
a = 1
b = 1
while a <= 5:
    while b <= a:
        print('*', end=' ')
        b += 1
    print('')
    b = 1
    a += 1


# 输出乘法表
# i = 1
# j = 1
# while i <= 9:
#     j = 1
#     while j <= i:
#         ret = i * j
#         # = %xd，从等号开始，向右空两个格。可以为负。
#         print('%d X %d = %02d    ' % (j, i, ret), end='')
#         j += 1
#     print()
#     i += 1


# for循环
# 将name内容一个一个的放入i
name = 'lebo'
for i in name:
    print(i)

# range(起始，终点，步长)。左闭右开，取1不取5.
for i in range(0, 5, 1):
    print(i)

while True:
    pass  # TODO 下个月再写（便签）

# ctrl+f，是搜索文件内容
# 按两下shift，可查询文件名、类名
