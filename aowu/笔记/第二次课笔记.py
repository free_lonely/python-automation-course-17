# 参考第一次课-基础文档

# 单行注释,井号后面需要带空格。符合PEP8规则，不符合则有灰色波浪线。绿波浪线表示python不认识此英文。

# 多行注释时
# 全选
# 然后ctrl+/

'''
多行注释
可以回车
引号中间所有的内容
全是注释
'''

# Python支持中文。

# python2不支持中文，开头要写coding=utf-8

school = 'lebo'
a = 123
# 布尔类型，TF必须大写
b = True
c = False
print(school)
# 打印b的变量类型
print(type(b))
# 标识符：数字、字母、下划线，数字不能开头。关键字是特殊的标识符。
# python中标识符严格区分大小写。
# 标识符命名。大驼峰：所有单词首字母大写。小驼峰：首字母小写，后面单词首字母大写。下划线命名法。
# 一般来说：方法用下划线命名法。类用大驼峰。

# 关键字，是具有特殊功能的标识符，不允许开发者自己定义。下方可查关键字。
import keyword
print(keyword.kwlist)
# list后不能加括号

# python console中可以输入内容
# 看实现逻辑:ctrl+鼠标点方法名

# 这样可以使输出内容不自动换行
print('lebo',end='')
print('lebo')

# 占位符%d,%s,%f
age = 18
name = 'le\nbo'
print('我今年%d岁, 我叫%s' % (age, name))
print('我今年%d岁' % age)
print('我今年%d岁' % age)

# 输入,返回的都是字符串类型。python2的输入：raw_input（）。
# ru = input('您今年贵庚？')
# print(ru)

# python程序，最后一行一定要空出来
# 运算符，具体的可参考文档。混合运算时，注意优先级。
# 不同类型数字运算时，整数会转为浮点数进行运算。
e = 20
f = 10
ret = e - f
print(ret)
# 除法结果为浮点型。
ret2 = e / f
print(ret2)

# 多个赋值
g, h = 10, 20
# 复合赋值运算符
age=21
age += 1
print(age)

# 强制类型转换：类型（原内容）
# int(原内容, base=2)，转化为2进制
# a = 10
# b = int(input("请输入数字："))
# ret = a-b
# print(ret)


