# 学习break和continue
# i = 1
# while i < 5:
#     i += 1
#     if i == 5:
#         continue
#     print(i)
#
# else:
#     print("执行了else")
#     print(i)

# 数组
# name = "abcdefg"
# name1 = '这也是字符串'
# name2 = """aaa

# 从前数0开头，从后数-1开头
name = "abcdefg"
print(len(name))
print(name[0])
print(name[-1])
i = 0
l = len(name)
while i < l:
    print(i, end='')
    print(name[i])
    i += 1

# 切片语法：[起始：结束：步长],左闭右开。
# 开头为0、结尾为最后时，可以省略，但冒号不能省略。步长为1时，冒号和数字都可省略。

sr = 'abcdefga'

# 步长为负时，从后往前数，起始值默认为-1。
print(sr[-2:-5:-1])
# find找字符串中的内容，返回下标。无此内容的话，返回-1
print(sr.find('1'))
# index查找字符串内容。rindex是从后往前查。index('内容', 起始, 终点),左闭右开。
print(sr.rindex('d', 1, 4))
print(sr[3])
# 出现次数
print(sr.count('a'))

# 替换，用后方内容替换前方内容，可以替换空格为空字符串。如果没匹配到，则不执行替换。实际原数组未变。字符串是不可变的数据类型。
print(sr.replace('abc', '123'))
print(sr)

# split已列表形式输出['123', '456', '789', 'aaa']
sr1 = '123045607890aaa'
print(sr1.split('0'))

# 每个单词首字母大写
sr2 = 'asd xsd'
ret = sr2.title()
print(ret)












