# 列表及其增删改查

# 列表，中括号。里面可以放不同类型的数据。
mlis = ['ab', 'cd', 'fg']
mlis2 = ['abc', 666, True, 3.14]
# 空列表
mlis3 = []
mlis4 = list()
print(type(mlis4))
print(mlis)
# 一种列表简写，每个字母一个元素
mlis5 = list('abcde')
print(mlis5)
# len也可用于测列表长度（元素个数）
print(len(mlis2))

# join方法，合并两个字符串、列表。交叉。
ret = '*'.join(mlis)
print(ret)

# 挨个循环拿出列表的内容.
for value in mlis2:
    print(value)
# 或者
i = 0
while i < len(mlis2):
    print(mlis2[i])
    i += 1

# 拿出列表某元素的某一位内容
x = mlis2[0]
print(x[2])

# 列表是可变的数据类型，可以改变其本身，所以改变时不需要另外用变量承载，与字符串不同。
# append(['a'])：可往里添加一个"整体"：['abc', 666, True, 3.14, ['a']]
mlis2.append(['a'])
mlis2.append(['c', 'd'])
print(mlis2)
# 以下方式赋值，a为None
a = mlis2.append('aa')
print(a)

# 给列表插入数据.一个一个地放入。默认为插入到列表最后。['abc', 666, True, 3.14, ['a'], '1', '2']
mlis2.extend(['1', '2'])
print(mlis2)
# ret = input('请输入：')
# mlis2.extend(ret)
# mlis2.append(ret)
# print(mlis2)

# insert,在特定位置（前，下标,不存在则为添加到最末尾）前插入元素（后）
mlis6 = ['aaa', 9, 'ccc']
mlis6.insert(100, 'hhh')
print(mlis6)

# 改：通过下标直接修改。
mlis6[0] = 'xiugaihou'
print(mlis6)

# 查。in:需要与某元素完全匹配（区别大小写），匹配则结果为True。not in用法相同，结果相反。
mlis7 = ['xiaoWang', 'xiaoZhang', 'xiaoHua']
# findName = input('查找内容：')
# if findName not in mlis7:
#     print('找到了')
# else:
#     print('没找到')

# index查找。找到则返回下标（多个则返回找到的第一个），找不到则报错。
index = mlis7.index('xiaoHua')
print(index)
# count查询，返回次数,没有就返回0，还可规定区间，左闭右开。
print(mlis7.count('xiaoHua'))

# 删。
# del：根据下标进行删除，下标不存在则报错：index out of range下标越界。
mlis8 = ['加勒比海盗', '骇客帝国', '第一滴血', '指环王', '霍比特人', '速度与激情']
del mlis8[2]
print(mlis8)

# pop：加下标则同del（包括下标越界），不加下标则删除最后一个元素
mlis9 = ['加勒比海盗', '骇客帝国', '第一滴血', '指环王', '霍比特人', '速度与激情']
mlis9.pop()
print(mlis9)

# remove：根据元素的值进行删除,多个相同内容时，删掉第一个匹配上的内容。
mlis10 = ['加勒比海盗', '指环王', '骇客帝国', '第一滴血', '指环王', '霍比特人', '速度与激情']
mlis10.remove('指环王')
print(mlis10)

if '霍比特人' in mlis10:
    a = mlis10.index('霍比特人')
    mlis10.pop(a)
    print(mlis10)

# 两种排序。str和int类型不能同时排序，但全为str时可以排序，以ASC码排序。
mlis11 = [1, 5, 6, 9, 8, 544654, 546, 787, 32, 4, 7, 9]
# reverse排序，直接将原列表倒序。
mlis11.reverse()
print(mlis11)
# sort排序。reverse=False不反转，即升序，True则降序。不填写默认升序。
mlis11.sort(reverse=True)
print(mlis11)

# 列表的嵌套：列表中某个元素为一个列表。
# 三层嵌套，三维列表。取数据时，以三个下标取。
schoolNames = [['北京大学', '清华大学'],
               ['南开大学', ['天津大学'], '天津师范大学'],
               ['山东大学', '中国海洋大学']]
a = schoolNames[1]
print(schoolNames[1][1][0])
