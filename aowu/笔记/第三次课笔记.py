
# 布尔类型的占位符，看具体用处，可用不同的占位符。非零即真。
is_flag = True
print('我说的是%s, %d'% (is_flag , is_flag))

# 百分号需要打%%
a = 90
print('%d%%的人良好'% a)

# eval:用来计算在字符串中的有效Python表达式，并返回一个对象。
# 用法没太听懂。必须是字符串.
b = 'True'
num1 = eval(b)
print(type(num1))

# 判断语句
flag = False
age = 20
pw = 123456
user = 'lebo'
# 逻辑运算符：and、or、not
if pw == 123456 and user == 'lebo':
    print('你好')
print('6')

# 剪刀石头布。用随机数模块，出现波浪线的原因是最好应该用在开头。闭区间。
import random
player = int(input('请输入剪刀（0）石头（1）布（2）：'))
computer = random.randint(0, 2)
print('player: %d =VS= computer: %d' % (player, computer))

if(player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print('你赢了！')
elif computer == player:
    print('平局！')
else:
    print('你输了！')
