# 1.	下列将字符串"100"转换为数字100的正确的是( A )
# A、int(“100”)  B、int[“100”]  C、toInt(“100”)  D、toUp(”100”)

# 2.	下列程序执行结果是( A  )
# numbers = [1，5，3，9，7]
# numbers.sort(reverse=True)
# print(numbers)
# A、[9，7，5，3，1]
# B、[1，3，5，7，9]
# C、1，3，5，7，9
# D、9，7，5，3，1

# 3.	如何在列表中添加一个元素
# 答：append、extend、insert

# 4.	对于列表什么是越界
# 答：下标的长度超过列表本身的长度就是越界

# 5.	说出变量类型中，哪些是可变数据类型，哪些不可变数据类型
# 答：可变的数据类型：列表、计划、字典
#    不可变的数据类型：元祖、字符串、整型、浮点型、布尔型

# 6.	从键盘中输入5个学生的名字，存储到列表中，然后打印出每个学生名字中的第2个字母
"""
a = input("请输入学生1姓名：")
b = input("请输入学生2姓名：")
c = input("请输入学生3姓名：")
d = input("请输入学生4姓名：")
e = input("请输入学生5姓名：")
student_list = [a, b, c, d, e]
print(student_list)
for name in student_list:
    print("%s 的名字中的第二个字母是：%s" % (name, name[1]))
"""

# 7.	随机生成五个数字保存在列表中，取反并输出至终端.(取反:求出相反数，随机数范围是0到100)
"""
import random

num_list = list()
for i in range(5):
    index = random.randint(0, 100)
    num_list.append(index)
    print(num_list)
num_list.reverse()
print(num_list)
"""
# i = 0     #实现循环
# while i < 5:
#     index = random.randint(0, 100)
#     num_list.append(index)
#     print(num_list)
#     i += 1

# 8.	将下列两个列表合并，将合并后的列表升序并输出.
# list1 = [1,3,4,5,7]
# list2 = [0,66,8,9]
"""
list1 = [1, 3, 4, 5, 7]
list2 = [0, 66, 8, 9]
list1.extend(list2)
list1.sort()
print(list1)
"""

# 9.	使用字典来存储一个人的信息(姓名、年龄[数字]、学号)，这些信息来自键盘的输入，储存完输出至终端.
"""
a = input("请输入姓名：")
b = int(input("请输入年龄："))
c = input("请输入学号：")
dict1 = {'姓名': a, '年龄': b, '学号': c}
print(dict1)
"""

# 10.	有下列字典dict1,查找值为“lebo”对应的key并输出到终端.(结果应该是输出school)
# dict1={“school”:”lebo”,”date”:2018,”address”:”beijing”}
"""
dict1 = {"school": "lebo", "date": 2018, "address": "beijing"}
for key, value in dict1.items():
    if value == "lebo":
        print(key)
"""

# 11.	使用切片翻转列表num，将翻转完后的列表中所有偶数位置的元素相加求和并输出至终端.
# num = [0,1,2,3,4,5,6,7,8,9]
"""
num = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
var = num[::-1]
sum = 0
for i in range(len(var)):
    if i % 2 == 0:
        sum += var[i]
print(sum)
"""






