# 1、	说出变量名可以由哪些字符组成
# 答：变量由数字、字母、下划线组成，且数字不能开头

# 2、	写出变量命名时的规则
# 答：变量的命名规则是：小驼峰、大驼峰、下划线命名

# 3、	写出什么是驼峰法
# 答案：小驼峰式命名法（lower camel case）： 第一个单词以小写字母开始；第二个单词的首字母大写。
# 大驼峰式命名法（upper camel case）： 每一个单字的首字母都采用大写字母。

# 4、编写程序，完成以下要求：
# •	提示用户进行输入数据
# •	获取用户的数据（需要获取2个）
# •	对获取的两个数字进行求和运行，并输出相应的结果
"""
a = int(input('请输入一个数字a：'))
b = int(input('请输入另一个数字b：'))
print(type(a))
print(type(b))
c = a + b
print('a + b = %d' % c)
"""

# 5、编写程序，完成以下信息的显示:
# •	==================================
# •	=        欢迎进入到身份认证系统V1.0
# •	= 1. 登录
# •	= 2. 退出
# •	= 3. 认证
# •	= 4. 修改密码
# •	==================================
"""
print('==================================')
print('=        欢迎进入到身份认证系统V1.0')
option = int(input('请输入您需要使用的功能：1.登录，2.退出，3.认证，4.修改密码：'))
if option == 1:
    print('= 1. 登录')
elif option == 2:
    print('= 2. 退出')
elif option == 3:
    print('= 3. 认证')
else:
    print('= 4. 修改密码')
print('==================================')
"""

# 6、编写程序，通过input()获取一个人的信息，然后按照下面格式显示
# ==================================
# 姓名: xxxxx
# QQ:xxxxxxx
# 手机号:131xxxxxx
# 公司地址:北京市xxxx
# ==================================
"""
name=input('姓名：')
QQ=input('QQ:')
mobile=input('手机号：')
address=input('公司地址：')
print("==================================")
print("姓名: %s" % name)
print("QQ: %s" % QQ)
print("手机号: %s" % mobile)
print("公司地址: %s" % address)
print("==================================")
"""


# 7、使用if，编写程序，实现以下功能：
# •	从键盘获取用户名、密码
# •	如果用户名和密码都正确（预先设定一个用户名和密码），那么就显示“欢迎进入xxx的世界”，否则提示密码或者用户名错误
"""
username = input('请输入用户名：')
password = input('请输入密码：')
if username == 'admin' and password == '123456':
    print('欢迎进入xxx的世界')
else:
    print('密码或用户名错误')
"""
# 8、有能力的同学可以用循环的方式做出石头剪刀布的游戏并且输入中文
"""
import random
player = int(input("请输入: 剪刀(0) 石头(1) 布(2):"))
computer = random.randint(0, 2)
print("玩家:%d--电脑:%d" % (player, computer))
if (player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print("玩家胜利!!!")
elif player == computer:
    print("平局")
else:
    print("玩家失败!!!")
"""

