# 综合应用: 学生管理系统(文件版)
# # 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
#
# 有如下7个方法
# print("      学生管理系统 V1.0")
# print(" 1:添加学生")
# print(" 2:删除学生")
# print(" 3:修改学生")
# print(" 4:查询学生")
# print(" 5:显示所有学生")
# print(" 6:保存数据")
# print(" 7:退出系统")
# print("---------------------------")
#
# """添加学生信息"""
# new_name = input("请输入姓名:")
# new_tel = input("请输入手机号:")
# new_qq = input("请输入QQ:")
# 添加学生可能出现：
#
# 此用户名已经被占用, 请重新输入
#
# 定义一个字典，用来存储用户的学生信息(这是一个字典)
# 向字典中添加数据
# 向列表中添加这个字典
#
# """删除学生信息"""
# "请输入要删除的序号:"
#
# 你确定要删除么?yes or no
#
# 输入序号有误, 请重新输入
#
# """修改学生信息"""
# "请输入要修改的序号:"
# "你要修改的信息是:"
# input("请输入新的姓名:")
# input("请输入新的手机号:")
# input("请输入新QQ:")
# print("输入序号有误,请重新输入")
#
# """查询学生信息"""
# "请输入要查询的学生姓名:"
# 查询到的信息如下:
# "没有您要找的信息...."
#
# """遍历学生信息"""
# 序号\t姓名\t\t手机号\t\tQQ
#
# """加载之前存储的数据"""

import ast

# 定一个列表，用来存储所有的学生信息(每个学生是一个字典)
student_list = []


# 定义菜单
def menu():
    print("---------------------------")
    print("      学生管理系统 V1.0")
    print(" 1:添加学生")
    print(" 2:删除学生")
    print(" 3:修改学生")
    print(" 4:查询学生")
    print(" 5:显示所有学生")
    print(" 6:保存数据")
    print(" 7：退出系统")
    print("---------------------------")


# """添加学生信息"""
def add_new_info():
    global student_list
    new_name = input("请输入姓名:")
    if check_name(new_name):
        add_new_info()
    else:
        pass
    new_tel = input("请输入手机号:")
    new_qq = input("请输入QQ:")
    num = len(student_list) + 1
    student_dict = {"序号": num, "姓名": new_name, "手机号": new_tel, "QQ号": new_qq}
    print("新输入的学生信息为：%s" % student_dict)
    student_list.append(student_dict)
    print("学生信息为：%s" % student_list)


def check_name(arg):
    global student_list
    for i in student_list:
        if arg == i["姓名"]:
            print("此用户名已经被占用, 请重新输入")
            return True
    return False


# """删除学生信息"""
def del_info():
    global student_list
    del_num = int(input("请输入要删除的序号:"))
    for i in student_list:
        if del_num == i["序号"]:
            del_queren = input("你确定要删除么?yes or no \n")
            if del_queren == "yes":
                student_list.remove(i)
                print(student_list)
            elif del_queren == "no":
                pass
            else:
                print("输入错误，请重新输入")
                del_info()
            return
    print("输入序号有误, 请重新输入")
    del_info()


# """修改学生信息"""
def update_info():
    global student_list
    update_num = int(input("请输入要修改的序号:"))
    flag = True
    for i in student_list:
        if update_num == i["序号"]:
            flag = False
            update_xx = input("你要修改的信息是:")
            if update_xx == "姓名":
                update_name = input("请输入新的姓名:")
                i["姓名"] = update_name
                print(student_list)
                return
            elif update_xx == "手机号":
                update_tel = input("请输入新的手机号:")
                i["手机号"] = update_tel
                print(student_list)
                return
            elif update_xx == "QQ":
                update_qq = input("请输入新QQ:")
                i["QQ号"] = update_qq
                print(student_list)
                return
            else:
                print("输入错误，请重新输入")
                update_info()
    if flag:
        print("输入序号有误,请重新输入")
        update_info()


# """查询学生信息"""
def search_info():
    global student_list
    search_name = input("请输入要查询的学生姓名:")
    for i in student_list:
        if search_name == i['姓名']:
            print("查询到的信息如下：%s" % i)
            return
    print("没有您要找的信息....")


# """遍历学生信息"""
def traversal_info():
    print("遍历学生信息")
    for i in student_list:
        print(i)


def save_info():
    f = open("student_info.txt", "w")
    f.write(str(student_list))
    f.close()


def load_info():
    global student_list
    f = open("student_info.txt", "a+")
    content = f.read()
    if content != "":
        student_list = ast.literal_eval(content)
    f.close()


def main():
    """用来控制整个流程"""
    while True:
        # 打印菜单
        menu()
        load_info()
        # 获取菜单数据
        num = input("请输入要进行的操作(数字)")
        # 根据输入的菜单数据,调用对应的方法
        if num == "1":
            add_new_info()
        elif num == "2":
            del_info()
        elif num == "3":
            update_info()
        elif num == "4":
            search_info()
        elif num == "5":
            traversal_info()
        elif num == "6":
            save_info()
        elif num == "7":
            exit_flag = input("亲,你确定要退出么?~~~~(>_<)~~~~(yes or no) ")
            if exit_flag == "yes":
                break
        else:
            print("输入有误,请重新输入......")

        input("\n\n\n按回车键继续....")


if __name__ == '__main__':
    main()
