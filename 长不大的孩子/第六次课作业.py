# 1、一个学校3个办公室，8位老师，随机排座
from random import random
import random

school_list = [[], [], []]
teacher_list = list("ABCDEFGH")
for name in teacher_list:
    index = random.randint(0, 2)
    school_list[index].append(name)
    print("第 % d 个办公室的老师是：% s" % (index, school_list[index]))
print("办公室位置分配结束，位置安排如下：% s " % school_list)
