# 1、	用lambda函数实现两个数相乘
# num = lambda x, y: x * y
# print(num(2, 2))

# 2、	列表推导式求列表所有奇数并构造新列表，a =  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# list1 = list()
# for i in range(10):
#     if i % 2 == 1:
#         list1.append(i)
# print(list1)
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
b = [i for i in a if i % 2 == 1]
print(b)

