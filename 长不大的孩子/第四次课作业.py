# 1、使用while，完成以下图形的输出
# *
# * *
# * * *
# * * * *
# * * * * *
# * * * *
# * * *
# * *
# *

i = 1   # 中间数，用于判断列数是增还是减
j = 1   # 控制列数
while 0 < j:
    print('*'*j)
    i += 1
    if i > 5:
        j -= 1
    else:
        j += 1



