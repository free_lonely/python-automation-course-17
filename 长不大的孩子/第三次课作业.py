# 8、有能力的同学可以用循环的方式做出石头剪刀布的游戏并且输入中文
"""
import random
player = int(input("请输入: 剪刀(0) 石头(1) 布(2):"))
computer = random.randint(0, 2)
print("玩家:%d--电脑:%d" % (player, computer))
if (player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print("玩家胜利!!!")
elif player == computer:
    print("平局")
else:
    print("玩家失败!!!")
"""