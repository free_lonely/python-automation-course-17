# 2、break和continue的区别
# 答案：break是立刻结束break所在的循环；continue是跳过本次循环，继续下一个循环

# 3、按要求处理字符串：
# 现有字符串： str1 = '1234567890'，根据题目要求，将截取后的新字符串赋值给str2
# 1.	截取字符串的第一位到第三位的字符
# str1 = '1234567890'
# str2 = str1[0:3]
# print(str2)

# 2.	截取字符串最后三位的字符
# str1 = '1234567890'
# str2 = str1[-3:]
# print(str2)

# 3.	截取字符串的全部字符
# str1 = '1234567890'
# str2 = str1[:]
# print(str2)

# 4.	截取字符串的第七个字符到结尾
# str1 = '1234567890'
# str2 = str1[6:]
# print(str2)

# 5.	截取字符串的第一位到倒数第三位之间的字符
# str1 = '1234567890'
# str2 = str1[:-2]
# print(str2)

# 6.	截取字符串的第三个字符
# str1 = '1234567890'
# str2 = str1[2:3]
# print(str2)

# 7.	截取字符串的倒数第一个字符
# str1 = '1234567890'
# str2 = str1[-1:]
# print(str2)

# 8.	截取与原字符串顺序相反的字符串
# str1 = '1234567890'
# str2 = str1[::-1]
# print(str2)

# 9.	截取字符串倒数第三位与倒数第一位之间的字符
# str1 = '1234567890'
# str2 = str1[-2:-1]
# print(str2)

# 10.	截取字符串的第一位字符到最后一位字符之间的字符，每隔一个字符截取一次。
# str1 = '1234567890'
# str2 = str1[1:-1:2]
# print(str2)


# 字符串中，索引与内容一起显示
# str1 = '1234567890'
# a = 0
# for i in str1:
#     print('第 %d 位字符是： %s' % (a, i))
#     a += 1
