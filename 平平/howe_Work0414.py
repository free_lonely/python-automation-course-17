#
# 1、创建一个对象后默认调用(A)
# A、__init__    B、__str__    C、__add__    D、__and__
# 2、类是对象的定义、对象是类的实例.
# 3、对象是由属性、方法两部分构成.
# 4、创建学生类：
# 	类名：Student
# 	属性：name（姓名）、age（年龄）、sex（性别）
# 方法：
# 	def info(self) # 打印学生的姓名、年龄、性别
# 	def draw(self) #打印”XX会画画呢”
# 描述：创建学生类，通过学生类创建一个学生对象，分别调用学生的info方法.
class Student(object):

    def info(self):
        print('学生姓名:', self.name)
        print('年龄:', self.age)
        print('性别:', self.sex)

    def draw(self):
        print('%s会画画' %self.name)

xiaowang = Student()
xiaowang.name = '小王'
xiaowang.age =18
xiaowang.sex = '女'
xiaowang.info()
xiaowang.draw()

# 4、创建动物类：
# 	类名：animal
# 	属性(使用魔法方法实现)：name（姓名）、age（年龄）、color（颜色）
# 方法：
# 	def info(self) # 打印姓名、年龄、毛颜色
# 	def run（self）#打印“XX会跑呢”
# 描述：创建动物类，通过动物类创建一个动物对象，分别调用动物的info和run方法.
class Animal(object):
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def info(self):
        print('姓名:', self.name)
        print('年龄:', self.age)
        print('颜色:', self.color)

    def draw(self):
        print('%s会飞呢' %self.name)

xiaotianquan = Animal('哮天犬', 18, '黑不拉几')
xiaotianquan.info()
xiaotianquan.draw()

