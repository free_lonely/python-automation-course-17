import pymysql


def create_table():
    # 连接数据库
    db = pymysql.connect(host='49.233.39.160',
                         user='user',
                         password='leboAa!#$123',
                         database='lebo16',
                         port=3306,
                         charset='utf8')
    # 获取游标
    cursor = db.cursor()
    # 使用游标操作数据库
    cursor.execute('drop table if exists ceshi;')

    sql = """create table ceshi(
    id int unsigned primary key auto_increment,
    name varchar(20),
    age int unsigned);"""

    try:
        cursor.execute(sql)
        print('数据表创建成功')
    except Exception as e:
        print('数据库创建失败的原因:%s' % e)
    # 关闭游标,关闭数据库
    cursor.close()
    db.close()


create_table()