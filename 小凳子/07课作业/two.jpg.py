# 1 Python 基础
# 1.1 str = 'a,hello'在字符串str中查找hello

a = 'a,hello'
print(a[2:7:1])

# 1.2 str = 'a,b,c,d'用逗号分割str字符串，并保存到列表

a = 'a,b,c,d'
b = a.split(sep=',')
print(b)

# 1.3 将“笔试题 123A” 中的 123 替换为 “进行中”

a = '笔试题 123A'
b = a.replace('123', '进行中')
print(b)

