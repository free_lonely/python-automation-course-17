class Cat(object):  # 创造类Cat 继承基类object
    def __str__(self):  # 魔法方法str 当用print打印对象时，通常是打印出一个内存地址，而如果类中定义了str方法，则当打印对象时，调用该方法
        return '喵？喵？喵？'    # 魔法方法__str__通常返回字符串 描述对象的信息，通常用于调试 这里返回喵？喵？喵？


cat = Cat()  # 创建Cat类的一个对象cat

print(cat)  # 打印对象cat，如果没有str方法，则打印出内存地址， 这里调用str方法 打印 喵？喵？喵？

