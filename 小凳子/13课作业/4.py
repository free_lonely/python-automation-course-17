class Calculator(object):
    def __init__(self,number_1,number_2):
        self.number1 = number_1
        self.number2 = number_2

    def add(self):
        print(self.number1+self.number2)

    def sub(self):
        print(self.number1-self.number2)

    def div(self):
        print(self.number1/self.number2)

    def mul(self):
        print(self.number1*self.number2)


calculator = Calculator(5, 2)
calculator.add()
calculator.sub()
calculator.mul()
calculator.div()

