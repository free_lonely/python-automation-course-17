class Animal(object):
    def shut(self):
        print('动物的叫声')

    def eat(self):
        print('动物爱吃的食物')


class Cat(Animal):
    def shut(self):
        print('喵喵喵')

    def eat(self):
        print('鱼')


class Dog(Animal):
    def shut(self):
        print('汪汪汪')

    def eat(self):
        print('骨头')


cat = Cat()
cat.shut()
cat.eat()

dog = Dog()
dog.shut()
dog.eat()

