class Animal(object):  # 创造动物类 继承基类object
    def shut(self):    # 定义一个方法
        print('动物在叫')


class Dog(Animal):  # 创建狗类 继承父类Animal
    def shut(self):  # 重写父类的shut（）方法
        super().shut()  # super（）调用父类的方法 这里调用父类Animal中的shut方法
        print('汪汪汪')


dog = Dog()


dog.shut()

# 本题结果就是：
# 动物在叫
# 汪汪汪

