# 第4题

result = lambda a, b: a * b  # 匿名函数lambda
print(result(20, 30))  # 用变量result去调用匿名函数


# 第5题

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
b = [i for i in range(1, 11) if i % 2 != 0]
print(b)
