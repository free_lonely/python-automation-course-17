# try:
#     open('aaaaa')
#     print(abc)
# except Exception as e:  # 捕获异常 as e表示报错内容 Exception是所有错误类型的父类
#     print('haha', e)

# try:
#     # open('aaaaa')
#     a = 10
# except:
#     print('haha')
# else:
#     print('else')
# finally:
#     print('finally')


# 嵌套捕获异常：
# try:
#     try:
#         print(aaaa)
#     finally:  # 如果里面有捕获 就不需要外层的捕获了
#         print(1)
# except:
#     print(2)


# 自定义异常：
class AgeError(Exception):
    def __init__(self, age):
        self.age = age

    def __str__(self):
        return '您输入的年龄%d有误，请重新输入！' % self.age


class Person(object):
    def __init__(self, age):
        self.age = age
        if 0 < age < 100:
            self.age = age
        else:
            raise AgeError(age)


xm = Person(999)
