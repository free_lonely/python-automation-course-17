from decimal import Decimal
from decimal import getcontext

print(Decimal('4.20') + Decimal('2.10'))

getcontext().prec = 5  # 设置计算精度为5

a = 4.20

b = 2.10

c = Decimal(a) + Decimal(b)
print(c)

