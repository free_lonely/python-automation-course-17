def t1():
    l = []
    for i in range(1000):
        l.append(i)


def t2():
    l = [i for i in range(1000)]


def t3():
    l = []
    for i in range(1000):
        l = l + [i]


def t4():
    l = list(range(1000))


def t5():
    l = []
    for i in range(1000):
        l.insert(0, i)

# 测试上面5种方法的执行速度：


from timeit import Timer

timer1 = Timer('t1()', 'from __main__ import t1')
print('T1', timer1.timeit(number=1000), 'second')

timer1 = Timer('t2()', 'from __main__ import t2')
print('T2', timer1.timeit(number=1000), 'second')

timer1 = Timer('t3()', 'from __main__ import t3')
print('T3', timer1.timeit(number=1000), 'second')

timer1 = Timer('t4()', 'from __main__ import t4')
print('T4', timer1.timeit(number=1000), 'second')

timer1 = Timer('t5()', 'from __main__ import t5')
print('T5', timer1.timeit(number=1000), 'second')
