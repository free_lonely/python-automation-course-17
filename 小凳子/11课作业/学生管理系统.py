"""
__project__ = '17lebohomework'

__file_name__ = '学生管理系统'

__author__ = 'Administrator'

__time__ = '2021/4/17 20:43'

__product_name__ = PyCharm

"""

students_info_list = []


def display_menu():
    print('**********学生管理系统*************')
    print('1-添加学生')
    print('2-修改学生信息')
    print('3-查询学生信息')
    print('4-删除学生信息')
    print('5-查询所有学生信息')
    print('6-退出学生管理系统')
    print('********************************')


def add_student():
    student_name = input('请输入学生的姓名：')
    student_phone_number = input('请输入学生的手机号码：')
    student_qq_number = input('请输入学生的QQ号码：')

    students_info = {'studentName': student_name, 'studentPhoneNumber': student_phone_number,
                     'studentQQNumber': student_qq_number}

    students_info_list.append(students_info)

    print(students_info_list)


def modify_student_info():
    modify_num = int(input("请输入要修改的序号:"))
    if 0 <= modify_num < len(students_info_list):
        print("你要修改的信息是:")
        print("name:%s, tel:%s, QQ:%s" % (students_info_list[modify_num]['studentName'],
                                          students_info_list[modify_num]['studentPhoneNumber'],
                                          students_info_list[modify_num]['studentQQNumber']))
        students_info_list[modify_num]['studentName'] = input("请输入新的姓名:")
        students_info_list[modify_num]['studentPhoneNumber'] = input("请输入新的手机号:")
        students_info_list[modify_num]['studentQQNumber'] = input("请输入新QQ:")

        print(students_info_list)
    else:
        print("输入序号有误,请重新输入")


def delete_student_info():
    del_num = int(input("请输入要删除的序号:"))
    if 0 <= del_num < len(students_info_list):
        del students_info_list[del_num]
    else:
        print("输入序号有误,请重新输入")


def search_student_info():
    search_name = input("请输入要查询的学生姓名:")
    for temp_info in students_info_list:
        if temp_info['studentName'] == search_name:
            print("查询到的信息如下:")
            print("name:%s, tel:%s, QQ:%s" % (temp_info['studentName'],
                                              temp_info['studentPhoneNumber'],
                                              temp_info['studentQQNumber']))
            break
    else:
        print("没有您要找的信息....")


def search_all_students_info():
    print("序号\t\t姓名\t\t手机号\tQQ")
    i = 0
    for temp in students_info_list:
        print("%d\t\t%s\t\t%s\t\t%s" % (i, temp['studentName'], temp['studentPhoneNumber'], temp['studentQQNumber']))
        i += 1


def main():
    while True:
        display_menu()
        number = input('请输入您的选择（数字）：')

        if number == '1':
            add_student()
        elif number == '2':
            modify_student_info()
        elif number == '3':
            search_student_info()
        elif number == '4':
            delete_student_info()
        elif number == '5':
            search_all_students_info()
        elif number == '6':
            print('您已退出，再见！')
            break
        else:
            print('输入错误，请重新开始！')


main()
