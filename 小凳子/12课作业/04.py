class Student(object):  # object是基类，所有的类都继承它 旧式类：只有类名 或者是类名（） 新式类：类名（object）
    def __init__(self, name, age, sex):
        self.student_name = name
        self.student_age = age
        self.student_sex =sex

    def info(self):
        print('该学生的姓名是：', self.student_name)
        print('该学生的年龄是：', self.student_age)
        print('该学生的性别是：', self.student_sex)

    def draw(self):
        print('%s 会画画呢' % self.student_name)


student1 = Student('小凳子', 29, '男')
student1.info()
student1.draw()

print('-'*100)

student2 = Student('火花', 16, '女')
student2.info()
student2.draw()
