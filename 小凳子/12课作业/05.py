class Animal(object):  # object是基类，所有的类都继承它 旧式类：只有类名 或者是类名（） 新式类：类名（object）
    def __init__(self, name, age, color):  # 魔法方法 对象创建时调用
        self.animal_name = name
        self.animal_age = age
        self.animal_color = color

    def info(self):
        print('这个动物是：', self.animal_name)
        print('这个动物的年龄是：', self.animal_age)
        print('这个动物的毛的颜色是：', self.animal_color)

    def run(self):
        print('%s 会跑呢' % self.animal_name)


animal1 = Animal('猫', 6, '橘黄色')
animal1.info()
animal1.run()

print('-'*100)

animal1 = Animal('狗', 10, '白色')
animal1.info()
animal1.run()
