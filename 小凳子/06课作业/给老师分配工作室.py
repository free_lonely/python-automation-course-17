import random

school_list = [[], [], []]  # 初始给老师3个办公室
teacher_list = list('ABCDEFGH')  # 8个老师 列表形式

for name in teacher_list:  # 先把每个老师从列表中遍历出来赋给name
    index = random.randint(0, 2)  # 房间的标号 就是下标索引值
    school_list[index].append(name)  # 把老师分配给三个办公室中的一个

print(school_list)
